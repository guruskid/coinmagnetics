<?php
session_start();

include('connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['id'])){
	
	header("location:index");
}else{

$get_user = mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$_SESSION['id']."' ");
$rows = mysqli_fetch_assoc($get_user);
    if(isset($_SESSION['2fa'])){

        if( ($_SESSION['2fa'] =="no" or $_SESSION['2fa'] =="pending") and $rows['2fa']==1){
            header("location:index");
        }


    }


}

if($rows['cycle']==1){ 

  header("location:dashboard");

}


?>
<!DOCTYPE html>
<html lang="en">
  

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    <meta name="author" content="treasure">
    <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
    <title>Withdrawal - Coin Magnetics</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="assets/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="assets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/feather-icon.css">
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.css">
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link id="color" rel="stylesheet" href="assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5dde42de43be710e1d1f5485/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
  </head>
  <body class="dark-sidebar dark-only">
    <!-- tap on top starts-->
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>
    <!-- tap on tap ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
      <!-- Page Header Start-->
     <?php include('header.php'); ?>
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper sidebar-icon">
        <!-- Page Sidebar Start-->
        <?php include('sidebar.php'); ?>
        <!-- Page Sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col-6">
                  <h3>Withdrawal</h3>
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="dashboard"><i data-feather="home"></i></a></li>
                    <li class="breadcrumb-item">Withdrawal   </li>
                  </ol>
                </div>
                <div class="col-6">
                  <!-- Bookmark Start-->
                
                  <!-- Bookmark Ends-->
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            <div class="row">
           
           
              <div class="col-sm-12">
                <div class="card">
                  <div class="card-header"> 
                    <h5>Available balance $<?php echo $rows['wallet'];  ?></h5>
                  </div>
                  <div class="card-body pricing-card-design-3">
                    <div class="row pricing-content-ribbons">

                    <?php  
                      $get = mysqli_query($mysqli,"SELECT * FROM withdrawal_method");

                      while($row=mysqli_fetch_assoc($get)){
                          $show = true;
                        if($row['id']==3 and $rows['country']=="NG"){
                          $show = true;
                        }elseif($rows['country'] !="NG" and $row['id']!=3 ){
                          $show = true;
                        }else{
                          $show = false;
                        }

                        if($show){
                    ?>
                    
                      <div class="col-md-4">
                      <?php if($row['id']==1){  ?> <div class="ribbon ribbon-bookmark ribbon-danger">Popular</div> <?php } ?>
                        <div class="pricing-block card text-center ">
                          <svg x="0" y="0" viewBox="0 0 360 220">
                            <g>
                              <path fill="#7366ff" d="M0.732,193.75c0,0,29.706,28.572,43.736-4.512c12.976-30.599,37.005-27.589,44.983-7.061                                          c8.09,20.815,22.83,41.034,48.324,27.781c21.875-11.372,46.499,4.066,49.155,5.591c6.242,3.586,28.729,7.626,38.246-14.243                                          s27.202-37.185,46.917-8.488c19.715,28.693,38.687,13.116,46.502,4.832c7.817-8.282,27.386-15.906,41.405,6.294V0H0.48                                          L0.732,193.75z"></path>
                            </g>
                            <text transform="matrix(1 0 0 1 69.7256 116.2686)" fill="#fff" font-size="50">$<?php if($row['id'] =="1"){ echo "50"; }elseif($row['id'] =="3"){ echo "10"; }else{ echo '10';}  ?></text>
                            <text transform="matrix(0.9566 0 0 1 197.3096 83.9121)" fill="#fff" font-size="29.0829"></text>
                            <text transform="matrix(1 0 0 1 233.9629 115.5303)" fill="#fff" font-size="15.4128">/minimum</text>
                          </svg>
                          <div class="pricing-inner">
                            <h3 class="font-primary"><?php echo $row['name']; ?></h3>
                            <?php if($row['id']==2){  ?>
                            <h3>Coming Soon</h3>
                            <?php } ?>
                            <ul class="pricing-inner">
                              <li>
                                <h6><b>$<?php echo $row['charge']; ?> </b> Charges </h6>
                              </li>
                              <?php if($row['id']==1){  ?>
                              <li>
                                <h6><b><?php if($rows['bitcoin'] ==""){ echo "############";}else{ echo $rows['bitcoin']; } ?></b> <br/> <i class="fa fa-arrow-up"></i> <br/> Your Bitcoin wallet</h6>
                              </li>
                              <?php  }  ?>

                              <?php if($row['id']==2){  ?>
                              <li>
                                <h6><b><?php if($rows['tron'] ==""){ echo "############";}else{ echo $rows['tron']; } ?></b> <br/> <i class="fa fa-arrow-up"></i> <br/> Your Tron wallet</h6>
                              </li>
                             
                              
                             
                              <?php  }  ?>

                              <?php if($row['id']==3){  ?>
                              <li>
                                <h6><b><?php if($rows['account_num'] ==""){ echo "############";}else{ echo $rows['account_num']; } ?></b> <br/> <i class="fa fa-arrow-up"></i> <br/> Your Account number</h6><br/> FX Users Only</h6>
                              </li>
                              <?php 
                             }  ?>
                             
                            </ul>
                            

                            <br/><br/>
                            <?php if($row['id']!=2){  ?>
                              <form method="POST" >
                                <input type="hidden" name="withdrawid" value="<?php echo $row['id']; ?>" />
                                <input type="hidden" name="name" value="<?php echo $row['name']; ?>" />
                                
                                <div class="form-group">
                                  <input type="number" min="<?php if($row['id'] =="1"){ echo "50"; }elseif($row['id'] =="3"){ echo "10"; }  ?>" max="<?php echo $rows['possible_withdrawal_amount']; ?>" required name="amount" class="form-control" Placeholder="Enter Amount"  />
                                </div>

                                <button class="btn btn-primary btn-lg" name="withdraw" type="submit" data-original-title="Make Withdrawal" title="">Proceed</button>

                            </form>
                            <?php } ?>
                         


                          </div>
                        </div>
                      </div>

                     
<?php  }  
      } ?>



                   
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid Ends-->
        </div>
        <!-- footer start-->
        <footer class="footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6 footer-copyright">
              <p class="mb-0">Copyright <?php echo date('Y'); ?> © Coin Magnetics All rights reserved.</p>
            </div>
            <div class="col-md-6">
              <p class="pull-right mb-0"> </p>
            </div>
          </div>
        </div>
      </footer>
      </div>
    </div>
    <!-- latest jquery-->
    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap js-->
    <script src="assets/js/bootstrap/popper.min.js"></script>
    <script src="assets/js/bootstrap/bootstrap.js"></script>
    <!-- feather icon js-->
    <script src="assets/js/icons/feather-icon/feather.min.js"></script>
    <script src="assets/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
  
    <script src="assets/js/config.js"></script>
    <!-- Plugins JS start-->
    <script src="assets/js/sidebar-menu.js"></script>
    <script src="assets/js/counter/jquery.waypoints.min.js"></script>
    <script src="assets/js/counter/jquery.counterup.min.js"></script>
    <script src="assets/js/counter/counter-custom.js"></script>
    <script src="assets/js/sweet-alert/sweetalert.min.js"></script>
    <script src="assets/js/tooltip-init.js"></script>
    <!-- Plugins JS Ends-->
    
    <!-- Theme js-->
    <script src="assets/js/script.js"></script>

    <!-- login js-->
    <!-- Plugin used-->
  </body>

  <?php

if(isset($_POST['withdraw'])){

  $userid =  $rows['id'];
	$name =  mysqli_real_escape_string($mysqli,$_POST['name']);

  $withdrawid =  mysqli_real_escape_string($mysqli,$_POST['withdrawid']);
  $amount =  mysqli_real_escape_string($mysqli,$_POST['amount']);


  if( $amount <=  $rows['wallet']){


    if($rows['can_withdraw'] == 0){


    if($withdrawid == 3){
      //bank transfer

      //ensure the user has enter bank account b4 withdrawing with bank
      if($rows['account_num']!="" and $rows['bankname']!="" ){

      $recipient_code ="";


      if($rows['recipient_code'] ==""){

        $curl = curl_init();

curl_setopt_array($curl, array(
CURLOPT_URL => 'https://api.paystack.co/transferrecipient',
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => '',
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 0,
CURLOPT_FOLLOWLOCATION => true,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => 'POST',
CURLOPT_POSTFIELDS =>'{ "type": "nuban", "name": "'.$rows['account_name'].'", "account_number": "'.$rows['account_num'].'", 
	 "bank_code": "'.$rows['bankname'].'", "currency": "NGN"}',
CURLOPT_HTTPHEADER => array(
'Authorization: Bearer sk_live_3331a17e91a746f62a03f7362ac0a43ab1a25ead',
'Content-Type: application/json',
'Cookie: __cfduid=d6c8e6ca1b24a7fa1f2ab22bd0d2eaf1b1614648991; sails.sid=s%3AP1AEhHw70zo99ZxtVVSMd3B3aIto4yT2.o%2BPf7n0dAeCaRPxcT%2BIJ55SiR0AzFnXx3MNZc%2Fezahs'
),
));

$response = curl_exec($curl);

curl_close($curl);

$response = json_decode($response, true);

if($response['status'] =="true"){


 $recipient_code = $response['data']['recipient_code'];



        $update = mysqli_query($mysqli,"UPDATE users SET recipient_code ='$recipient_code' WHERE id='$userid'");
        
      }

      }else{

          $recipient_code =$rows['recipient_code'];
        }





    
    
    //depending on the withdrawal Id, to decide if it bank transfer or btc
  

  





//uncomment to test
//$nariaamount = convertCurrency($amount, 'USD', 'NGN');
$nariaamount = 480*$amount;

$realamount = $nariaamount*100 ;




$curl = curl_init();

curl_setopt_array($curl, array(
CURLOPT_URL => 'https://api.paystack.co/transfer',
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => '',
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 0,
CURLOPT_FOLLOWLOCATION => true,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => 'POST',
CURLOPT_POSTFIELDS =>'{ "source": "balance", "amount": "'.$realamount.'", "recipient": "'.$recipient_code.'", 
	 "reason": "'.$desc.'" }',
CURLOPT_HTTPHEADER => array(
'Authorization: Bearer sk_live_3331a17e91a746f62a03f7362ac0a43ab1a25ead',
'Content-Type: application/json',
'Cookie: __cfduid=d6c8e6ca1b24a7fa1f2ab22bd0d2eaf1b1614648991; sails.sid=s%3A5nSY6lP2E9eYFYufzKjcNnF3sMfSj1Ou.TWyqyHMazWeWRbAb0MK1n6Zna8cpddAcI0Y2GiEH%2B60'
),
));

$res = curl_exec($curl);

curl_close($curl);





//echo $response;
$res = json_decode($res,true);

if($res['status']==true){


  $date= date("d")." ".date("F")." ".date("Y")." , ".date("h")." : ".date("i").date("a");

/////////////////////////////////

$action = "Withdrawal Initated";
$describe ="Withdrawal of  $".$amount." was intitated.";


$add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Requested') ");



$insert = mysqli_query($mysqli,"INSERT INTO withdrawal(`userid`, `type`, `name`, `amount`, `date`) VALUES('$userid', '$withdrawid',  '$name', '$amount',  '$date')");




$desc = "Withdrawal From ".$rows['firstname']." ".$rows['lastname'];
$order= "tc-".uniqid();




  if($insert){

    //debit the user
    $newamount =  $rows['wallet']-$amount;

    $updateuser = mysqli_query($mysqli,"UPDATE users SET wallet='$newamount' , cycle='1',  can_withdraw='288'   WHERE id='$userid'");


    $action = "Withdrawal Processed";
$describe ="Withdrawal of  $".$amount." was Processed.";


$add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Completed') ");




    ?>
    <script>
    
    
    swal(
         'Withdrawal Processed',
        "Your withdrawal is undergoing processing",
        'success'
    )
    
    
    </script>
    
    <?php
   
   
   
   }else{
  
    ?>
    <script>
    
    
    swal(
       'Error Processing Withdrawal',
      "Please try again",
      'warning'
  )
    
    
    </script>
    
    <?php
   }
  




}else{


  ?>
  <script>
  
  
  swal(
       'Error Processing Withdrawal',
      "Please try again",
      'warning'
  )
  
  
  </script>
  
  <?php


}


}else{


  ?>
  <script>
  
  
  swal(
       'Error no active bank account',
      "Please update your profile with bank account details ",
      'warning'
  )
  
  
  </script>
  
  <?php


}








  }elseif($withdrawid ==1){
//btc transfer


     //ensure the user has enter bank account b4 withdrawing with bank
     if($rows['bitcoin']!=""  ){


$date= date("d")." ".date("F")." ".date("Y")." , ".date("h")." : ".date("i").date("a");

/////////////////////////////////

$action = "Withdrawal Initated";
$describe ="Withdrawal of  $".$amount." was intitated.";


$add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Requested') ");



$insert = mysqli_query($mysqli,"INSERT INTO withdrawal(`userid`, `type`, `name`, `amount`, `date`) VALUES('$userid', '$withdrawid',  '$name', '$amount',  '$date')");




$desc = "Withdrawal From ".$rows['firstname']." ".$rows['lastname'];
$order= "tc-".uniqid();


$btc = file_get_contents('https://blockchain.info/tobtc?currency=USD&value='.$amount);



$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://api.luno.com/api/1/send',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => 'amount='.$btc.'&currency=XBT&address='.$rows['bitcoin'].'&description='.urlencode($desc),
  CURLOPT_HTTPHEADER => array(
    'Authorization: Basic YzVrdnhmYmZ3OWhlNDp0YWp1VTVzYllQOVIzTnk3QzVQdUt4dEc4T2F5UWI4S1hLMTlPREttc2t3',
    'Content-Type: application/x-www-form-urlencoded',
    'Cookie: __cfduid=dec53d1c1c5562d735ecfd3bea4356e8e1610463708; device=ZHQxMwRI0CgGTv1kyQjgr8srFg==:RFx+w8UjVjPDxiHoEUaFbyrhWdE='
  ),
));

$response = curl_exec($curl);

$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

curl_close($curl);

//echo $http_status;

$response = json_decode($response,true);

if($http_status == 200){


  //debit the user
  $newamount =  $rows['wallet']-$amount;

  $updateuser = mysqli_query($mysqli,"UPDATE users SET wallet='$newamount', cycle='1', can_withdraw='288'  WHERE id='$userid'");


  $action = "Withdrawal Processed";
$describe ="Withdrawal of  $".$amount." was Processed.";


$add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Completed') ");




  ?>
  <script>
  
  
  swal(
       'Withdrawal Processed',
      "Your withdrawal is undergoing processing",
      'success'
  )
  
  
  </script>
  
  <?php
 
 


  
}else{


  ?>
  <script>
  
  
  swal(
       'Error Processing Withdrawal',
      "Please try again",
      'warning'
  )
  
  
  </script>
  
  <?php

}



}else{


  ?>
  <script>
  
  
  swal(
       'Error no bitcoin wallet address',
      "Please update your profile with bitcoin wallet account address ",
      'warning'
  )
  
  
  </script>
  
  <?php


}





//end of bitcoin sending
  }



}else{

  $with = $rows['can_withdraw']/60;
  ?>
  <script>
  
  
  swal(
       'Withdrawal inaccessible for next 24hrs',
      "You have <?php echo $with ?>hrs till your next withdrawal",
      'warning'
  )
  
  
  </script>
  
  <?php


}


}else{

  ?>
  <script>
  
  
  swal(
       'Insufficient Funds',
      "Please try again",
      'warning'
  )
  
  
  </script>
  
  <?php


}



}



?>


</html>