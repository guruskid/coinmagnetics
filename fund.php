<?php
session_start();

include('connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['id'])){
	
	header("location:login");
}else{

$get_user = mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$_SESSION['id']."' ");
$rows = mysqli_fetch_assoc($get_user);
    if(isset($_SESSION['2fa'])){

        if( ($_SESSION['2fa'] =="no" or $_SESSION['2fa'] =="pending") and $rows['2fa']==1){
            header("location:login");
        }


    }


}



$orderid = $_GET['orderid'];

$getorder = mysqli_query($mysqli,"SELECT * FROM pending WHERE chargeid='$orderid'");
$row = mysqli_fetch_assoc($getorder);


?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title> Treasure Capital - Funding information </title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="copyright" content="PineCrest">
    <meta name="description" content="Fund Information">
    <link rel="stylesheet" href="./SpectroCoin - Order information_files/68312e1d7e62f9fe14bc249e1cc22582-payment.css">
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.css">
    <script type="text/javascript" async="" src="./SpectroCoin - Order information_files/tracking.js.download"></script>
    <script src="./SpectroCoin - Order information_files/4550dcf380d40a065032866da649c356-payment.js.download"
        type="text/javascript"></script>
        <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">


    <script>
    var lenght = 0.0;



    var timer = 900,
        minutes, seconds;
    setInterval(() => {
        lenght += 0.111;

        document.getElementById('time-bar').style = "width:" + lenght + "%";
        //////////////////////////////
        /////////////////////////////
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        document.getElementById("time-label").innerHTML = minutes + ":" + seconds;


        if (--timer < 0) {
            //time is over
            timer = 0;
            document.getElementById('orderinvalid').style = "display:block";
            document.getElementById('pay1').style = "display:none";
            document.getElementById('pay2').style = "display:none";
        }






    }, 1000);
    </script>







    <script charset="utf-8" src="./SpectroCoin - Order information_files/19.c46ee032.js.download"></script>
    <script charset="utf-8" src="./SpectroCoin - Order information_files/23.785cd86f.js.download"></script>
    <script charset="utf-8" src="./SpectroCoin - Order information_files/127.b8a2fb9a.js.download"></script>
    <script charset="utf-8" src="./SpectroCoin - Order information_files/163.5f2da8ff.js.download"></script>

    <style>
        .payAmount {
    color: #7366ff;
    font-weight: 600;
    font-size: 24px;
}

.btn {
    background-color: #7366ff;
    color: #fff;
    font-size: 14px;
    font-weight: 700;
    border-radius: 0;
    text-transform: uppercase;
    border: 0;
    width: 60%;
    padding: 15px;
    margin: 18px auto 0;
    -webkit-transition: all .2s ease;
    transition: all .2s ease;
}

.progress{
    background-color: #dcc520
}
    </style>
</head>

<body class="payment-page-body">
    <div class="vtable">
        <div class="vcell">
            <div class="container">
                <div id="payment-page-area">

                    <div class="loaderUI" style="display: none;"></div>
                    <div class="order-box" style="">

                        <div class="header">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="fl-l">
                                        <h3>
                                        Treasure capital
                                        </h3>
                                    </div>
                                    <div class="fl-r">
                                        <h4>

                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="content">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="fl-l">
                                        Order ID: <span class="orderId"><?php echo $orderid; ?> </span>
                                    </div>
                                    <div class="fl-r">
                                        <span id="validForLabel">Valid for</span>
                                        <span id="time-label" class="time orderId">15:00</span>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="progress small blue">
                                        <div id="time-bar" class="progress-bar progress-bar-success"
                                            style="width: 0.0%;"></div>
                                        <div class="received">
                                            <span id="receivedField"></span>
                                            <i class="fa fa-btc"></i>
                                        </div>
                                    </div>
                                </div>
                                <div  class="col-xs-12">
                                    <div id="pay1" style="<?php if(isset($_GET['paid'])){ echo 'display:none'; } ?>" class="paymentInfoBlock">
                                        <div>
                                            Payment using Bitcoin
                                        </div>
                                        <div id="converted" class="payAmount">

                                        </div>
                                        <div class="payAmountFiat">
                                            <ul class="currencyExchangeList">

                                                <li class="">≈ <?php echo $row['amount']; ?> USD</li>

                                            </ul>
                                        </div>
                                    </div>
                                    <div id="pay2" style="<?php if(isset($_GET['paid'])){ echo 'display:none'; } ?>" class="paymentBlock">
                                        <center>
                                        <a id="qrcode" href="javascript:;">
                                           
                                        </a>
                                        </center>

                                        <div class="barcode">
                                            <span>Address:</span>
                                            <div id="address" class="address">
                                            <?php echo $row['wallet']; ?>
                                            </div>
                                            <div onclick="mylink()" id="copyAddress" class="copyButton">
                                                <img class="icon"
                                                    src="./SpectroCoin - Order information_files/831eb17fd0e9c5e5cdf6931e2b3c7db3-copy.svg"
                                                    alt="Copy">
                                                Copy address
                                            </div>
                                        </div>

                                        <form method="POST">
                                            <input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
                                            <input type="hidden" name="userid" value="<?php echo $row['userid']; ?>" />
                                             <input type="hidden" name="amount" value="<?php echo $row['amount']; ?>" />
                                            <button class="btn" name="paid" type="submit"><strong>Mark as Paid</strong></button>

                                        </form>
                                        <p>
                                            <strong>Please send only token you have chosen (bitcoin) to the appropriate address generated, if otherwise is done, it will be lost in the system</strong> 
                                        </p>
                                    </div>
                                    <div id="orderinvalid" style="display: none;">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="title">This order has expired.</div>
                                                <a href="dashboard">Back
                                                    to merchant</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="orderpending"  style="<?php if(isset($_GET['paid'])){ echo 'display:block'; }else{echo 'display:none'; } ?>" >
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="title success"> Waiting for
                                                    confirmation.</div>
                                                <div class="description"><a
                                                        href="dashboard">Back
                                                        to Dashboard</a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="orderpaid" style="display: none;">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="title success">Order has been paid!</div>
                                                <div class="description"><a
                                                        href="dashboard">Back
                                                        to Dashboard</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="footer">
                            <div class="row">
                                <div class="col-xs-3">
                                    <span class="currentLang title-tooltip" title-tooltip="" title="
English (EN)
">
                                        English (EN)
                                    </span>

                                    <ul class="lang-switcher">
                                        <li>
                                            <a href="javascript:;">
                                                English ( EN )
                                            </a>
                                        </li>


                                    </ul>

                                </div>
                                <div class="col-xs-6">
                                    Powered by <a href="https://treasurecapital.net" class="color-gray"
                                        target="_blank">Treasure Capital</a>
                                </div>
                                <div class="col-xs-3">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script src="./SpectroCoin - Order information_files/get_dynamic_config.js.download"></script>
    <script src="qrcode/qrcode.min.js"></script>
    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <script src="assets/js/sweet-alert/sweetalert.min.js"></script>

    <input type="text" style="display:none" value="<?php echo $row['wallet']; ?>" id="wallet-address" />
    <script>
   
$.ajax({
        url: 'https://blockchain.info/tobtc?currency=USD&value=<?php echo $row['amount']; ?>',
        type: 'GET',
        success: function(data) {
            //***********************************


            console.log(data);


            //data = parseInt(amount)/parseInt(sell);

            $('#converted').text(data + " BTC ");

            var qrcode = new QRCode("qrcode", {
                text: 'bitcoin:<?php echo $row['wallet']; ?>?amount='+data+'&label=Treasure',
                width: 128,
                height: 128,
                colorDark: "#0a2450",
                colorLight: "#ffffff",
                correctLevel: QRCode.CorrectLevel.H
            });

            var link = 'bitcoin:<?php echo $row['wallet']; ?>?amount='+data+'&label=Treasure';
            $('#qrcode').attr('href',link);

            var settings = {
                    "url":"upbtc?orderid="+orderid+"&btc="+data,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                };

                $.ajax(settings).done(function (response) {
                    console.log(response);

                   


                });






        },
        error: function(data) {
            //hide preloader



            //also show the yeye error in console
            console.log(data);

        }
    });


   

    //End -- of  Ajax call ************








    function mylink() {
        /* Get the text field */
        var copyText = document.getElementById("wallet-address");

        copyText.style = "display:block";
        /* Select the text field */
        copyText.select();

        /* Copy the text inside the text field */
        document.execCommand("copy");

        copyText.style = "display:none";

        alert('Wallet Address Copied!');

    }
    </script>


    <?php

    if(isset($_POST['paid'])){

        $id =  mysqli_real_escape_string($mysqli,$_POST['id']);
        $userid =  mysqli_real_escape_string($mysqli,$_POST['userid']);
        $amount =  mysqli_real_escape_string($mysqli,$_POST['amount']);
                
        $date= date("d")." ".date("F")." ".date("Y")." , ".date("h")." : ".date("i").date("a");

          $pick2 = mysqli_query($mysqli,"SELECT * FROM users WHERE id='$userid' ");
        $user = mysqli_fetch_assoc($pick2);

           
        
        

?>
<script>

location ='fund?orderid=<?php $_GET['orderid']; ?>&paid=yes';
</script>
<?php


    }

    ?>

<script>

setInterval(() => {

var orderid ='<?php echo $orderid; ?>';

var settings = {
                    "url":"checkers?orderid="+orderid,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                };

                $.ajax(settings).done(function (response) {
                    console.log(response);

                    if(response.treasure.status==1){

                                        swal(
                      'Payment Successful',
                      "You have successfully made your purchase",
                      'success'
                  );

                  setTimeout(() => {
                    location ='mining';
                  }, 3000);

                    }


                });



}, 5000);


        </script>

</body>

</html>