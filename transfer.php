<?php
session_start();

include('connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['id'])){
	
	header("location:login");
}else{

$get_user = mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$_SESSION['id']."' ");
$rows = mysqli_fetch_assoc($get_user);
    if(isset($_SESSION['2fa'])){

        if( ($_SESSION['2fa'] =="no" or $_SESSION['2fa'] =="pending") and $rows['2fa']==1){
            header("location:login");
        }


    }


}

if($rows['cycle']==1){ 

  header("location:dashboard");

}

?>
<!DOCTYPE html>
<html lang="en">


<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="author" content="treasure">
    <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
    <title>Transfer Funds - Coin Magnetics</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap"
        rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="assets/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="assets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/feather-icon.css">
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.css">
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link id="color" rel="stylesheet" href="assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
</head>

<body class="dark-sidebar dark-only">
    <!-- tap on top starts-->
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>
    <!-- tap on tap ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
        <!-- Page Header Start-->
        <?php include('header.php'); ?>
        <!-- Page Header Ends                              -->
        <!-- Page Body Start-->
        <div class="page-body-wrapper sidebar-icon">
            <!-- Page Sidebar Start-->
            <?php include('sidebar.php'); ?>
            <!-- Page Sidebar Ends-->
            <div class="page-body">
                <div class="container-fluid">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-6">
                                <h3>Transfer Funds </h3>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="dashboard"><i data-feather="home"></i></a></li>
                                    <li class="breadcrumb-item">Transfer </li>
                                </ol>
                            </div>
                            <div class="col-6">
                                <!-- Bookmark Start-->

                                <!-- Bookmark Ends-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Container-fluid starts-->
                <div class="container-fluid">
                    <div class="row">


                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Transfer </h5>
                                </div>
                                <div class="card-body pricing-card-design-3">
                                    <div class="row pricing-content-ribbons">

                                        <div class="col-md-3"></div>

                                        <div class="col-md-6">

                                            <div class="pricing-block card text-center ">
                                                <svg x="0" y="0" viewBox="0 0 360 220">
                                                    <g>
                                                        <path fill="#7366ff"
                                                            d="M0.732,193.75c0,0,29.706,28.572,43.736-4.512c12.976-30.599,37.005-27.589,44.983-7.061                                          c8.09,20.815,22.83,41.034,48.324,27.781c21.875-11.372,46.499,4.066,49.155,5.591c6.242,3.586,28.729,7.626,38.246-14.243                                          s27.202-37.185,46.917-8.488c19.715,28.693,38.687,13.116,46.502,4.832c7.817-8.282,27.386-15.906,41.405,6.294V0H0.48                                          L0.732,193.75z">
                                                        </path>
                                                    </g>
                                                    <text transform="matrix(1 0 0 1 69.7256 116.2686)" fill="#fff"
                                                        font-size="30"
                                                        id="figure">$<?php echo $rows['wallet']; ?></text>
                                                    <text transform="matrix(0.9566 0 0 1 197.3096 83.9121)" fill="#fff"
                                                        font-size="29.0829"></text>

                                                    <text transform="matrix(1 0 0 1 233.9629 115.5303)" fill="#fff"
                                                        font-size="15.4128">Wallet Balance</text>
                                                </svg>
                                                <div class="pricing-inner">
                                                    
                                                    <ul class="pricing-inner">
                                                       



                                                    </ul>


                                                    <br /><br />

                                                    <form method="POST" style="padding:10px">


                                                        <div class="form-group">

                                                            <input type="email" id="email" name="email" class="form-control"
                                                                placeholder="Enter email of recieving account" />
                                                        </div>

                                                        <center>
                                                        <div id="load" style="display:none"
                                                            class="spinner-border text-primary m-1" role="status">
                                                            <span class="sr-only">Loading...</span>
                                                        </div>
                                                        <h4 class="text-success" id="accountname"
                                                            style="display:none"
                                                            ></h4>
                                                        <h4 class=" text-danger" id="error" style="display:none">Error
                                                        </h4>
                                                    </center>

                                                        <div class="form-group">

                                                            <input type="number" id="amount" required name="amount"
                                                                class="form-control"
                                                                Placeholder="Enter Amount. E.G $10" />
                                                        </div>



                                                    


                                                        <button class="btn btn-primary btn-lg" id="transfer" name="transfer" type="submit"
                                                            data-original-title="Make Transfer"
                                                            title="">Transfer</button>

                                                    </form>



                                                </div>
                                            </div>
                                        </div>








                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Container-fluid Ends-->
            </div>
            <!-- footer start-->
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 footer-copyright">
                            <p class="mb-0">Copyright <?php echo date('Y'); ?> © Coin Magnetics All rights reserved.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p class="pull-right mb-0"> </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!-- latest jquery-->
    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap js-->
    <script src="assets/js/bootstrap/popper.min.js"></script>
    <script src="assets/js/bootstrap/bootstrap.js"></script>
    <!-- feather icon js-->
    <script src="assets/js/icons/feather-icon/feather.min.js"></script>
    <script src="assets/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->

    <script src="assets/js/config.js"></script>
    <!-- Plugins JS start-->
    <script src="assets/js/sidebar-menu.js"></script>
    <script src="assets/js/counter/jquery.waypoints.min.js"></script>
    <script src="assets/js/counter/jquery.counterup.min.js"></script>
    <script src="assets/js/counter/counter-custom.js"></script>
    <script src="assets/js/sweet-alert/sweetalert.min.js"></script>
    <script src="assets/js/tooltip-init.js"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="assets/js/script.js"></script>

    <!-- login js-->

    <script>
function getprofile() {

    var email = $('#email').val();


    var settings = {
        "url": "checkprofile?email=" + email ,
        "method": "GET",
        "timeout": 0
    };

    $.ajax(settings).done(function(response) {
        console.log(response);

        $('#load').hide();

        if(response.status == true) {
            $('#accountname').show();
            $('#accountname').text(response.account_name);
            $('#transfer').removeAttr("disabled");
        }else{
            $('#error').show();
            $('#transfer').attr("disabled", "disabled");
            $('#error').text(response.message);
        }




    });

}

$('#email').change(function() {

if ($('#email').val() != '') {
    $('#load').show();
    $('#error').hide();
    $('#accountname').hide();
    getprofile();
}


});
    </script>
    <!-- Plugin used-->
</body>

<?php

if(isset($_POST['transfer'])){


	
	$userid = $rows['id'];
	$email = mysqli_real_escape_string($mysqli,$_POST['email']);
	$amount = mysqli_real_escape_string($mysqli,$_POST['amount']);

    if($rows['transferstatus']==1 and $rows['can_withdraw']==0){

	
	
	//since password is encryted want to check if user exist first // dat is receiver/crediter
		$qryx=mysqli_query($mysqli,"SELECT * FROM `users` WHERE email='$email'");
    $qry=mysqli_query($mysqli,"SELECT * FROM `users` WHERE id='$userid'");
		
		  //if num of rows is 0 user not found
		if (mysqli_num_rows($qryx) > 0 and $email != $rows['email']){
			
			$row=mysqli_fetch_array($qry);
			$name = $row['firstname']." ".$row['lastname'];

			if($amount <= $row['wallet'] and $amount >0 ){
		
				//debit me
		
  			$newamount = $row['wallet']-$amount;
			
				
				$update = mysqli_query($mysqli,"UPDATE users SET wallet='$newamount', can_withdraw='288' WHERE id='$userid'  ");

				$qry2=mysqli_query($mysqli,"SELECT * FROM `users` WHERE email='$email'");
				$row2=mysqli_fetch_assoc($qry2);
				$name2= $row2['firstname']." ".$row2['lastname'];



				$newamount2 = $row2['wallet']+$amount;
			
				

				$update = mysqli_query($mysqli,"UPDATE users SET wallet='$newamount2' WHERE id='".$row2['id']."'  ");


                $action = "Transfer Successful";
				$describe = "You transferred NGN".$amount." to ".$name2." and have been debited.";
				$date= date("d")." ".date("F")." ".date("Y")." , ".date("h")." : ".date("i").date("a");

				//insert into activity
				$insert = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`, `status`) VALUES('$userid', '$action', '$describe', '$date',  '$amount',  'Successful')");

				


				$action2 = "Account Credited";
				$describe2 = "You received  NGN".$amount." from ".$name." and have been credited.";
				$date= date("d")." ".date("F")." ".date("Y")." , ".date("h")." : ".date("i").date("a");

				//insert into activity
				$insert = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`, `status`) VALUES( '".$row2['id']."', '$action2', '$describe2', '$date',  '$amount',  'Credited')");




				$amount = number_format($amount);
			  
			 









        ?>
        <script>
        swal(
            'Transfer Successful',
            "You have successfully transfer <?php echo $amount; ?> to <?php echo $name2; ?>",
            'warning'
        );
        
        setTimeout(() => {
            location = location;
        }, 3000);
        </script>
        
        <?php

		
				
		
			}else{


			
        ?>
        <script>
        swal(
            'Insufficient funds',
            "You do not have enough fund to transfer",
            'warning'
        );
        
        setTimeout(() => {
            location = location;
        }, 3000);
        </script>
        
        <?php
	
	
			}
		
			
				

			
			
		}else{
	

      ?>
<script>
swal(
    'Account not found',
    "No account with this email ",
    'warning'
);

setTimeout(() => {
    location = location;
}, 3000);
</script>

<?php

			
		}
		  




    }else{


        ?>
<script>
swal(
    'You are Ineligible for transfer',
    "Ensure you can transfer   ",
    'warning'
);

setTimeout(() => {
    location = location;
}, 3000);
</script>

<?php






    }







}



?>


</html>