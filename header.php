<?php
 //Get file name
 //so as to know which file is active
      $currentFile = $_SERVER["SCRIPT_NAME"];
	  //extract it from forward /
      $parts = Explode('/', $currentFile);
      $currentFile = $parts[count($parts) - 1]; 
	  
	  

 
 ?>


<div class="page-main-header">
      <div class="main-header-right row m-0">

        <div class="main-header-left">
          <div class="logo-wrapper"><a href="dashboard"><img class="img-fluid" src="assets/images/logo/logo2.png"
                alt=""></a></div>
          <div class="toggle-sidebar"><i class="status_toggle middle" data-feather="grid" id="sidebar-toggle"> </i>
          </div>
        </div>
        <div class="left-menu-header col horizontal-wrapper pl-0">
          <ul class="horizontal-menu">

          </ul>
        </div>
        <div class="nav-right col-8 pull-right right-menu">
          <ul class="nav-menus">
            <li class="language-nav">
              <div class="translate_wrapper">
                <div class="current_lang">
                  <div class="lang"><i class="flag-icon flag-icon-us"></i><span class="lang-txt">EN </span></div>
                </div>
                <div class="more_lang" style="color:black !important">
                  <div class="lang selected" onclick="doGTranslate('en|en');return false;" data-value="en"><i class="flag-icon flag-icon-us"></i><span
                      class="lang-txt">English<span> (US)</span></span></div>
                      <div class="lang " onclick="doGTranslate('en|af');return false;" data-value="af"><span
                      class="lang-txt">Afrikaans<span> </span>(AF)</span></div>
                      <div class="lang " onclick="doGTranslate('en|fr');return false;" data-value="fr"><span
                      class="lang-txt">French<span>(FR) </span></span></div>
                      <div class="lang " onclick="doGTranslate('en|de');return false;" data-value="de"><span
                      class="lang-txt">German<span>(DE) </span></span></div>
                      <div class="lang " onclick="doGTranslate('en|ru');return false;" data-value="ru"><span
                      class="lang-txt">Russian<span>(RU) </span></span></div>
                      <div class="lang " onclick="doGTranslate('en|es');return false;" data-value="es"><span
                      class="lang-txt">Spanish<span>(ES) </span></span></div>
                      <div class="lang " onclick="doGTranslate('en|pt');return false;" data-value="pt"><span
                      class="lang-txt">Portuguese<span>(PT) </span></span></div>
                      <div class="lang " onclick="doGTranslate('en|it');return false;" data-value="it"><span
                      class="lang-txt">Italian<span>(IT) </span></span></div>
                      <div class="lang " onclick="doGTranslate('en|ja');return false;" data-value="ja"><span
                      class="lang-txt">Japanese<span>(JA) </span></span></div>
                      <div class="lang " onclick="doGTranslate('en|zh-CN');return false;" data-value="zh-CN"><span
                      class="lang-txt">Chinese<span>(CN) </span></span></div>

                      
                      
                      
                      
                      
                      
                      

                </div>
              </div>
            </li>





<style type="text/css">

a.gflag {vertical-align:middle;font-size:16px;padding:1px 0;background-repeat:no-repeat;background-image:url(//gtranslate.net/flags/16.png);}
a.gflag img {border:0;}
a.gflag:hover {background-image:url(//gtranslate.net/flags/16a.png);}
#goog-gt-tt {display:none !important;}
.goog-te-banner-frame {display:none !important;}
.goog-te-menu-value:hover {text-decoration:none !important;}
body {top:0 !important;}
#google_translate_element2 {display:none!important;}

</style>


<br /><select onchange="doGTranslate(this);" style="display:none" ><option value="">Select Language</option><option value="en|sq">Albanian</option><option value="en|ar">Arabic</option><option value="en|hy">Armenian</option><option value="en|az">Azerbaijani</option><option value="en|eu">Basque</option><option value="en|be">Belarusian</option><option value="en|bg">Bulgarian</option><option value="en|ca">Catalan</option><option value="en|zh-CN">Chinese (Simplified)</option><option value="en|zh-TW">Chinese (Traditional)</option><option value="en|hr">Croatian</option><option value="en|cs">Czech</option><option value="en|da">Danish</option><option value="en|nl">Dutch</option><option value="en|en">English</option><option value="en|et">Estonian</option><option value="en|tl">Filipino</option><option value="en|fi">Finnish</option><option value="en|fr">French</option><option value="en|gl">Galician</option><option value="en|ka">Georgian</option><option value="en|de">German</option><option value="en|el">Greek</option><option value="en|ht">Haitian Creole</option><option value="en|iw">Hebrew</option><option value="en|hi">Hindi</option><option value="en|hu">Hungarian</option><option value="en|is">Icelandic</option><option value="en|id">Indonesian</option><option value="en|ga">Irish</option><option value="en|it">Italian</option><option value="en|ja">Japanese</option><option value="en|ko">Korean</option><option value="en|lv">Latvian</option><option value="en|lt">Lithuanian</option><option value="en|mk">Macedonian</option><option value="en|ms">Malay</option><option value="en|mt">Maltese</option><option value="en|no">Norwegian</option><option value="en|fa">Persian</option><option value="en|pl">Polish</option><option value="en|pt">Portuguese</option><option value="en|ro">Romanian</option><option value="en|ru">Russian</option><option value="en|sr">Serbian</option><option value="en|sk">Slovak</option><option value="en|sl">Slovenian</option><option value="en|es">Spanish</option><option value="en|sw">Swahili</option><option value="en|sv">Swedish</option><option value="en|th">Thai</option><option value="en|tr">Turkish</option><option value="en|uk">Ukrainian</option><option value="en|ur">Urdu</option><option value="en|vi">Vietnamese</option><option value="en|cy">Welsh</option><option value="en|yi">Yiddish</option></select><div style="display:none" id="google_translate_element2"></div>
<script type="text/javascript">
function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'en',autoDisplay: false}, 'google_translate_element2');}
</script><script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>


<script type="text/javascript">
/* <![CDATA[ */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,f,f);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}',43,43,'||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'),0,{}))
/* ]]> */
</script>











            <li class="onhover-dropdown">
              <div class="notification-box"><i data-feather="bell"></i><span
                  class="badge badge-pill badge-secondary">#</span></div>
              <ul class="notification-dropdown onhover-show-div">
                <li class="bg-primary">
                  <h6 class="f-18 mb-0">Notitication</h6>
                  <p class="mb-0">Your lastest notifications</p>
                </li>

                <?php 
                $getnote = mysqli_query($mysqli,"SELECT * FROM activity WHERE userid='".$rows['id']."' ORDER BY id DESC LIMIT 5");  

                while ($n = mysqli_fetch_assoc($getnote)){
                ?>
                <li>
                  <p class="mb-0"><i class="fa fa-circle-o mr-3 font-primary"> </i><?php echo $n['action']; ?> <span
                      class="pull-right"><?php echo $n['status']; ?>.</span></p>
                </li>
                <?php  } ?>
                
                <li><a class="btn btn-primary" href="activity">Check all notification</a>
                  <!--a.f-15.f-w-500.txt-dark(href="#") Check all notification-->
                </li>
              </ul>
            </li>
            <li>
              <div class="mode"><i class="fa fa-moon-o"></i></div>
            </li>
           
           
            <li class="maximize"><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i
                  data-feather="maximize"></i></a></li>
            <li class="profile-nav onhover-dropdown p-0">
              <div class="media profile-media"><img class="b-r-10" src="assets/images/dashboard/profile.jpg" alt="">
                <div class="media-body"><span><?php echo $rows['firstname']." ".$rows['lastname']; ?></span>
                  <p class="mb-0 font-roboto">Welcome <i class="middle fa fa-angle-down"></i></p>
                </div>
              </div>
              <ul class="profile-dropdown onhover-show-div">
                <li onclick="location='profile'"><i data-feather="user"></i><span>Account </span></li>
                <?php if($rows['cycle']==0){  ?>
                <li onclick="location='mining'"><i data-feather="mail"></i><span>Mining Pools</span></li>
                <li onclick="location='hash'"><i data-feather="file-text"></i><span>Rent Pool</span></li>
                <li onclick="mylink()" ><i data-feather="users"></i><span>Referral Link</span></li>
                <li onclick="location='2fa'"><i data-feather="settings"></i><span>Settings</span></li>
                  <?php } ?>
                <li><a href="logout"><i data-feather="log-in"> </i><span>Log Out</span></a></li>
              </ul>
            </li>
          </ul>
        </div>
        <script id="result-template" type="text/x-handlebars-template">
            <div class="ProfileCard u-cf">                        
            <div class="ProfileCard-avatar"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay m-0"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg></div>
            <div class="ProfileCard-details">
            <div class="ProfileCard-realName">{{name}}</div>
            </div>
            </div>
          </script>
     
      </div>
    </div> 




				<input type="text" style="display:none" value="https://Coinmagnetics.com/register?refer=<?php echo $rows['referal_link']; ?>" id="refer-link" />
  <script>
function mylink() {
    /* Get the text field */
    var copyText =  document.getElementById("refer-link");

        copyText.style="display:block";
    /* Select the text field */
    copyText.select();

    /* Copy the text inside the text field */
    document.execCommand("copy");

 copyText.style="display:none";

 alert('Referral Link Copied!');

}
  </script>