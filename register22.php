<?php
session_start();


//check if session id is set if it is redirect to dashboard
if(isset($_SESSION['id']) and (isset($_SESSION['2fa']) and $_SESSION['2fa'] == "yes")){
	
    header("location:dashboard");
    exit();
}else{
     

    if(isset($_SESSION['2fa'])){

        if($_SESSION['2fa'] == "pending"){
            header("location:authenticate");
            exit();
        }

        if($_SESSION['2fa'] == "no"){
            header("location:dashboard");
            exit();
        } 

    }

}


include('connection.php');

?>
<!doctype html>
<html class="no-js" lang="">

<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- Title -->
	<title>Coin Magnetics - Buy, Sell, Swap</title>
	<!-- Favicon -->
	<link href="img/core-img/favicon.png" rel="icon" /><!-- Core Stylesheet -->
	<link href="css/style.css" rel="stylesheet" /><!-- Responsive Stylesheet -->
	<link href="css/responsive.css" rel="stylesheet" />
	<style>
@media (max-width: 768px) {
    .carousel-inner .carousel-item > div {
        display: none;
    }
    .carousel-inner .carousel-item > div:first-child {
        display: block;
    }
}

.carousel-inner .carousel-item.active,
.carousel-inner .carousel-item-next,
.carousel-inner .carousel-item-prev {
    display: flex;
}

/* display 3 */
@media (min-width: 768px) {
    
    .carousel-inner .carousel-item-right.active,
    .carousel-inner .carousel-item-next {
      transform: translateX(33.333%);
    }
    
    .carousel-inner .carousel-item-left.active, 
    .carousel-inner .carousel-item-prev {
      transform: translateX(-33.333%);
    }
}

.carousel-inner .carousel-item-right,
.carousel-inner .carousel-item-left{ 
  transform: translateX(0);
}


</style>

</head>

<body class="light-version">
	<!-- Preloader -->
	<div id="preloader">
		<div class="preload-content">
			<div id="dream-load"></div>
		</div>
	</div>
	<!-- ##### Header Area Start ##### -->

<header class="header-area fadeInDown" data-wow-delay="0.2s">
        <div class="classy-nav-container light breakpoint-off">
            <div class="container">
                <!-- Classy Menu -->
                <nav class="classy-navbar light justify-content-between" id="dreamNav">

                    <!-- Logo -->
                    <a class="nav-brand light" href="#"><img src="./logo2.png" style="width: 150px; height: 30px;" /></a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler demo">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- close btn -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul id="nav">
                             <li><a href="https://coinmagnetics.com">Home</a></li>
								
								<li><a href="https://coinmagnetics.com/about">About</a></li>
								<li><a href="#services">Buy/Sell</a></li>
								<li><a href="https://coinmagnetics.com/faq">FAQ</a></li>
								<li><a href="https://coinmagnetics.com/contact">Contact</a></li>
								<li>
								<style type="text/css">

a.gflag {vertical-align:middle;font-size:16px;padding:1px 0;background-repeat:no-repeat;background-image:url(//gtranslate.net/flags/16.png);}
a.gflag img {border:0;}
a.gflag:hover {background-image:url(//gtranslate.net/flags/16a.png);}
#goog-gt-tt {display:none !important;}
.goog-te-banner-frame {display:none !important;}
.goog-te-menu-value:hover {text-decoration:none !important;}
body {top:0 !important;}
#google_translate_element2 {display:none!important;}

</style>


<br /><select onchange="doGTranslate(this);"  ><option value="">Select Language</option><option value="en|en">English</option><option value="en|hy">Armenian</option><option value="en|az">Azerbaijani</option><option value="en|eu">Basque</option><option value="en|be">Belarusian</option><option value="en|ca">Catalan</option><option value="en|zh-CN">Chinese (Simplified)</option><option value="en|nl">Dutch</option><option value="en|fr">French</option><option value="en|de">German</option><option value="en|pt">Portuguese</option><option value="en|ru">Russian</option><option value="en|es">Spanish</option></select><div style="display:none" id="google_translate_element2"></div>
<script type="text/javascript">
function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'en',autoDisplay: false}, 'google_translate_element2');}
</script><script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>


<script type="text/javascript">
/* <![CDATA[ */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,f,f);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}',43,43,'||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'),0,{}))
/* ]]> */
</script>


								</li>
							</ul>
							<!-- Button --><a class="btn login-btn ml-50"
								href="https://Coinmagnetics.com/">Log in</a>
						</div>
						<!-- Nav End -->
					</div>
				</nav>
			</div>
		</div>
	</header>
	<!-- ##### Header Area End ##### -->
	<!-- ##### Welcome Area Start ##### -->



<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Register | Coin Magnetics </title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- Fontawesome CSS -->
	<link rel="stylesheet" href="css/fontawesome-all.min.css">
	<!-- Flaticon CSS -->
	<link rel="stylesheet" href="font/flaticon.css">
	<!-- Google Web Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;display=swap" rel="stylesheet">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="style.css">
	<link type="text/css" rel="stylesheet" href="js/sweetalert.css">
	<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5dde42de43be710e1d1f5485/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</head>

<body>
	<!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
  <section class="faq-timeline-area  section-padding-100-0 clearfix " id="about">  			
	<div class="wrapper">
		<div class="fxt-template-animation fxt-template-layout5">
			<div class="fxt-bg-img fxt-none-767" data-bg-image="img/figure/bg5-5.png">
				<div class="fxt-intro">
					<div class="sub-title">Welcome To</div>
					<h1>Coin Magnetics, World's N0 1 Digital Asset management system</h1>
					<h2 class="special-head dark">Trade, Buy, Sell, Swap and Save.</h2>
				</div>
			</div>
			<div class="fxt-bg-color">
				<div class="fxt-header">
					<div class="fxt-page-switcher">
						<a href="login" class="switcher-text switcher-text1 active">LogIn</a>
						<a href="register" class="switcher-text switcher-text2">Register</a>
					</div>
				</div>
				<div class="fxt-form">
					<form method="POST">
						<div class="form-group fxt-transformY-50 fxt-transition-delay-1">
							<input type="text" class="form-control" name="firstname" placeholder="First Name" required="required">
							<i class="flaticon-user"></i>
						</div>
						<div class="form-group fxt-transformY-50 fxt-transition-delay-1">
							<input type="text" class="form-control" name="lastname" placeholder="Last Name" required="required">
							<i class="flaticon-user"></i>
						</div>
						<div class="form-group fxt-transformY-50 fxt-transition-delay-2">
							<input type="email" class="form-control" name="email" placeholder="Your Email" required="required">
							<i class="flaticon-envelope"></i>
						</div>
						<div class="form-group fxt-transformY-50 fxt-transition-delay-1">
							<input type="number" class="form-control" name="phone" placeholder="Your Phone NUmber" required="required">
							<i class="flaticon-user"></i>
						</div>
						<div class="form-group fxt-transformY-50 fxt-transition-delay-3">
							<input type="password" class="form-control" id="password" name="password" placeholder="Password" required="required">
							<i class="flaticon-padlock"></i>
						</div>

						<div class="form-group fxt-transformY-50 fxt-transition-delay-3">
							<input type="password" class="form-control" id="confirm" name="confirm" placeholder="Confirm Password" required="required">
							<i class="flaticon-padlock"></i>
						</div>


						<div class="form-group fxt-transformY-50 fxt-transition-delay-3">  
                                                                                    
                                        <select  id="countrys" name="country" required class="form-control"
                                        placeholder="Country">
                                        <option value=""selected="">Select Country </option>
                                        <option value="AF">Afghanistan (AF)</option>
                                        <option value="AL">Albania (AL)</option>
                                        <option value="DZ">Algeria (DZ)</option>
                                        <option value="AS">American Samoa (AS)</option>
                                        <option value="AD">Andorra (AD)</option>
                                        <option value="AO">Angola (AO)</option>
                                        <option value="AI">Anguilla (AI)</option>
                                        <option value="AG">Antigua (AG)</option>
                                        <option value="AR">Argentina (AR)</option>
                                        <option value="AM">Armenia (AM)</option>
                                        <option value="AW">Aruba (AW)</option>
                                        <option value="AU">Australia (AU)</option>
                                        <option value="AT">Austria (AT)</option>
                                        <option value="AZ">Azerbaijan (AZ)</option>
                                        <option value="BS">The Bahamas (BS)</option>
                                        <option value="BH">Bahrain (BH)</option>
                                        <option value="BD">Bangladesh (BD)</option>
                                        <option value="BB">Barbados (BB)</option>
                                        <option value="BY">Belarus (BY)</option>
                                        <option value="BE">Belgium (BE)</option>
                                        <option value="BZ">Belize (BZ)</option>
                                        <option value="BJ">Benin (BJ)</option>
                                        <option value="BM">Bermuda (BM)</option>
                                        <option value="BT">Bhutan (BT)</option>
                                        <option value="BO">Bolivia (BO)</option>
                                        <option value="BA">Bosnia and Herzegovina (BA)</option>
                                        <option value="BW">Botswana (BW)</option>
                                        <option value="BR">Brazil (BR)</option>
                                        <option value="IO">British Indian Ocean Territory (IO)</option>
                                        <option value="BN">Brunei (BN)</option>
                                        <option value="BG">Bulgaria (BG)</option>
                                        <option value="BF">Burkina Faso (BF)</option>
                                        <option value="BI">Burundi (BI)</option>
                                        <option value="KH">Cambodia (KH)</option>
                                        <option value="CM">Cameroon (CM)</option>
                                        <option value="CA">Canada (CA)</option>
                                        <option value="CV">Cape Verde (CV)</option>
                                        <option value="KY">Cayman Islands (KY)</option>
                                        <option value="CF">Central African Republic (CF)</option>
                                        <option value="TD">Chad (TD)</option>
                                        <option value="CL">Chile (CL)</option>
                                        <option value="CN">China (CN)</option>
                                        <option value="CO">Colombia (CO)</option>
                                        <option value="KM">Comoros (KM)</option>
                                        <option value="CG">Republic of the Congo (CG)</option>
                                        <option value="CD">Democratic Republic of Congo (CD)</option>
                                        <option value="CK">Cook Islands (CK)</option>
                                        <option value="CR">Costa Rica (CR)</option>
                                        <option value="CI">CÃ´te d'Ivoire (CI)</option>
                                        <option value="HR">Croatia (HR)</option>
                                        <option value="CU">Cuba (CU)</option>
                                        <option value="CY">Cyprus (CY)</option>
                                        <option value="CZ">Czech Republic (CZ)</option>
                                        <option value="DK">Denmark (DK)</option>
                                        <option value="DJ">Djibouti (DJ)</option>
                                        <option value="DM">Dominica (DM)</option>
                                        <option value="DO">Dominican Republic (DO)</option>
                                        <option value="EC">Ecuador (EC)</option>
                                        <option value="EG">Egypt (EG)</option>
                                        <option value="SV">El Salvador (SV)</option>
                                        <option value="GQ">Equatorial Guinea (GQ)</option>
                                        <option value="ER">Eritrea (ER)</option>
                                        <option value="EE">Estonia (EE)</option>
                                        <option value="ET">Ethiopia (ET)</option>
                                        <option value="FK">Falkland Islands (FK)</option>
                                        <option value="FO">Faroe Islands (FO)</option>
                                        <option value="FJ">Fiji (FJ)</option>
                                        <option value="FI">Finland (FI)</option>
                                        <option value="FR">France (FR)</option>
                                        <option value="GF">French Guiana (GF)</option>
                                        <option value="PF">French Polynesia (PF)</option>
                                        <option value="GA">Gabon (GA)</option>
                                        <option value="GM">The Gambia (GM)</option>
                                        <option value="GE">Georgia (GE)</option>
                                        <option value="DE">Germany (DE)</option>
                                        <option value="GH">Ghana (GH)</option>
                                        <option value="GI">Gibraltar (GI)</option>
                                        <option value="GR">Greece (GR)</option>
                                        <option value="GL">Greenland (GL)</option>
                                        <option value="GD">Grenada (GD)</option>
                                        <option value="GP">Guadeloupe (GP)</option>
                                        <option value="GU">Guam (GU)</option>
                                        <option value="GT">Guatemala (GT)</option>
                                        <option value="GN">Guinea (GN)</option>
                                        <option value="GW">Guinea-Bissau (GW)</option>
                                        <option value="GY">Guyana (GY)</option>
                                        <option value="HT">Haiti (HT)</option>
                                        <option value="VA">Vatican City (VA)</option>
                                        <option value="HN">Honduras (HN)</option>
                                        <option value="HK">Hong Kong (HK)</option>
                                        <option value="HU">Hungary (HU)</option>
                                        <option value="IS">Iceland (IS)</option>
                                        <option value="IN">India (IN)</option>
                                        <option value="ID">Indonesia (ID)</option>
                                        <option value="IR">Iran (IR)</option>
                                        <option value="IQ">Iraq (IQ)</option>
                                        <option value="IE">Ireland (IE)</option>
                                        <option value="IL">Israel (IL)</option>
                                        <option value="IT">Italy (IT)</option>
                                        <option value="JM">Jamaica (JM)</option>
                                        <option value="JP">Japan (JP)</option>
                                        <option value="JO">Jordan (JO)</option>
                                        <option value="KZ">Kazakhstan (KZ)</option>
                                        <option value="KE">Kenya (KE)</option>
                                        <option value="KI">Kiribati (KI)</option>
                                        <option value="KR">South Korea (KR)</option>
                                        <option value="KW">Kuwait (KW)</option>
                                        <option value="KG">Kyrgyzstan (KG)</option>
                                        <option value="LA">Laos (LA)</option>
                                        <option value="LV">Latvia (LV)</option>
                                        <option value="LB">Lebanon (LB)</option>
                                        <option value="LS">Lesotho (LS)</option>
                                        <option value="LR">Liberia (LR)</option>
                                        <option value="LY">Libya (LY)</option>
                                        <option value="LI">Liechtenstein (LI)</option>
                                        <option value="LT">Lithuania (LT)</option>
                                        <option value="LU">Luxembourg (LU)</option>
                                        <option value="MO">Macau (MO)</option>
                                        <option value="MK">Macedonia (MK)</option>
                                        <option value="MG">Madagascar (MG)</option>
                                        <option value="MW">Malawi (MW)</option>
                                        <option value="MY">Malaysia (MY)</option>
                                        <option value="MV">Maldives (MV)</option>
                                        <option value="ML">Mali (ML)</option>
                                        <option value="MT">Malta (MT)</option>
                                        <option value="MH">Marshall Islands (MH)</option>
                                        <option value="MQ">Martinique (MQ)</option>
                                        <option value="MR">Mauritania (MR)</option>
                                        <option value="MU">Mauritius (MU)</option>
                                        <option value="YT">Mayotte (YT)</option>
                                        <option value="MX">Mexico (MX)</option>
                                        <option value="FM">Federated States of Micronesia (FM)</option>
                                        <option value="MD">Moldova (MD)</option>
                                        <option value="MC">Monaco (MC)</option>
                                        <option value="MN">Mongolia (MN)</option>
                                        <option value="ME">Montenegro (ME)</option>
                                        <option value="MS">Montserrat (MS)</option>
                                        <option value="MA">Morocco (MA)</option>
                                        <option value="MZ">Mozambique (MZ)</option>
                                        <option value="MM">Burma Myanmar (MM)</option>
                                        <option value="NA">Namibia (NA)</option>
                                        <option value="NR">Nauru (NR)</option>
                                        <option value="NP">Nepal (NP)</option>
                                        <option value="NL">Netherlands (NL)</option>
                                        <option value="AN">Netherlands Antilles (AN)</option>
                                        <option value="NC">New Caledonia (NC)</option>
                                        <option value="NZ">New Zealand (NZ)</option>
                                        <option value="NI">Nicaragua (NI)</option>
                                        <option value="NE">Niger (NE)</option>
                                        <option value="NG">Nigeria (NG)</option>
                                        <option value="NU">Niue (NU)</option>
                                        <option value="NF">Norfolk Island (NF)</option>
                                        <option value="MP">Northern Mariana Islands (MP)</option>
                                        <option value="NO">Norway (NO)</option>
                                        <option value="OM">Oman (OM)</option>
                                        <option value="PK">Pakistan (PK)</option>
                                        <option value="PW">Palau (PW)</option>
                                        <option value="PS">Palestine (PS)</option>
                                        <option value="PA">Panama (PA)</option>
                                        <option value="PG">Papua New Guinea (PG)</option>
                                        <option value="PY">Paraguay (PY)</option>
                                        <option value="PE">Peru (PE)</option>
                                        <option value="PH">Philippines (PH)</option>
                                        <option value="PL">Poland (PL)</option>
                                        <option value="PT">Portugal (PT)</option>
                                        <option value="PR">Puerto Rico (PR)</option>
                                        <option value="QA">Qatar (QA)</option>
                                        <option value="RE">RÃ©union (RE)</option>
                                        <option value="RO">Romania (RO)</option>
                                        <option value="RU">Russia (RU)</option>
                                        <option value="RW">Rwanda (RW)</option>
                                        <option value="BL">Saint BarthÃ©lemy (BL)</option>
                                        <option value="SH">Saint Helena (SH)</option>
                                        <option value="KN">Saint Kitts and Nevis (KN)</option>
                                        <option value="LC">St. Lucia (LC)</option>
                                        <option value="MF">Saint Martin (MF)</option>
                                        <option value="PM">Saint Pierre and Miquelon (PM)</option>
                                        <option value="VC">Saint Vincent and the Grenadines (VC)</option>
                                        <option value="WS">Samoa (WS)</option>
                                        <option value="SM">San Marino (SM)</option>
                                        <option value="ST">SÃ£o TomÃ© and PrÃncipe (ST)</option>
                                        <option value="SA">Saudi Arabia (SA)</option>
                                        <option value="SN">Senegal (SN)</option>
                                        <option value="RS">Serbia (RS)</option>
                                        <option value="SC">Seychelles (SC)</option>
                                        <option value="SL">Sierra Leone (SL)</option>
                                        <option value="SG">Singapore (SG)</option>
                                        <option value="SK">Slovakia (SK)</option>
                                        <option value="SI">Slovenia (SI)</option>
                                        <option value="SB">Solomon Islands (SB)</option>
                                        <option value="SO">Somalia (SO)</option>
                                        <option value="ZA">South Africa (ZA)</option>
                                        <option value="ES">Spain (ES)</option>
                                        <option value="LK">Sri Lanka (LK)</option>
                                        <option value="SD">Sudan (SD)</option>
                                        <option value="SR">Suriname (SR)</option>
                                        <option value="SZ">Swaziland (SZ)</option>
                                        <option value="SE">Sweden (SE)</option>
                                        <option value="CH">Switzerland (CH)</option>
                                        <option value="SY">Syria (SY)</option>
                                        <option value="TW">Taiwan (TW)</option>
                                        <option value="TJ">Tajikistan (TJ)</option>
                                        <option value="TZ">Tanzania (TZ)</option>
                                        <option value="TH">Thailand (TH)</option>
                                        <option value="TL">Timor-Leste (TL)</option>
                                        <option value="TG">Togo (TG)</option>
                                        <option value="TK">Tokelau (TK)</option>
                                        <option value="TO">Tonga (TO)</option>
                                        <option value="TT">Trinidad and Tobago (TT)</option>
                                        <option value="TN">Tunisia (TN)</option>
                                        <option value="TR">Turkey (TR)</option>
                                        <option value="TM">Turkmenistan (TM)</option>
                                        <option value="TC">Turks and Caicos Islands (TC)</option>
                                        <option value="TV">Tuvalu (TV)</option>
                                        <option value="UG">Uganda (UG)</option>
                                        <option value="UA">Ukraine (UA)</option>
                                        <option value="AE">United Arab Emirates (AE)</option>
                                        <option value="GB">United Kingdom (GB)</option>
                                        <option value="US">United States (US)</option>
                                        <option value="UY">Uruguay (UY)</option>
                                        <option value="VI">US Virgin Islands (VI)</option>
                                        <option value="UZ">Uzbekistan (UZ)</option>
                                        <option value="VU">Vanuatu (VU)</option>
                                        <option value="VE">Venezuela (VE)</option>
                                        <option value="VN">Vietnam (VN)</option>
                                        <option value="WF">Wallis and Futuna (WF)</option>
                                        <option value="YE">Yemen (YE)</option>
                                        <option value="ZM">Zambia (ZM)</option>
                                        <option value="ZW">Zimbabwe (ZW)</option>
                                    </select>
                                    
                                </div>



						<div class="form-group fxt-transformY-50 fxt-transition-delay-4">
							<div class="fxt-content-between">
								<button type="submit" name="register" class="fxt-btn-fill">Register</button>
								<div class="checkbox">
									<input id="checkbox1" type="checkbox">
									<label for="checkbox1">I agree the</label> <a href="https://coinmagnetics.com/terms">Terms Of Services</a>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="fxt-footer">
					<ul class="fxt-socials">
						<li class="fxt-facebook fxt-transformY-50 fxt-transition-delay-5"></li>
						
						
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- jquery-->
	<script src="js/jquery-3.5.0.min.js"></script>
	<!-- Popper js -->
	<script src="js/popper.min.js"></script>
	<!-- Bootstrap js -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Imagesloaded js -->
	<script src="js/imagesloaded.pkgd.min.js"></script>
	<!-- Validator js -->
	<script src="js/validator.min.js"></script>
	<!-- Custom Js -->
	<script src="js/main.js"></script>
	<script src="js/sweetalert.js"></script>

	<script>
        //change password 
        $("#confirm, #password").keyup(function () {
            var password = $("#password").val();
            var confirm = $("#confirm").val();

            if (password != confirm || password == "") {
                $("#password").css("border-color", "#9b1d1d");
                $("#confirm").css("border-color", "#9b1d1d");
            } else {
                $("#password").css("border-color", "green");
                $("#confirm").css("border-color", "green");

            }


        });


       

    </script>


</body>


<?php

	if(isset($_POST['register'])){
	//retrive all the input from user

	$firstname =  mysqli_real_escape_string($mysqli,$_POST['firstname']);
	$lastname =  mysqli_real_escape_string($mysqli,$_POST['lastname']);
    $email = mysqli_real_escape_string($mysqli,$_POST['email']);
    $phone = mysqli_real_escape_string($mysqli,$_POST['phone']);
	$password = mysqli_real_escape_string($mysqli,$_POST['password']);
    $hashpassword = password_hash($password, PASSWORD_DEFAULT);
    $country = mysqli_real_escape_string($mysqli,$_POST['country']);
	

	
	$date= date("d")." ".date("F")." ".date("Y")." , ".date("h")." : ".date("i").date("a");
	
		//check if email exist aready
        $check_email = mysqli_query($mysqli,"SELECT * FROM users WHERE email='$email' or phone='$phone' ");
	
	//if zero does not exist proceed to register
	if(mysqli_num_rows($check_email) < 1){
	
		//check if user was reffered
		$refer = "";
		if(isset($_GET['refer'])){
      $refer = $_GET['refer'];
      

		}
	
		//generate referer code for this person
	   $code ="";
												do{
											
																//generate code for user verification
													$code = "tc-".mt_rand(128998, 999998);
													
													$search2 = mysqli_query($mysqli, "SELECT * FROM users WHERE referal_link='$code'");
	
												}while(mysqli_num_rows($search2) > 0);


    $smscode = mt_rand(238998, 999998);
    
 
    
    //now generate 2 factor autherication code for this user
require_once("google_authenticator/index.php"); 

$g = new \Google\Authenticator\GoogleAuthenticator();

$secret = $g->generateSecret();

$two_fa_link = $g->getURL($email, 'coinmagnetics.com', $secret);



	//now we can insert register the user
	
	$reg = mysqli_query($mysqli,"INSERT INTO `users`(`firstname`, `lastname`, `email`, `password`, `phone`,  `country`,`referal_link`, `referred`, `refer_date`, `img`, `smscode`, `2fa_key`, `2fa_link`, `active`, `userstatus`, `can_donate`, `wallet`,  `status`) VALUES('$firstname', '$lastname', '$email', '$hashpassword', '$phone', '$country',  '$code', '$refer', '$date', 'img/profile.png', '$smscode', '$secret', '$two_fa_link', 0, 0, 1, 2, 1 ) ");
	
	
	if($reg){
	
	//email will be sent here to admin starting a new user just registtered
	$name = $firstname." ".$lastname;
	
	 //start email sending
	

    //end of email sending
                

	/*
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=DhJMgGa5VGBsHxAgljZuLn7chY6mQzJqUqiziAiPMoAWCi4V3hQV8DkprAT0&from=T-Capital&to=".$phone."&body=Welcome%20to%20Treasure%20Capital%20Here%20is%20your%20token%20".$smscode."&dnd=4",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
));

$response = curl_exec($curl);

curl_close($curl);
*/


        



	
	
	
	?>
<script>


swal(
     'Registration Successful',
    "Your Account is pending activation. Proceed to Sign In.",
     'success'
);

setTimeout(() => {
    location ='index';
}, 2000);
</script>

<?php
	
	
	}
	
	
	
	}else{
	//it exist stop and throw alert
	
	?>
<script>


swal(
     'Email or Phone Number Already Exist',
    "This email or phone number is registered with another account!",
    'warning'
)


</script>

<?php
	}
	
	
	
	
	
	}
	
	
	
	?>

					
					</div>
				</div>
	</section>

	<div class="clearfix"></div>
	<!-- ##### Our Trial Area End ##### -->

	<section class="faq-timeline-area  section-padding-100-0 clearfix " id="about">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-12 col-lg-5 offset-lg-1 col-sm-10 offset-sm-1">
					<div class="who-we-contant">
						<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s"
							style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
							<span>Get Started</span></div>

						<h4 class="fadeInUp" data-wow-delay="0.3s">It’s never too late to get started</h4>

						<h2 class="special-head dark">Trade, Buy, Sell, Swap, Mine and Save All Digtal Assets</h2>
							
						<div class="clearfix"></div>
						<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><href="#home"><img alt="" src="./ic_website_sign_up.svg" style="width: 120px; height: 25px;" /></a>
							
								<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-google.svg" style="width: 120px; height: 25px;" /></a>
							
							<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-apple.svg" style="width: 120px; height: 25px;" /></a>
					</div>
				</div>

				<div class="col-12 col-lg-6 mt-s no-padding-right">
					<div class="welcome-meter fadeInUp" data-wow-delay="0.7s"><img alt=""
							class="img-responsive center-block" src="img/core-img/trading1.png" style="width: 460px; height: 260px;" /></div>
				</div>
			</div>
		</div>

			<!-- ##### Contact Area End ##### -->
			
		<!-- ##### Welcome Area End ##### -->
	
			<!-- ##### Contact Area End ##### -->

			<div class="footer-content-area">
				<div class="container">
					<div class="row ">
						<div class="col-12 col-lg-4 col-md-6">
							<div class="footer-copywrite-info">
								<!-- Copywrite -->
								<div class="copywrite_text fadeInUp" data-wow-delay="0.2s">
									<div class="footer-logo"><a href="https://coinmagnetics.com/"><img alt="logo" src="./logo2.png"
												style="width: 230px; height: 40px;" /></a></div>

									<p>Plug-in to World's N0 1 Digital Asset management system today and Start Earning Big!.
									</p>
								</div>
								<!-- Social Icon -->

								<div class="footer-social-info fadeInUp" data-wow-delay="0.4s"></div>
							</div>
						</div>

						<div class="col-12 col-lg-3 col-md-6">
							<div class="contact_info_area d-sm-flex justify-content-between">
								<!-- Content Info -->
								<div class="contact_info mt-x text-center fadeInUp" data-wow-delay="0.3s">
									<h5>PRIVACY &amp; TOS</h5>

									<p><a href="https://coinmagnetics.com/terms">Terms Of Use</a></p>

									<p><a href="https://coinmagnetics.com/Privacy-policy">Privacy Policy</a></p>

									<p><a href="https://coinmagnetics.com/compliance">Legal & Compliance</a></p>

									<p><a href="https://coinmagnetics.com/Customer-pro">Customer protection</a></p>								
								
									<p><a href="#">Advertiser Agreement</a></p>

								</div>
							</div>
						</div>

						<div class="col-12 col-lg-2 col-md-6 ">
							<!-- Content Info -->
							<div class="contact_info_area d-sm-flex justify-content-between">
								<div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.2s">
									<h5>NAVIGATE</h5>

									<p><a href="https://coinmagnetics.com/about">About Us</a></p>
									
									<p><a href="https://coinmagnetics.com/contact">Contact Us</a></p>
									
									<p><a href="https://coinmagnetics.com/faq">FAQ</a></p>

									<p>Developers</p>

									<p>Resources</p>
								</div>
							</div>
						</div>

						<div class="col-12 col-lg-3 col-md-6 ">
							<div class="contact_info_area d-sm-flex justify-content-between">
								<!-- Content Info -->
								<div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.4s">
									<h5>FIND US HERE</h5>

									<p>Mailing Address:265 Akowonjo RD,</p>

									<p>Lagos, Nigeria. Los 23401</p>

									
									<p>Email: support@coinmagnetics.com</p>
									
									<div class="dream-btn-group fadeInUp" data-wow-delay="0.4s"><a
									href="https://t.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/tg.png"
										style="width: 43px; height: 40px; float: left;" /></a>&nbsp; &nbsp;&nbsp;
								&nbsp;<a href="https://www.instagram.com/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/ig.png" style="width: 42px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp; &nbsp;<a href="https://twitter.com/Coinmagnetics"><img alt=""
										src="/img/social/tw.png" style="width: 43px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp;&nbsp;<a href="https://fb.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/fb.png" style="width: 43px; height: 40px;" /></a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- ##### Footer Area End ##### -->
		<!-- ########## All JS ########## -->
		<!-- jQuery js -->
		<script src="js/jquery.min.js"></script><!-- Popper js -->
		<script src="js/popper.min.js"></script><!-- Bootstrap js -->
		<script src="js/bootstrap.min.js"></script><!-- All Plugins js -->
		<script src="js/plugins.js"></script><!-- Parallax js -->
		<script src="js/dzsparallaxer.js"></script>
		<script src="js/jquery.syotimer.min.js"></script><!-- script js -->
		<script src="js/script.js"></script>
	</div>
</body>
<script>
$('#recipeCarousel').carousel({
  interval: 4000
})

$('.carousel .carousel-item').each(function(){
    var minPerSlide = 3;
    var next = $(this).next();
    if (!next.length) {
    next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
    
    for (var i=0;i<minPerSlide;i++) {
        next=next.next();
        if (!next.length) {
        	next = $(this).siblings(':first');
      	}
        
        next.children(':first-child').clone().appendTo($(this));
      }
});

	</script>

</html>