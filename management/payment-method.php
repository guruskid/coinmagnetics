<?php
session_start();

include('../connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['adminid'])){
	
    header("location:index");
    
}


$get_admin = mysqli_query($mysqli,"SELECT * FROM admins WHERE id='".$_SESSION['adminid']."' ");
$rows = mysqli_fetch_assoc($get_admin);



?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
    <title>Payment Method- Argen Capital </title>
    <link rel="apple-touch-icon" href="../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="../app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/sweetalert2.min.css">
    <!-- END: Theme CSS-->
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

   <?php include('header.php'); ?>

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Payment Request</h5>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="index"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">Approve Payment
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                    <div class="card">
                                <div class="card-body">
                                    <h4 class="mt-0 header-title">Add Payment Method</h4>
                                    <form method="POST" enctype="multipart/form-data">
                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Select Payment Logo</label>
                                            <div class="col-sm-10"><input class="form-control" type="file"
                                                    id="input-file-now" name="logo" required></div>
                                        </div>
                                        <div class="form-group row"><label for="example-search-input"
                                                class="col-sm-2 col-form-label">Payment Method Name</label>
                                            <div class="col-sm-10"><input class="form-control" name="name"
                                                    id="exampleInputName" placeholder="Enter Name e.g BitCoin"
                                                    type="text" required></div>
                                        </div>
                                        <div class="form-group row"><label for="example-email-input"
                                                class="col-sm-2 col-form-label">Charge</label>
                                            <div class="col-sm-10"><input class="form-control" type="number"
                                                    id="input-success" name="charge" required></div>
                                        </div>
                                        <div class="form-group row"><label for="example-email-input"
                                                class="col-sm-2 col-form-label">Default Wallet Address</label>
                                            <div class="col-sm-10"><input class="form-control" name="wallet_address"
                                                    placeholder="Enter wallet address 32 character long" type="text"
                                                    required></div>
                                        </div>
                                         <div class="form-group row"><label for="example-email-input"
                                                class="col-sm-2 col-form-label">Minimum Amount</label>
                                            <div class="col-sm-10"><input class="form-control" type="number"
                                                    id="input-success" name="min" required></div>
                                        </div>

                                        <div><button type="submit" name="add-payment"
                                                class="btn btn-primary waves-effect waves-light mr-1">Add Payment
                                                Method</button> </div>
                                    </form>
                                </div>
                            </div>
                       
                    </div>
                </div>
                <!-- Zero configuration table -->
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">All Method</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                       
                                        <div class="table-responsive">
                                            <table class="table zero-configuration">
                                                <thead>
                                                    <tr>
                                                    <th>S/N</th>
                                                <th>Logo</th>
                                                <th>Method Name</th>
                                                <th>Wallet Address</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                               
                                           
                                               
                                            <?php
                                        //start the loop for see all users
                                        $get_users = mysqli_query($mysqli,"SELECT * FROM payment_method ORDER BY id DESC");
                                            $i=0;
                                            while($row= mysqli_fetch_assoc($get_users)){
                                                $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><img src="../<?php echo $row['logo']; ?>" width="50" height="50" />
                                                </td>
                                                <td><?php echo $row['name']; ?></td>
                                                <td><?php echo $row['wallet_address']; ?></td>

                                                <td><?php if($row['status'] ==0){
                                                   echo '<span class=" badge badge-danger">Deactivated</span>'; 
                                                }else{
                                                     echo '<span class=" badge badge-info">Active </span>';
                                                   
                                                } ?></td>

                                                <td><?php if($row['status'] ==0){
                                                    echo "<a class='btn btn-primary waves-effect waves-light' href='#toveroedit".$row['id']."' data-toggle='modal' data-target='#toveroedit".$row['id']."' >Edit</a>";
                                                }else{
                                                   echo "<a class='btn btn-primary waves-effect waves-light ' href='#toveroedit".$row['id']."' data-toggle='modal' data-target='#toveroedit".$row['id']."'  >Edit</a>"; 
                                                } ?></td>
                                            </tr>





                                            <?php

                                            }

                                             ?>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                    <th>S/N</th>
                                                <th>Logo</th>
                                                <th>Method Name</th>
                                                <th>Wallet Address</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Zero configuration table -->
               

              
            </div>
        </div>
    </div>
    <!-- END: Content-->





    <?php
                                        //start the loop for see all users
                                        $get_users = mysqli_query($mysqli,"SELECT * FROM payment_method ORDER BY id DESC");
                                            $i=0;
                                            while($row= mysqli_fetch_assoc($get_users)){
                                                $i++;
                                            ?>



    <div class="modal fade bs-example-modal-lg"
                                                id="toveroedit<?php echo $row['id']; ?>" tabindex="-1" role="dialog"
                                                aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0" id="myLargeModalLabel">
                                                                <?php echo $row['name']; ?></h5><button type="button"
                                                                class="close" data-dismiss="modal"
                                                                aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">

                                                            <div class="form-group row">
                                                                <img src="../<?php echo $row['logo']; ?>" alt=""
                                                                    width="100" height="100">
                                                            </div>
                                                            <div class="form-group row">
                                                                <h6>Method Status <?php if($row['status'] ==0){
                                                                        echo '<span class=" badge badge-danger">Disabled</span>'; 
                                                                        }else{
                                                                            echo '<span class=" badge badge-success">Active </span>';
                                                                        
                                                                        } ?></h6>



                                                            </div>

                                                            <form id="update-<?php echo $row['id']; ?>" method="POST"
                                                                enctype="multipart/form-data">

                                                                <input type="hidden" value="<?php echo $row['id']; ?>"
                                                                    name="id" id="hidden-id<?php echo $row['id']; ?>" />

                                                                <div class="form-group row"
                                                                    id="name<?php echo $row['id']; ?>"><label
                                                                        for="example-search-input"
                                                                        class="col-sm-2 col-form-label">Payment Method
                                                                        Name</label>
                                                                    <div class="col-sm-10"><input class="form-control"
                                                                            name="name" 
                                                                            placeholder="Enter Name e.g BitCoin"
                                                                            type="text"
                                                                            value="<?php echo $row['name']; ?>"
                                                                            required></div>
                                                                </div>
                                                                <div class="form-group row"
                                                                    id="charge<?php echo $row['id']; ?>"><label
                                                                        for="example-email-input"
                                                                        class="col-sm-2 col-form-label">Charge</label>
                                                                    <div class="col-sm-10"><input class="form-control"
                                                                            type="number"
                                                                            value="<?php echo $row['charge']; ?>"
                                                                             name="charge" required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row"
                                                                    id="wallet<?php echo $row['id']; ?>"><label
                                                                        for="example-email-input"
                                                                        class="col-sm-2 col-form-label">Default Wallet
                                                                        Address</label>
                                                                    <div class="col-sm-10"><input class="form-control"
                                                                            name="wallet_address"
                                                                            value="<?php echo $row['wallet_address']; ?>"
                                                                            placeholder="Enter wallet address 32 character long"
                                                                            type="text" required></div>
                                                                </div>
                                                                <div class="form-group row"  id="min<?php echo $row['id']; ?>"><label for="example-email-input"
                                                                    class="col-sm-2 col-form-label">Minimum Amount</label>
                                                                <div class="col-sm-10"><input class="form-control" type="number" value="<?php echo $row['min']; ?>"
                                                                         name="min" required></div>
                                                            </div>

                                                                <div><button id="btn<?php echo $row['id']; ?>"
                                                                        type="submit" name="update"
                                                                        class="btn btn-primary waves-effect waves-light mr-1" onclick="document.getElementById('update-<?php echo $row['id']; ?>').submit()">Update
                                                                        Payment
                                                                        Method</button>
                                                                </div>
                                                            </form>



                                                            <form method="POST"
                                                                id="form-approve-<?php echo $row['id']; ?>">

                                                                <input type="hidden" name="id"
                                                                    value="<?php echo $row['id']; ?>" />
                                                                <input type="hidden" name="name"
                                                                    value="<?php echo $row['name']; ?>" />
                                                                <input type="hidden" name="enable-method"
                                                                    value="<?php echo $row['id']; ?>" />

                                                            </form>

                                                            <form method="POST"
                                                                id="form-disable-<?php echo $row['id']; ?>">

                                                                <input type="hidden" name="id"
                                                                    value="<?php echo $row['id']; ?>" />

                                                                <input type="hidden" name="name"
                                                                    value="<?php echo $row['name']; ?>" />
                                                                <input type="hidden" name="disable-method"
                                                                    value="<?php echo $row['id']; ?>" />

                                                            </form>


                                                        </div>
                                                        <div class="modal-footer"><button type="button"
                                                                class="btn btn-secondary waves-effect"
                                                                data-dismiss="modal">Close</button>
                                                                
                                                                <?php if($row['status'] ==0){
                                                    echo " <button type='button' onclick="."document.getElementById('form-approve-".$row['id']."').submit()"." class='btn btn-primary waves-effect waves-light'>Enable Method</button>";
                                                }else{
                                                   echo "<button class='btn btn-primary waves-effect waves-light' onclick="."document.getElementById('form-disable-".$row['id']."').submit()"." >Disable Method</button>"; 
                                                } ?>
                                                                
                                                               </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->



<?php } ?>






      <!-- BEGIN: Footer-->
  <footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-left d-inline-block">2020 &copy; </span></span>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
    </p>
</footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="../app-assets/vendors/js/vendors.min.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="../app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="../app-assets/js/scripts/configs/vertical-menu-light.js"></script>
    <script src="../app-assets/js/core/app-menu.js"></script>
    <script src="../app-assets/js/core/app.js"></script>
    <script src="../app-assets/js/scripts/components.js"></script>
    <script src="../app-assets/js/scripts/footer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="../app-assets/js/scripts/datatables/datatable.js"></script>
    <!-- END: Page JS-->
   
    <?php


if(isset($_POST['add-payment'])){
//retrive the inputs
$name =  mysqli_real_escape_string($mysqli,$_POST['name']);
$charge = mysqli_real_escape_string($mysqli,$_POST['charge']);
$wallet_address =  mysqli_real_escape_string($mysqli,$_POST['wallet_address']);
$min = mysqli_real_escape_string($mysqli,$_POST['min']);

//set location that image should b uploaded to
$target_locate = "img/payment/";

//the image with full path
$logo= $target_locate.basename($_FILES["logo"]["name"]);

//add this neww payment method
$add = mysqli_query($mysqli, "INSERT INTO payment_method(name, charge, wallet_address, logo,  status) VALUES('$name', '$charge', '$wallet_address', '$logo', 1)");



if($add and move_uploaded_file($_FILES["logo"]["tmp_name"],"../".$logo)  ){
//method added


?>
    <script>
    Swal.fire({
        title: 'Payment Method Added!',
        text: 'New Payment Method has been added Successfully.',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })


    setTimeout(() => {
        location = 'payment-method'
    }, 3000);
    </script>

    <?php


}


}
//end of adding





//start the updates
if(isset($_POST['update'])){

//retrive the inputs
$id =  mysqli_real_escape_string($mysqli,$_POST['id']);
$name =  mysqli_real_escape_string($mysqli,$_POST['name']);
$charge = mysqli_real_escape_string($mysqli,$_POST['charge']);
$wallet_address =  mysqli_real_escape_string($mysqli,$_POST['wallet_address']);
$min = mysqli_real_escape_string($mysqli,$_POST['min']);

$update = mysqli_query($mysqli, "UPDATE `payment_method` SET  `name`='$name', `charge`='$charge', `wallet_address`='$wallet_address'  WHERE `id`='$id' ");

if($update){

  ?>
    <script>
    Swal.fire({
        title: 'Payment Method Updated!',
        text: 'Payment Method has been Updated Successfully.',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })

    setTimeout(() => {
        location = 'payment-method'
    }, 3000);
    </script>

    <?php  

}



}
//end of updated





//enable the payment method
if(isset($_POST['enable-method'])){
// to Approve person account
$name = mysqli_real_escape_string($mysqli,$_POST['name']);
$id =  mysqli_real_escape_string($mysqli,$_POST['id']);

$enable = mysqli_query($mysqli,"UPDATE payment_method SET status=1 WHERE id='$id' ");


if($enable){

    ?>
    <script>
    Swal.fire({
        title: 'Method Enabled',
        text: 'Payment Method has been enabled successfully.',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })

    setTimeout(() => {
        location = location;
    }, 3000);
    </script>

    <?php


}



}
//end of disbale





//disable the payment method
if(isset($_POST['disable-method'])){
// to Approve person account
$name = mysqli_real_escape_string($mysqli,$_POST['name']);
$id =  mysqli_real_escape_string($mysqli,$_POST['id']);

$disbale = mysqli_query($mysqli,"UPDATE payment_method SET status=0 WHERE id='$id' ");

if($disbale){

    ?>
    <script>
    Swal.fire({
        title: 'Method Disabled',
        text: 'Payment Method has been disabled successfully.',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })

    setTimeout(() => {
        location = location;
    }, 3000);
    </script>

    <?php


}



}
//end of disbale




?>



</body>
<!-- END: Body-->

</html>