<?php
session_start();

include('../connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['adminid'])){
	
    header("location:index");
    
}


$get_admin = mysqli_query($mysqli,"SELECT * FROM admins WHERE id='".$_SESSION['adminid']."' ");
$rows = mysqli_fetch_assoc($get_admin);



?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
    <title>Video Testimony - Treasure Capital </title>
    <link rel="apple-touch-icon" href="../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="../app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/sweetalert2.min.css">
    <!-- END: Theme CSS-->
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

   <?php include('header.php'); ?>

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Video Testimony</h5>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="index"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">Video Testimony
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                       
                    </div>
                </div>
                <!-- Zero configuration table -->
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">All testimony</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                       
                                        <div class="table-responsive">
                                            <table class="table zero-configuration">
                                                <thead>
                                                    <tr>
                                                    <th>S/N</th>
                                                            
                                                            <th>Full name</th>
                                                            <th>Email</th>
                                                            <th>Bot Amount </th>
                                                            <th>Video Link</th>
                                                            
                                                            
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                            
                                                    </tr>
                                                </thead>
                                                <tbody>
                                               
                                                <?php
                                        //start the loop for see all users
                                        $get_funds = mysqli_query($mysqli,"SELECT * FROM video  WHERE status=0 ORDER BY id DESC");
                                            $i=0;
                                            while($row= mysqli_fetch_assoc($get_funds)){
                                                $i++;

                                                $in = mysqli_query($mysqli,"SELECT * FROM investment WHERE id='".$row['investmentid']."'");
                                                $in = mysqli_fetch_assoc($in);
                                                

                                                 $getuser= mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$row['userid']."'");
                                                $user = mysqli_fetch_assoc($getuser);
                                            ?>



                                            <tr>
                                                <td><?php echo $i; ?></td>

                                               
                                                <td><?php echo $user['firstname']." ".$user['secondname']." ".$user['lastname']; ?>
                                                </td>
                                                <td><?php echo $user['email']; ?></td>
                                                <td>$<?php echo $in['amount']; ?></td>

                                                <td><a class="btn btn-warning" href="<?php echo $row['videolink']; ?>" target="_blank">View Video</a></td>
                                                
                                                
                                                
                                                <td><?php if($row['status'] ==0){
                                                   echo "<span class='badge badge-danger' >Not Approved</span>"; 
                                                }else{
                                                     echo "<span class='badge badge-info' >Approved</span>";
                                                   
                                                } ?></td>

                                                <td>
                                                
                                                    <form method="POST">    
                                                        <input type="hidden" value="<?php echo $row['id']; ?>"
                                                            name="id" />
                                                            <input type="hidden" value="<?php echo $in['id']; ?>"
                                                            name="investid" />

                                                            <input type="hidden" value="<?php echo $row['userid']; ?>"
                                                            name="userid" />

                                                        <input type="hidden" value="<?php echo $in['amount']; ?>"
                                                            name="amount" />
                                                        
                                                        
                                                        <button type="submit" name="assign"
                                                            class="btn btn-info btn-block ">Assign Bonus</button>
                                                        
                                                    </form>
                                                    <br/>
                                                    <form method="POST">    
                                                        <input type="hidden" value="<?php echo $row['id']; ?>"
                                                            name="id" />
                                                            
                                                        
                                                        
                                                        <button type="submit" name="del-test"
                                                            class="btn btn-danger btn-block ">Delete Testimony</button>
                                                        
                                                    </form>
                                                
                                                </td>


                                                
                                            </tr>





                                            <?php

                                            }

                                             ?>




                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                    <th>S/N</th>
                                                           
                                                            <th>Full name</th>
                                                            <th>Email</th>
                                                            <th>Bot Amount </th>
                                                            <th>Video Link</th>
                                                           
                                                            
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                            
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Zero configuration table -->
               

              
            </div>
        </div>
    </div>
    <!-- END: Content-->



      <!-- BEGIN: Footer-->
  <footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-left d-inline-block">2021 &copy; </span></span>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
    </p>
</footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="../app-assets/vendors/js/vendors.min.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="../app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="../app-assets/js/scripts/configs/vertical-menu-light.js"></script>
    <script src="../app-assets/js/core/app-menu.js"></script>
    <script src="../app-assets/js/core/app.js"></script>
    <script src="../app-assets/js/scripts/components.js"></script>
    <script src="../app-assets/js/scripts/footer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="../app-assets/js/scripts/datatables/datatable.js"></script>
    <!-- END: Page JS-->
   


<?php
//to enable a user withdrawal
if(isset($_POST['assign'])){
    $amount = mysqli_real_escape_string($mysqli,$_POST['amount']);
    $id =  mysqli_real_escape_string($mysqli,$_POST['id']);
    $investid =  mysqli_real_escape_string($mysqli,$_POST['investid']);
    $userid =  mysqli_real_escape_string($mysqli,$_POST['userid']);
    $date= date("d")." ".date("F")." ".date("Y")." , ".date("h")." : ".date("i").date("a");

    $get = mysqli_query($mysqli,"SELECT * FROM users WHERE id='$userid'");
    $u = mysqli_fetch_assoc($get);

    
    $bonus = $amount*0.025;

    $new = $u['wallet']+$bonus;



    $up = mysqli_query($mysqli,"UPDATE users SET wallet='$new' WHERE id='$userid'");

    $upin = mysqli_query($mysqli,"UPDATE investment SET testimony=1 WHERE id='$investid'");

    $act ="Testimonial Bonus of 2.5%";
    $desc ="Testimonial Bonus of $".$bonus;
    //now update the referer acc by adding it to referals table
    //status is zero unles user claim it
    $updaterefer = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$act', '$desc', '$date','$bonus', 'Credited')");
    

    if($up){ 

        $up2 = mysqli_query($mysqli,"UPDATE video SET status=1 WHERE id='$id'");




$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.mailjet.com/v3.1/send",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>'{
    "SandboxMode": false,
    "Messages": [
        {
            "From": {
                "Email": "info@treasurecapital.net",
                "Name": "Treasure Capital"
            },
            
            
            "To": [
                {
                    "Email": "'.$u['email'].'",
                    "Name": ""
                }
            ],
            
            "Subject": "Video Bonus Added",
            "TextPart": "",
            "HTMLPart": "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"><html data-editor-version=\"2\" class=\"sg-campaigns\" xmlns=\"http://www.w3.org/1999/xhtml\"><head>\n      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1\">\n      <!--[if !mso]><!-->\n      <meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\">\n      <!--<![endif]-->\n      <!--[if (gte mso 9)|(IE)]>\n      <xml>\n        <o:OfficeDocumentSettings>\n          <o:AllowPNG/>\n          <o:PixelsPerInch>96</o:PixelsPerInch>\n        </o:OfficeDocumentSettings>\n      </xml>\n      <![endif]-->\n      <!--[if (gte mso 9)|(IE)]>\n  <style type=\"text/css\">\n    body {width: 600px;margin: 0 auto;}\n    table {border-collapse: collapse;}\n    table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}\n    img {-ms-interpolation-mode: bicubic;}\n  </style>\n<![endif]-->\n      <style type=\"text/css\">\n    body, p, div {\n      font-family: inherit;\n      font-size: 14px;\n    }\n    body {\n      color: #000000;\n    }\n    body a {\n      color: #1188E6;\n      text-decoration: none;\n    }\n    p { margin: 0; padding: 0; }\n    table.wrapper {\n      width:100% !important;\n      table-layout: fixed;\n      -webkit-font-smoothing: antialiased;\n      -webkit-text-size-adjust: 100%;\n      -moz-text-size-adjust: 100%;\n      -ms-text-size-adjust: 100%;\n    }\n    img.max-width {\n      max-width: 100% !important;\n    }\n    .column.of-2 {\n      width: 50%;\n    }\n    .column.of-3 {\n      width: 33.333%;\n    }\n    .column.of-4 {\n      width: 25%;\n    }\n    @media screen and (max-width:480px) {\n      .preheader .rightColumnContent,\n      .footer .rightColumnContent {\n        text-align: left !important;\n      }\n      .preheader .rightColumnContent div,\n      .preheader .rightColumnContent span,\n      .footer .rightColumnContent div,\n      .footer .rightColumnContent span {\n        text-align: left !important;\n      }\n      .preheader .rightColumnContent,\n      .preheader .leftColumnContent {\n        font-size: 80% !important;\n        padding: 5px 0;\n      }\n      table.wrapper-mobile {\n        width: 100% !important;\n        table-layout: fixed;\n      }\n      img.max-width {\n        height: auto !important;\n        max-width: 100% !important;\n      }\n      a.bulletproof-button {\n        display: block !important;\n        width: auto !important;\n        font-size: 80%;\n        padding-left: 0 !important;\n        padding-right: 0 !important;\n      }\n      .columns {\n        width: 100% !important;\n      }\n      .column {\n        display: block !important;\n        width: 100% !important;\n        padding-left: 0 !important;\n        padding-right: 0 !important;\n        margin-left: 0 !important;\n        margin-right: 0 !important;\n      }\n    }\n  </style>\n      <!--user entered Head Start--><link href=\"https://fonts.googleapis.com/css?family=Chivo&display=swap\" rel=\"stylesheet\"><style>\nbody {font-family: \'Chivo\', sans-serif;}\n</style><!--End Head user entered-->\n    </head>\n    <body>\n      <center class=\"wrapper\" data-link-color=\"#1188E6\" data-body-style=\"font-size:14px; font-family:inherit; color:#000000; background-color:#FFFFFF;\">\n        <div class=\"webkit\">\n          <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" class=\"wrapper\" bgcolor=\"#FFFFFF\">\n            <tbody><tr>\n              <td valign=\"top\" bgcolor=\"#FFFFFF\" width=\"100%\">\n                <table width=\"100%\" role=\"content-container\" class=\"outer\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n                  <tbody><tr>\n                    <td width=\"100%\">\n                      <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n                        <tbody><tr>\n                          <td>\n                            <!--[if mso]>\n    <center>\n    <table><tr><td width=\"600\">\n  <![endif]-->\n                                    <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:100%; max-width:600px;\" align=\"center\">\n                                      <tbody><tr>\n                                        <td role=\"modules-container\" style=\"padding:0px 0px 0px 0px; color:#000000; text-align:left;\" bgcolor=\"#FFFFFF\" width=\"100%\" align=\"left\"><table class=\"module preheader preheader-hide\" role=\"module\" data-type=\"preheader\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;\">\n    <tbody><tr>\n      <td role=\"module-content\">\n        <p></p>\n      </td>\n    </tr>\n  </tbody></table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"100%\" role=\"module\" data-type=\"columns\" style=\"padding:30px 20px 30px 30px;\" bgcolor=\"#f7f9f5\">\n    <tbody>\n      <tr role=\"module-content\">\n        <td height=\"100%\" valign=\"top\">\n          <table class=\"column\" width=\"265\" style=\"width:265px; border-spacing:0; border-collapse:collapse; margin:0px 10px 0px 0px;\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\" border=\"0\" bgcolor=\"\">\n            <tbody>\n              <tr>\n                <td style=\"padding:0px;margin:0px;border-spacing:0;\"><table class=\"wrapper\" role=\"module\" data-type=\"image\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"9bb94d6b-a9d9-4d66-b867-f52241c7c7c5\">\n    <tbody>\n      <tr>\n        <td style=\"font-size:6px; line-height:10px; padding:0px 0px 0px 0px;\" valign=\"top\" align=\"left\">\n          <img class=\"max-width\" border=\"0\" style=\"display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px;\" width=\"117\" alt=\"\" data-proportionally-constrained=\"true\" data-responsive=\"false\" src=\"https://treasurecapital.net/img/logo-5.png\" height=\"26\">\n        </td>\n      </tr>\n    </tbody>\n  </table></td>\n              </tr>\n            </tbody>\n          </table>\n          <table class=\"column\" width=\"265\" style=\"width:265px; border-spacing:0; border-collapse:collapse; margin:0px 0px 0px 10px;\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\" border=\"0\" bgcolor=\"\">\n            <tbody>\n              <tr>\n                <td style=\"padding:0px;margin:0px;border-spacing:0;\"><table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"b0d9194c-17b6-4db0-8250-0a56573ea469\" data-mc-module-version=\"2019-10-22\">\n    <tbody>\n      <tr>\n        <td style=\"padding:0px 0px 0px 0px; line-height:22px; text-align:inherit;\" height=\"100%\" valign=\"top\" bgcolor=\"\" role=\"module-content\"><div><div style=\"font-family: inherit; text-align: inherit\"><span style=\"font-size: 10px\">Treasure Capital</span></div><div></div></div></td>\n      </tr>\n    </tbody>\n  </table></td>\n              </tr>\n            </tbody>\n          </table>\n        </td>\n      </tr>\n    </tbody>\n  </table><table class=\"module\" role=\"module\" data-type=\"divider\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"2d3862ba-e4da-4865-8605-53fb723d695b\">\n    <tbody>\n      <tr>\n        <td style=\"padding:0px 0px 0px 0px;\" role=\"module-content\" height=\"100%\" valign=\"top\" bgcolor=\"\">\n          <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"100%\" height=\"1px\" style=\"line-height:1px; font-size:1px;\">\n            <tbody>\n              <tr>\n                <td style=\"padding:0px 0px 1px 0px;\" bgcolor=\"#e2efd4\"></td>\n              </tr>\n            </tbody>\n          </table>\n        </td>\n      </tr>\n    </tbody>\n  </table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"100%\" role=\"module\" data-type=\"columns\" style=\"padding:30px 20px 40px 30px;\" bgcolor=\"#f7f9f5\">\n    <tbody>\n      <tr role=\"module-content\">\n        <td height=\"100%\" valign=\"top\">\n          <table class=\"column\" width=\"530\" style=\"width:530px; border-spacing:0; border-collapse:collapse; margin:0px 10px 0px 10px;\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\" border=\"0\" bgcolor=\"\">\n            <tbody>\n              <tr>\n                <td style=\"padding:0px;margin:0px;border-spacing:0;\"><table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"6cf92250-86a4-41d8-865a-ae5fb2569556\" data-mc-module-version=\"2019-10-22\">\n    <tbody>\n      <tr>\n        <td style=\"padding:18px 0px 18px 0px; line-height:22px; text-align:inherit;\" height=\"100%\" valign=\"top\" bgcolor=\"\" role=\"module-content\"><div><div style=\"font-family: inherit; text-align: center\">Change Password</div><div></div></div></td>\n      </tr>\n    </tbody>\n  </table><table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"8c54c8a5-caee-4b33-b6b0-e8aaed51c545\" data-mc-module-version=\"2019-10-22\">\n    <tbody>\n      <tr>\n        <td style=\"padding:18px 10px 18px 10px; line-height:32px; text-align:inherit;\" height=\"100%\" valign=\"top\" bgcolor=\"\" role=\"module-content\"><div><div style=\"font-family: inherit; text-align: center\"><span style=\"color: #79a6ff; font-size: 50px\">Video Testimony Bonus !</span></div><div></div></div></td>\n      </tr>\n    </tbody>\n  </table><table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"6cf92250-86a4-41d8-865a-ae5fb2569556.1\" data-mc-module-version=\"2019-10-22\">\n    <tbody>\n      <tr>\n        <td style=\"padding:0px 0px 18px 0px; line-height:22px; text-align:inherit;\" height=\"100%\" valign=\"top\" bgcolor=\"\" role=\"module-content\"><div><div style=\"font-family: inherit; text-align: center\">A bonus of $'.$bonus.' has been added to your wallet. </div>\n\n<div></div></div></td>\n      </tr>\n    </tbody>\n  </table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"module\" data-role=\"module-button\" data-type=\"button\" role=\"module\" style=\"table-layout:fixed;\" width=\"100%\" data-muid=\"4bcb53df-57db-48a5-9aa3-4060ac494a64\">\n      <tbody>\n        <tr>\n          <td align=\"center\" bgcolor=\"\" class=\"outer-td\" style=\"padding:0px 0px 0px 0px;\">\n            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"wrapper-mobile\" style=\"text-align:center;\">\n              <tbody>\n                <tr>\n                <td align=\"center\" bgcolor=\"#333333\" class=\"inner-td\" style=\"border-radius:6px; font-size:16px; text-align:center; background-color:inherit;\">\n                  <a href=\"https://treasurecapital.net/\" style=\"background-color:#333333; border:1px solid #333333; border-color:#333333; border-radius:0px; border-width:1px; color:#ffffff; display:inline-block; font-size:14px; font-weight:normal; letter-spacing:0px; line-height:normal; padding:12px 30px 12px 30px; text-align:center; text-decoration:none; border-style:solid;\" target=\"_blank\">Reset Password</a>\n                </td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n        </tr>\n      </tbody>\n    </table><table class=\"module\" role=\"module\" data-type=\"spacer\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"2edb1546-1598-49eb-995c-baf7d429f31c\">\n    <tbody>\n      <tr>\n        <td style=\"padding:0px 0px 30px 0px;\" role=\"module-content\" bgcolor=\"\">\n        </td>\n      </tr>\n    </tbody>\n  </table><table class=\"wrapper\" role=\"module\" data-type=\"image\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"3923e4a3-acc3-4c88-bb58-8e5cd32f0162\">\n    <tbody>\n      <tr>\n        <td style=\"font-size:6px; line-height:10px; padding:0px 0px 0px 0px;\" valign=\"top\" align=\"center\">\n         \n        </td>\n      </tr>\n    </tbody>\n  </table><table class=\"module\" role=\"module\" data-type=\"spacer\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"2edb1546-1598-49eb-995c-baf7d429f31c.1\">\n    <tbody>\n      <tr>\n        <td style=\"padding:0px 0px 30px 0px;\" role=\"module-content\" bgcolor=\"\">\n        </td>\n      </tr>\n    </tbody>\n  </table><table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"6cf92250-86a4-41d8-865a-ae5fb2569556.1.1\" data-mc-module-version=\"2019-10-22\">\n    <tbody>\n      <tr>\n        \n      </tr>\n    </tbody>\n  </table>\n </td>\n              </tr>\n            </tbody>\n          </table>\n          \n        </td>\n      </tr>\n    </tbody>\n  </table>\n<table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"ba2cc448-1854-4e31-b50f-551e3101d5b2\" data-mc-module-version=\"2019-10-22\">\n    <tbody>\n      <tr>\n        <td style=\"padding:40px 30px 40px 30px; line-height:22px; text-align:inherit; background-color:#79a6ff;\" height=\"100%\" valign=\"top\" bgcolor=\"#79a6ff\" role=\"module-content\"><div><div style=\"font-family: inherit; text-align: center\"><span style=\"color: #ffffff\">Need help choosing a plan? Questions about accounts?</span></div>\n<div style=\"font-family: inherit; text-align: center\"><span style=\"color: #ffffff\">We \'re here to help. Our customer services reps are available 24/7.</span></div>\n<div style=\"font-family: inherit; text-align: center\"><br></div>\n<div style=\"font-family: inherit; text-align: center\"><a href=\"https://treasurecapital.net\"><span style=\"color: #ffffff\"><u><strong>Contact Us&nbsp;</strong></u></span></a></div><div></div></div></td>\n      </tr>\n    </tbody>\n  </table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"module\" data-role=\"module-button\" data-type=\"button\" role=\"module\" style=\"table-layout:fixed;\" width=\"100%\" data-muid=\"d93b6db5-0e1f-4374-99f9-62212c9cf0f1\">\n      <tbody>\n        <tr>\n          <td align=\"center\" bgcolor=\"\" class=\"outer-td\" style=\"padding:0px 0px 20px 0px;\">\n            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"wrapper-mobile\" style=\"text-align:center;\">\n              <tbody>\n                <tr>\n                <td align=\"center\" bgcolor=\"#f5f8fd\" class=\"inner-td\" style=\"border-radius:6px; font-size:16px; text-align:center; background-color:inherit;\"></td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n        </tr>\n      </tbody>\n    </table></td>\n                                      </tr>\n                                    </tbody></table>\n                                    <!--[if mso]>\n                                  </td>\n                                </tr>\n                              </table>\n                            </center>\n                            <![endif]-->\n                          </td>\n                        </tr>\n                      </tbody></table>\n                    </td>\n                  </tr>\n                </tbody></table>\n              </td>\n            </tr>\n          </tbody></table>\n        </div>\n      </center>\n    \n  \n</body></html>",
           
            "TemplateLanguage": true,
          
            "TrackOpens": "account_default",
            "TrackClicks": "account_default"
            
        }
    ]
}',
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json",
    "Authorization: Basic YjcwM2IzMjA4MGMyNDhlMTVhOGNiZGU1ZWU4ZDZmOGE6YjMxNjAwMGVlNzk3MmQ4NjliZGUwOTgxMmE2MjMxODQ="
  ),
));

$response = curl_exec($curl);

curl_close($curl);





    
      ?>
    <script>
    
    
    Swal.fire({
            title: 'Video Approved ',
            text: 'Bonus has been Assigned ',
            type: 'success',
            showCancelButton: true,
            confirmButtonColor: '#626ed4',
            cancelButtonColor: "#ec4561"
        })
    
    setTimeout(() => {
        location = location;
    }, 3000);
    </script>
    
    <?php
    
    }
    //successful
    
    }

?>


<?php
if(isset($_POST['del-test'])){
//start of approval

$id = $_POST['id'];

$delete = mysqli_query($mysqli,"DELETE FROM `video` WHERE `id`='$id' ");

if($delete){

	    ?>
    <script>
    Swal.fire({
        title: 'Delete Successful',
        text: 'Testimony has been deleted ',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })


    setTimeout(() => {
        location = location;
    }, 3000);
    </script>

    <?php

}



}
?>

</body>
<!-- END: Body-->

</html>