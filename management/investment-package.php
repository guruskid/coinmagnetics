<?php
session_start();

include('../connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['adminid'])){
	
    header("location:index");
    
}


$get_admin = mysqli_query($mysqli,"SELECT * FROM admins WHERE id='".$_SESSION['adminid']."' ");
$rows = mysqli_fetch_assoc($get_admin);

$getinvestment = mysqli_query($mysqli, "SELECT * FROM investment_packages");

?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
    <title>Investment portfolio - Argen Capital</title>
    <link rel="apple-touch-icon" href="../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="../app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/charts/apexcharts.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/dragula.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/daterange/daterangepicker.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/sweetalert2.min.css">
    <!-- END: Theme CSS-->
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/pages/widgets.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

   <?php include('header.php'); ?>

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Investment Portfolio</h5>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="dashboard"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">Select Package
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
              

                <!-- Widgets Advance start -->
                <section id="widgets-advance">
                    <div class="row">
                        <div class="col-12 mt-1 mb-2">
                        <div class="card">
                                <div class="card-body">
                                    <h4 class="mt-0 header-title">Add New Package</h4>



                                    <form method="POST">

                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Package Name</label>
                                            <div class="col-sm-10"><input class="form-control" type="text" name="name"
                                                    id="exampleInputName" autofocus placeholder="Enter Package Name"
                                                    value="" required></div>
                                        </div>

                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Minimum Investment Amount</label>
                                            <div class="col-sm-10"><input class="form-control" type="number"
                                                    name="min_amount" id="exampleInputName" placeholder="Enter Amount"
                                                    required></div>
                                        </div>

                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Maximum Investment Amount</label>
                                            <div class="col-sm-10"><input class="form-control" type="number"
                                                    name="max_amount" id="exampleInputName" placeholder="Enter Amount">
                                            </div>
                                        </div>


                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Percentage</label>
                                            <div class="col-sm-10"><input class="form-control" type="text"
                                                    name="percent" id="exampleInputName" placeholder="Enter Percent"
                                                    required></div>
                                        </div>

                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Referal Percentage</label>
                                            <div class="col-sm-10"><input class="form-control" type="text"
                                                    name="referpercent" id="exampleInputName" placeholder="Enter Referal Percent"
                                                    required></div>
                                        </div>

                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Duration</label>
                                            <div class="col-sm-10"><input class="form-control" type="text"
                                                    name="duration" id="exampleInputName" placeholder="Enter duration">
                                            </div>
                                        </div>


                                        <div class="form-group row"><label
                                                class="col-sm-2 col-form-label">Compounding</label>
                                            <div class="col-sm-10"><select name="compounding" id="compounding"
                                                    class="form-control">
                                                    <option value="" disabled selected>Select Type</option>
                                                    <option value="5 Months">5 Months</option>
                                                    <option value="4 Months">4 Months</option>
                                                    <option value="6 Months">6 Months</option>

                                                </select></div>
                                        </div>

                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Compounding Percent</label>
                                            <div class="col-sm-10"><input class="form-control"  type="text" name="compound_percent" id="exampleInputName" placeholder="Enter Percent" required></div>
                                        </div>


                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Heading 1</label>
                                            <div class="col-sm-10"><input class="form-control"  type="text" name="infohead1" id="exampleInputName"  placeholder="Enter Heading"></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Details 1</label>
                                            <div class="col-sm-10"><input class="form-control" type="text" name="info1" id="exampleInputName" placeholder="Enter info" value=""></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Heading 2</label>
                                            <div class="col-sm-10"><input class="form-control"  type="text" name="infohead2" id="exampleInputName"  placeholder="Enter Heading"></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Details 2</label>
                                            <div class="col-sm-10"><input class="form-control" type="text" name="info2" id="exampleInputName" placeholder="Enter info" value=""></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Heading 3</label>
                                            <div class="col-sm-10"><input class="form-control"  type="text" name="infohead3" id="exampleInputName"  placeholder="Enter Heading"></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Details 3</label>
                                            <div class="col-sm-10"><input class="form-control" type="text" name="info3" id="exampleInputName" placeholder="Enter info" value=""></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Heading 4</label>
                                            <div class="col-sm-10"><input class="form-control"  type="text" name="infohead4" id="exampleInputName"  placeholder="Enter Heading"></div>
                                        </div>

                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Details 4</label>
                                            <div class="col-sm-10"><input class="form-control" type="text" name="info4" id="exampleInputName" placeholder="Enter info" value=""></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Heading 5</label>
                                            <div class="col-sm-10"><input class="form-control"  type="text" name="infohead4" id="exampleInputName"  placeholder="Enter Heading"></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Details 5</label>
                                            <div class="col-sm-10"><input class="form-control" type="text" name="info4" id="exampleInputName" placeholder="Enter info" value=""></div>
                                        </div>






                                        <div><button type="submit" name="create"
                                                class="btn btn-primary waves-effect waves-light mr-1">Create
                                                Package</button> </div>
                                    </form>
                                </div>
                            </div>
                            
                            <hr>
                        </div>
                    </div>




                    <?php

                                
                                  

while($data = mysqli_fetch_assoc($getinvestment)){

 ?>
                    <div class="row" id="edit-<?php echo $data['id']; ?>" style="display:none">
                        <div class="col-12 mt-1 mb-2">
                        <div class="card">
                                <div class="card-body">
                                    <h4 class="mt-0 header-title">Edit <?php echo $data['name']; ?></h4>



                                    <form method="POST">
                                         <input type="hidden" name="id" value="<?php echo $data['id']; ?>" />

                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Package Name</label>
                                            <div class="col-sm-10"><input class="form-control" type="text" name="name"
                                                    id="exampleInputName" autofocus placeholder="Enter Package Name"
                                                    value="<?php echo $data['name']; ?>" required></div>
                                        </div>

                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Minimum Investment Amount</label>
                                            <div class="col-sm-10"><input class="form-control" type="number"
                                                    name="min_amount" value="<?php echo $data['min_amount']; ?>" id="exampleInputName" placeholder="Enter Amount"
                                                    required></div>
                                        </div>

                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Maximum Investment Amount</label>
                                            <div class="col-sm-10"><input class="form-control" type="number"
                                                    name="max_amount" value="<?php echo $data['max_amount']; ?>" id="exampleInputName" placeholder="Enter Amount">
                                            </div>
                                        </div>


                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Percentage</label>
                                            <div class="col-sm-10"><input class="form-control" type="text"
                                                    name="percent" value="<?php echo $data['percent']; ?>" id="exampleInputName" placeholder="Enter Percent"
                                                    required></div>
                                        </div>

                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Referal Percentage</label>
                                            <div class="col-sm-10"><input class="form-control" type="text"
                                                    name="referpercent" id="exampleInputName" placeholder="Enter Referal Percent" value="<?php echo $data['referpercent']; ?>" 
                                                    required></div>
                                        </div>


                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Duration</label>
                                            <div class="col-sm-10"><input class="form-control" type="text"
                                                    name="duration" value="<?php echo $data['duration']; ?>" id="exampleInputName" placeholder="Enter duration">
                                            </div>
                                        </div>


                                        <div class="form-group row"><label
                                                class="col-sm-2 col-form-label">Compounding</label>
                                            <div class="col-sm-10"><select name="compounding" id="compounding"
                                                    class="form-control">
                                                    <option value="" disabled selected>Select Type</option>
                                                        <option value="5 Months"  <?php if($data['compounding']=='5 Months'){ echo 'selected';} ?>  >5 Months</option>
                                                        <option value="4 Months" <?php if($data['compounding']=='4 Months'){ echo 'selected';} ?> >4 Months</option>
                                                        <option value="6 Months" <?php if($data['compounding']=='6 Months'){ echo 'selected';} ?> >6 Months</option>

                                                </select></div>
                                        </div>

                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Compounding Percent</label>
                                            <div class="col-sm-10"><input class="form-control"  type="text" name="compound_percent" value="<?php echo $data['compound_percent']; ?>" id="exampleInputName" placeholder="Enter Percent" required></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Heading 1</label>
                                            <div class="col-sm-10"><input class="form-control"  type="text" name="infohead1" id="exampleInputName" value="<?php echo $data['infohead1']; ?>" placeholder="Enter Heading"></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Details 1</label>
                                            <div class="col-sm-10"><input class="form-control" type="text" name="info1" id="exampleInputName" placeholder="Enter info" value="<?php echo $data['infohead2']; ?>"></div>
                                        </div>

                                        <div class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Heading 2</label>
                                            <div class="col-sm-10"><input class="form-control"  type="text" name="infohead2" id="exampleInputName" value="<?php echo $data['infohead2']; ?>" placeholder="Enter Heading"></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Details 2</label>
                                            <div class="col-sm-10"><input class="form-control" type="text" name="info2" id="exampleInputName" placeholder="Enter info" value="<?php echo $data['info2']; ?>"></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Heading 3</label>
                                            <div class="col-sm-10"><input class="form-control"  type="text" name="infohead3" id="exampleInputName" value="<?php echo $data['infohead3']; ?>" placeholder="Enter Heading"></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Details 3</label>
                                            <div class="col-sm-10"><input class="form-control" type="text" name="info3" id="exampleInputName" placeholder="Enter info" value="<?php echo $data['info3']; ?>"></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Heading 4</label>
                                            <div class="col-sm-10"><input class="form-control"  type="text" name="infohead4" id="exampleInputName" value="<?php echo $data['infohead4']; ?>" placeholder="Enter Heading"></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Details 4</label>
                                            <div class="col-sm-10"><input class="form-control" type="text" name="info4" id="exampleInputName" placeholder="Enter info" value="<?php echo $data['info4']; ?>"></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Heading 5</label>
                                            <div class="col-sm-10"><input class="form-control"  type="text" name="infohead5" id="exampleInputName" value="<?php echo $data['infohead5']; ?>" placeholder="Enter Heading"></div>
                                        </div>

                                        <div  class="form-group row"><label for="example-text-input"
                                                class="col-sm-2 col-form-label">Info Details 5</label>
                                            <div class="col-sm-10"><input class="form-control" type="text" name="info5" id="exampleInputName" placeholder="Enter info" value="<?php echo $data['info5']; ?>" ></div>
                                        </div>







                                        <div><button type="submit" name="edit"
                                                class="btn btn-primary waves-effect waves-light mr-1">Edit Package</button> </div>
                                    </form>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>

                    <?php
                    }
                    ?>




                   
                    <div class="row">
                
                    <?php

                                
$getinvest = mysqli_query($mysqli, "SELECT * FROM investment_packages");

while($row = mysqli_fetch_assoc($getinvest)){

?>

                        <!-- User Widget with Overlay Image Starts -->
                        <div class="col-xl-4 col-md-6 overlay-image-card">
                            <div class="card widget-overlay">
                                <div class="card widget-overlay-card mb-0">
                                    <div class="card-content">
                                        <div class="card-body p-0">
                                            <img class="card-img img-fluid" src="../app-assets/images/cards/finance.jpg" alt="Card image">
                                            <div class="card-img-overlay overlay-primary">
                                                <div class="d-flex justify-content-between">
                                                    <span class="card-title text-white">Investment</span>
                                                    <div class="dropdown">
                                                        <button class="btn btn-outline-white " type="button" id="dropdownMenuButtonSec" >
                                                            2020
                                                        </button>
                                                       
                                                    </div>
                                                </div>
                                                <h1 class="font-large-2 text-center widget-overlay-card-title">$<?php echo $row['min_amount']; ?> </h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card widget-overlay-content mb-0">
                                    <div class="card-content text-center">
                                        <button class="btn btn-lg btn-white shadow inclusive-btn"  type="button"><?php echo $row['name']; ?></button>
                                    </div>
                                    <div class="card-body px-0 pb-0">
                                        <ul class="nav nav-tabs justify-content-center" role="tablist">
                                            <li class="nav-item mr-1" style="padding-left:10px">
                                            <p><i class="bx bx-check text-primary mr-2"></i> <?php echo $row['duration']; ?> Months - Investment Duration</p>
                                        
                                            </li>
                                           
                                        </ul>
                                        <div class="tab-content pl-0">
                                            
                                           
                                          
                                          
                                        </div>
                                        <ul class="nav nav-tabs justify-content-center" role="tablist">
                                            <li class="nav-item mr-1">
                                                <a class=" nav-link active" id="january-tab" href="javascript:;" onclick="document.getElementById('edit-<?php echo $row['id']; ?>').style='display:block';">Edit</a>
                                                    <br/>
                                                    <a class=" nav-link active" id="january-tab"  href="javascript:;" onclick="document.getElementById('delete-<?php echo $row['id']?>').submit()">Delete</a>
                                            </li>
                                           
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- User Widget with Overlay Image Ends -->
                        <form method="POST" id="delete-<?php echo $row['id']; ?>">

<input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
<input type="hidden" name="delete" />

</form>







                       


                        <?php
                    

                            }
                        ?>





                       
                    </div>
                </section>
                <!-- Widgets Advance End -->

              
            </div>
        </div>
    </div>
    <!-- END: Content-->


      <!-- BEGIN: Footer-->
      <footer class="footer footer-static footer-light">
        <p class="clearfix mb-0"><span class="float-left d-inline-block">2020 &copy; </span></span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
        </p>
    </footer>
        <!-- END: Footer-->

    <!-- BEGIN: Vendor JS-->
    <script src="../app-assets/vendors/js/vendors.min.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="../app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="../app-assets/vendors/js/pickers/daterange/moment.min.js"></script>
    <script src="../app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
    <script src="../app-assets/vendors/js/charts/apexcharts.min.js"></script>
    <script src="../app-assets/vendors/js/extensions/dragula.min.js"></script>
    <script src="../app-assets/vendors/js/extensions/swiper.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="../app-assets/js/scripts/configs/vertical-menu-light.js"></script>
    <script src="../app-assets/js/core/app-menu.js"></script>
    <script src="../app-assets/js/core/app.js"></script>
    <script src="../app-assets/js/scripts/components.js"></script>
    <script src="../app-assets/js/scripts/footer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="../app-assets/js/scripts/cards/widgets.js"></script>
    <!-- END: Page JS-->
    <?php
//when click to creat new package
if(isset($_POST['create'])){
//retrive inputs


$name =  mysqli_real_escape_string($mysqli,$_POST['name']);
$min_amount =  mysqli_real_escape_string($mysqli,$_POST['min_amount']);
$max_amount =  mysqli_real_escape_string($mysqli,$_POST['max_amount']);
$referpercent = mysqli_real_escape_string($mysqli,$_POST['referpercent']);
$percent = mysqli_real_escape_string($mysqli,$_POST['percent']);
$duration = mysqli_real_escape_string($mysqli,$_POST['duration']);
$compounding = mysqli_real_escape_string($mysqli,$_POST['compounding']);
$compound_percent = mysqli_real_escape_string($mysqli,$_POST['compound_percent']);
$infohead1 = mysqli_real_escape_string($mysqli,$_POST['infohead1']);
$info1 = mysqli_real_escape_string($mysqli,$_POST['info1']);
$infohead2 = mysqli_real_escape_string($mysqli,$_POST['infohead2']);
$info2 = mysqli_real_escape_string($mysqli,$_POST['info2']);
$infohead3 = mysqli_real_escape_string($mysqli,$_POST['infohead3']);
$info3 = mysqli_real_escape_string($mysqli,$_POST['info3']);
$infohead4 = mysqli_real_escape_string($mysqli,$_POST['infohead4']);
$info4 = mysqli_real_escape_string($mysqli,$_POST['info4']);
$infohead5 = mysqli_real_escape_string($mysqli,$_POST['infohead5']);
$info5 = mysqli_real_escape_string($mysqli,$_POST['info5']);



//add to database
$create = mysqli_query($mysqli,"INSERT INTO investment_packages (name, min_amount, max_amount, referpercent, percent, duration, compounding, compound_percent, infohead1, info1, infohead2, info2, infohead3, info3, infohead4, info4, infohead5, info5) VALUES('$name', '$min_amount', '$max_amount', '$referpercent', '$percent', '$duration', '$compounding', '$compound_percent', '$infohead1', '$info1', '$infohead2', '$info2', '$infohead3', '$info3', '$infohead4', '$info4', '$infohead5', '$info5' )");

if($create){

    
?>
<script>


  Swal.fire({
        title: 'Package Created Successfully',
        text: 'New Package <?php echo $name; ?> has been created.',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })

setTimeout(() => {
    location='investment-package';
}, 3000);
</script>

<?php

}




}
//end of creating a new package


//start of delete
if(isset($_POST['delete'])){

    $id =  mysqli_real_escape_string($mysqli,$_POST['id']);

    $del = mysqli_query($mysqli,"DELETE FROM `investment_packages` WHERE id='$id'");

if($del){

?>
<script>

 Swal.fire({
        title: 'Package Delected Successfully',
        text: 'A package  has been Deleted.',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })


setTimeout(() => {
    location='investment-package';
}, 3000);
</script>

<?php

}


}
//end of delete



//start of editing
if(isset($_POST['edit'])){

$id =  mysqli_real_escape_string($mysqli,$_POST['id']);
$name =  mysqli_real_escape_string($mysqli,$_POST['name']);
$min_amount =  mysqli_real_escape_string($mysqli,$_POST['min_amount']);
$max_amount =  mysqli_real_escape_string($mysqli,$_POST['max_amount']);
$referpercent = mysqli_real_escape_string($mysqli,$_POST['referpercent']);
$percent = mysqli_real_escape_string($mysqli,$_POST['percent']);
$duration = mysqli_real_escape_string($mysqli,$_POST['duration']);
$compounding = mysqli_real_escape_string($mysqli,$_POST['compounding']);
$compound_percent = mysqli_real_escape_string($mysqli,$_POST['compound_percent']);
$infohead1 = mysqli_real_escape_string($mysqli,$_POST['infohead1']);
$info1 = mysqli_real_escape_string($mysqli,$_POST['info1']);
$infohead2 = mysqli_real_escape_string($mysqli,$_POST['infohead2']);
$info2 = mysqli_real_escape_string($mysqli,$_POST['info2']);
$infohead3 = mysqli_real_escape_string($mysqli,$_POST['infohead3']);
$info3 = mysqli_real_escape_string($mysqli,$_POST['info3']);
$infohead4 = mysqli_real_escape_string($mysqli,$_POST['infohead4']);
$info4 = mysqli_real_escape_string($mysqli,$_POST['info4']);
$infohead5 = mysqli_real_escape_string($mysqli,$_POST['infohead5']);
$info5 = mysqli_real_escape_string($mysqli,$_POST['info5']);


//add to database
$update = mysqli_query($mysqli,"UPDATE `investment_packages`  SET name='$name', min_amount='$min_amount', max_amount='$max_amount', referpercent='$referpercent', percent='$percent', duration='$duration', compounding='$compounding', compound_percent='$compound_percent', infohead1='$infohead1', info1='$info1', infohead2='$infohead2', info2='$info2' , infohead3='$infohead3', info3='$info3' , infohead4='$infohead4', info4='$info4' , infohead5='$infohead5', info5='$info5'   WHERE id='$id'  ");

if($update){

?>
<script>


  Swal.fire({
        title: 'Package Updated Successfully',
        text: '<?php echo $name; ?>  has been Updated. ',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })



setTimeout(() => {
    location='investment-package';
}, 3000);
</script>

<?php

}



}

//end of editnig



?>



</body>
<!-- END: Body-->

</html>