<?php
session_start();

include('../connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['adminid'])){
	
    header("location:index");
    
}


$get_admin = mysqli_query($mysqli,"SELECT * FROM admins WHERE id='".$_SESSION['adminid']."' ");
$rows = mysqli_fetch_assoc($get_admin);



?>
<?php
//to login into a user account
if(isset($_POST['user-login'])){

    $id = $_POST['id'];

    $_SESSION['id'] = $id;

    ?>
<script>
window.open('../dashboard', '_blank');
</script>

<?php

}


$curl = curl_init();
                      
curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.paystack.co/bank",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
 
  ),
));

$response = curl_exec($curl);

curl_close($curl);

$data = json_decode($response,true);


?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
    <title>Registered users - Treasure Capital </title>
    <link rel="apple-touch-icon" href="../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="../app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/sweetalert2.min.css">
    <!-- END: Theme CSS-->
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

   <?php include('header.php'); ?>

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Users</h5>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="index"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">Users
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                       
                    </div>
                </div>
                <!-- Zero configuration table -->
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Registered users</h4>

                                    <a class='btn btn-primary ' style="float:right"
                                                                href='#treasurelimit'
                                                                data-toggle='modal'
                                                                data-target='#treasurelimit'>Update
                                                                withdrawal limit For All</a>
                                </div>
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                       
                                        <div class="table-responsive">
                                            <table class="table zero-configuration">
                                                <thead>
                                                    <tr>
                                                    <th>S/N</th>
                                                    <th>Fullname</th>
                                                    <th>Email</th>
                                                    <th>Phone Number</th>
                                                    <th>Wallet</th>
                                                    <th>Referred By</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                    <th>Access User</th>
                                                    <th>Block Withdrawal</th>

                                                    <th>More Actions</th>
                                                    <th>More Actions</th>
                                               
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php

                                                //preparation for pagination feature for the posts. 
//attempt to know the num of rows
$query=mysqli_query($mysqli,"SELECT * FROM users");

$numrows = mysqli_num_rows($query);
//how many posts per page? for now 3
$rowsperpage = 150;

//from the posts fecthed from Database how many pages will i have?
$totalpages = ceil($numrows / $rowsperpage);

//check the link(url) and get the current page or set default value

if(isset($_GET['currentpage']) && is_numeric($_GET['currentpage'])){
	//make the varable int, capture the currentpage frm url and store 
	$currentpage = (int) $_GET['currentpage'];
}else{
	//if not set default is 1
	$currentpage = 1;
}

//since am rounding up  the total page with ceil
//if the currentpage is > than the totalpages add total page to currentpage
if($currentpage > $totalpages){
	
	$currentpage = $totalpages;
}

//if the current is less that 1
if($currentpage < 1){
	//make it 1 by force
	$currentpage = 1;
}

//from currentpage i want to know how many post have been shown to the user
// then from there pick the post after that number.
$offset = ($currentpage - 1) * $rowsperpage;

//fectching from the database the





                                        //start the loop for see all users
                                        $get_users = mysqli_query($mysqli,"SELECT * FROM users LIMIT $offset, $rowsperpage");


                                            $j=0;
                                            while($row= mysqli_fetch_assoc($get_users)){
                                                $j++;

                                                $getrefer = mysqli_query($mysqli,"SELECT * FROM users WHERE referal_link='".$row['referred']."' ");
                                                $rr = mysqli_fetch_assoc($getrefer);
                                            ?>


                                            <tr>
                                                <td><?php echo $j;?></td>
                                                <td><?php echo $row['firstname']." ".$row['lastname']; ?>
                                                </td>
                                                <td><?php echo $row['email']; ?></td>
                                                <td><?php echo $row['phone']; ?></td>
                                                <td>$<?php echo $row['wallet']; ?></td>
                                                <td><?php echo @$rr['email']."(".@$rr['firstname'].")"; ?></td>
                                                <td><?php if($row['status'] ==0){
                                                   echo '<span class=" badge badge-danger">Pending</span>'; 
                                                }else{
                                                     echo '<span class=" badge badge-info">Verified</span>';
                                                   
                                                } ?></td>
                                                <td><?php if($row['status'] ==0){
                                                    echo "<a class=' btn btn-success '  href='#toverouser".$row['id']."' data-toggle='modal'
                                            data-target='#toverouser".$row['id']."' >Approve Account</a>";
                                                }else{
                                                   echo "<a class=' btn  btn-warning '  href='#toverouser".$row['id']."'  data-toggle='modal'
                                            data-target='#toverouser".$row['id']."'  >Block Account</a>"; 
                                                } ?>
                                               

                                                <td>
                                                    <form method="POST">
                                                        <input type="hidden" value="<?php echo $row['id']; ?>"
                                                            name="id" />
                                                        <br />
                                                        <button type="submit" name="user-login"
                                                            class="btn btn-danger ">Login to user
                                                            Account</button>
                                                    </form>
                                                </td>
                                                <td>
                                                    <form method="POST">
                                                        <input type="hidden" value="<?php echo $row['id']; ?>"
                                                            name="id" />
                                                        <input type="hidden" value="<?php echo $row['email']; ?>"
                                                            name="email" />
                                                        <br />
                                                        <?php  if($row['can_withdraw'] == 0){ ?>
                                                        <button type="submit" name="block-withdrawal"
                                                            class="btn btn-danger  btn-block">Block
                                                            Withdrawal</button>
                                                        <?php  }else{ ?>
                                                        <button type="submit" name="enable-withdrawal"
                                                            class="btn btn-danger btn-block ">Enable
                                                            Withdrawal</button>
                                                        <?php } ?>
                                                    </form>

                                                </td>


                                                <td>
                                                    <a class='btn btn-primary btn-block '
                                                                href='#toverowith<?php echo $row['id']; ?>'
                                                                data-toggle='modal'
                                                                data-target='#toverowith<?php echo $row['id']; ?>'>Update
                                                                withdrawal limit</a>

                                                                <a class='btn btn-primary btn-block'
                                                                href='#toverorefer<?php echo $row['id']; ?>'
                                                                data-toggle='modal'
                                                                data-target='#toverorefer<?php echo $row['id']; ?>'>Assign
                                                                Referal Upline</a>

                                                      

                                                        
                                                </td>

                                                <td>
                                                
                                                                
                                                                <a class='btn btn-primary btn-block'
                                                                href='#toverowallet<?php echo $row['id']; ?>'
                                                                data-toggle='modal'
                                                                data-target='#toverowallet<?php echo $row['id']; ?>'>Edit
                                                                Wallet balance</a>
                                                                
                                                               
                                                           

                                                        </td>

                                                       

                                            </tr>




                                            <!-- /.modal -->
                                            <div class="modal fade bs-example-modal-sm"
                                                id="toverouser<?php echo $row['id']; ?>" tabindex="-1" role="dialog"
                                                aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0" id="myLargeModalLabel">
                                                                <?php echo $row['firstname']." ".$row['lastname']; ?>
                                                            </h5><button type="button" class="close"
                                                                data-dismiss="modal" aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12">

                                                                    <h6>Account Status <?php if($row['status'] ==0){
                                                                        echo '<span class=" badge badge-danger">Pending</span>'; 
                                                                        }else{
                                                                            echo '<span class="badge badge-info">Verified </span>';
                                                                        } ?></h6>
                                                                    <hr />
                                                                    <div class="timeline-item-header">
                                                                        <h6>Uses Two Factor Authentication For login
                                                                        </h6>
                                                                        <?php if($row['2fa'] ==0){
                                                                        echo '<span class=" badge badge-danger"> No </span>'; 
                                                                        }else{
                                                                            echo '<span class="badge badge-info" style="float:left"> Yes </span>';
                                                                        
                                                                        } ?>
                                                                    </div>
                                                                    <hr />



                                                                    <div class="form-group row"><label
                                                                            for="example-text-input"
                                                                            class="col-sm-2 col-form-label">Email</label>
                                                                        <div class="col-sm-10"><input
                                                                                class="form-control" id="email"
                                                                                type="email" name="email"
                                                                                value="<?php echo $row['email']; ?>"
                                                                                disabled>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row"><label
                                                                            for="example-text-input"
                                                                            class="col-sm-2 col-form-label">Phone
                                                                            Number</label>
                                                                        <div class="col-sm-10"><input
                                                                                class="form-control" id="phone"
                                                                                name="phone" type="number"
                                                                                value="<?php echo $row['phone']; ?>">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row"><label
                                                                            for="example-text-input"
                                                                            class="col-sm-2 col-form-label">Bank Name</label>
                                                                            <select class="form-control" id="bankname" name="bankname"
                                                        placeholder="Select Bank " >

                                                        <?php
                      
                                                                          for($i=0; $i<count($data['data']); $i++){
                                                                          ?>
                                                        <option
                                                            <?php if($data['data'][$i]['code'] == $row['bankname']){echo "selected";} ?>
                                                            value="<?php echo $data['data'][$i]['code']; ?>">
                                                            <?php echo $data['data'][$i]['name']; ?></option>


                                                        <?php } ?>


                                                    </select>
                                                                    </div>

                                                                    <div class="form-group row"><label
                                                                            for="example-text-input"
                                                                            class="col-sm-2 col-form-label">Account Number</label>
                                                                        <div class="col-sm-10"><input
                                                                                class="form-control" type="text"
                                                                                value="<?php echo $row['account_num']; ?>">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row"><label
                                                                            for="example-text-input"
                                                                            class="col-sm-2 col-form-label">Account
                                                                            Name</label>
                                                                        <div class="col-sm-10"><input
                                                                                class="form-control" type="text"
                                                                                value="<?php echo $row['account_name']; ?>">
                                                                        </div>
                                                                    </div>




                                                                </div>
                                                                <div>
                                                                

                                                                <form method="POST"
                                                                        id="form-approve-<?php echo $row['id']; ?>">

                                                                        <input type="hidden" name="id"
                                                                            value="<?php echo $row['id']; ?>" />
                                                                        <input type="hidden" name="email"
                                                                            value="<?php echo $row['email']; ?>" />
                                                                        <input type="hidden" name="approve-user"
                                                                            value="<?php echo $row['id']; ?>" />

                                                                    </form>

                                                                    <form method="POST"
                                                                        id="form-disable-<?php echo $row['id']; ?>">

                                                                        <input type="hidden" name="id"
                                                                            value="<?php echo $row['id']; ?>" />
                                                                        <input type="hidden" name="email"
                                                                            value="<?php echo $row['email']; ?>" />
                                                                        <input type="hidden" name="disable-user"
                                                                            value="<?php echo $row['id']; ?>" />

                                                                    </form>



                                                                </div>

                                                               







                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary waves-effect"
                                                                data-dismiss="modal">Close</button>

                                                            <?php if($row['status'] ==0){
                                                    echo " <button type='button' onclick="."document.getElementById('form-approve-".$row['id']."').submit()"." class='modal-action waves-effect waves-green btn btn-primary '>Approve Account</button>";
                                                }else{
                                                   echo "<button class='modal-action waves-effect waves-green btn btn-primary' onclick="."document.getElementById('form-disable-".$row['id']."').submit()"." >Block Account</button>"; 
                                                } ?>

                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->




                                            <!-- /.modal -->
                                            <div class="modal fade bs-example-modal-sm"
                                                id="toverowith<?php echo $row['id']; ?>" tabindex="-1" role="dialog"
                                                aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0" id="myLargeModalLabel">
                                                                Set withdrawal limit for
                                                                <?php echo $row['firstname']." ".$row['lastname']; ?>
                                                            </h5><button type="button" class="close"
                                                                data-dismiss="modal" aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12">

                                                                    <form method="POST">

                                                                        <input type="hidden"
                                                                            value="<?php echo $row['id']; ?>"
                                                                            name="id" />
                                                                        <input type="hidden"
                                                                            value="<?php echo $row['email']; ?>"
                                                                            name="email" />


                                                                        <div class="form-group row"><label
                                                                                for="example-text-input"
                                                                                class="col-sm-2 col-form-label">Withdrawal
                                                                                limit</label>
                                                                            <div class="col-sm-10"><input
                                                                                    class="form-control" name="amount"
                                                                                    id="amount"
                                                                                    placeholder="Enter amount"
                                                                                    value="<?php echo $row['possible_withdrawal_amount']; ?>"
                                                                                    type="number">
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group row">
                                                                            <button type="submit"
                                                                                class="btn btn-primary "
                                                                                name="set-withdraw-limit">Update</button>
                                                                        </div>




                                                                    </form>


                                                                </div>


                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary waves-effect"
                                                                data-dismiss="modal">Close</button>

                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->



                                          


                                            <!-- /.modal -->
                                            <div class="modal fade bs-example-modal-sm"
                                                id="toverorefer<?php echo $row['id']; ?>" tabindex="-1" role="dialog"
                                                aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0" id="myLargeModalLabel">
                                                                Add referal upline for
                                                                <?php echo $row['firstname']." ".$row['lastname']; ?>
                                                            </h5><button type="button" class="close"
                                                                data-dismiss="modal" aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <?php if($row['referred'] !=""){echo "<h4>This user already has an upline</h4>"; } ?>

                                                                    <form method="POST">


                                                                        <input type="hidden"
                                                                            value="<?php echo $row['id']; ?>"
                                                                            name="id" />
                                                                        <input type="hidden"
                                                                            value="<?php echo $row['email']; ?>"
                                                                            name="email" />



                                                                        <div class="form-group row"><label
                                                                                for="example-text-input"
                                                                                class="col-sm-2 col-form-label">Enter
                                                                                Upline email</label>
                                                                            <div class="col-sm-10"><input
                                                                                    class="form-control" name="upline"
                                                                                    id="upline"
                                                                                    placeholder="Enter email"
                                                                                    type="email" required>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group row">
                                                                            <button type="submit"
                                                                                class="btn btn-primary "
                                                                                name="assign-upline">Update</button>
                                                                        </div>




                                                                    </form>


                                                                </div>


                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary waves-effect"
                                                                data-dismiss="modal">Close</button>

                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->



                                          





                                            <!-- /.modal -->
                                            <div class="modal fade bs-example-modal-sm"
                                                id="toverowallet<?php echo $row['id']; ?>" tabindex="-1" role="dialog"
                                                aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0" id="myLargeModalLabel">
                                                                Edit wallet for
                                                                <?php echo $row['firstname']." ".$row['lastname']; ?>
                                                            </h5><button type="button" class="close"
                                                                data-dismiss="modal" aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12">


                                                                    <form method="POST"
                                                                        id="form-approve-<?php echo $row['id']; ?>">

                                                                        <input type="hidden"
                                                                            value="<?php echo $row['id']; ?>"
                                                                            name="id" />
                                                                        <input type="hidden"
                                                                            value="<?php echo $row['email']; ?>"
                                                                            name="email" />
                                                                        <input type="hidden"
                                                                            value="<?php echo $row['firstname']." ".$row['lastname']; ?>"
                                                                            name="username" />





                                                                        <div class="form-group row"><label
                                                                                for="example-text-input"
                                                                                class="col-sm-2 col-form-label">Wallet
                                                                                Balance</label>
                                                                            <div class="col-sm-10"><input
                                                                                    class="form-control"
                                                                                    placeholder="Enter new balance"
                                                                                    name="amount" type="number"
                                                                                    value="<?php echo $row['wallet']; ?>"
                                                                                    id="exampleInputName"
                                                                                    placeholder="Enter Amount" required>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group row">
                                                                            <button type="submit"
                                                                                class="btn btn-primary "
                                                                                name="update-wallet"
                                                                                onclick="document.getElementById('update-<?php echo $row['id']; ?>').submit()">Update
                                                                                Balance</button>
                                                                        </div>




                                                                    </form>



                                                                       


                                                                </div>


                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary waves-effect"
                                                                data-dismiss="modal">Close</button>

                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->





<?php
                       }

?>


                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                    <th>S/N</th>
                                                    <th>Fullname</th>
                                                <th>Email</th>
                                                <th>Phone Number</th>
                                                <th>Wallet</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                                <th>Access User</th>
                                                <th>Block Withdrawal</th>

                                                <th>More Actions</th>
                                                <th>More Actions</th>
                                             
                                                    </tr>
                                                </tfoot>
                                            </table>



<?php

//range of number in the link to before and after the current page
$range =3;
	
//show back << link when person has gone more than page 1 the "<<" take person back to page 1 
if($currentpage > 1){
    echo "<a  class='btn btn-secondary'  href='{$_SERVER['PHP_SELF']}?currentpage=1' > First </a> ";
    
    //then show the < link the "<" take persoon back one page
    $prevpage = $currentpage - 1 ;
    
    echo "<a class='btn btn-secondary'  href='{$_SERVER['PHP_SELF']}?currentpage=$prevpage' > < </a> ";
    
}

//using loop to show  links to range of pages before and after the currentpage page
for($x =($currentpage - $range); $x < (($currentpage + $range) + 1); $x++){
    
    //filter to show only valid page number , so that nagative values will not show and nmber greater than the
    // total number of pages
    
    if(($x>0) && ($x <= $totalpages)){
        
        if($x == $currentpage){
            //make it highlighted but not clickable
            echo " <button class='btn btn-primary'> $x</button> ";
        }else{
            //if its not current paage just show it as link
            echo "<a class='btn btn-secondary' href='{$_SERVER['PHP_SELF']}?currentpage=$x' >$x </a> ";
            
        }
        
    }
}


//now show forward(>>) and  next(>) when not on the last page
if($currentpage != $totalpages){
    //get next page first
    $nextpage = $currentpage + 1;
    echo "<a class='btn btn-secondary' href='{$_SERVER['PHP_SELF']}?currentpage=$nextpage' >></a> ";
    
    //then show the forward link for last page
    echo "<a class='btn btn-secondary' href='{$_SERVER['PHP_SELF']}?currentpage=$totalpages' > Last </a> ";
}



//end of the whole paginator


?>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Zero configuration table -->
               

              
            </div>
        </div>
    </div>
    <!-- END: Content-->



  <!-- /.modal -->
  <div class="modal fade bs-example-modal-sm"
                                                id="treasurelimit" tabindex="-1" role="dialog"
                                                aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0" id="myLargeModalLabel">
                                                                Set withdrawal limit for Everyone
                                                                
                                                            </h5><button type="button" class="close"
                                                                data-dismiss="modal" aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12">

                                                                    <form method="POST">

                                                                        


                                                                        <div class="form-group row"><label
                                                                                for="example-text-input"
                                                                                class="col-sm-2 col-form-label">Withdrawal
                                                                                limit</label>
                                                                            <div class="col-sm-10"><input
                                                                                    class="form-control" name="amount"
                                                                                    id="amount"
                                                                                    placeholder="Enter amount"
                                                                                   
                                                                                    type="number">
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group row">
                                                                            <button type="submit"
                                                                                class="btn btn-primary "
                                                                                name="set-withdraw-limit-all">Update</button>
                                                                        </div>




                                                                    </form>


                                                                </div>


                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary waves-effect"
                                                                data-dismiss="modal">Close</button>

                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->



      <!-- BEGIN: Footer-->
  <footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-left d-inline-block">2020 &copy; </span></span>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
    </p>
</footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="../app-assets/vendors/js/vendors.min.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="../app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="../app-assets/js/scripts/configs/vertical-menu-light.js"></script>
    <script src="../app-assets/js/core/app-menu.js"></script>
    <script src="../app-assets/js/core/app.js"></script>
    <script src="../app-assets/js/scripts/components.js"></script>
    <script src="../app-assets/js/scripts/footer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="../app-assets/js/scripts/datatables/datatable.js"></script>
    <!-- END: Page JS-->
           
<?php
if(isset($_POST['approve-user'])){
// to Approve person account
$email = mysqli_real_escape_string($mysqli,$_POST['email']);
$id =  mysqli_real_escape_string($mysqli,$_POST['id']);

$approve = mysqli_query($mysqli,"UPDATE users SET status=1 WHERE id='$id' and email='$email' ");
    
if($approve){

        $pick = mysqli_query($mysqli,"SELECT * FROM users WHERE id='$id' and email='$email' ");
        $r = mysqli_fetch_assoc($pick);
    $name = $r['firstname']." ".$r['lastname'];
//send the welcome email with verification link here
//start email sending




            //end of email sending



    ?>
<script>


Swal.fire({
        title: 'Approval Successful',
        text: 'Account with <?php echo $email; ?>, has been approved successfully.',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })

setTimeout(() => {
    location = location;
}, 3000);
</script>

<?php


}



}





//disbae the user account
if(isset($_POST['disable-user'])){
// to Approve person account
$email = mysqli_real_escape_string($mysqli,$_POST['email']);
$id =  mysqli_real_escape_string($mysqli,$_POST['id']);

$disbale = mysqli_query($mysqli,"UPDATE users SET status=0 WHERE id='$id' and email='$email' ");

if($disbale){

    ?>
<script>


Swal.fire({
        title: 'Account Disabled',
        text: 'Account with <?php echo $email; ?>, has been disabled successfully.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })

setTimeout(() => {
    location = location;
}, 3000);
</script>

<?php


}



}

?>



<?php

//to block a user withdrawal
if(isset($_POST['block-withdrawal'])){
$email = mysqli_real_escape_string($mysqli,$_POST['email']);
$id =  mysqli_real_escape_string($mysqli,$_POST['id']);

$block = mysqli_query($mysqli,"UPDATE users SET can_withdraw=200 WHERE id='$id'");

if($block){

  ?>
<script>


Swal.fire({
        title: 'Withdrawal Blocked',
        text: 'The Account with <?php echo $email; ?> has been blocked from withdrawing',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })

setTimeout(() => {
    location = location;
}, 3000);
</script>

<?php

}
//successful

}


//to enable a user withdrawal
if(isset($_POST['enable-withdrawal'])){
$email = mysqli_real_escape_string($mysqli,$_POST['email']);
$id =  mysqli_real_escape_string($mysqli,$_POST['id']);

$block = mysqli_query($mysqli,"UPDATE users SET can_withdraw=0 WHERE id='$id'");

if($block){ 

  ?>
<script>


Swal.fire({
        title: 'Withdrawal Enabled',
        text: 'The Account with <?php echo $email; ?> has been Enabled for withdrawing',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })

setTimeout(() => {
    location = location;
}, 3000);
</script>

<?php

}
//successful

}



//set withdrawal limit
if(isset($_POST['set-withdraw-limit'])){

$email = mysqli_real_escape_string($mysqli,$_POST['email']);
$id =  mysqli_real_escape_string($mysqli,$_POST['id']);
$amount = mysqli_real_escape_string($mysqli,$_POST['amount']);

$change = mysqli_query($mysqli,"UPDATE users SET possible_withdrawal_amount='$amount' WHERE id='$id'");

if($change){

  ?>
<script>

Swal.fire({
        title: 'Withdrawal Limit Updated',
        text: 'The Account with <?php echo $email; ?> has a new withdrawal limit of <?php echo $amount; ?>',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })

setTimeout(() => {
    location = location;
}, 3000);
</script>

<?php

}
//successful


}



//set withdrawal limit
if(isset($_POST['set-withdraw-limit-all'])){

    $amount = mysqli_real_escape_string($mysqli,$_POST['amount']);
    
    $change = mysqli_query($mysqli,"UPDATE users SET possible_withdrawal_amount='$amount' ");
    
    if($change){
    
      ?>
    <script>
    
    Swal.fire({
            title: 'Withdrawal Limit Updated',
            text: 'The evryone has a new withdrawal limit of <?php echo $amount; ?>',
            type: 'success',
            showCancelButton: true,
            confirmButtonColor: '#626ed4',
            cancelButtonColor: "#ec4561"
        })
    
    setTimeout(() => {
        location = location;
    }, 3000);
    </script>
    
    <?php
    
    }
    //successful
    
    
    }
    





//update wallet balance
if(isset($_POST['update-wallet'])){

//get wallet
$email = mysqli_real_escape_string($mysqli,$_POST['email']);
$id =  mysqli_real_escape_string($mysqli,$_POST['id']);
$amount = mysqli_real_escape_string($mysqli,$_POST['amount']);

$update = mysqli_query($mysqli,"UPDATE users SET wallet='$amount' WHERE id='$id' ");

if($update){

          ?>
<script>

Swal.fire({
        title: 'Wallet updated Successfull!',
        text: 'You have update wallet to  $<?php echo $amount; ?> .',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })



setTimeout(() => {
    location = location;
}, 3000);
</script>

<?php

}


}




//assing  upline for a user 
if(isset($_POST['assign-upline'])){

    $email = mysqli_real_escape_string($mysqli,$_POST['email']);
$id =  mysqli_real_escape_string($mysqli,$_POST['id']);
$upline = mysqli_real_escape_string($mysqli,$_POST['upline']);

//go and get upline referal code
$getcode = mysqli_query($mysqli,"SELECT * from users WHERE email='$upline' ");
$g = mysqli_fetch_assoc($getcode);

if(mysqli_num_rows($getcode) > 0){

$change = mysqli_query($mysqli,"UPDATE users SET referred='".$g['referal_link']."' WHERE id='$id'");

if($change){

  ?>
<script>

Swal.fire({
        title: 'Upline referal set successfully',
        text: 'The Account with <?php echo $email; ?> has a referal',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })

setTimeout(() => {
    location = location;
}, 3000);
</script>

<?php

}
//successful

}else{
//upline does not exist

 ?>
<script>


Swal.fire({
        title: 'Upline Email is wrong',
        text: 'The email enter for upline does not exist',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })

setTimeout(() => {
    location = location;
}, 3000);
</script>

<?php

}



}



?>



</body>
<!-- END: Body-->

</html>