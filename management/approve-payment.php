<?php
session_start();

include('../connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['adminid'])){
	
    header("location:index");
    
}


$get_admin = mysqli_query($mysqli,"SELECT * FROM admins WHERE id='".$_SESSION['adminid']."' ");
$rows = mysqli_fetch_assoc($get_admin);



?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
    <title>Payment Pending - Treasure Capital </title>
    <link rel="apple-touch-icon" href="../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="../app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/sweetalert2.min.css">
    <!-- END: Theme CSS-->
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

   <?php include('header.php'); ?>

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Pending Payment </h5>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="index"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">Manage Payment
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                       
                    </div>
                </div>
                <!-- Zero configuration table -->
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Pending Payments</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                       
                                        <div class="table-responsive">
                                            <table class="table zero-configuration">
                                                <thead>
                                                    <tr>
                                                    <th>S/N</th>
                                                <th>Logo</th>
                                                <th>Payment Method</th>
                                                <th>Date</th>
                                                <th>Full name</th>
                                                <th>Email</th>
                                                <th>Order-Id</th>
                                                <th>Amount Paid</th>
                                                
                                               
                                                <th>Action</th>
                                                <th>More</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                               
                                           
                                                <?php
                                        //start the loop for see all users
                                        $get_funds = mysqli_query($mysqli,"SELECT * FROM pending WHERE status=0  ORDER BY id DESC");
                                            $i=0;
                                            while($row= mysqli_fetch_assoc($get_funds)){
                                                $i++;

                                               

                                                 $getuser= mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$row['userid']."'");
                                                $user = mysqli_fetch_assoc($getuser);
                                            ?>




                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php if($row['type']==1){
                                    echo '<div class="col-md-12" style="padding:10px" >
                                    <a href="javascript:;" id="pay1"  class="btn btn-warning"><img src="../img/bitcoin.png" width="50" /><br/> Bitcoin</a>
                                    </div>';
                                  }elseif($row['type']==2){
                                    echo '<div class="col-md-4" style="padding:10px">
                                    <a href="javascript:;" id="pay2"  class="btn btn-warning"><img src="../img/tron.png" width="50" /><br/> Tron</a>
                                    </div>';
                                  }elseif($row['type']==3 or $row['type']==4 or $row['type']==5){
                                    echo '<div class="col-md-4" style="padding:10px">
                                    <a href="javascript:;" id="pay3"  class="btn btn-warning"><img src="../img/naira.png" width="50" /><br/> FIAT</a>
                                    </div>';
                                  } ?>
                                                </td>
                                                <td><?php echo $row['pbank']; ?></td>
                                                <td><?php echo $row['date']; ?></td>
                                                <td><?php echo $user['firstname']." ".$user['secondname']." ".$user['lastname']; ?>
                                                </td>
                                                <td><?php echo $user['email']; ?></td>
                                                <td><?php echo $row['chargeid']; ?></td>
                                                <td>$<?php echo $row['amount']; ?></td>
                                               

                                               

                                                <td><?php if($row['status'] ==0){
                                                   echo " <button type='button' onclick="."document.getElementById('form-approve-".$row['id']."').submit()"." class='btn btn-primary'>Activate</button>";
                                                }else{
                                                   
                                                    
                                                } ?></td>
                                                <td><?php 
                                                   echo "<button type='button' onclick="."document.getElementById('form-delete-".$row['id']."').submit()"." class='btn btn-danger'>Delete</button>";
                                                 ?></td>
                                            </tr>

                                            <div class="modal">
                                                <div class="modal-content">
                                                    <form method="POST" id="form-approve-<?php echo $row['id']; ?>">

                                                        <input type="hidden" name="order_id"
                                                            value="<?php echo $row['chargeid']; ?>" />
                                                        <input type="hidden" name="approve-pay" />

                                                    </form>

                                                     <form method="POST" id="form-delete-<?php echo $row['id']; ?>">

                                                        <input type="hidden" name="order_id"
                                                            value="<?php echo $row['chargeid']; ?>" />
                                                        <input type="hidden" name="delete-pay" />

                                                    </form>
                                                </div>
                                            </div>





                                            <?php

                                            }

                                             ?>


                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                    <th>S/N</th>
                                                <th>Logo</th>
                                                <th>Payment Method</th>
                                                <th>Date</th>
                                                <th>Full name</th>
                                                <th>Email</th>
                                                <th>Order-Id</th>
                                                <th>Amount Paid</th>
                                               
                                             
                                                <th>Action</th>
                                                <th>More</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Zero configuration table -->
               

              
            </div>
        </div>
    </div>
    <!-- END: Content-->



      <!-- BEGIN: Footer-->
  <footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-left d-inline-block">2021 &copy; </span></span>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
    </p>
</footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="../app-assets/vendors/js/vendors.min.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="../app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="../app-assets/js/scripts/configs/vertical-menu-light.js"></script>
    <script src="../app-assets/js/core/app-menu.js"></script>
    <script src="../app-assets/js/core/app.js"></script>
    <script src="../app-assets/js/scripts/components.js"></script>
    <script src="../app-assets/js/scripts/footer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="../app-assets/js/scripts/datatables/datatable.js"></script>
    <!-- END: Page JS-->
    <?php
if(isset($_POST['approve-pay'])){
//start of approval

$order_id = $_POST['order_id'];

$date= date("d")." ".date("F")." ".date("Y")." , ".date("h")." : ".date("i").date("a");



	//increment the user pinecrest wallet
	
	$get = mysqli_query($mysqli,"SELECT * FROM pending where chargeid='$order_id' ");
    $row = mysqli_fetch_assoc($get);

    
    if($row['status']==0 ){

  
    

        $userid = $row['userid'];
        $amount = $row['amount'];
        $name = $row['name'];
        $tag = $row['tag'];
        $investmentid = $row['investmentid'];
        $date= date("d")." ".date("F")." ".date("Y")." , ".date("h")." : ".date("i").date("a");
        
        $getpack = mysqli_query($mysqli,"SELECT * FROM `investment_packages` WHERE id='$investmentid' ");
        $in = mysqli_fetch_assoc($getpack);
        
        
        
        $addinvestment = mysqli_query($mysqli,"INSERT INTO `investment`(userid, investmentid, name, amount, daily_roi, `duration`, `tag`, date)  VALUES('$userid', '$investmentid', '$name', '$amount', '".$row['daily_roi']."', '".$in['duration']."', '$tag', '$date') ");
        
        
        
        
        $getuser = mysqli_query($mysqli,"SELECT * FROM  users WHERE id='$userid'");
        $user = mysqli_fetch_assoc($getuser);
        
        //referal and update 
        $price_amount = $amount;
            
            //next check if the person was refered and den update the referer with 10% bouns
            //the person was refered and the 
            
            if($user['referred'] !="" and $user['pay_refer1']==0 ){
            
                $bouns = 0.05*$price_amount;
            
                echo $bouns;
            
            
                //find the person who refered himm account
            $getrefer = mysqli_query($mysqli,"SELECT * FROM users WHERE referal_link='".$user['referred']."' ");
            $refer = mysqli_fetch_assoc($getrefer);
            
            if($refer['level_command'] >=1 or $refer['level_command']==0){
        
            $act ="Referral Bonus of 5%, 1st level";
            $desc ="Referral commission of $".$bouns;
            //now update the referer acc by adding it to referals table
            //status is zero unles user claim it
            $updaterefer = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('".$refer['id']."', '$act', '$desc', '$date','$bouns', 'Credited')");
            
            $newwallet = $refer['wallet']+$bouns;
            
            $updateuser = mysqli_query($mysqli,"UPDATE users SET  wallet='$newwallet'  where id='".$refer['id']."' ");
            
            $updaterefer = mysqli_query($mysqli,"INSERT INTO `referal` (`claimerid`, `status`, `date`, `amount`, `detail`) VALUES('".$refer['id']."', 1, '$date', '$bouns', '$act'  )  ");
            
            //now update the referee that bouns has been given to his referer step 1
            $updateuser = mysqli_query($mysqli,"UPDATE users SET  pay_refer1=1  where id='".$user['id']."' ");
        
            }
            
            }
            
            
            //step 1 is done move over to step 2 of referal bouns
            if($user['referred']!="" and $user['pay_refer2']==0){
            
                $bouns2 = 0.02*$price_amount;
                //find the person who refered himm account
                      //find the person who refered himm account
            $getrefer = mysqli_query($mysqli,"SELECT * FROM users WHERE referal_link='".$user['referred']."' ");
            $refer = mysqli_fetch_assoc($getrefer);
            ///send level
            $getrefer2 = mysqli_query($mysqli,"SELECT * FROM users WHERE referal_link='".$refer['referred']."' ");
            $refer2 = mysqli_fetch_assoc($getrefer2);
        
            if($refer2['level_command']==2){
            
            $details2 ="Referral Funding Bonus of 2%, 2nd level";
            $act ="Referral Funding Bonus of 2%, 2nd level";
            $desc ="Referral commission of $".$bouns2;
            //now update the referer acc by adding it to referals table
            //status is zero unles user claim it
            $updaterefer = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('".$refer2['id']."', '$act', '$desc', '$date','$bouns2', 'Credited')");
        
            //now update the referer acc by adding it to referals table
            //status is zero unles user claim it
            $updaterefer = mysqli_query($mysqli,"INSERT INTO `referal` (`claimerid`, `status`, `date`, `amount`, `detail`) VALUES('".$refer2['id']."', 1, '$date', '$bouns2', '$details2'  )  ");
            
            //now update the referee that bouns has been given to his referer step 2
            $updateuser = mysqli_query($mysqli,"UPDATE users SET  pay_refer2=0  where id='".$user['id']."' ");
            
            $newwallet2 = $refer2['wallet']+$bouns2;
            
            $updateuser = mysqli_query($mysqli,"UPDATE users SET  wallet='$newwallet2'  where id='".$refer2['id']."' ");
            
            
            }
            
            
            }
            
            
            
        
        
        
        /////////////////////////////////
        
        $action = "Mining Pool Purchased";
        $describe ="Mining pool purchased for  $".$amount." successfully";
        
        
        $add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Confirmed') ");
        
        
        
        
        
        $update = mysqli_query($mysqli,"UPDATE users SET wes=1, wesscore=20 WHERE id='".$user['id']."' ");


        
        
        $update = mysqli_query($mysqli,"UPDATE pending SET status=1 WHERE chargeid='$order_id' ");
        
        http_response_code(200);
        
        }
	
//next check if the person was refered and den update the referer with 10% bouns
//the person was refered and the 





            //end of email sending






	    ?>
    <script>
  

  Swal.fire({
        title: 'Activation Successful',
        text: 'Bot has been activated Successfully ',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })


    setTimeout(() => {
        location = location;
    }, 3000);
    </script>

    <?php






//end of approval
}

?>


<?php
if(isset($_POST['delete-pay'])){
//start of approval

$order_id = $_POST['order_id'];

$delete = mysqli_query($mysqli,"DELETE FROM `pending` WHERE `chargeid`='$order_id' ");

if($delete){

	    ?>
    <script>
    Swal.fire({
        title: 'Delete Successful',
        text: 'Bot has been deleted ',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })


    setTimeout(() => {
        location = location;
    }, 3000);
    </script>

    <?php

}



}
?>





</body>
<!-- END: Body-->

</html>