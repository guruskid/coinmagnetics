<?php
session_start();

include('../connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['adminid'])){
	
    header("location:index");
    
}


$get_admin = mysqli_query($mysqli,"SELECT * FROM admins WHERE id='".$_SESSION['adminid']."' ");
$rows = mysqli_fetch_assoc($get_admin);



?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
    <title>Bot History - Treasure Capital </title>
    <link rel="apple-touch-icon" href="../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="../app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/sweetalert2.min.css">
    <!-- END: Theme CSS-->
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

   <?php include('header.php'); ?>

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Bot History</h5>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="index"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">Bot History
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                       
                    </div>
                </div>
                <!-- Zero configuration table -->
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Bot History</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                       
                                        <div class="table-responsive">
                                            <table class="table zero-configuration">
                                                <thead>
                                                    <tr>
                                                    <th>S/N</th>
                                                            <th>Investor Name</th>
                                                            <th>Email</th>
                                                            <th>Bot Package</th>
                                                            <th>Date</th>
                                                            <th>Amount</th>
                                                           
                                                            <th>Daily Returns(ROI)</th>
                                                            <th>Payout</th>
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                        //start the loop for see all users
                                        $get_investment = mysqli_query($mysqli,"SELECT * FROM investment ORDER BY id DESC");
                                            $i=0;
                                            while($row= mysqli_fetch_assoc($get_investment)){
                                                $i++;

                                                

                                                 $getuser= mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$row['userid']."'");
                                                $user = mysqli_fetch_assoc($getuser);

                                            ?>








                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                
                                                <td><?php echo $user['firstname']." ".$user['secondname']." ".$user['lastname']; ?>
                                                </td>
                                                <td><?php echo $user['email']; ?></td>
                                                <td><?php echo $row['name']; ?></td>
                                                 <td><?php echo $row['date']; ?></td>
                                                <td>$<?php echo $row['amount']; ?></td>
                                              
                                                <td>$<?php echo $row['daily_roi']; ?></td>
                                                <td>30 Days</td>

                                                <td><?php if($row['status'] ==0){
                                                   echo "<span class='badge badge-danger' >Not Active</span>"; 
                                                }else{
                                                     echo "<span class='badge badge-info' >Active</span>";
                                                   
                                                } ?></td>
                                                
                                                <td> <a class='btn btn-primary btn-block'
                                                                href='#toverowallet<?php echo $row['id']; ?>'
                                                                data-toggle='modal'
                                                                data-target='#toverowallet<?php echo $row['id']; ?>'>Edit
                                                               Bot balance</a></td>


                                                
                                            </tr>





                                            <!-- /.modal -->
                                            <div class="modal fade bs-example-modal-sm"
                                                id="toverowallet<?php echo $row['id']; ?>" tabindex="-1" role="dialog"
                                                aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0" id="myLargeModalLabel">
                                                                Edit  Bot for <?php echo $user['firstname']." ".$user['secondname']." ".$user['lastname']; ?>
                                                               
                                                            </h5><button type="button" class="close"
                                                                data-dismiss="modal" aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12">


                                                                 


                                                                    <form method="POST"
                                                                        id="form-approve-<?php echo $row['id']; ?>">


                                                                       <input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
                                                                       <input type="hidden" name="investmentid" value="<?php echo $row['investmentid']; ?>" />

                                                                        
                                                                        <div class="form-group row"><label
                                                                                for="example-text-input"
                                                                                class="col-sm-2 col-form-label">Investment Balance</label>
                                                                            <div class="col-sm-10"><input
                                                                                    class="form-control"
                                                                                     name="amount" type="number"
                                                                            value="<?php echo $row['amount']; ?>"
                                                                            id="exampleInputName"
                                                                            placeholder="Enter Amount" required>
                                                                            </div>
                                                                        </div>
                                                                          




                                                                        
                                                                        <div class="form-group row">
                                                                            <button type="submit"
                                                                                class="btn btn-primary "
                                                                                name="update-invest"
                                                                                onclick="document.getElementById('update-<?php echo $row['id']; ?>').submit()">Update
                                                                                Bot info</button>
                                                                        </div>


                                                                    </form>



                                                                </div>


                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary waves-effect"
                                                                data-dismiss="modal">Close</button>

                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->






                                            <?php

                                            }

                                             ?>


                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                    <th>S/N</th>
                                                            <th>Investor Name</th>
                                                            <th>Email</th>
                                                            <th>Investment Package</th>
                                                            <th>Date</th>
                                                            <th>Amount</th>
                                                           
                                                            <th>Daily Returns(ROI)</th>
                                                            <th>Payout</th>
                                                            <th>Status</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Zero configuration table -->
               

              
            </div>
        </div>
    </div>
    <!-- END: Content-->



      <!-- BEGIN: Footer-->
  <footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-left d-inline-block">2021 &copy; </span></span>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
    </p>
</footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="../app-assets/vendors/js/vendors.min.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="../app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="../app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="../app-assets/js/scripts/configs/vertical-menu-light.js"></script>
    <script src="../app-assets/js/core/app-menu.js"></script>
    <script src="../app-assets/js/core/app.js"></script>
    <script src="../app-assets/js/scripts/components.js"></script>
    <script src="../app-assets/js/scripts/footer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="../app-assets/js/scripts/datatables/datatable.js"></script>
    <!-- END: Page JS-->
   


    <?php
//to login into a user account
if(isset($_POST['user-login'])){

    $id = $_POST['id'];

    $_SESSION['id'] = $id;

    ?>
<script>
window.open('../dashboard', '_blank');
</script>

<?php

}

//to block a user withdrawal
if(isset($_POST['update-invest'])){

$id =  mysqli_real_escape_string($mysqli,$_POST['id']);
$amount = mysqli_real_escape_string($mysqli,$_POST['amount']);
$investmentid  = mysqli_real_escape_string($mysqli,$_POST['investmentid']);

$getp = mysqli_query($mysqli,"SELECT * FROM investment_packages where id ='$investmentid'");
$invest = mysqli_fetch_assoc($getp);

$daily_roi = $amount*(5/100);


$updateinvestment = mysqli_query($mysqli,"UPDATE `investment` SET amount='$amount', daily_roi='$daily_roi' WHERE id='$id' ");


if($updateinvestment){

  ?>
<script>


Swal.fire({
        title: 'Investment info Updated',
        text: 'The investment has been updated ',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })

setTimeout(() => {
    location = location;
}, 3000);
</script>

<?php

}
//successful

}

?>
</body>
<!-- END: Body-->

</html>