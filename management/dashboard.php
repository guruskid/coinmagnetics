<?php
session_start();

include('../connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['adminid'])){
	
    header("location:index");
    
}


$get_admin = mysqli_query($mysqli,"SELECT * FROM admins WHERE id='".$_SESSION['adminid']."' ");
$rows = mysqli_fetch_assoc($get_admin);



?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <title>Dashboard - Coin Magnetics </title>
    <link rel="apple-touch-icon" href="../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="../app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700"
        rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/charts/apexcharts.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/swiper.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/semi-dark-layout.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/sweetalert2.min.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/pages/dashboard-ecommerce.css">
    <link href='../app-assets/fonts/boxicons/css/boxicons.css' rel='stylesheet'>
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-sticky footer-static  " data-open="click"
    data-menu="vertical-menu-modern" data-col="2-columns">

    <?php include('header.php'); ?>
    <?php
//get all the needed info for dashboard
$getusers = mysqli_query($mysqli,"SELECT * FROM users");
$totalusers = mysqli_num_rows($getusers);

$getblocked = mysqli_query($mysqli,"SELECT * FROM pending where status=0");
$blocked = mysqli_num_rows($getblocked);
//loop throughusers
$totalbalance=0;

while($count = mysqli_fetch_assoc($getusers)){
 $totalbalance += $count['wallet'];   
 
}

$getinvestment = mysqli_query($mysqli,"SELECT * FROM investment");
$totalinvestamount=0;
while($in = mysqli_fetch_assoc($getinvestment)){
 $totalinvestamount += $in['amount'];   
 

}

$getinvestmentpack = mysqli_query($mysqli,"SELECT * FROM investment_packages");
$totalinvestpack=mysqli_num_rows($getinvestmentpack);


$getwithdraw = mysqli_query($mysqli,"SELECT * FROM withdrawal ");
$totalwithamount=0;
while($out = mysqli_fetch_assoc($getwithdraw)){
 $totalwithamount += $out['amount'];   
 

}


$getvideo = mysqli_query($mysqli,"SELECT * FROM video ");






?>

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- Dashboard Ecommerce Starts -->
                <section id="dashboard-ecommerce">
                    <div class="row">
                        <!-- Greetings Content Starts -->
                        <div class="col-xl-4 col-md-6 col-12 dashboard-greetings">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="greeting-text">Welcome Admin !</h3>
                                    <p class="mb-0">Total Users</p>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between align-items-end">
                                            <div class="dashboard-content-left">
                                                <h1 class="text-primary font-large-2 text-bold-500">
                                                <?php echo number_format($totalusers); ?></h1>
                                                <p>Available and Registered Users</p>
<?php 
$getsetting = mysqli_query($mysqli,"SELECT * FROM settings");
 $setting = mysqli_fetch_assoc($getsetting);

 ?>

<form method="POST">
<input type="hidden" value="<?php echo $setting['automatic']; ?>" name="automatic" />

<?php if($setting['automatic']==1){
?>
<button type="submit" name="disable-mining" class="btn btn-primary glow">Disable Mining</button>

<?php }else{ ?>
<button type="submit" name="enable-mining" class="btn btn-primary glow">Enable Mining</button>
<?php } ?>
</form>
                                               


                                            </div>
                                            <div class="dashboard-content-right">
                                                <img src="../app-assets/images/icon/cup.png" height="220" width="220"
                                                    class="img-fluid" alt="Dashboard " />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Multi Radial Chart Starts -->
                        <div class="col-lg-4 col-md-6">
                            <div class="card">
                                <div
                                    class="card-header border-bottom d-flex justify-content-between align-items-center">
                                    <h4 class="card-title">TOTAL WALLET BALANCE</h4>
                                    <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                                </div>
                                <div class="card-content">
                                    <div class="card-body pt-1" style="position: relative;">
                                        <div id="radial-chart-half" style="min-height: 224.772px;">
                                            <div id="apexcharts2dtbnstr"
                                                class="apexcharts-canvas apexcharts2dtbnstr light"
                                                style="width: 295px; height: 224.772px;"><svg id="SvgjsSvg3889"
                                                    width="295" height="224.7724609375"
                                                    xmlns="http://www.w3.org/2000/svg" version="1.1"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                                    xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg"
                                                    xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                                    style="background: transparent;">
                                                    <g id="SvgjsG3891" class="apexcharts-inner apexcharts-graphical"
                                                        transform="translate(42, 0)">
                                                        <defs id="SvgjsDefs3890">
                                                            <clipPath id="gridRectMask2dtbnstr">
                                                                <rect id="SvgjsRect3892" width="215" height="237" x="-1"
                                                                    y="-1" rx="0" ry="0" fill="#ffffff" opacity="1"
                                                                    stroke-width="0" stroke="none" stroke-dasharray="0">
                                                                </rect>
                                                            </clipPath>
                                                            <clipPath id="gridRectMarkerMask2dtbnstr">
                                                                <rect id="SvgjsRect3893" width="215" height="237" x="-1"
                                                                    y="-1" rx="0" ry="0" fill="#ffffff" opacity="1"
                                                                    stroke-width="0" stroke="none" stroke-dasharray="0">
                                                                </rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="SvgjsG3895" class="apexcharts-radialbar">
                                                            <g id="SvgjsG3896">
                                                                <g id="SvgjsG3897" class="apexcharts-tracks">
                                                                    <g id="SvgjsG3898"
                                                                        class="apexcharts-radialbar-track apexcharts-track"
                                                                        rel="1">
                                                                        <path id="apexcharts-radialbarTrack-0"
                                                                            d="M 9.089902883913823 134.67602831499784 A 98.91280487804877 98.91280487804877 0 1 1 192.16100178396357 166.95640243902437"
                                                                            fill="none" fill-opacity="1"
                                                                            stroke="rgba(242,244,244,0.85)"
                                                                            stroke-opacity="1" stroke-linecap="round"
                                                                            stroke-width="3.053134146341464"
                                                                            stroke-dasharray="0"
                                                                            class="apexcharts-radialbar-area"
                                                                            data:pathOrig="M 9.089902883913823 134.67602831499784 A 98.91280487804877 98.91280487804877 0 1 1 192.16100178396357 166.95640243902437">
                                                                        </path>
                                                                    </g>
                                                                </g>
                                                                <g id="SvgjsG3900">
                                                                
                                                                    <g id="SvgjsG3905"
                                                                        class="apexcharts-series apexcharts-radial-series"
                                                                        seriesName="842k" rel="1" data:realIndex="0">
                                                                        <path id="SvgjsPath3906"
                                                                            d="M 20.838998216036416 166.9564024390244 A 98.91280487804877 98.91280487804877 0 0 1 180.0065391185771 51.3144148952958"
                                                                            fill="none" fill-opacity="0.85"
                                                                            stroke="rgba(253,172,65,0.85)"
                                                                            stroke-opacity="1" stroke-linecap="round"
                                                                            stroke-width="3.147560975609757"
                                                                            stroke-dasharray="0"
                                                                            class="apexcharts-radialbar-area apexcharts-radialbar-slice-0"
                                                                            data:angle="168" data:value="67" index="0"
                                                                            j="0"
                                                                            data:pathOrig="M 20.838998216036416 166.9564024390244 A 98.91280487804877 98.91280487804877 0 0 1 180.0065391185771 51.3144148952958">
                                                                        </path>
                                                                    </g>
                                                                    <circle id="SvgjsCircle3902" r="102.38623780487804"
                                                                        cx="106.5" cy="117.5"
                                                                        class="apexcharts-radialbar-hollow"
                                                                        fill="transparent"></circle>
                                                                    <g id="SvgjsG3903"
                                                                        class="apexcharts-datalabels-group"
                                                                        transform="translate(0, 0)" style="opacity: 1;">
                                                                        <text id="SvgjsText3904"
                                                                            font-family="Helvetica, Arial, sans-serif"
                                                                            x="106.5" y="167.5" text-anchor="middle"
                                                                            dominant-baseline="auto" font-size="34px"
                                                                            fill="#304156"
                                                                            class="apexcharts-datalabel-label"
                                                                            style="font-family: Helvetica, Arial, sans-serif;">$<?php echo number_format($totalbalance); ?> </text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                        <line id="SvgjsLine3907" x1="0" y1="0" x2="213" y2="0"
                                                            stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1"
                                                            class="apexcharts-ycrosshairs"></line>
                                                        <line id="SvgjsLine3908" x1="0" y1="0" x2="213" y2="0"
                                                            stroke-dasharray="0" stroke-width="0"
                                                            class="apexcharts-ycrosshairs-hidden"></line>
                                                    </g>
                                                </svg>
                                                <div class="apexcharts-legend"></div>
                                            </div>
                                        </div>

                                        <div class="resize-triggers">
                                            <div class="expand-trigger">
                                                <div style="width: 347px; height: 339px;"></div>
                                            </div>
                                            <div class="contract-trigger"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-12 dashboard-users">
                            <div class="row  ">
                                <!-- Statistics Cards Starts -->
                            
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-sm-12 col-12 dashboard-users-success">
                                            <div class="card text-center">
                                                <div class="card-content">
                                                    <div class="card-body py-1">
                                                        <div
                                                            class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                                                            <i class="bx bx-briefcase-alt font-medium-5"></i>
                                                        </div>
                                                        <div class="text-muted line-ellipsis">Total Pending Payment
                                                        </div>
                                                        <h3 class="mb-0"><?php echo number_format($blocked); ?> </h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-12 dashboard-users-danger">
                                            <div class="card text-center">
                                                <div class="card-content">
                                                    <div class="card-body py-1">
                                                        <div
                                                            class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                                                            <i class="bx bx-user font-medium-5"></i>
                                                        </div>
                                                        <div class="text-muted line-ellipsis">Total Amount Invested(Currently)</div>
                                                        <h3 class="mb-0">$<?php echo number_format($totalinvestamount); ?> </h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- Revenue Growth Chart Starts -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-12 dashboard-order-summary">
                            <div class="card" style="display:none">
                                <div class="row">
                                    <!-- Order Summary Starts -->
                                    <div class="col-md-8 col-12 order-summary border-right pr-md-0">
                                        <div class="card mb-0">
                                            <div class="card-header d-flex justify-content-between align-items-center">
                                                <h4 class="card-title">Order Summary</h4>
                                                <div class="d-flex">
                                                    <button type="button"
                                                        class="btn btn-sm btn-light-primary mr-1">Month</button>
                                                    <button type="button"
                                                        class="btn btn-sm btn-primary glow">Week</button>
                                                </div>
                                            </div>
                                            <div class="card-content">
                                                <div class="card-body p-0">
                                                    <div id="order-summary-chart"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Sales History Starts -->
                                    <div class="col-md-4 col-12 pl-md-0">
                                        <div class="card mb-0">
                                            <div class="card-header pb-50">
                                                <h4 class="card-title">Sales History</h4>
                                            </div>
                                            <div class="card-content">
                                                <div class="card-body py-1">
                                                    <div class="d-flex justify-content-between align-items-center mb-2">
                                                        <div class="sales-item-name">
                                                            <p class="mb-0">Airpods</p>
                                                            <small class="text-muted">30 min ago</small>
                                                        </div>
                                                        <div class="sales-item-amount">
                                                            <h6 class="mb-0"><span class="text-success">+</span> $50
                                                            </h6>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between align-items-center mb-2">
                                                        <div class="sales-item-name">
                                                            <p class="mb-0">iPhone</p>
                                                            <small class="text-muted">2 hour ago</small>
                                                        </div>
                                                        <div class="sales-item-amount">
                                                            <h6 class="mb-0"><span class="text-danger">-</span> $59</h6>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <div class="sales-item-name">
                                                            <p class="mb-0">Macbook</p>
                                                            <small class="text-muted">1 day ago</small>
                                                        </div>
                                                        <div class="sales-item-amount">
                                                            <h6 class="mb-0"><span class="text-success">+</span> $12
                                                            </h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-footer border-top pb-0">
                                                    <h5>Total Sales</h5>
                                                    <span class="text-primary text-bold-500">$82,950.96</span>
                                                    <div class="progress progress-bar-primary progress-sm my-50">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="78"
                                                            style="width:78%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Latest Update Starts -->
                        <div class="col-xl-4 col-md-4 col-sm-6">
      <div class="card text-center">
        <div class="card-content">
          <div class="card-body">
            <div class="badge-circle badge-circle-lg badge-circle-light-info mx-auto my-1">
              <i class="bx bx-edit-alt font-medium-5"></i>
            </div>
            <p class="text-muted mb-0 line-ellipsis">All Active Bot</p>
            <h2 class="mb-0"><?php echo mysqli_num_rows($getinvestment); ?></h2>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-4 col-md-4 col-sm-6">
      <div class="card text-center">
        <div class="card-content">
          <div class="card-body">
            <div class="badge-circle badge-circle-lg badge-circle-light-warning mx-auto my-1">
              <i class="bx bx-file font-medium-5"></i>
            </div>
            
            <p class="text-muted mb-0 line-ellipsis">Total Amount Withdrawed</p>
            <h2 class="mb-0">$<?php echo number_format($totalwithamount); ?> </h2>
          </div>
        </div>
      </div>
    </div>


    <div class="col-xl-4 col-md-4 col-sm-6">
      <div class="card text-center">
        <div class="card-content">
          <div class="card-body">
            <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto my-1">
              <i class="bx bx-purchase-tag font-medium-5"></i>
            </div>
          
            <p class="text-muted mb-0 line-ellipsis">Total Video Testimony</p>
            <h2 class="mb-0"><?php echo mysqli_num_rows($getvideo); ?></h2>
          </div>
        </div>
      </div>
    </div>


                       
                        <!-- Earning Swiper Starts -->
                     



                    </div>







                    </div>
                </section>
                <!-- Dashboard Ecommerce ends -->

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix mb-0"><span class="float-left d-inline-block">2021 &copy; </span></span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i
                    class="bx bx-up-arrow-alt"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="../app-assets/vendors/js/vendors.min.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>

    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="../app-assets/vendors/js/charts/apexcharts.min.js"></script>
    <script src="../app-assets/vendors/js/extensions/swiper.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS -->
    <script src="../app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="../app-assets/js/scripts/configs/vertical-menu-light.js"></script>
    <script src="../app-assets/js/core/app-menu.js"></script>
    <script src="../app-assets/js/core/app.js"></script>
    <script src="../app-assets/js/scripts/components.js"></script>
    <script src="../app-assets/js/scripts/footer.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="../app-assets/js/scripts/pages/dashboard-ecommerce.js"></script>
    <!-- END: Page JS-->



    <?php

//to block a user withdrawal
if(isset($_POST['disable-mining'])){


$block = mysqli_query($mysqli,"UPDATE settings SET automatic=o");

if($block){

  ?>
<script>


Swal.fire({
        title: 'Mining Disabled',
        text: 'Mining Disabled on System ',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })

setTimeout(() => {
    location = location;
}, 3000);
</script>

<?php

}
//successful

}


//to enable a user withdrawal
if(isset($_POST['enable-mining'])){


$block = mysqli_query($mysqli,"UPDATE settings SET automatic=1 ");

if($block){ 

  ?>
<script>


Swal.fire({
        title: 'Mining Enabled',
        text: 'Mining Enabled on System ',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#626ed4',
        cancelButtonColor: "#ec4561"
    })

setTimeout(() => {
    location = location;
}, 3000);
</script>

<?php

}
//successful

}


?>
</body>
<!-- END: Body-->

</html>