<?php
session_start();

include('../connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['adminid'])){
	
    header("location:index");
    
}


$get_admin = mysqli_query($mysqli,"SELECT * FROM admins WHERE id='".$_SESSION['adminid']."' ");
$rows = mysqli_fetch_assoc($get_admin);



?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <title>Email - Argen Capital </title>
    <link rel="apple-touch-icon" href="../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="../app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700"
        rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/sweetalert2.min.css">
    <!-- END: Theme CSS-->
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.css">

    <link rel="stylesheet" href="choices.min.css">

    <script src="choices.min.js"></script>
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-sticky footer-static  " data-open="click"
    data-menu="vertical-menu-modern" data-col="2-columns">

    <?php include('header.php'); ?>

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Send email</h5>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb p-0 mb-0">
                                    <li class="breadcrumb-item"><a href="index"><i class="bx bx-home-alt"></i></a>
                                    </li>
                                    <li class="breadcrumb-item active">mail
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mt-0 header-title">Type Mail</h4>
                                <form method="POST" enctype="multipart/form-data">



                                    <div class="form-group row"><label class="col-sm-2 col-form-label">Select
                                            User</label>
                                        <div class="col-sm-10"><select id="demo-2" multiple name="email[]"
                                                class="custom-select custom-select-lg mb-3"
                                                placeholder="Select only All(everyone) to send to everybody at once or pick specfic emails">
                                                <option value="all"> All (everyone)</option>
                                                <?php
                                    $getuser = mysqli_query($mysqli,"SELECT * FROM users");

                                    while($row = mysqli_fetch_assoc($getuser)){
                                    ?>
                                                <option value="<?php echo $row['email']; ?>">
                                                    <?php echo $row['firstname']."(".$row['email'].")"; ?></option>

                                                <?php
                                    }
                                    ?>

                                            </select></div>
                                    </div>

                                    <div class="form-group row"><label class="col-sm-2 col-form-label">Select
                                            Email</label>
                                        <div class="col-sm-10"><select name="support_email" class="form-control">
                                                <option value="info@argencapital.com">info@argencapital.com</option>
                                                <option value="support@argencapital.com">support@argencapital.com
                                                </option>

                                            </select></div>
                                    </div>







                                    <div class="form-group row"><label for="example-email-input"
                                            class="col-sm-2 col-form-label">Subject</label>
                                        <div class="col-sm-10"><input class="form-control" name="title" type="text"
                                                required></div>
                                    </div>

                                    <div class="form-group row"><label for="example-email-input"
                                            class="col-sm-2 col-form-label">Enter Message</label>
                                        <div class="col-sm-10"><textarea id="message" class="form-control"
                                                name="message" required></textarea></div>
                                    </div>
                                    <script>
                                    CKEDITOR.replace('message');
                                    </script>




                                    <div><button type="submit" class="btn btn-primary " name="send">Send Email</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>



                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mt-0 header-title">Individual Mail</h4>
                                <form method="POST" enctype="multipart/form-data">



                                    <div class="form-group row"><label class="col-sm-2 col-form-label">Type
                                            User Email</label>
                                        <div class="col-sm-10"><input name="email" class="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group row"><label class="col-sm-2 col-form-label">Select
                                            Email</label>
                                        <div class="col-sm-10"><select name="support_email" class="form-control">
                                                <option value="info@argencapital.com">info@argencapital.com</option>
                                                <option value="support@argencapital.com">support@argencapital.com
                                                </option>

                                            </select></div>
                                    </div>







                                    <div class="form-group row"><label for="example-email-input"
                                            class="col-sm-2 col-form-label">Subject</label>
                                        <div class="col-sm-10"><input class="form-control" name="title" type="text"
                                                required></div>
                                    </div>

                                    <div class="form-group row"><label for="example-email-input"
                                            class="col-sm-2 col-form-label">Enter Message</label>
                                        <div class="col-sm-10"><textarea id="message2" class="form-control"
                                                name="message" required></textarea></div>
                                    </div>
                                    <script>
                                    CKEDITOR.replace('message2');
                                    </script>




                                    <div><button type="submit" class="btn btn-primary " name="send2">Send Email</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>








                </div>
            </div>
        </div>
        <!-- END: Content-->



        <!-- BEGIN: Footer-->
        <footer class="footer footer-static footer-light">
            <p class="clearfix mb-0"><span class="float-left d-inline-block">2021 &copy; </span></span>
                <button class="btn btn-primary btn-icon scroll-top" type="button"><i
                        class="bx bx-up-arrow-alt"></i></button>
            </p>
        </footer>
        <!-- END: Footer-->


        <!-- BEGIN: Vendor JS-->
        <script src="../app-assets/vendors/js/vendors.min.js"></script>
        <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
        <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
        <script src="../app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="../app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
        <script src="../app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="../app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
        <script src="../app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
        <script src="../app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
        <script src="../app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
        <script src="../app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
        <script src="../app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
        <script src="../app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="../app-assets/js/scripts/configs/vertical-menu-light.js"></script>
        <script src="../app-assets/js/core/app-menu.js"></script>
        <script src="../app-assets/js/core/app.js"></script>
        <script src="../app-assets/js/scripts/components.js"></script>
        <script src="../app-assets/js/scripts/footer.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="../app-assets/js/scripts/datatables/datatable.js"></script>
        <!-- END: Page JS-->

        <script>
        var secondElement = new Choices('#demo-2', {
            allowSearch: true
        });
        </script>





        <?php

if(isset($_POST['send'])){

    $email = $_POST['email'];
	$support_email = $_POST['support_email'];
    $title = $_POST['title'];
    $message = $_POST['message'];

    echo $email;

    //main code now
foreach($_POST['email'] as $oneemail) //loop over values
{

    if($oneemail != "all"){


$getuser = mysqli_query($mysqli,"SELECT * FROM users WHERE email='$oneemail'");
        $g = mysqli_fetch_assoc($getuser);
//start email sending
          
	
			//construct and structure the way the welcome message will look
	//include the structure
	include_once("email_structure.php");
		
		$actualmessage = welcome_mail($g['firstname'], $message, $title);
		
		//echo $actualmessage;
		
		
		
		//next is the smtp message attributes and sending the message den insert message into database
	
	include_once('mailer/class.phpmailer.php');
//include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

$mail             = new PHPMailer();


$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host       = "argencapital.com"; // SMTP server
$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                           // 1 = errors and messages
                                           // 2 = messages only
$mail->SMTPAuth   = true;                  // enable SMTP authentication

$mail->Port       = 465;                    // set the SMTP port for the GMAIL server
$mail->Username   = "info@argencapital.com"; // SMTP account username
$mail->Password   = "UP=@iv1Bz6lX";        // SMTP account password

$mail->SetFrom('info@argencapital.com', "Argen Capital");

$mail->AddReplyTo('info@argencapital.com', "Argen Capital");

$mail->IsHTML =(true);

$mail->Subject    = $title;

$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);

$mail->SMTPSecure = 'ssl';

$mail->MsgHTML($actualmessage);

//destination for email.

$mail->AddAddress($oneemail, "Argen Capital");


if(!$mail->Send()) {
  $_SESSION['MSG']="Mailer Error: " . $mail->ErrorInfo;
} else {
	
	$_SESSION['update_msg'] = "Email Sent";
	
}
	
	
	
	
		
	
	
}else{

//get all user 
        $getuser = mysqli_query($mysqli,"SELECT * FROM users");

        while($g=mysqli_fetch_assoc($getuser)){

          
	
	//start email sending
          
	
			//construct and structure the way the welcome message will look
	//include the structure
	include_once("email_structure.php");
		
		$actualmessage = welcome_mail($g['firstname'], $message, $title);
		
		//echo $actualmessage;
		
		
		
		//next is the smtp message attributes and sending the message den insert message into database
	
	include_once('mailer/class.phpmailer.php');
//include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

$mail             = new PHPMailer();


$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host       = "argencapital.com"; // SMTP server
$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                           // 1 = errors and messages
                                           // 2 = messages only
$mail->SMTPAuth   = true;                  // enable SMTP authentication

$mail->Port       = 465;                    // set the SMTP port for the GMAIL server
$mail->Username   = "info@argencapital.com"; // SMTP account username
$mail->Password   = "UP=@iv1Bz6lX";        // SMTP account password

$mail->SetFrom('info@argencapital.com', "Argen Capital");

$mail->AddReplyTo('info@argencapital.com', "Argen Capital");

$mail->IsHTML =(true);

$mail->Subject    = $title;

$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);

$mail->SMTPSecure = 'ssl';

$mail->MsgHTML($actualmessage);

//destination for email.

$mail->AddAddress($g['email'], "Argen Capital");


if(!$mail->Send()) {
  $_SESSION['MSG']="Mailer Error: " . $mail->ErrorInfo;
} else {
	
	$_SESSION['update_msg'] = "Email Sent";
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

        }


}


//end of main
}
    


     ?>

        <script>
        Swal.fire({
            title: 'Email Successfull',
            text: 'Email has been sent',
            type: 'success',
            showCancelButton: true,
            confirmButtonColor: '#626ed4',
            cancelButtonColor: "#ec4561"
        })
        </script>

        <?php


}





if(isset($_POST['send2'])){

    $email = $_POST['email'];
	$support_email = $_POST['support_email'];
    $title = $_POST['title'];
    $message = $_POST['message'];


    //start email sending
          
	
$getuser = mysqli_query($mysqli,"SELECT * FROM users WHERE email='$email'");
$g = mysqli_fetch_assoc($getuser);
			//construct and structure the way the welcome message will look
	//include the structure
	include_once("email_structure.php");
		
    $actualmessage = welcome_mail($g['firstname'], $message, $title);
    
    //echo $actualmessage;
    
    
    
    //next is the smtp message attributes and sending the message den insert message into database

include_once('mailer/class.phpmailer.php');
//include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

$mail  = new PHPMailer();


$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host       = "argencapital.com";   // SMTP server
$mail->SMTPDebug  = 1;                 // enables SMTP debug information (for testing)
                                       // 1 = errors and messages
                                       // 2 = messages only
$mail->SMTPAuth   = true;                  // enable SMTP authentication

$mail->Port       = 465;                    // set the SMTP port for the GMAIL server
$mail->Username   = "info@argencapital.com"; // SMTP account username
$mail->Password   = "UP=@iv1Bz6lX";        // SMTP account password

$mail->SetFrom('info@argencapital.com', "Argen Capital");

$mail->AddReplyTo('info@argencapital.com', "Argen Capital");

$mail->IsHTML =(true);

$mail->Subject    = $title;

$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

$mail->SMTPOptions = array(
'ssl' => array(
    'verify_peer' => false,
    'verify_peer_name' => false,
    'allow_self_signed' => true
)
);

$mail->SMTPSecure = 'ssl';

$mail->MsgHTML($actualmessage);

//destination for email.

$mail->AddAddress($email, "Argen Capital");


if(!$mail->Send()) {
$_SESSION['MSG']="Mailer Error: " . $mail->ErrorInfo;
} else {

$_SESSION['update_msg'] = "Email Sent";

}

?>

<script>

Swal.fire({
       title: 'Email Successfull',
       text: 'Email has been sent',
       type: 'success',
       showCancelButton: true,
       confirmButtonColor: '#626ed4',
       cancelButtonColor: "#ec4561"
   })



</script>

<?php



}


?>




</body>
<!-- END: Body-->

</html>