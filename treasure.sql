CREATE TABLE `users` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `firstname` varchar(255) NOT NULL DEFAULT '',
  `secondname` varchar(255) NOT NULL DEFAULT '',
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `country` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(255) NOT NULL DEFAULT '',
  `bankname` varchar(255) NOT NULL DEFAULT '',
  `account_num` varchar(255) NOT NULL DEFAULT '',
  `account_name` varchar(255) NOT NULL DEFAULT '',
  `bitcoin` varchar(255) NOT NULL DEFAULT '',
  `tron` varchar(255) NOT NULL DEFAULT '',
  `paypal` varchar(255) NOT NULL DEFAULT '',
  `idcard` varchar(255) NOT NULL DEFAULT '',
  `sent` int(255) NOT NULL DEFAULT '0' ,
  `recieve` int(255) NOT NULL DEFAULT '0' ,
  `2fa` int(255) NOT NULL DEFAULT '0',
  `2fa_key` varchar(255) NOT NULL DEFAULT '',
  `2fa_link` varchar(255) NOT NULL DEFAULT '',
  `img` varchar(255) NOT NULL DEFAULT '',
  `smscode` varchar(255) NOT NULL DEFAULT '',
  `forgot` varchar(255) NOT NULL DEFAULT '',
  `referal_link` varchar(255) NOT NULL DEFAULT '',
  `referred` varchar(255) NOT NULL DEFAULT '',
  `refer_date` varchar(255) NOT NULL DEFAULT '',
  `refer_bouns` int(255) NOT NULL DEFAULT '0' ,
  `wallet` int(255) NOT NULL DEFAULT '0' ,
  `cycle` int(255) NOT NULL DEFAULT '0' ,
  `system_hold` varchar(255) NOT NULL DEFAULT '0',
  `pay_refer` int(255) NOT NULL DEFAULT '0' ,
  `level_command` int(255) NOT NULL DEFAULT '0' ,
  `num_donation` int(255) NOT NULL DEFAULT '0',
  `active` int(255) NOT NULL DEFAULT '0',
  `can_donate` int(255) NOT NULL DEFAULT '0',
  `userstatus` int(255) NOT NULL DEFAULT '0',
  `status` int(255) NOT NULL DEFAULT '0' 
);



CREATE TABLE `activity` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `userid` int(255) NOT NULL DEFAULT '0',
  `action` varchar(255) NOT NULL DEFAULT '',
  `describe` varchar(255) NOT NULL DEFAULT '',
  `date` varchar(255) NOT NULL DEFAULT '',
  `amount` int(255) NOT NULL DEFAULT '0',
  `status` varchar(255) NOT NULL DEFAULT '0'
);




CREATE TABLE `investment` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `userid` varchar(255) NOT NULL DEFAULT '',
  `investmentid` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `amount` int(255) NOT NULL DEFAULT '0',
  `daily_roi` int(255) NOT NULL DEFAULT '0',
  `added_roi` int(255) NOT NULL DEFAULT '0',
  `duration` varchar(255) NOT NULL DEFAULT '',
  `date` varchar(255) NOT NULL DEFAULT '',
  `status` int(255) NOT NULL DEFAULT '1'
  
);


CREATE TABLE `investment_packages` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `min_amount` int(255) NOT NULL DEFAULT '0',
  `max_amount` int(255) NOT NULL DEFAULT '0',
  `percent` varchar(255) NOT NULL DEFAULT '',
  `duration` varchar(255) NOT NULL DEFAULT '',
 
  `status` int(255) NOT NULL DEFAULT '1'
  
);


CREATE TABLE `withdrawal` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `userid` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `date` varchar(255) NOT NULL DEFAULT '',
  
  `amount` int(255) NOT NULL DEFAULT '0',
  
  `status` int(255) NOT NULL DEFAULT '1'
  
);

CREATE TABLE `withdrawal_method` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '0',
  `min_amount` int(255) NOT NULL DEFAULT '0',
  `max_amount` int(255) NOT NULL DEFAULT '0',
  `charge` int(255) NOT NULL DEFAULT '0',
  `status` int(255) NOT NULL DEFAULT '1'
  
);















CREATE TABLE `settings` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `bankname` varchar(255) NOT NULL DEFAULT '',
  `account_num` varchar(255) NOT NULL DEFAULT '',
  `account_name` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `automatic` int(255) NOT NULL DEFAULT '1'
  
);


CREATE TABLE `video` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `userid` varchar(255) NOT NULL DEFAULT '',
  `investmentid` varchar(255) NOT NULL DEFAULT '',
  `videolink` varchar(255) NOT NULL DEFAULT '',
  `status` int(255) NOT NULL DEFAULT '0'
  
);


CREATE TABLE `pop` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `payerid` varchar(255) NOT NULL DEFAULT '',
  `receiverid` varchar(255) NOT NULL DEFAULT '',
  `amount` varchar(255) NOT NULL DEFAULT '',
  `img` varchar(255) NOT NULL DEFAULT '',
  `payerinvestment` varchar(255) NOT NULL DEFAULT '',
  `receiverinvestment` varchar(255) NOT NULL DEFAULT '',
  `status` int(255) NOT NULL DEFAULT '0'
);


CREATE TABLE `referal` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `claimerid` int(255) NOT NULL DEFAULT '0',
  `status` int(255) NOT NULL DEFAULT '0',
  `date` varchar(255) NOT NULL DEFAULT '',
  `amount` int(255) NOT NULL DEFAULT '0',
  `detail` varchar(255) NOT NULL DEFAULT ''
);



CREATE TABLE `message` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `userid` int(255) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `messages`  varchar(255) NOT NULL DEFAULT '',
  `has_read` int(255) NOT NULL DEFAULT '1',
  `status` int(255) NOT NULL DEFAULT '1'
  
);
