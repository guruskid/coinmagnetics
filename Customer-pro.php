<?php  
include('account/connection.php');

?>
<!DOCTYPE html>
<html lang="zxx">

<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- Title -->
	<title>Coin Magnetics - Terms</title>
	<!-- Favicon -->
	<link href="img/core-img/favicon.png" rel="icon" /><!-- Core Stylesheet -->
	<link href="css/style.css" rel="stylesheet" /><!-- Responsive Stylesheet -->
	<link href="css/responsive.css" rel="stylesheet" />
	<style>
@media (max-width: 768px) {
    .carousel-inner .carousel-item > div {
        display: none;
    }
    .carousel-inner .carousel-item > div:first-child {
        display: block;
    }
}

.carousel-inner .carousel-item.active,
.carousel-inner .carousel-item-next,
.carousel-inner .carousel-item-prev {
    display: flex;
}

/* display 3 */
@media (min-width: 768px) {
    
    .carousel-inner .carousel-item-right.active,
    .carousel-inner .carousel-item-next {
      transform: translateX(33.333%);
    }
    
    .carousel-inner .carousel-item-left.active, 
    .carousel-inner .carousel-item-prev {
      transform: translateX(-33.333%);
    }
}

.carousel-inner .carousel-item-right,
.carousel-inner .carousel-item-left{ 
  transform: translateX(0);
}


</style>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5dde42de43be710e1d1f5485/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</head>

<body class="light-version">
	<!-- Preloader -->
	<div id="preloader">
		<div class="preload-content">
			<div id="dream-load"></div>
		</div>
	</div>
	<!-- ##### Header Area Start ##### -->

<header class="header-area fadeInDown" data-wow-delay="0.2s">
        <div class="classy-nav-container light breakpoint-off">
            <div class="container">
                <!-- Classy Menu -->
                <nav class="classy-navbar light justify-content-between" id="dreamNav">

                    <!-- Logo -->
                    <a class="nav-brand light" href="#"><img src="./logo2.png" style="width: 150px; height: 30px;" /></a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler demo">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- close btn -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul id="nav">
                             <li><a href="https://coinmagnetics.com">Home</a></li>
								
								<li><a href="https://coinmagnetics.com/about">About</a></li>
								<li><a href="#services">Features</a></li>
								<li><a href="https://coinmagnetics.com/faq">FAQ</a></li>
								<li><a href="https://coinmagnetics.com/contact">Contact</a></li>
								<li>
								<style type="text/css">

a.gflag {vertical-align:middle;font-size:16px;padding:1px 0;background-repeat:no-repeat;background-image:url(//gtranslate.net/flags/16.png);}
a.gflag img {border:0;}
a.gflag:hover {background-image:url(//gtranslate.net/flags/16a.png);}
#goog-gt-tt {display:none !important;}
.goog-te-banner-frame {display:none !important;}
.goog-te-menu-value:hover {text-decoration:none !important;}
body {top:0 !important;}
#google_translate_element2 {display:none!important;}

</style>


<br /><select onchange="doGTranslate(this);"  ><option value="">Select Language</option><option value="en|en">English</option><option value="en|hy">Armenian</option><option value="en|az">Azerbaijani</option><option value="en|eu">Basque</option><option value="en|be">Belarusian</option><option value="en|ca">Catalan</option><option value="en|zh-CN">Chinese (Simplified)</option><option value="en|nl">Dutch</option><option value="en|fr">French</option><option value="en|de">German</option><option value="en|pt">Portuguese</option><option value="en|ru">Russian</option><option value="en|es">Spanish</option></select><div style="display:none" id="google_translate_element2"></div>
<script type="text/javascript">
function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'en',autoDisplay: false}, 'google_translate_element2');}
</script><script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>


<script type="text/javascript">
/* <![CDATA[ */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,f,f);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}',43,43,'||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'),0,{}))
/* ]]> */
</script>


								</li>
							</ul>
							<!-- Button --><a class="btn login-btn ml-50"
								href="https://coinmagnetics.com">Log in</a>
						</div>
						<!-- Nav End -->
					</div>
				</nav>
			</div>
		</div>
	</header>
	<!-- ##### Header Area End ##### -->

	<!-- ##### About Us Area Start ##### -->

	<section class="faq-timeline-area  section-padding-150-0 clearfix " id="about">
		<div class="container-fluid h-100">
		    				<div class="row h-100 align-items-center">
			<div class="row align-items-center">
				<div class="col-12 col-lg-5 offset-lg-1 col-sm-10 offset-sm-1">
					<div class="who-we-contant">
					    
					    <h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Tips to keep your account safe<strong></h2>
					    
					    <h5 class="fadeInUp" data-wow-delay="0.3s">Know how to keep your account from getting compromised.</h4>
					    
						<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s"
							style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Last updated: August 2021</span></div>

					
						<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics follows strict internal security practices aimed at 
						keeping your cryptocurrency safe, which we’ve detailed here. It is however, important that you also take certain 
						precautions to ensure that you are the only person with access to your account. If someone gains access to your account, 
						your cryptocurrency may be at risk. Some of the most common methods that scammers may use to gain access to and perform 
						unauthorized transactions on your account are explained below:.</h6>
						
						<h6 class="fadeInUp" data-wow-delay="0.3s">Please read these Terms carefully before using the 
						Coin Magnetics Site because they affect your legal rights and obligations.</h6>
						
						<h6 class="fadeInUp" data-wow-delay="0.3s">Some of the most common methods that scammers may use to gain access 
						to and perform unauthorized transactions on your account are explained below:</h6>
						
						<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Account takeovers:<strong></h6>
						
						<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics relies on your email address and phone number for 
						communication with you. If someone gains unauthorized control over these, they could potentially gain access 
						to your Coin Magnetics account. It is therefore, important to make sure that you have strong security 
						on your email and Coin Magnetics accounts</h6>
						
						<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Phishing:<strong></h6>

						<h6 class="fadeInUp" data-wow-delay="0.3s">This is when you are tricked into revealing personal information, 
						such as usernames, passwords or other sensitive information. Phishing can take many forms, for example:</h6>
						
						<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>•	Emails:<strong></h6> 
						
						<h6 class="fadeInUp" data-wow-delay="0.3s">You may receive an email that appears to be from Coin Magnetics 
						but was actually sent by a scammer, asking you for information or leading you to a fraudulent website.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>•	Phishing websites: <strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">These websites are set up to look like Coin Magnetics’s website, but everything 
entered there (like your username and password) gets recorded in order for scammers to access your actual 
Coin Magnetics account. In some cases, these websites are promoted by running adverts that look 
like they’re taking you to the legitimate Coin Magnetics website.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>•	Phone scams:<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Someone may call you, pretending to be from Coin Magnetics, asking 
you to provide your username, password and other information in order to access your Coin Magnetics account.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>•	SMS / instant messaging scams:<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">In this case a fraudster will send you a message purporting to be 
from Coin Magnetics and prompting you to share your username, password or other information, or to select a link.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics will never call, email or SMS/instant message you and ask you 
for your password. If anyone contacts you claiming to be from Coin Magnetics and asks for this information, 
you should end communication and get in touch with us by contacting support yourself.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Malware:<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">This is an advanced method where scammers gain access to your devices through 
sophisticated software programs that allow them to take control of your computer, steal your passwords or other sensitive information.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Identity theft:<strong></h6> 

<h6 class="fadeInUp" data-wow-delay="0.3s">This is when someone obtains access to your personal information and pretends 
to be you in order to, for example, open an account in your name and/or transact on your account. 
Identity theft may occur when your identification document, passport or other information is stolen 
(either physically, through phishing or by other forms of social engineering). There is also a risk of 
identity theft taking place when your device is stolen.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>Measures you should take<strong></h4> 

<h6 class="fadeInUp" data-wow-delay="0.3s">Transactions in our Supported Cryptocurrencies are generally irreversible, 
which means that losses due to fraudulent or accidental transactions are not recoverable. It is therefore very important that 
you understand how to identify and avoid falling victim to these types of attacks. Here are some precautions you can take:</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>•	Strong password security:<strong></h6>  

<h6 class="fadeInUp" data-wow-delay="0.3s">Use a unique, strong password with at least 16 characters and never share it or reuse it on other websites. 
Password managers make it easy to generate and store strong passwords. Coin Magnetics recommends Last Pass, but there are several alternatives.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>•	Enable two-factor authentication (2FA):<strong></h6>  

<h6 class="fadeInUp" data-wow-delay="0.3s">This is an added level of security that requires a unique code every time you log in to an account. 
The code is generated by your device, which is required in addition to your username and password.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>•	Protect your email account:<strong></h6>  

<h6 class="fadeInUp" data-wow-delay="0.3s">Keep your email account secure to ensure that nobody can access your Coin Magnetics account through your email account. Most email providers support 2FA.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>•	Don’t share sensitive information:<strong></h6>  

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics employees will never ask you for your password, 2FA code, one time pins (OTPs) or ask you to authorize transactions.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>•	Check the URL:<strong></h6>  

<h6 class="fadeInUp" data-wow-delay="0.3s">Before signing into Coin Magnetics or clicking an advert, ensure that you are on www.Coinmagnetics.com</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>•	Check the email domain:<strong></h6>  

<h6 class="fadeInUp" data-wow-delay="0.3s">Any communication from Coin Magnetics will come from a @Coinmagnetics.com or @Coinmagnetics.mailer.com address.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>•	Lock your account:<strong></h6>  

<h6 class="fadeInUp" data-wow-delay="0.3s">If you suspect your account has been compromised, lock it immediately and then contact support.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">We encourage you to visit our Learning Portal where you can 
learn more about securing your Coin Magnetics account, and how to avoid being scammed. Also keep an 
eye on our monthly newsletter as well as our blog where we publish topical information</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>If your account has been compromised<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">If you believe your account has been compromised, please let us know by submitting a ticket to us as soon as possible.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics cannot be held responsible for a loss of funds where you have 
fallen victim to one of these attacks, as set out in our Terms of use, but we will do everything we can to assist you.</h6>
						
				


					</div>
				</div>
	</section>

	<div class="clearfix"></div>
	<!-- ##### Our Trial Area End ##### -->

	<section class="about-us-area  section-padding-100-0 gradient-section-bg clearfix " id="affiliate">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-12 col-lg-5 offset-lg-1 col-sm-10 offset-sm-1">
					<div class="who-we-contant">
						<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s"
							style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
							<span>Get Started</span></div>

						<h4 class="fadeInUp" data-wow-delay="0.3s">It’s never too late to get started</h4>

						<p class="text-white">Trade, Buy, Sell, Swap, Mine and Save All Digtal Assets</p>
							
						<div class="clearfix"></div>
						<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><href="#home"><img alt="" src="./ic_website_sign_up.svg" style="width: 120px; height: 25px;" /></a>
							
								<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-google.svg" style="width: 120px; height: 25px;" /></a>
							
							<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-apple.svg" style="width: 120px; height: 25px;" /></a>
					</div>
				</div>

				<div class="col-12 col-lg-6 mt-s no-padding-right">
					<div class="welcome-meter fadeInUp" data-wow-delay="0.7s"><img alt=""
							class="img-responsive center-block" src="img/core-img/trading1.png" style="width: 460px; height: 260px;" /></div>
				</div>
			</div>
		</div>

			<!-- ##### Contact Area End ##### -->

			<div class="footer-content-area ">
				<div class="container">
					<div class="row ">
						<div class="col-12 col-lg-4 col-md-6">
							<div class="footer-copywrite-info">
								<!-- Copywrite -->
								<div class="copywrite_text fadeInUp" data-wow-delay="0.2s">
									<div class="footer-logo"><a href="#"><img alt="logo" src="./logo2.png"
												style="width: 230px; height: 40px;" /></a></div>

									<p>Plug-in to World's N0 1 Digital Asset management system today and Start Earning Big!.
									</p>
								</div>
								<!-- Social Icon -->

								<div class="footer-social-info fadeInUp" data-wow-delay="0.4s"></div>
							</div>
						</div>

						<div class="col-12 col-lg-3 col-md-6">
							<div class="contact_info_area d-sm-flex justify-content-between">
								<!-- Content Info -->
								<div class="contact_info mt-x text-center fadeInUp" data-wow-delay="0.3s">
									<h5>PRIVACY &amp; TOS</h5>

									<p><a href="#">Advertiser Agreement</a></p>

									<p><a href="https://coinmagnetics.com/terms">Terms Of Use</a></p>

									<p><a href="https://coinmagnetics.com/Privacy-policy">Privacy Policy</a></p>

									<p><a href="https://coinmagnetics.com/compliance">Legal & Compliance</a></p>

									<p><a href="https://coinmagnetics.com/Customer-pro">Customer protection</a></p>
								</div>
							</div>
						</div>

						<div class="col-12 col-lg-2 col-md-6 ">
							<!-- Content Info -->
							<div class="contact_info_area d-sm-flex justify-content-between">
								<div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.2s">
									<h5>NAVIGATE</h5>

									<p><a href="https://coinmagnetics.com/about">About Us</a></p>

									<p><a href="#">Developers</a></p>

									<p><a href="#">Resources</a></p>

									<p><a href="https://coinmagnetics.com/faq">FAQ</a></p>

									<p><a href="https://coinmagnetics.com/contact">Contact Us</a></p>
								</div>
							</div>
						</div>

						<div class="col-12 col-lg-3 col-md-6 ">
							<div class="contact_info_area d-sm-flex justify-content-between">
								<!-- Content Info -->
								<div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.4s">
									<h5>FIND US HERE</h5>

									<p>Mailing Address:265 Akowonjo RD,</p>

									<p>Lagos, Nigeria. Los 23401</p>

									
									<p>Email: support@coinmagnetics.com</p>
									
										<div class="dream-btn-group fadeInUp" data-wow-delay="0.4s"><a
									href="https://t.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/tg.png"
										style="width: 43px; height: 40px; float: left;" /></a>&nbsp; &nbsp;&nbsp;
								&nbsp;<a href="https://www.instagram.com/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/ig.png" style="width: 42px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp; &nbsp;<a href="https://twitter.com/Coinmagnetics"><img alt=""
										src="/img/social/tw.png" style="width: 43px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp;&nbsp;<a href="https://fb.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/fb.png" style="width: 43px; height: 40px;" /></a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- ##### Footer Area End ##### -->
		<!-- ########## All JS ########## -->
		<!-- jQuery js -->
		<script src="js/jquery.min.js"></script><!-- Popper js -->
		<script src="js/popper.min.js"></script><!-- Bootstrap js -->
		<script src="js/bootstrap.min.js"></script><!-- All Plugins js -->
		<script src="js/plugins.js"></script><!-- Parallax js -->
		<script src="js/dzsparallaxer.js"></script>
		<script src="js/jquery.syotimer.min.js"></script><!-- script js -->
		<script src="js/script.js"></script>
	</div>
</body>
<script>
$('#recipeCarousel').carousel({
  interval: 4000
})

$('.carousel .carousel-item').each(function(){
    var minPerSlide = 3;
    var next = $(this).next();
    if (!next.length) {
    next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
    
    for (var i=0;i<minPerSlide;i++) {
        next=next.next();
        if (!next.length) {
        	next = $(this).siblings(':first');
      	}
        
        next.children(':first-child').clone().appendTo($(this));
      }
});

	</script>

</html>