<?php
session_start();

include('connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['id'])){
	
	header("location:index");
}else{

$get_user = mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$_SESSION['id']."' ");
$rows = mysqli_fetch_assoc($get_user);
    if(isset($_SESSION['2fa'])){

        if( ($_SESSION['2fa'] =="no" or $_SESSION['2fa'] =="pending") and $rows['2fa']==1){
            header("location:index");
        }


    }


}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
    <title>Change Password - Treasure Capital</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="assets/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="assets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/feather-icon.css">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="assets/css/datatables.css">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link id="color" rel="stylesheet" href="assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
  </head>
  <body class="dark-sidebar dark-only" >
    <!-- tap on top starts-->
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>
    <!-- tap on tap ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
      <!-- Page Header Start-->
     <?php include('header.php'); ?>
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper sidebar-icon">
        <!-- Page Sidebar Start-->
      <?php include('sidebar.php'); ?>
        <!-- Page Sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col-6">
                  <h3>Change Password</h3>
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="dashboard"><i data-feather="home"></i></a></li>
                    <li class="breadcrumb-item">password</li>
                  </ol>
                </div>
                <div class="col-6">
                  <!-- Bookmark Start-->
                 
                  <!-- Bookmark Ends-->
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            <div class="row">
              <!-- Zero Configuration  Starts-->
              <div class="col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h5>Change Password</h5>
                  </div>
                  <div class="card-body">

                  <form method="POST">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <h5>Former Password<span class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="password" name="former"
                                                                    class="form-control" required
                                                                    data-validation-required-message="This field is required">
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <h5>New Password<span class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="password" name="new" class="form-control"
                                                                    required
                                                                    data-validation-required-message="This field is required">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <h5>Confirm Password <span class="text-danger">*</span></h5>
                                                            <div class="controls">
                                                                <input type="password" name="confirm"
                                                                    data-validation-match-match="password"
                                                                    class="form-control" required>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="text-xs-right">
                                                    <button type="submit" name="update-pass"
                                                        class="btn btn-rounded btn-info">Update Password</button>
                                                </div>
                                            </form>

                    


                  </div>
                </div>
              </div>
              <!-- Zero Configuration  Ends-->
          
            </div>
          </div>
          <!-- Container-fluid Ends-->
        </div>
        <!-- footer start-->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0">Copyright <?php echo date('Y'); ?> © Treasure Capital All rights reserved.</p>
              </div>
              <div class="col-md-6">
                <p class="pull-right mb-0"> </p>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- latest jquery-->
    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap js-->
    <script src="assets/js/bootstrap/popper.min.js"></script>
    <script src="assets/js/bootstrap/bootstrap.js"></script>
    <!-- feather icon js-->
    <script src="assets/js/icons/feather-icon/feather.min.js"></script>
    <script src="assets/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
    <script src="assets/js/sidebar-menu.js"></script>
    <script src="assets/js/config.js"></script>
    <!-- Plugins JS start-->
    <script src="assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/js/datatable/datatables/datatable.custom.js"></script>
    <script src="assets/js/tooltip-init.js"></script>
    <script src="assets/js/sweet-alert/sweetalert.min.js"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="assets/js/script.js"></script>

    <!-- login js-->
    <!-- Plugin used-->
  </body>



  <?php
  //code to update two factor authenication
  if(isset($_POST['update-pass'])){
  
  $former = mysqli_real_escape_string($mysqli, $_POST['former']);
  $new = mysqli_real_escape_string($mysqli, $_POST['new']);
  $confirm = mysqli_real_escape_string($mysqli, $_POST['confirm']);
  
  $newx = password_hash($new, PASSWORD_DEFAULT);
  
  if($new == $confirm and password_verify($former,$rows['password'])){
  
  //password match procced
  $changepass = mysqli_query($mysqli,"UPDATE users SET password ='$newx' WHERE id='".$rows['id']."' ");
  
  if($changepass){
  
  ?>
<script>
swal('Password Changed Successfully',
    'Your password has been updated.', 'success')
</script>

<?php
  
  }
    
  
  }else{
  
  ?>
<script>
swal('Password do not match',
    'Or former password is incorrect..', 'warning')
</script>


<?php
  
  }
  
  
  
  }
  
  
  
  ?>

</html>