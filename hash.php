<?php
session_start();

include('connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['id'])){
	
	header("location:index");
}else{

$get_user = mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$_SESSION['id']."' ");
$rows = mysqli_fetch_assoc($get_user);
    if(isset($_SESSION['2fa'])){

        if( ($_SESSION['2fa'] =="no" or $_SESSION['2fa'] =="pending") and $rows['2fa']==1){
            header("location:index");
        }


    }


}

if($rows['cycle']==1){ 

  header("location:dashboard");

}

?>
<!DOCTYPE html>
<html lang="en">
  

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    <meta name="author" content="treasure">
    <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
    <title>Rent Mining Pool - Coin Magnetics</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="assets/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="assets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/feather-icon.css">
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.css">
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link id="color" rel="stylesheet" href="assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
  </head>
  <body class="dark-sidebar dark-only">
    <!-- tap on top starts-->
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>
    <!-- tap on tap ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
      <!-- Page Header Start-->
     <?php include('header.php'); ?>
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper sidebar-icon">
        <!-- Page Sidebar Start-->
        <?php include('sidebar.php'); ?>
        <!-- Page Sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col-6">
                  <h3>Rent Mining Pool</h3>
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="dashboard"><i data-feather="home"></i></a></li>
                    <li class="breadcrumb-item">Mining Pool   </li>
                  </ol>
                </div>
                <div class="col-6">
                  <!-- Bookmark Start-->
                
                  <!-- Bookmark Ends-->
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            <div class="row">
           
           
              <div class="col-sm-12">
                <div class="card">
                  <div class="card-header"> 
                    <h5>Select Mining Pool</h5>
                  </div>
                  <div class="card-body pricing-card-design-3">
                    <div class="row pricing-content-ribbons">

                    <?php  
                      $get = mysqli_query($mysqli,"SELECT * FROM investment_packages");

                      while($row=mysqli_fetch_assoc($get)){
                    ?>
                    
                      <div class="col-md-6">
                     <div class="ribbon ribbon-bookmark ribbon-danger">Popular</div> 
                        <div class="pricing-block card text-center ">
                          <svg x="0" y="0" viewBox="0 0 360 220">
                            <g>
                              <path fill="#7366ff" d="M0.732,193.75c0,0,29.706,28.572,43.736-4.512c12.976-30.599,37.005-27.589,44.983-7.061                                          c8.09,20.815,22.83,41.034,48.324,27.781c21.875-11.372,46.499,4.066,49.155,5.591c6.242,3.586,28.729,7.626,38.246-14.243                                          s27.202-37.185,46.917-8.488c19.715,28.693,38.687,13.116,46.502,4.832c7.817-8.282,27.386-15.906,41.405,6.294V0H0.48                                          L0.732,193.75z"></path>
                            </g>
                            <text transform="matrix(1 0 0 1 69.7256 116.2686)" fill="#fff" font-size="30"id="figure">4135</text>
                            <text transform="matrix(0.9566 0 0 1 197.3096 83.9121)" fill="#fff" font-size="29.0829"></text>
                            
                            <text transform="matrix(1 0 0 1 233.9629 115.5303)" fill="#fff" font-size="15.4128">:TH/s</text>
                          </svg>
                          <div class="pricing-inner">
                            <h4 class="font-primary"><?php echo $row['name']; ?></h4>
                            <ul class="pricing-inner">
                              <li>
                                <h6><b><?php echo $row['duration'] ?> Days</b> Duration </h6>
                              </li>
                              <li>
                                <h6><b>50%</b> Pecentage Revenue</h6>
                              </li>

                            
                             
                            </ul>
                            

                            <br/><br/>
              
                              <form method="POST" style="padding:10px" >
                                <input type="hidden" name="investmentid" value="<?php echo $row['id']; ?>" />
                                <input type="hidden" name="name" value="<?php echo $row['name']; ?>" />
                                <input type="hidden" name="percent" value="<?php echo $row['percent']; ?>" />
                                <div class="form-group">
                                <?php //get last pool
                                /*
                                  $getlast = mysqli_query($mysqli,"SELECT * FROM  investment WHERE userid='".$rows['id']."' and stauts=1 ORDER BY id DESC LIMIT 1 ");
                                  $inn = mysqli_fetch_assoc($getlast);
                                  */
                                  $totalx =0;
                                  $getlast = mysqli_query($mysqli, "SELECT * FROM investment WHERE userid='".$rows['id']."' ");
                                  while($r=mysqli_fetch_assoc($getlast)){

                                    $totalx += $r['amount'];

                                  }
                                
                                ?>
                                  <input type="number" id="amount" min="<?php if(mysqli_num_rows($getlast)>0){  echo $row['wesamount']; }else{ echo 10; }  ?>" max="<?php echo $row['max_amount']; ?>" required name="amount" class="form-control" Placeholder="Enter Amount. E.G $10"  />
                                </div>

                                <div class="form-group">
                               
                                  <input type="range" id="range" min="<?php if(mysqli_num_rows($getlast)>0){  echo $totalx; }else{ echo 10; }  ?>" max="<?php echo $row['max_amount']; ?>"  class="form-control" value="10"  />
                                </div>

                                <div class="form-group row">
                                <div class="col-12" ><label id="lab">Payment Method</label></div>
                                  
                                  <?php { ?>

                                  <div class="col-md-4" style="padding:10px" >
                                  <a href="javascript:;" id="pay1" style="opacity:.3" class="btn btn-warning"><img src="img/bitcoin.png" width="50" /><br/> Bitcoin</a>
                                  </div>

                                  <div class="col-md-4" style="padding:10px">
                                  <a href="javascript:;" id="pay2" style="opacity:.3" class="btn btn-warning"><img src="img/tron.png" width="50" /><br/> Tron</a>
                                  </div>
                                  <?php } ?>



                                  <!--<div class="col-md-4" style="padding:10px">
                                  <a href="javascript:;" id="pay5" style="opacity:.3" class="btn btn-warning"><img src="img/fiat3.png" width="100" /><br/> FLUTTERWAVE <br/>(Recomended)</a>
                                  </div>-->
                                  
                                  <?php if($rows['country']=="NG"){ ?>
                                  

                                  
                                  <!--<div class="col-md-4" style="padding:10px">
                                  <a href="javascript:;" id="pay4" style="opacity:.3" class="btn btn-warning"><img src="img/fiat2.png" width="100" /><br/> PAYSTACK <br/></a>
                                  </div>-->
                                
                                 
                                  <?php } ?>

                                  <div class="col-md-4" style="padding:10px">
                                  <a href="javascript:;" id="wallett" style="opacity:.3" class="btn btn-warning"><img src="img/wallett.png" width="50" height="50" /><br/> Wallet <br/></a>
                                  </div>
                                 
                                
                                 
                                  <input type="hidden" name="currency" id="currency" value="" />
                                </div>


                                <button class="btn btn-primary btn-lg" name="rent" type="submit" data-original-title="Make Payment" title="">Proceed</button>

                            </form>
                            


                          </div>
                        </div>
                      </div>

                     
<?php } ?>

                        <div class="col-md-6">
                        <div class="ribbon ribbon-bookmark ribbon-danger">Expected Profit</div>
                        <div class="pricing-block card text-center ">
                          <svg x="0" y="0" viewBox="0 0 360 220">
                            <g>
                              <path fill="#7366ff" d="M0.732,193.75c0,0,29.706,28.572,43.736-4.512c12.976-30.599,37.005-27.589,44.983-7.061                                          c8.09,20.815,22.83,41.034,48.324,27.781c21.875-11.372,46.499,4.066,49.155,5.591c6.242,3.586,28.729,7.626,38.246-14.243                                          s27.202-37.185,46.917-8.488c19.715,28.693,38.687,13.116,46.502,4.832c7.817-8.282,27.386-15.906,41.405,6.294V0H0.48                                          L0.732,193.75z"></path>
                            </g>
                            <text transform="matrix(1 0 0 1 69.7256 116.2686)" fill="#fff" font-size="40" id="profit">$0</text>
                            <text transform="matrix(0.9566 0 0 1 197.3096 83.9121)" fill="#fff" font-size="29.0829"></text>
                            <text transform="matrix(1 0 0 1 233.9629 115.5303)" fill="#fff" font-size="15.4128">:24 hrs</text>
                          </svg>
                          <div class="pricing-inner">
                          <img src="img/cypt.png" class="img-fluid" />

                          </div>
                        </div>


                        </div>


                   
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid Ends-->
        </div>
        <!-- footer start-->
        <footer class="footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6 footer-copyright">
              <p class="mb-0">Copyright <?php echo date('Y'); ?> © Treasure Capital All rights reserved.</p>
            </div>
            <div class="col-md-6">
              <p class="pull-right mb-0"> </p>
            </div>
          </div>
        </div>
      </footer>
      </div>
    </div>
    <!-- latest jquery-->
    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap js-->
    <script src="assets/js/bootstrap/popper.min.js"></script>
    <script src="assets/js/bootstrap/bootstrap.js"></script>
    <!-- feather icon js-->
    <script src="assets/js/icons/feather-icon/feather.min.js"></script>
    <script src="assets/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
  
    <script src="assets/js/config.js"></script>
    <!-- Plugins JS start-->
    <script src="assets/js/sidebar-menu.js"></script>
    <script src="assets/js/counter/jquery.waypoints.min.js"></script>
    <script src="assets/js/counter/jquery.counterup.min.js"></script>
    <script src="assets/js/counter/counter-custom.js"></script>
    <script src="assets/js/sweet-alert/sweetalert.min.js"></script>
    <script src="assets/js/tooltip-init.js"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="assets/js/script.js"></script>

    <!-- login js-->
    <script>

    $('#amount').keyup(function(){
      var gh = 4135;
      var amount = $('#amount').val();
      var result = (gh*amount)/10;
      $('#figure').text(result);
      var profit = amount*(5/100)
      $('#profit').text('$'+profit.toFixed(2));

    });

    $('#amount').change(function(){
      var gh = 4135;
      var amount = $('#amount').val();
      var result = (gh*amount)/10;
      $('#figure').text(result);
      var profit = amount*(5/100)
      $('#profit').text('$'+profit.toFixed(2));

      swal(
         'You\'re About Paying $'+amount+'',
        "Payment cannot be changed after initiating. Amount will be converted to the selected currency.",
         'info'
    );

    });

    $('#range').change(function (){

      $('#amount').val( $('#range').val());

      var gh = 4135;
      var amount = $('#amount').val();
      var result = (gh*amount)/10;
      $('#figure').text(result);
      var profit = amount*(5/100)
      $('#profit').text('$'+profit.toFixed(2));

      swal(
         'You\'re About Paying $'+amount+'',
        "Payment cannot be changed after initiating. Amount will be converted to the selected currency.",
         'info'
    );



    })


    $('#pay1').click(function(){
      $('#pay1').css('opacity','1');
      $('#currency').val('1');

      $('#pay2, #pay3, #pay4 ,#pay5, #wallett').css('opacity','.3');
      $('#lab').text('Payment Method');

    })

    $('#pay2').click(function(){
      $('#pay2').css('opacity','1');
      $('#currency').val('2');

      $('#pay1, #pay3, #pay4 ,#pay5 , #wallett').css('opacity','.3');
      $('#lab').text('Payment Method');

    })

    $('#pay3').click(function(){
      $('#pay3').css('opacity','1');
      $('#currency').val('3');

      $('#pay1, #pay2, #pay4 ,#pay5 , #wallett').css('opacity','.3');
      $('#lab').text('Payment Method');

    })

    $('#pay4').click(function(){
      $('#pay4').css('opacity','1');
      $('#currency').val('4');

      $('#pay1, #pay2, #pay3 ,#pay5 , #wallett').css('opacity','.3');
      $('#lab').text('Payment Method');

    })


    $('#pay5').click(function(){
      $('#pay5').css('opacity','1');
      $('#currency').val('5');

      $('#pay1, #pay2, #pay3, #pay4 , #wallett').css('opacity','.3');
      $('#lab').text('Payment Method');

    })

    $('#wallett').click(function(){
      $('#wallett').css('opacity','1');
      $('#currency').val('wallet');

      $('#pay1, #pay2, #pay3, #pay4 , #pay5').css('opacity','.3');
      $('#lab').text('Payment Method');

    })




    </script>
    <!-- Plugin used-->
  </body>

  <?php

if(isset($_POST['rent'])){


  $userid =  $rows['id'];
	$name =  mysqli_real_escape_string($mysqli,$_POST['name']);
  $percent =  mysqli_real_escape_string($mysqli,$_POST['percent']);
  $investmentid =  mysqli_real_escape_string($mysqli,$_POST['investmentid']);
  $amount =  mysqli_real_escape_string($mysqli,$_POST['amount']);
  $currency = mysqli_real_escape_string($mysqli,$_POST['currency']);

  $gext = mysqli_query($mysqli,"SELECT * FROM investment_packages");

  $row=mysqli_fetch_assoc($gext);

 
  $percent =$row['percent'];
  $name = $row['name'];


  $daily_roi = $amount*($percent/100);

  $orderid = "tc-".uniqid();
  $date= date("d")." ".date("F")." ".date("Y")." , ".date("h")." : ".date("i").date("a");

  $total =0;
  $select = mysqli_query($mysqli, "SELECT * FROM investment WHERE userid='$userid' ");
  while($r=mysqli_fetch_assoc($select)){

    $total += $r['amount'];

  }


  //if(mysqli_num_rows($select)>0){ $min= $total; }else{ $min= $row['min_amount']; }
  if(mysqli_num_rows($select)>0){ $min= $rows['wesamount']; }else{ $min= $rows['wesamount']; }

  if($amount >= $min and $amount >0){


  $total2 = $amount + $total;

    if($total <= 1000 and $total2 <= 2000){
//ensure across all pool amount is < 7000


  $settle = mysqli_query($mysqli, "SELECT * FROM pending WHERE userid='".$rows['id']."' and status=0 ");

  $btc=0;
  $trx=0;
  $naira=0;
  $naira2=0;
  $naira3=0;

  if(mysqli_num_rows($settle)>0){
  while($r=mysqli_fetch_assoc($settle)){

      if($r['type']==1){
          $btc+=1;
      }

      if($r['type']==2){
          $trx+=1;
      }

      if($r['type']==3 ){
          $naira+=1;
      }

      if( $r['type']==4){
        $naira2+=1;
    }

    if( $r['type']==5){
      $naira3+=1;
  }




  }

}



  if($currency == 1 and $btc < 1){
    //bitcoin



$secret = 'ZzsMLGKe162CfA5EcGjj';

// $my_xpub='xpub6DNgYhKgVZXZzX2ag4muii4oFW8QfjPQUMzY9N8RhzdzVtH5GYgLtAfrubVMFjMvd3zcgHjyFDFCB7dK87ZPc5zR74F3ckk6pnhLZdZiFaq';
$my_xpub='xpub6DNgYhKgVZXa2bQ974JVpynjxbw6Qb5ozaMBzo35YrJYRdRY2XotyLVEjmJ562UBEz7r2whpq4rzEprgCKot9x9qJB3n1u4WKAJjF7q8ipc';
$my_api_key = '9db57561-7f03-4272-afa8-1c68ce250f1f';

$my_callback_url = 'https://treasurecapital.net/confirm?invoice_id='.$orderid.'&secret='.$secret;

$root_url = 'https://api.blockchain.info/v2/receive';

$parameters = 'xpub=' .$my_xpub. '&callback=' .urlencode($my_callback_url). '&key=' .$my_api_key.'&gap_limit=40';


// make reques
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $root_url . '?' . $parameters);
 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_HEADER, FALSE);


  
  $response = curl_exec($ch);
  curl_close($ch);

//echo $response;

$object = json_decode($response);


$wallet = $object->address;


$insert = mysqli_query($mysqli,"INSERT INTO pending(`userid`, `chargeid`, `wallet`, `investmentid`, `name`, `amount`, `daily_roi`, `type`, `date`) VALUES('$userid', '$orderid', '$wallet', '$investmentid', '$name', '$amount', '$daily_roi', 1, '$date')");



$action = "Mining Pool Pending";
$describe ="Mining pool pending confirmation  $".$amount."";


$add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Pending') ");




if($insert){


  //print_r($order);
 
 ?>
 
 <script>
 location = 'fund?orderid=<?php echo $orderid; ?>'
 </script>
 <?php
 
 
 
 }else{

  ?>
  <script>
  
  
  swal(
       'Error Processing',
      "Please try again",
      'warning'
  )
  
  
  </script>
  
  <?php
 }

}elseif($currency == 3 and $naira < 1){
//providus bank



//login to generate user providus account
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.monnify.com/api/v1/auth/login",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Basic TUtfUFJPRF80SlJRSkpWOE5TOlk1UzM5OUdaRjhMNE5LOFZMQ1Y5Rk1BV1U0UzMzRUs0",
    "Cookie: __cfduid=dc265c737ad9d51d2913b0d0d668f0c001604001534"
  ),
));

$response = curl_exec($curl);

curl_close($curl);
$login = json_decode($response,true);

$token = $login['responseBody']['accessToken'];


$namee = $rows['firstname']." ".$rows['lastname'];
$code = $rows['referal_link'];
$email = $rows['email'];


$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.monnify.com/api/v1/bank-transfer/reserved-accounts",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>"{\n    \"accountReference\": \"".$code."\",\n    \"accountName\": \"".$namee."\",\n    \"currencyCode\": \"NGN\",\n    \"contractCode\": \"141217541896\",\n    \"customerEmail\": \"".$email."\",\n    \"customerName\": \"".$namee."\",\"reservedAccountType\": \"INVOICE\"\n}",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Bearer ".$token,
    "Content-Type: application/json",
    "Cookie: __cfduid=dc265c737ad9d51d2913b0d0d668f0c001604001534"
  ),
));

$res = curl_exec($curl);

curl_close($curl);

$response = json_decode($res,true);

if($response['requestSuccessful']==true){

$bankname = $response['responseBody']['bankName'];
$accountnum = $response['responseBody']['accountNumber'];
$accountname= $response['responseBody']['accountName'];

//update users
$update = mysqli_query($mysqli,"UPDATE users SET pbank='$bankname', paccountnum='$accountnum', paccountname='$accountname' WHERE id='".$rows['id']."'  ");

}else{

$bankname = $rows['pbank'];
$accountnum = $rows['paccountnum'];
$accountname= $rows['paccountname'];


}



function convertCurrency($amount,$from_currency,$to_currency){
  $apikey = '97b9834303ba318b846a';

  $from_Currency = urlencode($from_currency);
  $to_Currency = urlencode($to_currency);
  $query =  "{$from_Currency}_{$to_Currency}";

  // change to the free URL if you're using the free version
  $json = file_get_contents("https://free.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$apikey}");
  $obj = json_decode($json, true);

  $val = floatval($obj["$query"]);


  $total = $val * $amount;
  return number_format($total, 2, '.', '');
}

//uncomment to test
//$nariaamount = convertCurrency($amount, 'USD', 'NGN');
$nariaamount = 480*$amount;


$expire = date('Y-m-d H:i:s');
$expire = date('Y-m-d H:i:s', strtotime( $expire . " +1 days"));

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.monnify.com/api/v1/invoice/create",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>"{\n    \"amount\": \"".$nariaamount."\",\n    \"invoiceReference\": \"".$orderid."\",\n    \"accountReference\": \"".$code."\",\n    \"description\": \"".$name."\",\n    \"currencyCode\": \"NGN\",\n    \"contractCode\": \"141217541896\",\n    \"customerEmail\": \"".$email."\",\n    \"customerName\": \"".$namee."\",\n    \"expiryDate\": \"".$expire."\"\n}",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Basic TUtfUFJPRF80SlJRSkpWOE5TOlk1UzM5OUdaRjhMNE5LOFZMQ1Y5Rk1BV1U0UzMzRUs0",
    "Content-Type: application/json",
    "Cookie: __cfduid=dc265c737ad9d51d2913b0d0d668f0c001604001534"
  ),
));

$response = curl_exec($curl);

curl_close($curl);
//echo $response;
$response = json_decode($response,true);

if($response['requestSuccessful']==true){


$insert = mysqli_query($mysqli,"INSERT INTO pending(`userid`, `chargeid`, `investmentid`, `name`, `amount`, `daily_roi`, `naira`, `pbank`, `paccountnum`, `paccountname`, `type`, `date`) VALUES('$userid', '$orderid',  '$investmentid', '$name', '$amount', '$daily_roi', '$nariaamount', '$bankname', '$accountnum', '$accountname', 3, '$date')");



$action = "Mining Pool Pending";
$describe ="Mining pool pending confirmation  $".$amount."";


$add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Pending') ");




if($insert){

  //print_r($order);
 
 ?>
 
 <script>
 location = 'monnify?orderid=<?php echo $orderid; ?>'
 </script>
 <?php
 
 
 
 }else{

  ?>
  <script>
  
  
  swal(
       'Error Processing',
      "Please try again",
      'warning'
  )
  
  
  </script>
  
  <?php
 }



}else{

  ?>
  <script>
  
  
  swal(
       'Settle Your Previous Payment',
      "Please try  pay your previous payment, before proceeding.",
      'warning'
  ).then((value) => {
  location='settle';
});
  
  
  </script>
  
  <?php



}




 

}elseif($currency == 2 and $trx < 1){
  //tron

  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://api.nowpayments.io/v1/payment',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS =>'{
    "price_amount": '.$amount.',
    "price_currency": "usd",
    "pay_currency": "trx",
    "ipn_callback_url": "https://treasurecapital.net/confirm-trx",
    "order_id": "'.$orderid.'",
    "order_description": "Rent pool payment by '.$rows['firstname'].'"
  }',
    CURLOPT_HTTPHEADER => array(
      'x-api-key: 1GZQE9C-YANMARK-P76YEFT-X1CHF2Q',
      'Content-Type: application/json',
      'Cookie: __cfduid=ddf783103019d87553bf658dc62997ae41612691806'
    ),
  ));
  
  $response = curl_exec($curl);
  
  curl_close($curl);
  echo $response;
//echo $response;

$object = json_decode($response);


$wallet = $object->pay_address;
$trx = $object->pay_amount;


$insert = mysqli_query($mysqli,"INSERT INTO pending(`userid`, `chargeid`, `wallet`, `investmentid`, `name`, `amount`, `trx`, `daily_roi`, `type`, `date`) VALUES('$userid', '$orderid', '$wallet', '$investmentid', '$name', '$amount', '$trx', '$daily_roi', 2, '$date')");



$action = "Mining Pool Pending";
$describe ="Mining pool pending confirmation  $".$amount."";


$add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Pending') ");




if($insert){


//print_r($order);

?>

<script>
location = 'trx?orderid=<?php echo $orderid; ?>'
</script>
<?php



}else{

?>
<script>


swal(
     'Error Processing',
    "Please try again",
    'warning'
)


</script>

<?php
}

}elseif($currency == 4 and $naira2 < 1 ){



function convertCurrency($amount,$from_currency,$to_currency){
  $apikey = '97b9834303ba318b846a';

  $from_Currency = urlencode($from_currency);
  $to_Currency = urlencode($to_currency);
  $query =  "{$from_Currency}_{$to_Currency}";

  // change to the free URL if you're using the free version
  $json = file_get_contents("https://free.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$apikey}");
  $obj = json_decode($json, true);

  $val = floatval($obj["$query"]);


  $total = $val * $amount;
  return number_format($total, 2, '.', '');
}

//uncomment to test
//$nariaamount = convertCurrency($amount, 'USD', 'NGN');
$nariaamount = 480*$amount;


$expire = date('Y-m-d H:i:s');
$expire = date('Y-m-d H:i:s', strtotime( $expire . " +1 days"));



$chargeamount = $nariaamount+($nariaamount *0.015);
$realamount = $chargeamount*100 ;   //the amount in kobo. This value is actually NGN 300
  $curl = curl_init();
  curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.paystack.co/transaction/initialize",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => json_encode([
  'amount'=>$realamount,
  'email'=>$rows['email'],
  'reference'=>$orderid,
  'callback_url'=>"https://treasurecapital.net/success"
  ]),
  CURLOPT_HTTPHEADER => [
  "authorization: Bearer sk_live_3331a17e91a746f62a03f7362ac0a43ab1a25ead",  //replace this with your own test key  sk_test_81c1245bdf8b1a0250e1b4a978ac7518e73c7961 
  "content-type: application/json",
  "cache-control: no-cache"
  ],
  ));

$response = curl_exec($curl);
$err = curl_error($curl);

if($err){
// there was an error contacting the Paystack API
die('Curl returned error: ' . $err);
}

$tranx = json_decode($response, true);


$insert = mysqli_query($mysqli,"INSERT INTO pending(`userid`, `chargeid`, `investmentid`, `name`, `amount`, `daily_roi`, `naira`, `paystack`, `pbank`,  `type`, `date`) VALUES('$userid', '$orderid',  '$investmentid', '$name', '$amount', '$daily_roi', '$nariaamount', '".$tranx['data']['authorization_url']."', 'PayStack',  4, '$date')");



$action = "Mining Pool Pending";
$describe ="Mining pool pending confirmation  $".$amount."";


$add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Pending') ");



// comment out this line if you want to redirect the user to the payment page
//print_r($tranx);

if($insert){
// redirect to page so User can pay
// uncomment this line to allow the user redirect to the payment page
//header('Location: ' . $tranx['data']['authorization_url']);
?>

<script>
location = '<?php echo $tranx['data']['authorization_url'] ?>'
</script>
<?php

}else{
  ?>
  <script>
  
  
  swal(
       'Error Processing',
      "Please try again",
      'warning'
  )
  
  
  </script>
  
  <?php


}





}elseif($currency == 5 and $naira3 < 1 ){



    function convertCurrency($amount,$from_currency,$to_currency){
      $apikey = '97b9834303ba318b846a';
    
      $from_Currency = urlencode($from_currency);
      $to_Currency = urlencode($to_currency);
      $query =  "{$from_Currency}_{$to_Currency}";
    
      // change to the free URL if you're using the free version
      $json = file_get_contents("https://free.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$apikey}");
      $obj = json_decode($json, true);
    
      $val = floatval($obj["$query"]);
    
    
      $total = $val * $amount;
      return number_format($total, 2, '.', '');
    }
    
    //uncomment to test
    //$nariaamount = convertCurrency($amount, 'USD', 'NGN');
    $nariaamount = 480*$amount;
    
    
    $expire = date('Y-m-d H:i:s');
    $expire = date('Y-m-d H:i:s', strtotime( $expire . " +1 days"));
    
    
    
    //$chargeamount = $nariaamount+($nariaamount *0.015);
    //$realamount = $chargeamount*100 ;   //the amount in kobo. This value is actually NGN 300
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://api.flutterwave.com/v3/payments',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS =>'{
       "tx_ref":"'.$orderid.'",
       "amount":"'. $nariaamount.'",
       "currency":"NGN",
       "redirect_url":"https://treasurecapital.net/success",
       "payment_options":"card",
       
       "customer":{
          "email":"'.$rows['email'].'",
          "phonenumber":"'.$rows['phone'].'",
          "name":"'.$rows['firstname'].'"
       },
       "customizations":{
          "title":"'.$rows['firstname'].' Bot Payment"
       }
    }',
    CURLOPT_HTTPHEADER => array(
    'Authorization: Bearer FLWSECK-8ea91b5855c2a001f7a0d17e0d81ce22-X',
    'Content-Type: application/json'
    ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);
    
    $tranx = json_decode($response, true);
    
    
    $insert = mysqli_query($mysqli,"INSERT INTO pending(`userid`, `chargeid`, `investmentid`, `name`, `amount`, `daily_roi`, `naira`, `paystack`, `pbank`,  `type`, `date`) VALUES('$userid', '$orderid',  '$investmentid', '$name', '$amount', '$daily_roi', '$nariaamount', '".$tranx['data']['link']."', 'Flutterwave',  5, '$date')");
    
    
    
    $action = "Mining Pool Pending";
    $describe ="Mining pool pending confirmation  $".$amount."";
    
    
    $add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Pending') ");
    
    
    
    // comment out this line if you want to redirect the user to the payment page
    //print_r($tranx);
    
    if($insert){
    // redirect to page so User can pay
    // uncomment this line to allow the user redirect to the payment page
    //header('Location: ' . $tranx['data']['authorization_url']);
    ?>
    
    <script>
    location = '<?php echo $tranx['data']['link'] ?>'
    </script>
    <?php
    
    }else{
      ?>
      <script>
      
      
      swal(
           'Error Processing',
          "Please try again",
          'warning'
      )
      
      
      </script>
      
      <?php
    
    
    }
    
    
    
    
    
    }elseif($currency == "wallet"){
    
    //check if has up to amount 
      if($rows['wallet']>=$amount){




        $new = $rows['wallet']-$amount;
        $userid = $rows['id'];



    $addinvestment = mysqli_query($mysqli,"INSERT INTO `investment`(`userid`, `investmentid`, `name`, `amount`, `daily_roi`, `duration`,  `date`)  VALUES('$userid', '$investmentid', '$name', '$amount', '$daily_roi', '".$row['duration']."', '$date') ");



      /////////////////////////////////
      
      $action = "Mining Pool Purchased from wallet";
      $describe ="Mining pool purchased for  $".$amount." from wallet";
      
      
      $add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Confirmed') ");


      $update = mysqli_query($mysqli,"UPDATE users SET wallet='$new' WHERE id='".$rows['id']."'");



        ?>
          <script>
          swal(
              'Mining Pool Purchased ',
              "You have successfully purchased new mining pool from wallet.",
              'success'
          );

          setTimeout(() => {
              location = location;
          }, 2000);
          </script>

        <?php









      }else{


        ?>
        <script>
        
            swal(
                'Insufficient Wallet Balance',
                "You do not have enough in wallet to purchase",
                'warning'
            ).then((value) => {
            location=location;
          });
        
        </script>
        
        <?php
      


      } 
  
    

    
    
    
  }else{


  ?>
    <script>
    
    
    swal(
         'Settle your pending payment',
        "Please select a payment method or Settle your pending payment.",
         'warning'
    );
    
    setTimeout(() => {
        location ='settle';
    }, 2000);
    </script>
    
    <?php


}





}else{

  ?>
    <script>
    
    
    swal(
         'Exceeded maximum amount',
        "You can only have a maximum of $1000 across all pools",
         'warning'
    );
    
    setTimeout(() => {
        location =location;
    }, 2000);
    </script>
    
    <?php



}





}else{


?>
    <script>
    
    
    swal(
         'Your Amount is below minimum amount',
        "Please rent a bot equal to or above last rented bot. ",
         'warning'
    );
    
    setTimeout(() => {
        location =location;
    }, 3000);
    </script>
    
    <?php


}




}



?>


</html>