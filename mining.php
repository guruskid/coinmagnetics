<?php
session_start();

include('connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['id'])){
	
	header("location:login");
}else{

$get_user = mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$_SESSION['id']."' ");
$rows = mysqli_fetch_assoc($get_user);
    if(isset($_SESSION['2fa'])){

        if( ($_SESSION['2fa'] =="no" or $_SESSION['2fa'] =="pending") and $rows['2fa']==1){
            header("location:login");
        }


    }


}

if($rows['cycle']==1){ 

    header("location:dashboard");
  
  }


  $getsetting = mysqli_query($mysqli,"SELECT * FROM settings");
  $setting = mysqli_fetch_assoc($getsetting);
 
  

  date_default_timezone_set("Africa/Lagos");

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
    <title>Mining Pools - Coin Magnetics</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap"
        rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="assets/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="assets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/feather-icon.css">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="assets/css/datatables.css">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link id="color" rel="stylesheet" href="assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
    <script src="assets/js/jquery-3.5.1.min.js"></script>
</head>

<body class="dark-sidebar dark-only">
    <!-- tap on top starts-->
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>
    <!-- tap on tap ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
        <!-- Page Header Start-->
        <?php include('header.php'); ?>
        <!-- Page Header Ends                              -->
        <!-- Page Body Start-->
        <div class="page-body-wrapper sidebar-icon">
            <!-- Page Sidebar Start-->
            <?php include('sidebar.php'); ?>
            <!-- Page Sidebar Ends-->
            <div class="page-body">
                <div class="container-fluid">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-6">
                                <h3>Active Bots</h3>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="dashboard"><i data-feather="home"></i></a></li>
                                    <li class="breadcrumb-item">account</li>
                                </ol>
                            </div>
                            <div class="col-6">
                                <!-- Bookmark Start-->

                                <!-- Bookmark Ends-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Container-fluid starts-->
                <div class="container-fluid">
                    <div class="row">

                        <?php 

  $getin = mysqli_query($mysqli,"SELECT * FROM investment WHERE userid='".$rows['id']."'  ");
  //and tag=''
  if(mysqli_num_rows($getin)>0){
  $i=0;
  while($row=mysqli_fetch_assoc($getin)){
    $i++;
    $totall=$row['amount']*4135;
    $amountt=$row['daily_roi'];
    $roi = $row['added_roi'];

  ?>

                        <!-- Zero Configuration  Starts-->
                        <div class="col-sm-12">
                            <div class="card " style="background-color:#000000">
                                <div class="card-header " style="background-color:#000000; color:white">
                                    <div class="row">
                                        <div class="col-12">
                                            <h5>Pool <?php echo $i; ?>
                                                <?php if($row['status']==1){ echo '<span class="badge badge-success">Active</span>';}else{ echo '<span class="badge badge-info">Completed</spna>';} ?>
                                            </h5>
                                        </div>


                                    </div>
                                    <br />


                                    <div class="row">

                                        <div class="col-sm-6 col-xl-4 col-lg-4">
                                            <div class="card o-hidden">
                                                <div class="bg-info b-r-4 card-body">
                                                    <div class="media static-top-widget">
                                                        <div class="align-self-center text-center"><i
                                                                data-feather="user-plus"></i></div>
                                                        <div class="media-body"><span class="m-0">Total Hashing Power</span>
                                                            <h4 class="mb-0 counter"
                                                                id="counttotal<?php echo $row['id']; ?>">0 -TH/s</h4><i
                                                                class="icon-bg" data-feather="plus"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-sm-6 col-xl-4 col-lg-4">
                                            <div class="card o-hidden">
                                                <div class="bg-warning b-r-4 card-body">
                                                    <div class="media static-top-widget">
                                                        <div class="align-self-center text-center"><i
                                                                data-feather="user-plus"></i></div>
                                                        <div class="media-body"><span class="m-0">Total Reveune</span>
                                                            <h4 class="mb-0 counter" id="roi<?php echo $row['id']; ?>">
                                                                $0</h4><i class="icon-bg" data-feather="plus"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-xl-4 col-lg-4">
                                        <?php if($row['status']==1){ ?>
                                            <center>
                                                <img src="img/tccc.gif" class="img-fluid" /> <br />
                                            </center>
                                            <?php }else{ ?>
                                                <center>
                                                <img src="img/tcc.png" class="img-fluid" /> <br />
                                            </center>

                                          <?php  
                                          
                                                }
                                          
                                          ?>

                                        </div>






                                    </div>
                                </div>
                                <div class="card-body">


                                    <br />








                                    <div class="row">


                                        <div class="col-12">
                                            <center>
                                            <?php 
                                             if( date("l")!="Saturday" and date("l")!="Sunday" and $setting['automatic'] == 1   ){
                                             
                                             if($rows['wes']==1 and $rows['wesscore']>=1 ){
                                            ?>
                                                <h5 class="text-light"> <?php echo $row['duration']; ?> - Days left</h5>
                                                <?php }else{ ?>
                                                    <h5 class="text-light"> Mining Pool is paused because Your WITHDRAWAL ELIGIBILITY SCORE(WES) is zero.</h5>

                                               <?php } ?>

                                               <?php }else{ ?>
                                               <h5 class="text-light"> Mining Pool Nodes will wake up once mining resumes by next Monday by 7am UAT (National holidays may affect this schedule)</h5>

                                               <?php } ?>
                                               
                                               

                                            
                                            </center>
                                            <br />
                                        </div>




                                        <div class="col-sm-6 col-xl-6 col-lg-6">
                                            <div class="card o-hidden">
                                                <div class="bg-primary b-r-4 card-body">
                                                    <div class="media static-top-widget">
                                                        <div class="align-self-center text-center"><i
                                                                data-feather="database"></i></div>
                                                        <div class="media-body"><span class="m-0">Hashing Power</span>
                                                            <h4 class="mb-0 counter">
                                                                <?php echo $row['amount']*4135; ?>-TH/S</h4><i
                                                                class="icon-bg" data-feather="database"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-sm-6 col-xl-6 col-lg-6">
                                            <div class="card o-hidden">
                                                <div class="bg-secondary b-r-4 card-body">
                                                    <div class="media static-top-widget">
                                                        <div class="align-self-center text-center"><i
                                                                data-feather="shopping-bag"></i></div>
                                                        <div class="media-body"><span class="m-0">Current Reveune</span>
                                                            <h4 class="mb-0 counter"
                                                                id="counter<?php echo $row['id']; ?>">
                                                                $<?php echo $row['added_roi']; ?></h4><i class="icon-bg"
                                                                data-feather="shopping-bag"></i>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>






                                    </div>

                                    <?php if($row['status']==1){ ?>
                                    <?php   //or  (date('l')=="Monday" and date('G') >= 8 ) or (date('l')=="Friday" and date('G') <= 18 )
                                        if( date("l")!="Saturday" and date("l")!="Sunday" and $setting['automatic'] == 1 ){ ?>
                                    <input type="hidden" id="c1<?php echo $row['id']; ?>"
                                        value="<?php echo $row['added_roi']; ?>" />
                                    <script>
                                    function update<?php echo $row['id']; ?>() {
                                        var amount = '<?php echo $row['daily_roi']; ?>';
                                        var added = $('#c1<?php echo $row['id']; ?>').val();

                                        var add = parseFloat(amount) / 86400;

                                        var result = parseFloat(added) + add;

                                        document.querySelector('#counter<?php echo $row['id']; ?>').innerHTML = '$' +
                                            result.toFixed(10);
                                        //document.querySelector('#c<?php echo $row['id']; ?>').value = result;
                                        $('#c1<?php echo $row['id']; ?>').val(result);
                                    }
                                    setInterval(() => {

                                        update<?php echo $row['id']; ?>();

                                    }, 1000);
                                    </script>
                                    <?php } ?>
                                    <?php } ?>


                                    <div class="row">
                                        <?php if($row['status']==1){ ?>
                                        <div class="col-sm-6 col-xl-4 col-lg-4" style="padding:10px">
                                            <a href="javascript:;" data-toggle="modal"
                                                data-original-title="Hashing Power"
                                                data-target="#hashing<?php echo $row['id']; ?>"
                                                class="btn btn-success btn-block">Increase Hashing Power</a>
                                        </div>
                                        <?php } ?>
                                        <div class="col-sm-6 col-xl-4 col-lg-4" style="padding:10px">
                                            <a href="javascript:;" data-toggle="modal" data-original-title="Withdrawal"
                                                data-target="#withdraw<?php echo $row['id']; ?>"
                                                class="btn btn-warning btn-block">Withdraw to Wallet</a>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                        <!-- Zero Configuration  Ends-->





                        <div class="modal fade" id="hashing<?php echo $row['id']; ?>" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Add to Hashing Power</h5>
                                        <button class="close" type="button" data-dismiss="modal"
                                            aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">

                                        <form method="POST">
                                            <input type="hidden" value="<?php echo $row['id'] ?>" name="id" />
                                            <input type="hidden" value="<?php echo $row['investmentid'] ?>"
                                                name="investmentid" />
                                            <div class="form-group">
                                                <label>Select Payment type</label>
                                                <select name="type" id="type" class="form-control">
                                                    <option value="1">Direct Payment</option>
                                                    <option value="2">Wallet Payment</option>
                                                </select>
                                            </div>

                                            <div class="form-group" id="method">
                                                <label>Select Payment Method</label>
                                                <select name="method" class="form-control">
                                                <?php { ?>
                                                    <option value="1">Bitcoin</option>
                                                    <option value="2">Tron</option>
                                                    <?php } ?>

                                                    <?php { ?>
                                                    <!--<option value="4">PayStack</option>--> 
                                                   
                                                    
                                                   
                                                    <?php } ?>
                                                    <!--<option value="5">Flutterwave</option>-->
                                                </select>
                                            </div>


                                            <div class="form-group">
                                                <label>Amount</label>
                                                <input type="number" name="amount" required placeholder="Enter amount"
                                                    min="10" max="7000" class="form-control" />
                                            </div>

                                            <div class="form-group">
                                                <button class="btn btn-info" type="submit"
                                                    name="hashing">Proceed</button>
                                            </div>


                                        </form>

                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" type="button"
                                            data-dismiss="modal">Close</button>

                                    </div>
                                </div>
                            </div>
                        </div>




                        <div class="modal fade" id="withdraw<?php echo $row['id']; ?>" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Withdraw to Wallet</h5>
                                        <button class="close" type="button" data-dismiss="modal"
                                            aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">

                                    <form method="POST">
                                            <input type="hidden" value="<?php echo $row['id'] ?>" name="id" />
                                            <input type="hidden" value="<?php echo $row['investmentid'] ?>"
                                                name="investmentid" />
                                            
                                           
                                                <div class="form-group" >
                                                <label>Select Withdrawal Method</label>
                                                <select name="method" class="form-control">
                                                     <option value="<?php if($row['status']==0){ echo 'max';}else{ echo 'min';} ?>">Available Reveune</option>
                                                  
                                                </select>
                                            </div>


                                           

                                            <div class="form-group">
                                                <button class="btn btn-info" type="submit"
                                                    name="walletwithdraw">Initate</button>
                                            </div>


                                        </form>

                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" type="button"
                                            data-dismiss="modal">Close</button>

                                    </div>
                                </div>
                            </div>
                        </div>







                        <?php 
             
     /*        
  $getinn = mysqli_query($mysqli,"SELECT * FROM investment WHERE  tag='".$row['id']."'  ");


  while($r=mysqli_fetch_assoc($getinn)){
    $totall+=$r['amount']*4135;
    $amountt+=$r['daily_roi'];
    $roi += $r['added_roi'];
  ?>
                        <!-- Zero Configuration  Starts-->
                        <div class="col-sm-12">
                            <div class="card bg-primary">
                                <div class="card-header bg-primary">
                                    <h5>Expansion of Bot <?php echo $i; ?>
                                        <?php if($r['status']==1){ echo '<span class="badge badge-success">Active</span>';}else{ echo '<span class="badge badge-info">Completed</spna>';} ?>
                                    </h5>

                                    <h5 style="float:right;"> <?php echo $r['duration']; ?> - Days left</h5>
                                </div>
                                <div class="card-body">



                                    <div class="row">
                                        <div class="col-sm-6 col-xl-6 col-lg-6">
                                            <div class="card o-hidden">
                                                <div class="bg-primary b-r-4 card-body">
                                                    <div class="media static-top-widget">
                                                        <div class="align-self-center text-center"><i
                                                                data-feather="database"></i></div>
                                                        <div class="media-body"><span class="m-0"> Trading Power</span>
                                                            <h4 class="mb-0 counter">
                                                                <?php echo $r['amount']*4135; ?>-TH/S</h4><i
                                                                class="icon-bg" data-feather="database"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xl-6 col-lg-6">
                                            <div class="card o-hidden">
                                                <div class="bg-secondary b-r-4 card-body">
                                                    <div class="media static-top-widget">
                                                        <div class="align-self-center text-center"><i
                                                                data-feather="shopping-bag"></i></div>
                                                        <div class="media-body"><span class="m-0">Current Reveune</span>
                                                            <h4 class="mb-0 counter"
                                                                id="counter<?php echo $r['id']; ?>">
                                                                $<?php echo $r['added_roi']; ?></h4><i class="icon-bg"
                                                                data-feather="shopping-bag"></i>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <?php if($r['status']==1){ ?>
                                    <input type="hidden" id="c1<?php echo $r['id']; ?>"
                                        value="<?php echo $r['added_roi']; ?>" />
                                    <script>
                                    function update<?php echo $r['id']; ?>() {
                                        var amount = '<?php echo $r['daily_roi']; ?>';
                                        var added = $('#c1<?php echo $r['id']; ?>').val();

                                        var add = parseFloat(amount) / 86400;

                                        var result = parseFloat(added) + add;

                                        document.querySelector('#counter<?php echo $r['id']; ?>').innerHTML = '$' +
                                            result;
                                        //document.querySelector('#c<?php echo $r['id']; ?>').value = result;
                                        $('#c1<?php echo $r['id']; ?>').val(result);
                                    }
                                    setInterval(() => {

                                        update<?php echo $r['id']; ?>();

                                    }, 1000);
                                    </script>
                                    <?php } ?>


                                    <div class="row">


                                        <div class="col-sm-6 col-xl-6 col-lg-6">
                                            <a href="javascript:;" data-toggle="modal" data-original-title="Withdrawal"
                                                data-target="#withdrawexpanse<?php echo $r['id']; ?>"
                                                class="btn btn-warning btn-block">Withdraw to Wallet</a>
                                        </div>

                                    </div>



                                </div>
                            </div>
                        </div>
                        <!-- Zero Configuration  Ends-->





                        <div class="modal fade" id="withdrawexpanse<?php echo $r['id']; ?>" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Withdraw to Wallet</h5>
                                        <button class="close" type="button" data-dismiss="modal"
                                            aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">

                                    <form method="POST">
                                            <input type="hidden" value="<?php echo $r['id'] ?>" name="id" />
                                            <input type="hidden" value="<?php echo $r['investmentid'] ?>"
                                                name="investmentid" />
                                            
                                           


                                             
                               
    <div class="form-group" >
    <label>Select Withdrawal Method</label>
    <select name="method" class="form-control">
        <option value="<?php if($r['status']==0){ echo 'max';}else{ echo 'min';} ?>">Available Reveune</option>
      
    </select>
</div>




                                            <div class="form-group">
                                                <button class="btn btn-info" type="submit"
                                                    name="walletwithdraw">Initate</button>
                                            </div>


                                        </form>

                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" type="button"
                                            data-dismiss="modal">Close</button>

                                    </div>
                                </div>
                            </div>
                        </div>







                        <?php 
                    
                    } 
                    */
                    ?>


                        <input type="hidden" id="rr<?php echo $row['id']; ?>" value="<?php echo $roi; ?>" />
                        <script>
                        var totall = '<?php echo $totall; ?>';
                        $('#counttotal<?php echo $row['id'] ?>').text(totall + '-TH/s');

                        var roi = '<?php echo $roi; ?>';
                        var amount = '<?php echo  $amountt; ?>';


                        function uppdate<?php echo $row['id']; ?>() {
                            //var amount = roi;
                            var added = $('#rr<?php echo $row['id']; ?>').val();;

                            var add = parseFloat(amount) / 86400;

                            var result = parseFloat(added) + add;

                            document.querySelector('#roi<?php echo $row['id']; ?>').innerHTML = '$' + result.toFixed(
                            10);
                            //document.querySelector('#c<?php echo $row['id']; ?>').value = result;
                            $('#rr<?php echo $row['id']; ?>').val(result);
                        }
                        setInterval(() => {

                            uppdate<?php echo $row['id']; ?>();

                        }, 1000);
                        </script>



                        <?php


}




  }else{
?>
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>No Active Trading Bot</h5>
                                </div>
                            </div>
                        </div>




                        <?php  
                    
                        
                    }
                
                    ?>



                    </div>
                </div>
                <!-- Container-fluid Ends-->
            </div>
            <!-- footer start-->
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 footer-copyright">
                            <p class="mb-0">Copyright <?php echo date('Y'); ?> © Coin Magnetics All rights reserved.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p class="pull-right mb-0"> </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!-- latest jquery-->

    <!-- Bootstrap js-->
    <script src="assets/js/bootstrap/popper.min.js"></script>
    <script src="assets/js/bootstrap/bootstrap.js"></script>
    <!-- feather icon js-->
    <script src="assets/js/icons/feather-icon/feather.min.js"></script>
    <script src="assets/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
    <script src="assets/js/sidebar-menu.js"></script>
    <script src="assets/js/config.js"></script>
    <!-- Plugins JS start-->
    <script src="assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/js/datatable/datatables/datatable.custom.js"></script>
    <script src="assets/js/sweet-alert/sweetalert.min.js"></script>
    <script src="assets/js/tooltip-init.js"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="assets/js/script.js"></script>
    <script>
    $('#type').change(function() {
        if ($('#type').val() == 2) {
            $('#method').hide();

        } else {
            $('#method').show();
        }

    });
    </script>
    <!-- login js-->
    <!-- Plugin used-->
</body>

<?php 

if(isset($_POST['hashing'])){ 

  $id = $_POST['id'];
  $investmentid = $_POST['investmentid'];
  $amount = $_POST['amount'];
  $method = $_POST['method'];
  $type = $_POST['type'];
  $userid = $rows['id'];
  $date= date("d")." ".date("F")." ".date("Y")." , ".date("h")." : ".date("i").date("a");

  $select = mysqli_query($mysqli, "SELECT * FROM investment_packages ");
  $rr=mysqli_fetch_assoc($select);
  $percent=$rr['percent'];
	$name =  $rr['name'];
  $daily_roi = $amount*($percent/100);
  $orderid = "tc-".uniqid();

  $total =0;
  $select = mysqli_query($mysqli, "SELECT * FROM investment WHERE userid='$userid' and status=1");
  while($r=mysqli_fetch_assoc($select)){

    $total += $r['amount'];

  }


  $total2 = $amount + $total;

    if($total <= 1000 and $total2 <= 1000){
//ensure across all pool amount is < 7000

$settle = mysqli_query($mysqli, "SELECT * FROM pending WHERE userid='".$rows['id']."' and status=0 ");

$btc=0;
$trx=0;
$naira=0;
$naira2=0;
$naira3=0;

if(mysqli_num_rows($settle)>0){
while($r=mysqli_fetch_assoc($settle)){ 

    if($r['type']==1){
        $btc+=1;
    }

    if($r['type']==2){
        $trx+=1;
    }

    if($r['type']==3 ){
        $naira+=1;
    }

    if( $r['type']==4){
      $naira2+=1;
  }

  if( $r['type']==5){
    $naira2+=1;
}




  }

}




    if($type==1){
      //direct


      if( $method == 1 and $btc < 1){
        //bitcoin





      $secret = 'ZzsMLGKe162CfA5EcGjj';

$my_xpub='xpub6DNgYhKgVZXZzX2ag4muii4oFW8QfjPQUMzY9N8RhzdzVtH5GYgLtAfrubVMFjMvd3zcgHjyFDFCB7dK87ZPc5zR74F3ckk6pnhLZdZiFaq';
$my_api_key = '9db57561-7f03-4272-afa8-1c68ce250f1f';

$my_callback_url = 'https://Coinmagnetics.com/confirm?invoice_id='.$orderid.'&secret='.$secret;

$root_url = 'https://api.blockchain.info/v2/receive';

$parameters = 'xpub=' .$my_xpub. '&callback=' .urlencode($my_callback_url). '&key=' .$my_api_key.'&gap_limit=40';


// make reques
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $root_url . '?' . $parameters);
 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_HEADER, FALSE);


  
  $response = curl_exec($ch);
  curl_close($ch);

//echo $response;

$object = json_decode($response);


$wallet = $object->address;


$insert = mysqli_query($mysqli,"INSERT INTO pending(`userid`, `chargeid`, `wallet`, `investmentid`, `name`, `amount`, `daily_roi`, `tag`, `type`, `date`) VALUES('$userid', '$orderid', '$wallet', '$investmentid', '$name', '$amount', '$daily_roi', '$id', 1, '$date')");


      ////////////////////////////////////
      
      $action = "Increased hashing power";
      $describe ="hashing power increased by  $".$amount."";
      
      
      $add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`, `status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Pending') ");
    



      if($insert){


        //print_r($order);
       
       ?>

<script>
location = 'fund?orderid=<?php echo $orderid; ?>'
</script>
<?php
       
       
       
       }else{
      
        ?>
<script>
swal(
    'Error Processing',
    "Please try again",
    'warning'
)
</script>

<?php
       }



    }elseif( $method == 2 and $trx < 1){
        //tron



        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api.nowpayments.io/v1/payment',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
          "price_amount": '.$amount.',
          "price_currency": "usd",
          "pay_currency": "trx",
          "ipn_callback_url": "https://Coinmagnetics.com/confirm-trx",
          "order_id": "'.$orderid.'",
          "order_description": "Rent pool payment by '.$rows['firstname'].'"
        }',
          CURLOPT_HTTPHEADER => array(
            'x-api-key: 1GZQE9C-YANMARK-P76YEFT-X1CHF2Q',
            'Content-Type: application/json',
            'Cookie: __cfduid=ddf783103019d87553bf658dc62997ae41612691806'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        echo $response;
      //echo $response;
      
      $object = json_decode($response);
      
      
      $wallet = $object->pay_address;
      $trx = $object->pay_amount;


$insert = mysqli_query($mysqli,"INSERT INTO pending(`userid`, `chargeid`, `wallet`, `investmentid`, `name`, `amount`, `trx`, `daily_roi`, `tag`, `type`, `date`) VALUES('$userid', '$orderid', '$wallet', '$investmentid', '$name', '$amount', '$trx','$daily_roi', '$id', 2, '$date')");


      ////////////////////////////////////
      
      $action = "Increased hashing power";
      $describe ="hashing power increased by  $".$amount."";
      
      
      $add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`, `status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Pending') ");
    



      if($insert){


        //print_r($order);
       
       ?>

<script>
location = 'trx?orderid=<?php echo $orderid; ?>'
</script>
<?php
       
       
       
       }else{
      
        ?>
<script>
swal(
    'Error Processing',
    "Please try again",
    'warning'
)
</script>

<?php
       }



    }elseif( $method == 3 and $naira < 1){
      //providus bank





//login to generate user providus account
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.monnify.com/api/v1/auth/login",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Basic TUtfUFJPRF80SlJRSkpWOE5TOlk1UzM5OUdaRjhMNE5LOFZMQ1Y5Rk1BV1U0UzMzRUs0",
    "Cookie: __cfduid=dc265c737ad9d51d2913b0d0d668f0c001604001534"
  ),
));

$response = curl_exec($curl);

curl_close($curl);
$login = json_decode($response,true);

$token = $login['responseBody']['accessToken'];


$namee = $rows['firstname']." ".$rows['lastname'];
$code = $rows['referal_link'];
$email = $rows['email'];


$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.monnify.com/api/v1/bank-transfer/reserved-accounts",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>"{\n    \"accountReference\": \"".$code."\",\n    \"accountName\": \"".$namee."\",\n    \"currencyCode\": \"NGN\",\n    \"contractCode\": \"141217541896\",\n    \"customerEmail\": \"".$email."\",\n    \"customerName\": \"".$namee."\",\"reservedAccountType\": \"INVOICE\"\n}",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Bearer ".$token,
    "Content-Type: application/json",
    "Cookie: __cfduid=dc265c737ad9d51d2913b0d0d668f0c001604001534"
  ),
));

$res = curl_exec($curl);

curl_close($curl);

$response = json_decode($res,true);

if($response['requestSuccessful']==true){

$bankname = $response['responseBody']['bankName'];
$accountnum = $response['responseBody']['accountNumber'];
$accountname= $response['responseBody']['accountName'];

//update users
$update = mysqli_query($mysqli,"UPDATE users SET pbank='$bankname', paccountnum='$accountnum', paccountname='$accountname' WHERE id='".$rows['id']."'  ");

}else{

$bankname = $rows['pbank'];
$accountnum = $rows['paccountnum'];
$accountname= $rows['paccountname'];


}



function convertCurrency($amount,$from_currency,$to_currency){
  $apikey = '97b9834303ba318b846a';

  $from_Currency = urlencode($from_currency);
  $to_Currency = urlencode($to_currency);
  $query =  "{$from_Currency}_{$to_Currency}";

  // change to the free URL if you're using the free version
  $json = file_get_contents("https://free.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$apikey}");
  $obj = json_decode($json, true);

  $val = floatval($obj["$query"]);


  $total = $val * $amount;
  return number_format($total, 2, '.', '');
}

//uncomment to test
//$nariaamount = convertCurrency($amount, 'USD', 'NGN');
$nariaamount = 480*$amount;


$expire = date('Y-m-d H:i:s');
$expire = date('Y-m-d H:i:s', strtotime( $expire . " +1 days"));

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.monnify.com/api/v1/invoice/create",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>"{\n    \"amount\": \"".$nariaamount."\",\n    \"invoiceReference\": \"".$orderid."\",\n    \"accountReference\": \"".$code."\",\n    \"description\": \"".$name."\",\n    \"currencyCode\": \"NGN\",\n    \"contractCode\": \"141217541896\",\n    \"customerEmail\": \"".$email."\",\n    \"customerName\": \"".$namee."\",\n    \"expiryDate\": \"".$expire."\"\n}",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Basic TUtfUFJPRF80SlJRSkpWOE5TOlk1UzM5OUdaRjhMNE5LOFZMQ1Y5Rk1BV1U0UzMzRUs0",
    "Content-Type: application/json",
    "Cookie: __cfduid=dc265c737ad9d51d2913b0d0d668f0c001604001534"
  ),
));

$response = curl_exec($curl);

curl_close($curl);
//echo $response;
$response = json_decode($response,true);

if($response['requestSuccessful']==true){


$insert = mysqli_query($mysqli,"INSERT INTO pending(`userid`, `chargeid`, `investmentid`, `name`, `amount`, `daily_roi`, `naira`, `pbank`, `paccountnum`, `paccountname`, `tag`, `type`, `date`) VALUES('$userid', '$orderid',  '$investmentid', '$name', '$amount', '$daily_roi', '$nariaamount', '$bankname', '$accountnum', '$accountname', '$id', 3, '$date')");



$action = "Increased hashing power";
$describe ="hashing power increased by  $".$amount."";


$add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`, `status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Pending') ");




if($insert){


  //print_r($order);
 
 ?>
 
 <script>
 location = 'monnify?orderid=<?php echo $orderid; ?>'
 </script>
 <?php
 
 
 
 }else{

  ?>
  <script>
  
  
  swal(
       'Error Processing',
      "Please try again",
      'warning'
  )
  
  
  </script>
  
  <?php
 }



}else{

  ?>
  <script>
  
  
  swal(
       'Settle Your Previous Payment',
      "Please try  pay your previous payment, before proceeding.",
      'warning'
  )
  
  
  </script>
  
  <?php

  }






    }elseif($method == 4 and $naira2 < 1 ){



        function convertCurrency($amount,$from_currency,$to_currency){
          $apikey = '97b9834303ba318b846a';
        
          $from_Currency = urlencode($from_currency);
          $to_Currency = urlencode($to_currency);
          $query =  "{$from_Currency}_{$to_Currency}";
        
          // change to the free URL if you're using the free version
          $json = file_get_contents("https://free.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$apikey}");
          $obj = json_decode($json, true);
        
          $val = floatval($obj["$query"]);
        
        
          $total = $val * $amount;
          return number_format($total, 2, '.', '');
        }
        
        //uncomment to test
        //$nariaamount = convertCurrency($amount, 'USD', 'NGN');
        $nariaamount = 480*$amount;
        
        
        $expire = date('Y-m-d H:i:s');
        $expire = date('Y-m-d H:i:s', strtotime( $expire . " +1 days"));
        
        
        $chargeamount = $nariaamount+($nariaamount *0.015);
        $realamount = $chargeamount*100 ; //the amount in kobo. This value is actually NGN 300
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.paystack.co/transaction/initialize",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode([
        'amount'=>$realamount,
        'email'=>$rows['email'],
        'reference'=>$orderid,
        'callback_url'=>"https://Coinmagnetics.com/success"
        ]),
        CURLOPT_HTTPHEADER => [
        "authorization: Bearer sk_live_3331a17e91a746f62a03f7362ac0a43ab1a25ead",  //replace this with your own test key  sk_test_81c1245bdf8b1a0250e1b4a978ac7518e73c7961 
        "content-type: application/json",
        "cache-control: no-cache"
        ],
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        if($err){
        // there was an error contacting the Paystack API
        die('Curl returned error: ' . $err);
        }
        
        $tranx = json_decode($response, true);
        
        
        $insert = mysqli_query($mysqli,"INSERT INTO pending(`userid`, `chargeid`, `investmentid`, `name`, `amount`, `daily_roi`, `naira`, `paystack`, `pbank`, `tag`, `type`, `date`) VALUES('$userid', '$orderid',  '$investmentid', '$name', '$amount', '$daily_roi', '$nariaamount',  '".$tranx['data']['authorization_url']."', 'PayStack', '$id', 4, '$date')");



        $action = "Increased hashing power";
        $describe ="hashing power increased by  $".$amount."";
        
        
        $add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`, `status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Pending') ");
        
        
        
        // comment out this line if you want to redirect the user to the payment page
        //print_r($tranx);
        
        if($insert){
        // redirect to page so User can pay
        // uncomment this line to allow the user redirect to the payment page
        //header('Location: ' . $tranx['data']['authorization_url']);
        ?>
        
        <script>
        location = '<?php echo $tranx['data']['authorization_url'] ?>'
        </script>
        <?php
        
        }else{
          ?>
          <script>
          
          
          swal(
               'Error Processing',
              "Please try again",
              'warning'
          )
          
          
          </script>
          
          <?php
        
        
        }
        
        
        
        
        
        }elseif($method == 5 and $naira3 < 1 ){



            function convertCurrency($amount,$from_currency,$to_currency){
              $apikey = '97b9834303ba318b846a';
            
              $from_Currency = urlencode($from_currency);
              $to_Currency = urlencode($to_currency);
              $query =  "{$from_Currency}_{$to_Currency}";
            
              // change to the free URL if you're using the free version
              $json = file_get_contents("https://free.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$apikey}");
              $obj = json_decode($json, true);
            
              $val = floatval($obj["$query"]);
            
            
              $total = $val * $amount;
              return number_format($total, 2, '.', '');
            }
            
            //uncomment to test
            //$nariaamount = convertCurrency($amount, 'USD', 'NGN');
            $nariaamount = 480*$amount;
            
            
            $expire = date('Y-m-d H:i:s');
            $expire = date('Y-m-d H:i:s', strtotime( $expire . " +1 days"));
            
            
           //$chargeamount = $nariaamount+($nariaamount *0.015);
    //$realamount = $chargeamount*100 ;   //the amount in kobo. This value is actually NGN 300
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://api.flutterwave.com/v3/payments',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS =>'{
       "tx_ref":"'.$orderid.'",
       "amount":"'. $nariaamount.'",
       "currency":"NGN",
       "redirect_url":"https://Coinmagnetics.com/success",
       "payment_options":"card",
       
       "customer":{
          "email":"'.$rows['email'].'",
          "phonenumber":"'.$rows['phone'].'",
          "name":"'.$rows['firstname'].'"
       },
       "customizations":{
          "title":"'.$rows['firstname'].' Bot Payment"
       }
    }',
    CURLOPT_HTTPHEADER => array(
    'Authorization: Bearer FLWSECK-8ea91b5855c2a001f7a0d17e0d81ce22-X',
    'Content-Type: application/json'
    ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);
            
            $tranx = json_decode($response, true);
            
            
            $insert = mysqli_query($mysqli,"INSERT INTO pending(`userid`, `chargeid`, `investmentid`, `name`, `amount`, `daily_roi`, `naira`, `paystack`, `pbank`, `tag`, `type`, `date`) VALUES('$userid', '$orderid',  '$investmentid', '$name', '$amount', '$daily_roi', '$nariaamount',  '".$tranx['data']['link']."', 'Flutterwave', '$id', 5, '$date')");
    
    
    
            $action = "Increased hashing power";
            $describe ="hashing power increased by  $".$amount."";
            
            
            $add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`, `status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Pending') ");
            
            
            
            // comment out this line if you want to redirect the user to the payment page
            //print_r($tranx);
            
            if($insert){
            // redirect to page so User can pay
            // uncomment this line to allow the user redirect to the payment page
            //header('Location: ' . $tranx['data']['authorization_url']);
            ?>
            
            <script>
            location = '<?php echo $tranx['data']['link'] ?>'
            </script>
            <?php
            
            }else{
              ?>
              <script>
              
              
              swal(
                   'Error Processing',
                  "Please try again",
                  'warning'
              )
              
              
              </script>
              
              <?php
            
            
            }
            
            
            
            
            
            }else{
            
              ?>
              <script>
              
              
              swal(
                   'Settle Your Previous Payment',
                  "Please try  pay your previous payment, before proceeding.",
                  'warning'
              ).then((value) => {
              location='settle';
            });
              
              
              </script>
              
              <?php
            
              }
            ///flutter


























    }else{
      //wallet

      if($rows['wallet']>=$amount and $amount >0){

        $new = $rows['wallet']-$amount;



$addinvestment = mysqli_query($mysqli,"INSERT INTO `investment`(`userid`, `investmentid`, `name`, `amount`, `daily_roi`, `duration`, `tag`, `date`)  VALUES('$userid', '$investmentid', '$name', '$amount', '$daily_roi', '".$rr['duration']."', '$id', '$date') ");



  /////////////////////////////////
  
  $action = "Increase hashing power from wallet";
  $describe ="hashing power increased by  $".$amount." from wallet";
  
  
  $add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Confirmed') ");


  $update = mysqli_query($mysqli,"UPDATE users SET wallet='$new' WHERE id='".$rows['id']."'");



  ?>
<script>
swal(
    'Hashing Power Increased',
    "You have successfully increased your hashing power",
    'success'
);

setTimeout(() => {
    location = location;
}, 2000);
</script>

<?php


      }else{

        ?>
<script>
swal(
    'Insufficient Wallet Balance',
    "You do not have enough in wallet to increase",
    'warning'
);

setTimeout(() => {
    location = location;
}, 2000);
</script>

<?php


      }




    }



  }else{

    ?>
<script>
swal(
    'Exceeded maximum amount',
    "You can only have a maximum of $1000 across all pools",
    'warning'
);

setTimeout(() => {
    location = location;
}, 2000);
</script>

<?php


  }






}




//withdrawing to wallet
if(isset($_POST['walletwithdraw'])){

    $id = $_POST['id'];
    $investmentid = $_POST['investmentid'];
    //$amount = $_POST['amount'];
    $method = $_POST['method'];
   
    $userid = $rows['id'];
    $date= date("d")." ".date("F")." ".date("Y")." , ".date("h")." : ".date("i").date("a");
  
    $getinvest = mysqli_query($mysqli,"SELECT * FROM investment WHERE id='$id' ");
    $row = mysqli_fetch_assoc($getinvest);

    $amount = $row['added_roi'];

    if($amount >=5){


    if($method =="max"){

        $amount = $row['added_roi'];
        //$newprofit = $row['added_roi']-$amount;
        $newwallet = $rows['wallet']+$row['added_roi'];

         //update
        $upuser = mysqli_query($mysqli,"UPDATE users SET wallet='$newwallet' WHERE id='$userid'");


    $action = "Withdrawal to wallet";
    $describe ="Withdrawal to wallet of $".$amount."";


$add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Proccessed') ");


//delete investment

$del = mysqli_query($mysqli,"DELETE FROM investment WHERE id='$id'  ");
        

?>
<script>


swal(
     'Withdrawal Successful',
    "You have successfully withdrawn to wallet",
    'success'
)

setTimeout(() => {
    location=location;
}, 3000);

</script>

<?php




    }else{



//ESURE THE PERSON HAS THE AMOUNT IS INPUT IN ADD_ROI/PROFIT
if($amount <= $row['added_roi']){

    //deduct
    $newprofit = $row['added_roi']-$amount;
    $newwallet = $rows['wallet']+$amount;

    //update
    $upuser = mysqli_query($mysqli,"UPDATE users SET wallet='$newwallet' WHERE id='$userid'");
    $upinvest = mysqli_query($mysqli,"UPDATE investment SET added_roi='$newprofit' WHERE id='$id'");


    $action = "Withdrawal to wallet";
$describe ="Withdrawal to wallet of $".$amount."";


$add = mysqli_query($mysqli,"INSERT INTO `activity`(`userid`, `action`, `describe`, `date`, `amount`,`status`) VALUES('$userid', '$action', '$describe', '$date','$amount', 'Proccessed') ");


?>
<script>


swal(
     'Withdrawal Successful',
    "You have successfully withdrawn to wallet",
    'success'
)

setTimeout(() => {
    location=location;
}, 3000);

</script>

<?php


}else{

    ?>
    <script>
    
    
    swal(
         'Insufficient Funds',
        "You do not have enough balance yet. Please try again",
        'warning'
    )
    
    
    </script>
    
    <?php

}


}


}else{

    ?>
    <script>
    
    
    swal(
         'Low Reveune',
        "Your reveune must be $5 or more to withdraw to wallet",
        'warning'
    )
    
    
    </script>
    
    <?php
        
}





}







?>

</html>