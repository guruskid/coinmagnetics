<?php
session_start();

include('connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['id'])){
	
	header("location:login");
}else{

$get_user = mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$_SESSION['id']."' ");
$rows = mysqli_fetch_assoc($get_user);
    if(isset($_SESSION['2fa'])){

        if( ($_SESSION['2fa'] =="no" or $_SESSION['2fa'] =="pending") and $rows['2fa']==1){
            header("location:login");
        }


    }


}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
    <title>Settlements - Coin Magnetics</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="assets/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="assets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/feather-icon.css">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="assets/css/datatables.css">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.css">
    <link id="color" rel="stylesheet" href="assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5dde42de43be710e1d1f5485/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
  </head>
  <body class="dark-sidebar dark-only" >
    <!-- tap on top starts-->
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>
    <!-- tap on tap ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
      <!-- Page Header Start-->
     <?php include('header.php'); ?>
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper sidebar-icon">
        <!-- Page Sidebar Start-->
      <?php include('sidebar.php'); ?>
        <!-- Page Sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col-6">
                  <h3>Settle  Pending Payments</h3>
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="dashboard"><i data-feather="home"></i></a></li>
                    <li class="breadcrumb-item">Payment</li>
                  </ol>
                </div>
                <div class="col-6">
                  <!-- Bookmark Start-->
                 
                  <!-- Bookmark Ends-->
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            <div class="row">
              <!-- Zero Configuration  Starts-->
              <div class="col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h5>Pending  Payments</h5>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="display" id="basic-1">
                        <thead>
                          <tr>
                            <th>S/N</th>
                            <th>Id</th>
                            <th>Type of Payment</th>
                            <th>Date</th>
                            
                            <th>Amount</th>
                            <th>Complete Payment</th>
                            <th>Delete Payment</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          //start the loop for see all users
                          $get = mysqli_query($mysqli,"SELECT * FROM pending WHERE userid='".$rows['id']."' and status=0 ORDER BY id DESC");
                              $i=0;
                              while($row= mysqli_fetch_assoc($get)){
                                  $i++;

                                  
                              ?>
                              <tr>

                                  <td><?php echo $i; ?></td>

                                  <td><?php echo $row['chargeid']; ?></td>

                                  <td><?php if($row['type']==1){
                                    echo '<div class="col-md-12" style="padding:10px" >
                                    <a href="javascript:;" id="pay1"  class="btn btn-warning"><img src="img/bitcoin.png" width="50" /><br/> Bitcoin</a>
                                    </div>';
                                  }elseif($row['type']==2){
                                    echo '<div class="col-md-4" style="padding:10px">
                                    <a href="javascript:;" id="pay2"  class="btn btn-warning"><img src="img/tron.png" width="50" /><br/> Tron</a>
                                    </div>';
                                  }elseif($row['type']==3 or $row['type']==4 or $row['type']==5){
                                    echo '<div class="col-md-4" style="padding:10px">
                                    <a href="javascript:;" id="pay3"  class="btn btn-warning"><img src="img/naira.png" width="50" /><br/> FIAT</a>
                                    </div>';
                                  } ?></td>

                                  <td><?php echo $row['date']; ?></td>
                                  
                                  
                                  <td>$<?php echo $row['amount']; ?></td>

                                  <td><?php if($row['type']==1){
                                    echo '<div class="col-md-12" style="padding:10px" >
                                    <a href="fund?orderid='.$row['chargeid'].'"   class="btn btn-primary">Proceed</a>
                                    </div>';
                                  }elseif($row['type']==2){
                                    echo '<div class="col-md-4" style="padding:10px">
                                    <a href="trx?orderid='.$row['chargeid'].'"  class="btn btn-primary">Proceed</a>
                                    </div>';
                                  }elseif($row['type']==3){
                                    echo '<div class="col-md-4" style="padding:10px">
                                    <a href="monnify?orderid='.$row['chargeid'].'"  class="btn btn-primary">Proceed</a>
                                    </div>';
                                  }elseif($row['type']==4 or $row['type']==5 ){
                                    echo '<div class="col-md-4" style="padding:10px">
                                    <a href="'.$row['paystack'].'"  class="btn btn-primary">Proceed</a>
                                    </div>';
                                  } ?></td>
                                  
                                  <td>
                                  <?php  if($row['duration'] < 1 or $row['type']==4 or  $row['type']==5 ){ ?>
                                  <form method="POST">
                                  <input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
                                  <input type="hidden" name="type" value="<?php echo $row['type']; ?>" />
                                  <input type="hidden" name="orderid" value="<?php echo $row['chargeid']; ?>" />
                                
                                    <button type="submit" name="delete"  class="btn btn-warning">Delete</a>
                                    
                                  </form>
                                  <?php } ?>
                                  </td>
                                 

                                  </tr>

                              <?php

                              }

                               ?>

                         
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Zero Configuration  Ends-->
          
            </div>
          </div>
          <!-- Container-fluid Ends-->
        </div>
        <!-- footer start-->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0">Copyright <?php echo date('Y'); ?> © Coin Magnetics All rights reserved.</p>
              </div>
              <div class="col-md-6">
                <p class="pull-right mb-0"> </p>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- latest jquery-->
    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap js-->
    <script src="assets/js/bootstrap/popper.min.js"></script>
    <script src="assets/js/bootstrap/bootstrap.js"></script>
    <!-- feather icon js-->
    <script src="assets/js/icons/feather-icon/feather.min.js"></script>
    <script src="assets/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
    <script src="assets/js/sidebar-menu.js"></script>
    <script src="assets/js/config.js"></script>
    <!-- Plugins JS start-->
    <script src="assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/js/datatable/datatables/datatable.custom.js"></script>
    <script src="assets/js/tooltip-init.js"></script>
    <script src="assets/js/sweet-alert/sweetalert.min.js"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="assets/js/script.js"></script>

    <!-- login js-->
    <!-- Plugin used-->
  </body>


<?php 

if(isset($_POST['delete'])){

  $id =$_POST['id'];
  $type= $_POST['type'];
  $orderid = $_POST['orderid'];

  if($type == 1){
    //btc

    $del = mysqli_query($mysqli,"DELETE FROM pending WHERE id='$id'");

    if($del){

      ?>
      <script>
      
      
      swal(
           'Deleted Successfully',
          "You have deleted this invoice successfully",
          'success'
      )
      
      
      </script>
      
      <?php
    }

  }


  if($type == 2){
    //trx

    $del = mysqli_query($mysqli,"DELETE FROM pending WHERE id='$id'");

    if($del){

      ?>
      <script>
      
      
      swal(
           'Deleted Successfully',
          "You have deleted this invoice successfully",
          'success'
      )
      
      
      </script>
      
      <?php
    }

  }



  if($type == 4 or $type == 5){
    //paystack & flutterwave

    $del = mysqli_query($mysqli,"DELETE FROM pending WHERE id='$id'");

    if($del){

      ?>
      <script>
      
      
      swal(
           'Deleted Successfully',
          "You have deleted this invoice successfully",
          'success'
      )
      
      
      </script>
      
      <?php
    }

  }


  if($type == 3){
    //naira


    $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.monnify.com/api/v1/invoice/".$orderid."/cancel",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>"{\n    \"amount\": \"".$nariaamount."\",\n    \"invoiceReference\": \"".$orderid."\",\n    \"accountReference\": \"".$code."\",\n    \"description\": \"".$name."\",\n    \"currencyCode\": \"NGN\",\n    \"contractCode\": \"141217541896\",\n    \"customerEmail\": \"".$email."\",\n    \"customerName\": \"".$namee."\",\n    \"expiryDate\": \"".$expire."\"\n}",
  CURLOPT_HTTPHEADER => array(
    "Authorization: Basic TUtfUFJPRF80SlJRSkpWOE5TOlk1UzM5OUdaRjhMNE5LOFZMQ1Y5Rk1BV1U0UzMzRUs0",
    "Content-Type: application/json",
    "Cookie: __cfduid=dc265c737ad9d51d2913b0d0d668f0c001604001534"
  ),
));

$response = curl_exec($curl);

curl_close($curl);

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://api.monnify.com/api/v1/invoice/'.$orderid.'/cancel',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'DELETE',
  CURLOPT_HTTPHEADER => array(
    'Authorization: Basic TUtfUFJPRF80SlJRSkpWOE5TOlk1UzM5OUdaRjhMNE5LOFZMQ1Y5Rk1BV1U0UzMzRUs0',
    'Cookie: __cfduid=d54e80af5eb39078c6789427d7e59eb2f1613314923'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;




    $del = mysqli_query($mysqli,"DELETE FROM pending WHERE id='$id'");

    if($del){

      ?>
      <script>
      
      
      swal(
           'Deleted Successfully',
          "You have deleted this invoice successfully",
          'success'
      )
      
      
      </script>
      
      <?php
    }

  }





}


?>

</html>