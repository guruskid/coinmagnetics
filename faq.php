<?php  
include('account/connection.php');

?>
<!DOCTYPE html>
<html lang="zxx">

<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- Title -->
	<title>Coin Magnetics - FAQ</title>
	<!-- Favicon -->
	<link href="img/core-img/favicon.png" rel="icon" /><!-- Core Stylesheet -->
	<link href="css/style.css" rel="stylesheet" /><!-- Responsive Stylesheet -->
	<link href="css/responsive.css" rel="stylesheet" />

</style>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5dde42de43be710e1d1f5485/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</head>

<body class="light-version">
	<!-- Preloader -->
	<div id="preloader">
		<div class="preload-content">
			<div id="dream-load"></div>
		</div>
	</div>
	<!-- ##### Header Area Start ##### -->

<header class="header-area fadeInDown" data-wow-delay="0.2s">
        <div class="classy-nav-container light breakpoint-off">
            <div class="container">
                <!-- Classy Menu -->
                <nav class="classy-navbar light justify-content-between" id="dreamNav">

                    <!-- Logo -->
                    <a class="nav-brand light" href="#"><img src="./logo2.png" style="width: 150px; height: 30px;" /></a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler demo">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- close btn -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul id="nav">
                             <li><a href="https://coinmagnetics.com">Home</a></li>
								
								<li><a href="https://coinmagnetics.com/about">About</a></li>
								<li><a href="#services">Buy/Sell</a></li>
								<li><a href="https://coinmagnetics.com/faq">FAQ</a></li>
								<li><a href="https://coinmagnetics.com/contact">Contact</a></li>
								<li>
								<style type="text/css">

a.gflag {vertical-align:middle;font-size:16px;padding:1px 0;background-repeat:no-repeat;background-image:url(//gtranslate.net/flags/16.png);}
a.gflag img {border:0;}
a.gflag:hover {background-image:url(//gtranslate.net/flags/16a.png);}
#goog-gt-tt {display:none !important;}
.goog-te-banner-frame {display:none !important;}
.goog-te-menu-value:hover {text-decoration:none !important;}
body {top:0 !important;}
#google_translate_element2 {display:none!important;}

</style>


<br /><select onchange="doGTranslate(this);"  ><option value="">Select Language</option><option value="en|en">English</option><option value="en|hy">Armenian</option><option value="en|az">Azerbaijani</option><option value="en|eu">Basque</option><option value="en|be">Belarusian</option><option value="en|ca">Catalan</option><option value="en|zh-CN">Chinese (Simplified)</option><option value="en|nl">Dutch</option><option value="en|fr">French</option><option value="en|de">German</option><option value="en|pt">Portuguese</option><option value="en|ru">Russian</option><option value="en|es">Spanish</option></select><div style="display:none" id="google_translate_element2"></div>
<script type="text/javascript">
function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'en',autoDisplay: false}, 'google_translate_element2');}
</script><script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>


<script type="text/javascript">
/* <![CDATA[ */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,f,f);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}',43,43,'||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'),0,{}))
/* ]]> */
</script>


								</li>
							</ul>
							<!-- Button --><a class="btn login-btn ml-50"
								href="https://coinmagnetics.com/">Log in</a>
						</div>
						<!-- Nav End -->
					</div>
				</nav>
			</div>
		</div>
	</header>
	<!-- ##### Header Area End ##### -->
	
		<!-- ##### FAQ & Timeline Area Start ##### -->

		<div class="faq-timeline-area section-padding-100-85" id="faq">
			<div class="container">
				<div class="section-heading text-center">
					<!-- Dream Dots -->
					<div class="dream-dots justify-content-center fadeInUp" data-wow-delay="0.2s"><span>Coin Magnetics
							FAQ</span></div>

					<h2 class="fadeInUp" data-wow-delay="0.3s">Frequently Questions</h2>

					<p class="fadeInUp" data-wow-delay="0.4s">Coin Magnetics Digital Asset management system Frequently Asked
						Question can be found bellow. Feel free to <a href="https://coinmagnetics.com/contact">Contact Us</a></p>
					
				</div>
			</div>

			<div class="row align-items-center">
				<div class="col-12 col-lg-6 offset-lg-0 col-md-8 offset-md-2 col-sm-12"><img alt=""
						class="center-block img-responsive" src="img/core-img/0faq.png" /></div>

				<div class="col-12 col-lg-6 col-md-12">
					<div class="dream-faq-area mt-s ">
						<dl style="margin-bottom:0">
							<!-- Single FAQ Area -->
							<dt class="wave fadeInUp" data-wow-delay="0.2s">How do I buy Cryptocurrency on Coin Magnetics?</dt>
							<dd class="fadeInUp" data-wow-delay="0.3s">
								<p></p>
								
								<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Using Web browser</span></div>
								
								<h6 class="fadeInUp" data-wow-delay="0.3s">Go to the Buy & Sell page.</h6>
<p>>>Select the type of digital currency you'd like to buy.</p>
<p>>>Enter the amount you'd like to buy denominated in either digital currency or your local currency value. (Eg. USD or NGN).</p>
<p>>>Select your desired payment method.</p>
<p>Confirm the order is correct and click Buy.</p>

<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Using the iOS & Android</span></div>
    
    <h6 class="fadeInUp" data-wow-delay="0.3s">Tap the “Buy Crypto” on the screen to open the modal..</h6>
								
<p>>>Select the type of digital currency you'd like to buy.</p>
<p>>>Enter the amount you'd like to buy denominated in either digital currency or your local currency value. (Eg. USD or NGN).</p>
<p>>>Select your desired payment method.</p>
<p>After reviewing your order, tap the Buy button.</p>
</dd>

							
							<!-- Single FAQ Area -->
							<dt class="wave fadeInUp" data-wow-delay="0.3s">How do I sell Cryptocurrency on Coin Magnetics?</dt>
							<dd>
								<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Using Web browser</span></div>
								
								<h6 class="fadeInUp" data-wow-delay="0.3s">Go to the Buy & Sell page.</h6>
<p>>>Select the type of digital currency you'd like to sell.<p>
<p>>>Enter the amount you'd like to sell denominated in either digital currency or your local currency value. (Eg. USD or NGN),</p>
<p>>>Select where you want your funds deposited.</p>
<p>Confirm the order is correct and click Sell.</p>

<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Using the iOS & Android</span></div>
    
    <h6 class="fadeInUp" data-wow-delay="0.3s">Tap the “Buy Crypto” on the screen to open the modal..</h6>
								
<p>>>Select the type of digital currency you'd like to buy.</p>
<p>>>Enter the amount you'd like to buy denominated in either digital currency or your local currency, value (Eg. USD or NGN).</p>
<p>>>Select your desired payment method.</p>
<p>After reviewing your order, tap the Buy button.</p>
							</dd>
							
							<!-- Single FAQ Area -->
							<dt class="wave fadeInUp" data-wow-delay="0.4s">How do I send out Cryptocurrency on Coin Magnetics?</dt>
							<dd>
								<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Using Web browser</span></div>
								
								<h6 class="fadeInUp" data-wow-delay="0.3s">Go to the Wallets page of the coin you wish to send out.</h6>
<p>>>Enter the address you’d like to send your cryptocurrency to.<p>
<p>>>Enter the amount you'd like to send denominated in either digital currency or your local currency value (Eg. USD or NGN).</p>
<p>>>Review and Confirm the order is correct.</p>
<p>>>Confirm and click Send.</p>

<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Using the iOS & Android</span></div>
    
    <h6 class="fadeInUp" data-wow-delay="0.3s">Open the Coin Magnetics app.</h6>
								
<p>>>Select the type of digital currency you'd like to send on the dashboard.</p>
<p>>>Tap the send button on the selected cryptocurrency page.</p>
<p>>>Enter the address of the wallet you’d like to send your cryptocurrency to and click on continue.</p>
<p>>>Enter the amount you’d like to send denominated in either digital currency or your local currency (Eg. USD or NGN).</p>
<p>>>Review and Confirm the order is correct and click Send.</p>
<p>>>Click on continue.</p>
<p>After reviewing your order, tap the Buy button.</p>
							</dd>
							
							<!-- Single FAQ Area -->
							<dt class="wave fadeInUp" data-wow-delay="0.5s">How do I receive Cryptocurrency on Coin Magnetics?</dt>
							<dd>
									<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Using Web browser</span></div>
								
								<h6 class="fadeInUp" data-wow-delay="0.3s">Go to the Wallets page of the coin you wish to receive.</h6>
<p>>>Copy your cryptocurrency wallet address.<p>
<p>>>Or click on “Display QR Code”</p>
<p>>>Give the copied wallet address to the sender.</p>
<p>Once the sender send the coin successfully, the coin will show up on your wallet.</p>

<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Using the iOS & Android</span></div>
    
    <h6 class="fadeInUp" data-wow-delay="0.3s">Open the Coin Magnetics app.</h6>
								
<p>>>Select the type of digital currency you'd like to receive on the dashboard.</p>
<p>>>Tap the receive button on the selected cryptocurrency page..</p>
<p>>>Confirm that you understand the information displayed.</p>
<p>>>Copy your cryptocurrency wallet address</p>
<p>>>Or click on “Display QR Code”</p>
<p>>>Give the copied wallet address to the sender.</p>
<p>Once the sender send the coin successfully, the coin will show up on your wallet.</p>
							</dd>
							
							<!-- Single FAQ Area -->
							<dt class="wave fadeInUp" data-wow-delay="0.5s">How do I deposit FAIT on my Coin Magnetics account?</dt>
							<dd>
								<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Using Web browser</span></div>
								
								<h6 class="fadeInUp" data-wow-delay="0.3s">Click on deposit icon on your dashboard or under the menu.</h6>
<p>>>Enter the FIAT amount you wish to deposit (Eg. USD or NGN) and click proceed.<p>
<p>>>Select your desired payment method and click proceed.</p>
<p>>>Review your payment invoice and complete the payment </p>
<p>Once your payment goes through successfully, the amount will show up on your coin magnetics account within seconds.</p>

<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Using the iOS & Android</span></div>
    
    <h6 class="fadeInUp" data-wow-delay="0.3s">Open the Coin Magnetics app.</h6>
								
<p>>>Click on deposit icon on your dashboard or at the base of the app.</p>
<p>>>Enter the FIAT amount you wish to deposit (Eg. USD or NGN) and click proceed.</p>
<p>>>Select your desired payment method and click proceed.</p>
<p>>>Review your payment invoice and complete the payment.</p> 
<p>Once your payment goes through successfully, the amount will show up on your coin magnetics account within seconds.</p>
							</dd>
							
														<!-- Single FAQ Area -->
							<dt class="wave fadeInUp" data-wow-delay="0.5s">Can’t find what you’re looking for?</dt>
							<dd>
								<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Contact Us</span></div>
								
								<p>Email: support@coinmagnetics.com</p>
									
								<div class="dream-btn-group fadeInUp" data-wow-delay="0.4s"><a
									href="https://t.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/tg.png"
										style="width: 43px; height: 40px; float: left;" /></a>&nbsp; &nbsp;&nbsp;
								&nbsp;<a href="https://www.instagram.com/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/ig.png" style="width: 42px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp; &nbsp;<a href="https://twitter.com/Coinmagnetics"><img alt=""
										src="/img/social/tw.png" style="width: 43px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp;&nbsp;<a href="https://fb.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/fb.png" style="width: 43px; height: 40px;" /></a></div>
						</dl>
					</div>
				</div>
			</div>
		</div>
		<!-- ##### FAQ & Timeline Area End ##### -->
	
	
	<section class="about-us-area  section-padding-100-0 gradient-section-bg clearfix " id="affiliate">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-12 col-lg-5 offset-lg-1 col-sm-10 offset-sm-1">
					<div class="who-we-contant">
						<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s"
							style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
							<span>Get Started</span></div>

						<h4 class="fadeInUp" data-wow-delay="0.3s">It’s never too late to get started</h4>

						<p class="text-white">Trade, Buy, Sell, Swap, Mine and Save All Digtal Assets</p>
							
						<div class="clearfix"></div>
						<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><href="#home"><img alt="" src="./ic_website_sign_up.svg" style="width: 120px; height: 25px;" /></a>
							
								<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-google.svg" style="width: 120px; height: 25px;" /></a>
							
							<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-apple.svg" style="width: 120px; height: 25px;" /></a>
					</div>
				</div>

				<div class="col-12 col-lg-6 mt-s no-padding-right">
					<div class="welcome-meter fadeInUp" data-wow-delay="0.7s"><img alt=""
							class="img-responsive center-block" src="img/core-img/trading1.png" style="width: 460px; height: 260px;" /></div>
				</div>
			</div>
		</div>

			<!-- ##### Contact Area End ##### -->

			<div class="footer-content-area ">
				<div class="container">
					<div class="row ">
						<div class="col-12 col-lg-4 col-md-6">
							<div class="footer-copywrite-info">
								<!-- Copywrite -->
								<div class="copywrite_text fadeInUp" data-wow-delay="0.2s">
									<div class="footer-logo"><a href="#"><img alt="logo" src="./logo2.png"
												style="width: 230px; height: 40px;" /></a></div>

									<p>Plug-in to World's N0 1 Digital Asset management system today and Start Earning Big!.
									</p>
								</div>
								<!-- Social Icon -->

								<div class="footer-social-info fadeInUp" data-wow-delay="0.4s"></div>
							</div>
						</div>

						<div class="col-12 col-lg-3 col-md-6">
							<div class="contact_info_area d-sm-flex justify-content-between">
								<!-- Content Info -->
								<div class="contact_info mt-x text-center fadeInUp" data-wow-delay="0.3s">
									<h5>PRIVACY &amp; TOS</h5>

									<p><a href="https://coinmagnetics.com/terms">Terms Of Use</a></p>

									<p><a href="https://coinmagnetics.com/Privacy-policy">Privacy Policy</a></p>

									<p><a href="https://coinmagnetics.com/compliance">Legal & Compliance</a></p>

									<p><a href="https://coinmagnetics.com/Customer-pro">Customer protection</a></p>
									
									<p>Advertiser Agreement</p>
								</div>
							</div>
						</div>

						<div class="col-12 col-lg-2 col-md-6 ">
							<!-- Content Info -->
							<div class="contact_info_area d-sm-flex justify-content-between">
								<div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.2s">
									<h5>NAVIGATE</h5>

									<p><a href="https://coinmagnetics.com/about">About Us</a></p>
									
									<p><a href="https://coinmagnetics.com/contact">Contact Us</a></p>
									
									<p><a href="https://coinmagnetics.com/faq">FAQ</a></p>

									<p>Developers</p>

									<p>Resources</p>
								</div>
							</div>
						</div>

						<div class="col-12 col-lg-3 col-md-6 ">
							<div class="contact_info_area d-sm-flex justify-content-between">
								<!-- Content Info -->
								<div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.4s">
									<h5>FIND US HERE</h5>

									<p>Mailing Address:265 Akowonjo RD,</p>

									<p>Lagos, Nigeria. Los 23401</p>

									
									<p>Email: support@coinmagnetics.com</p>
									
								<div class="dream-btn-group fadeInUp" data-wow-delay="0.4s"><a
									href="https://t.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/tg.png"
										style="width: 43px; height: 40px; float: left;" /></a>&nbsp; &nbsp;&nbsp;
								&nbsp;<a href="https://www.instagram.com/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/ig.png" style="width: 42px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp; &nbsp;<a href="https://twitter.com/Coinmagnetics"><img alt=""
										src="/img/social/tw.png" style="width: 43px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp;&nbsp;<a href="https://fb.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/fb.png" style="width: 43px; height: 40px;" /></a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- ##### Footer Area End ##### -->
		<!-- ########## All JS ########## -->
		<!-- jQuery js -->
		<script src="js/jquery.min.js"></script><!-- Popper js -->
		<script src="js/popper.min.js"></script><!-- Bootstrap js -->
		<script src="js/bootstrap.min.js"></script><!-- All Plugins js -->
		<script src="js/plugins.js"></script><!-- Parallax js -->
		<script src="js/dzsparallaxer.js"></script>
		<script src="js/jquery.syotimer.min.js"></script><!-- script js -->
		<script src="js/script.js"></script>
	</div>
</body>
<script>
$('#recipeCarousel').carousel({
  interval: 4000
})

$('.carousel .carousel-item').each(function(){
    var minPerSlide = 3;
    var next = $(this).next();
    if (!next.length) {
    next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
    
    for (var i=0;i<minPerSlide;i++) {
        next=next.next();
        if (!next.length) {
        	next = $(this).siblings(':first');
      	}
        
        next.children(':first-child').clone().appendTo($(this));
      }
});

	</script>

</html>	
	
	
	
	