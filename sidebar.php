<?php
 //Get file name
 //so as to know which file is active
      $currentFile = $_SERVER["SCRIPT_NAME"];
	  //extract it from forward /
      $parts = Explode('/', $currentFile);
      $currentFile = $parts[count($parts) - 1]; 
	  
	  

 
 ?>
<style>
.main-nav{
  height:100% !important;
}
  </style>

<header class="main-nav">
        <div class="logo-wrapper"><a href="dashboard" style="float:left"><img class="img-fluid for-light"
              src="assets/images/logo/logo2.png"  alt=""><img class="img-fluid for-dark"
              src="assets/images/logo/logo2.png" height="28" width="100"   alt=""></a>
          <div class="back-btn" style="color:white"><i class="fa fa-arrow-left"></i></div>
          <div class="toggle-sidebar"><i class="status_toggle middle" data-feather="grid" id="sidebar-toggle"> </i>
          </div>
        </div>
        <div class="logo-icon-wrapper"><a href="dashboard"><img class="img-fluid"
              src="assets/images/logo/logo-icon.png" width="20"   alt=""></a></div>
        <nav>
          <div class="main-navbar">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="mainnav">
              <ul class="nav-menu custom-scrollbar">
                <li class="back-btn"><a href="dashboard"><img class="img-fluid"   src="assets/images/logo/logo2.png"
                      alt=""></a>
                  <div class="mobile-back text-right"><span>Back</span><i class="fa fa-angle-right pl-2"
                      aria-hidden="true"></i></div>
                </li>
                <li class="sidebar-title">
                  <div>
                    <h6 class="lan-1">General</h6>
                    <p class="">Dashboard & Profile.</p>
                  </div>
                </li>
                <li class="dropdown"><a class="nav-link menu-title link-nav" href="dashboard"><i data-feather="home"></i><span >Dashboard</span>
                    </a>
                  
                </li>
                <?php if($rows['cycle']==0){  ?>
                <li class="dropdown"<?php if($rows['2fa']==0){ ?> data-intro="Please Enable 2FA On Your Account For More Security" <?php  } ?>><a class="nav-link menu-title " href="#"><i data-feather="user"></i><span >Account</span></a>
                  <ul class="nav-submenu menu-content">
                    <li><a href="profile">Account Information</a></li>
                    <li><a  href="2fa">2 Factor Authentication</a></li>
                    <li><a href="change-password">Change Password</a></li>
                  </ul>
                 
                </li>
               
                
                <li class="sidebar-title">
                  <div>
                    <h6 class="">Application</h6>
                    <p class="">Central App</p>
                  </div>
                </li>
                <li class="dropdown"><a class="nav-link menu-title link-nav" href="hash"><i data-feather="zap"></i><span>Rent Pool
                      </span></a>
                  
                </li>
                <li class="dropdown"><a class="nav-link menu-title link-nav" href="mining"><i
                  data-feather="git-pull-request"> </i><span>Mining Pools</span></a></li>

                  <li class="dropdown"><a class="nav-link menu-title link-nav" href="transfer"><i
                  data-feather="send"> </i><span>Transfer Funds</span></a></li>

                  <li class="dropdown"> <a class="nav-link menu-title link-nav" href="activity"><i
                    data-feather="activity"> </i><span>Activity
                    <label class="badge badge-primary">Recent</label></span></a></li>

                    <li class="dropdown"><a class="nav-link menu-title link-nav" href="settle"><i
                  data-feather="git-branch"> </i><span>Settlements</span></a></li>
                    

                <li class="dropdown"><a class="nav-link menu-title link-nav" href="withdraw"><i
                      data-feather="mail"> </i><span>Withdraw Revenue</span></a></li>
                 <?php } ?>

            
                <li class="sidebar-title">
                  <div>
                    <h6>Miscellaneous</h6>
                    <p>Pool commander & Logout </p>
                  </div>
                </li>
                <?php if($rows['cycle']==0){  ?>
                <li class="dropdown"><a class="nav-link menu-title link-nav" href="referrals"><i data-feather="users"></i><span>Co-Pool Members</span></a>
                </li>
                <?php } ?>
                <li class="dropdown"><a class="nav-link menu-title link-nav" href="video"><i data-feather="video"></i><span>Testimony</span></a>
                </li>

                <li class="dropdown"><a class="nav-link menu-title link-nav" href="logout"><i
                      data-feather="log-out"></i><span>Log Out</span></a>
                 
                </li>
                
               
         
              </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
          </div>
        </nav>
      </header>
