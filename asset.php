﻿<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="themexriver">

    <!-- Page Title -->
    <title> Asset and wealth management |  Argen Capital </title>

    <!-- Favicon and Touch Icons -->
    <link href="assets\images\favicon\favicon.png" rel="shortcut icon" type="image/png">
    <link href="assets\images\favicon\apple-touch-icon-57x57.png" rel="apple-touch-icon" sizes="57x57">
    <link href="assets\images\favicon\apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
    <link href="assets\images\favicon\apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
    <link href="assets\images\favicon\apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

    <!-- Icon fonts -->
    <link href="assets\css\font-awesome.min.css" rel="stylesheet">
    <link href="assets\css\flaticon.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="assets\css\bootstrap.min.css" rel="stylesheet">

    <!-- Plugins for this template -->
    <link href="assets\css\animate.css" rel="stylesheet">
    <link href="assets\css\owl.carousel.css" rel="stylesheet">
    <link href="assets\css\owl.theme.css" rel="stylesheet">
    <link href="assets\css\slick.css" rel="stylesheet">
    <link href="assets\css\slick-theme.css" rel="stylesheet">
    <link href="assets\css\owl.transitions.css" rel="stylesheet">
    <link href="assets\css\jquery.fancybox.css" rel="stylesheet">
    <link href="assets\css\bootstrap-select.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets\css\style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="service-single-pg">

    <!-- start page-wrapper -->
    <div class="page-wrapper">

        <!-- start preloader -->
        <div class="preloader">
            <div class="inner">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- end preloader -->

        <!-- Start header -->
       <?php include('header.php'); ?>
        <!-- end of header -->


        <!-- start page-title -->
        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>Asset and wealth management</h2>
                        <ol class="breadcrumb">
                            <li><a href="index">Home</a></li>
                            <li>Asset and wealth management</li>
                        </ol>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>        
        <!-- end page-title -->


        <!-- start service-single-section -->
        <section class="service-single-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-12 ">
                        <div class="service-single-content">
                            <div class="img-holder">
                               <img src="assets\images\service-single\img-1.jpg" alt=""> 
                            </div>
                            <h3>Asset and wealth management</h3>
                            <p>Here in ArgenCapital, assets and wealth management as the name implies, simply means the management of assets like equities, real estate and global investments, together with the overseeing of all financial aspects of our clients towards taking steps to maximize their wealths and protect it down the line, which tends to focus on your investments as an investor. We have well skilled personnels who are always there to help determine which investments are best-suited to your financial situation. This means they’ll help you with things like asset and wealth allocation and as well choose how to divide your investable assets among different asset and wealth management classes. AWM focus more on growing investor’s money in respect with the overall financial situations and steps to ensure it will be protected at long-run. This depends on what our investors goals are and what services he or she needs.  
                                <br/><br/>
                                As an organization, we are always here to help you find best investment portfolio that fits your needs but the question comes down to what services you need, about choosing and managing your investments, investing with us. We look broadly at ones financial life and portfolio without bridging any gaps between rich, average and low-class investor because investing is all about what you can afford to invest at a particular point in life. Asset and wealth management is a pretty straightforward financial services to understand as an investor because it relates to all your financial holdings which typically focus on your investments including your stocks, bonds, mutual funds, ETFs and any other quantifiable investments you hold which could gain value in the future. We are here as an organization to allocate wealth and divide investable assets to fall in line with your needs, we take a look at your portfolio and determine what percentage of it should be built on stocks and growth products, we also takes into account all financial aspects relating to our clients, we also focus on the preservation of our client finance while aiming to produce tangible returns on investments. 
                                <br/><br/>
                                As an investor, it’s vital ensuring that you continue to assure yourself of financial stability, consolidating for long-term wealth into the future, so as to maximize the potential of your assets and wealth. As an organization, we provide guidance when accounting for any alignments, marketing, operations, human resources, regulation, restructuring and in risk management when investing with us.   From our deep research work so far, assets and wealth management can accelerate the turnaround by funding the future, safeguard the future by providing for it and can change the future for better by embracing environmental, social and governance issues, integral to ones investment strategy. Our goal is to grow investors portfolios over time while mitigating risk.</p>
                           

                            
                          
                        </div> <!-- end service-single-content -->
                    </div> <!-- end col -->
                  
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end service-single-section -->


        <!-- start site-footer -->
      <?php include('footer.php'); ?>
        <!-- end site-footer -->        
    </div>
    <!-- end of page-wrapper -->



    <!-- All JavaScript files
    ================================================== -->
    <script src="assets\js\jquery.min.js"></script>
    <script src="assets\js\bootstrap.min.js"></script>

    <!-- Plugins for this template -->
    <script src="assets\js\jquery-plugin-collection.js"></script>

    <!-- Custom script for this template -->
    <script src="assets\js\script.js"></script>
</body>
</html>
