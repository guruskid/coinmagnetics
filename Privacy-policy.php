<?php  
include('account/connection.php');

?>
<!DOCTYPE html>
<html lang="zxx">

<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- Title -->
	<title>Coin Magnetics - Privacy-policy</title>
	<!-- Favicon -->
	<link href="img/core-img/favicon.png" rel="icon" /><!-- Core Stylesheet -->
	<link href="css/style.css" rel="stylesheet" /><!-- Responsive Stylesheet -->
	<link href="css/responsive.css" rel="stylesheet" />
	<style>
@media (max-width: 768px) {
    .carousel-inner .carousel-item > div {
        display: none;
    }
    .carousel-inner .carousel-item > div:first-child {
        display: block;
    }
}

.carousel-inner .carousel-item.active,
.carousel-inner .carousel-item-next,
.carousel-inner .carousel-item-prev {
    display: flex;
}

/* display 3 */
@media (min-width: 768px) {
    
    .carousel-inner .carousel-item-right.active,
    .carousel-inner .carousel-item-next {
      transform: translateX(33.333%);
    }
    
    .carousel-inner .carousel-item-left.active, 
    .carousel-inner .carousel-item-prev {
      transform: translateX(-33.333%);
    }
}

.carousel-inner .carousel-item-right,
.carousel-inner .carousel-item-left{ 
  transform: translateX(0);
}


</style>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5dde42de43be710e1d1f5485/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</head>

<body class="light-version">
	<!-- Preloader -->
	<div id="preloader">
		<div class="preload-content">
			<div id="dream-load"></div>
		</div>
	</div>
	<!-- ##### Header Area Start ##### -->

<header class="header-area fadeInDown" data-wow-delay="0.2s">
        <div class="classy-nav-container light breakpoint-off">
            <div class="container">
                <!-- Classy Menu -->
                <nav class="classy-navbar light justify-content-between" id="dreamNav">

                    <!-- Logo -->
                    <a class="nav-brand light" href="#"><img src="./logo2.png" style="width: 150px; height: 30px;" /></a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler demo">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- close btn -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul id="nav">
                             <li><a href="https://coinmagnetics.com">Home</a></li>
								
								<li><a href="https://coinmagnetics.com/about">About</a></li>
								<li><a href="#services">Features</a></li>
								<li><a href="https://coinmagnetics.com/faq">FAQ</a></li>
								<li><a href="https://coinmagnetics.com/contact">Contact</a></li>
								<li>
								<style type="text/css">

a.gflag {vertical-align:middle;font-size:16px;padding:1px 0;background-repeat:no-repeat;background-image:url(//gtranslate.net/flags/16.png);}
a.gflag img {border:0;}
a.gflag:hover {background-image:url(//gtranslate.net/flags/16a.png);}
#goog-gt-tt {display:none !important;}
.goog-te-banner-frame {display:none !important;}
.goog-te-menu-value:hover {text-decoration:none !important;}
body {top:0 !important;}
#google_translate_element2 {display:none!important;}

</style>


<br /><select onchange="doGTranslate(this);"  ><option value="">Select Language</option><option value="en|en">English</option><option value="en|hy">Armenian</option><option value="en|az">Azerbaijani</option><option value="en|eu">Basque</option><option value="en|be">Belarusian</option><option value="en|ca">Catalan</option><option value="en|zh-CN">Chinese (Simplified)</option><option value="en|nl">Dutch</option><option value="en|fr">French</option><option value="en|de">German</option><option value="en|pt">Portuguese</option><option value="en|ru">Russian</option><option value="en|es">Spanish</option></select><div style="display:none" id="google_translate_element2"></div>
<script type="text/javascript">
function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'en',autoDisplay: false}, 'google_translate_element2');}
</script><script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>


<script type="text/javascript">
/* <![CDATA[ */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,f,f);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}',43,43,'||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'),0,{}))
/* ]]> */
</script>


								</li>
							</ul>
							<!-- Button --><a class="btn login-btn ml-50"
								href="https://coinmagnetics.com/">Log in</a>
						</div>
						<!-- Nav End -->
					</div>
				</nav>
			</div>
		</div>
	</header>
	<!-- ##### Header Area End ##### -->
	<section class="faq-timeline-area  section-padding-150-0 clearfix " id="about">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-12 col-lg-5 offset-lg-1 col-sm-10 offset-sm-1">
					<div class="who-we-contant">

				<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Privacy policy<strong></h2>


<h4 class="fadeInUp" data-wow-delay="0.3s">How we gather, secure and use your personal data.</h4>

<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Last
updated: August 2021</span></div>

<h6 class="fadeInUp" data-wow-delay="0.3s">At Coin Magnetics, we value your privacy and
strive to protect your personal data. Coin Magnetics will only collect and use
your personal data in accordance with this Privacy policy and our&nbsp;<a
href="https://www.coinmagnetics.com/terms" target="_blank">Terms of use.</h6>

						<h6 class="fadeInUp" data-wow-delay="0.3s">Reference in this policy to “Coin Magnetics”, “we”, “us”, “our” or any similar term is to Cypher Mobiles Ltd.
who is the data controller.</h6>



				<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>How we collect personal data.</strong></h2>

<h6 class="fadeInUp" data-wow-delay="0.3s">When we open and operate an account for you,
provide you with our products and services, or communicate with you, we may
collect your personal data. We do this in various ways, including.</h6>

<ul type=disc>
 <h6 class="fadeInUp" data-wow-delay="0.3s">When you provide it to us</span></b><span
     style='font-size:12.0pt;font-family:"Helvetica","sans-serif";mso-fareast-font-family:
     "Times New Roman"'>&nbsp;such as when you sign up for a Coin Magnetics
     account, use our products and services, or take part in customer surveys,
     competitions and promotions;</p>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">When you communicate with us</span></b><span
     style='font-size:12.0pt;font-family:"Helvetica","sans-serif";mso-fareast-font-family:
     "Times New Roman"'>&nbsp;by email, chat, telephone or any other means, we
     collect the communication and any data provided in it;<o:p></o:p></span></li>
     
<h6 class="fadeInUp" data-wow-delay="0.3s">When you use</span></b><span
     style='font-size:12.0pt;font-family:"Helvetica","sans-serif";mso-fareast-font-family:
     "Times New Roman"'>&nbsp;the Coin Magnetics platform we collect
     information on your transactions and other use of your Coin Magnetics
     account;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">When we obtain information from
     third parties</span></b><span style='font-size:12.0pt;font-family:"Helvetica","sans-serif";
     mso-fareast-font-family:"Times New Roman"'>&nbsp;such as identity
     verification services, credit reference agencies, and regulatory and
     enforcement agencies.</h6>
</ul>


		
<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>How we use cookies</strong></h2>

<h6 class="fadeInUp" data-wow-delay="0.3s">We also collect personal data through the use
of cookies. Cookies (and other similar technologies) help us give you the best
experience of using our site.<o:p></o:p></span></p>

<h6 class="fadeInUp" data-wow-delay="0.3s">Cookies are small data files that we or
companies we work with may place on your computer or other devices when you
visit our website. They allow us to remember your actions or preferences over
time.<o:p></o:p></span></p>
</ul>

<h5 class="fadeInUp" data-wow-delay="0.3s"><strong>We use cookies to collect data that helps us
to:<strong></h5>

<ul type=disc>
 <h6 class="fadeInUp" data-wow-delay="0.3s">Track site usage and browsing behavior;<o:p></o:p></span></li>
 
 <h6 class="fadeInUp" data-wow-delay="0.3s">Allow you to sign in to your
     account and navigate through the website;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">Tailor our website’s functionality
     to you personally by letting us remember your preferences;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">Improve how our website
     performs;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">Allow third parties to provide
     services to our website;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">Monitor the effectiveness of
     our promotions and advertising; and<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">Mitigate</span></span><span
     style='font-size:12.0pt;font-family:"Helvetica","sans-serif";mso-fareast-font-family:
     "Times New Roman"'> risk, enhance security and help prevent fraud.<o:p></o:p></span></li>
</ul>

<h6 class="fadeInUp" data-wow-delay="0.3s">We use both session and persistent cookies.
Session cookies are deleted when you close down your browser, while persistent
cookies remain on your device until they expire or you delete them. Persistent
cookies allow us to remember things about you when you visit our website again.</h6>
</ul>


		
<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Third-party cookies</strong></h2>

<h6 class="fadeInUp" data-wow-delay="0.3s">We may use authorized third-party providers to
help us with various aspects of our business, such as website operations,
services, applications, advertising, <span class=GramE>support</span> tools and
to serve you relevant content.<o:p></o:p></span></p>

<h6 class="fadeInUp" data-wow-delay="0.3s">These service providers may place cookies on
your device (third-party cookies) or make use of similar tracking technologies
such as web beacons. No personally identifiable information is stored in third-party
cookies. The information reported to us is aggregated and anonymous. We use
this information to understand, for example, the effectiveness of our
advertising and marketing. Web beacons are small graphic images (also known as
“pixel tags” or “clear GIFs”) which may be used to collect and store
information about visits to our website (such as which pages you viewed and how
long you spent on the website) and communication efficiency (such as the email
delivery and open rates). Where necessary, the information gathered by web
beacons may be tied to your personal data strictly for the purposes set out
herein.</h6>

	
		
				<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Declining cookie</strong></h2>

				<h6 class="fadeInUp" data-wow-delay="0.3s">Overall, cookies help us provide you with a
better website, by enabling us to monitor which pages you find useful and which
you do not. A cookie in no way gives us access to your computer or any
information about you, other than the data you choose to share with us.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Certain aspects and features of our services
are only available through the use of cookies. By signing up for an account with
Coin Magnetics, or continuing to use our website, you agree to our use of
cookies as set out in this policy. You may decline our cookies if your browser
or browser add-on permits, but doing so may interfere with your use of Coin Magnetics's services. For information on how to delete or
reject cookies, you can consult the help function within your
browser, or visit www.allaboutcookies.org where you will also find more information data about cookies generally.</h6>


			
<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>How we use your
personal data</strong></h2>

<h5 class="fadeInUp" data-wow-delay="0.3s">We use personal data for one or more of the
following purposes:</h5>

<ul type=disc>
<h6 class="fadeInUp" data-wow-delay="0.3s">To verify your identity in
     accordance with Know Your Customer (KYC), Anti-Money Laundering (AML,
     Counter-Terrorist Financing (CFT) and sanctions screening requirements<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">To manage and maintain your
     account with us<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">To prevent fraudulent or unauthorized
     use of our products and services;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">To better manage our business
     and your relationship with us;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">To improve our products and
     services, and to develop new products and services;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">To notify you about benefits
     and changes to the features of our products and services;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">To provide you with personalized
     advertising and marketing;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s"> Respond to your enquiries and to resolve disputes.<o:p></o:p></span></li>
</ul>

<h6 class="fadeInUp" data-wow-delay="0.3s">Where necessary to protect our legal rights
and interests, or the interests of others, we may also use personal data in
relation to legal claims, compliance, audit, risk management, law enforcement
processes and regulatory functions. We may also use personal data in connection
with the acquisition, merger or sale of a business.</h6>


		
<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>How we share personal
data</strong></h2>

<h5 class="fadeInUp" data-wow-delay="0.3s">We share personal data with:</h5>

<ul type=disc>
 <h6 class="fadeInUp" data-wow-delay="0.3s">Any person that works for us or
     for one of our group companies;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">Any entity that forms part of
     the Coin Magnetics group of companies, including where relevant the Coin
     Magnetics operating entity in the country or region in which you live;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">Financial and other
     institutions we partner with to provide our products and services;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">Companies and organizations
     that provide services to us, including in relation to technical
     infrastructure, marketing and analytics, and web and app development;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">Companies and organizations
     that assist us with identity verification, background screening, due
     diligence and processing or otherwise fulfilling transactions that you
     have requested;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s"> Professional advisers, consultants and other similar
     services.<o:p></o:p></span></li>
</ul>


		
<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Security of your
personal data</strong></h2>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics places great importance on
ensuring the security of your personal data. We regularly review and implement
up-to-date technical and organizational security measures when processing your
personal data. Employees of Coin Magnetics are trained to handle personal data
securely and with the utmost respect, failing which they may be subject to
disciplinary action.</h6>


<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Global company</strong></h2>

<h6 class="fadeInUp" data-wow-delay="0.3s">The personal data we collect may be transferred
to, stored and processed outside of the jurisdiction in which you reside, and
the laws of those countries may differ from the laws applicable in your own
country. For example, data collected in the European Economic Area (EEA) may be
transferred to, stored and processed at a destination(s) outside of the EEA.
Any processing of such data will be undertaken by our staff, or the staff of
our third-party service providers, whose roles will include verifying your
identity, processing payment details, and providing customer support.<o:p></o:p></span></p>

<h6 class="fadeInUp" data-wow-delay="0.3s">By accepting this Privacy policy, and then
submitting your personal data, you agree to the transfer, storing or processing
of it outside of your jurisdiction.</h6>


<h2 class="fadeInUp" data-wow-delay="0.3s">Additional matters
relating to personal data</strong></h2>

<h5 class="fadeInUp" data-wow-delay="0.3s">Retention of personal
data</h5>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics will retain your personal data,
in compliance with this Privacy policy and the Terms of use, for the duration
of your relationship with us, and afterwards for such period as may be
necessary for our legitimate business purposes (including compliance with our
legal obligations, preventing fraud, resolving disputes and enforcing
agreements), to protect the interests of Coin Magnetics and its customers, and
as required by law.</h6>


<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Incomplete personal
data</strong></h2>

<h6 class="fadeInUp" data-wow-delay="0.3s">Where indicated (for example in application
forms or account opening forms), it is obligatory to provide your personal data
to us to enable us to process your application for our products or services.
Should you decline to provide such personal data, we may not be able to process
your application/request or provide you with our products or services.</h6>


<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Your <span
class=GramE>right to access, update</span>, or remove your personal data</strong></h2>

<h6 class="fadeInUp" data-wow-delay="0.3s">Most of the data Coin Magnetics collects, and
the ways in which we use it, are necessary for us to provide and improve the
services we provide to you, or to comply with our obligations. In certain
situations, we give you the ability to choose how we use your data.<o:p></o:p></span></p>

<h6 class="fadeInUp" data-wow-delay="0.3s">Depending on the country in which you live,
you may have certain rights under data protection law, including the right to
object to the processing of your personal data or to request that <span
class=GramE>we</span>:<o:p></o:p></span></p>

<ul type=disc>
 <h6 class="fadeInUp" data-wow-delay="0.3s">provide you with a copy of your
     personal data (including in a format that can be shared with a new
     provider); or<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">Correct, delete, or restrict
     the processing of your personal data.<o:p></o:p></span></li>
</ul>

<h6 class="fadeInUp" data-wow-delay="0.3s">Please submit a support ticket. if you would like to exercise any of the
above rights. These rights are limited in some situations, such as where we are
legally required to process your data, and may limit your ability to use our
products and services.</h6>

	
<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Data breaches</strong></h2>

<h6 class="fadeInUp" data-wow-delay="0.3s">We will notify you and the relevant
supervisory authority as soon as we become aware of any data breach that is
likely to result in a risk to your rights and freedoms.</h6>


<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Credit and debit card
data</strong></h2>

<h6 class="fadeInUp" data-wow-delay="0.3s">We do not store your full credit and debit
card data. This data is securely transferred and stored off-site by an authorized
payment vendor in compliance with Payment Card Industry Data Security Standards
(PCI DSS). This information is not accessible to Coin Magnetics or Coin <span
class=SpellE>Magnetics’s</span> employees or any agent acting for or on behalf
of Coin Magnetics.</h6>


<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Revisions to this
Privacy policy</strong></h2>

<h6 class="fadeInUp" data-wow-delay="0.3s">We may amend this Privacy policy from time to
time. You should visit the website regularly to check when this Privacy policy
was last updated and to review the current policy. We will do our best to
notify you of any substantive amendments to the Privacy policy and any such
notice will be posted on our application or our website, or sent by email to
the address associated with your Coin Magnetics Account.</h6>

		
<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Contacting Coin
Magnetics in relation to personal data</strong></h2>

<h6 class="fadeInUp" data-wow-delay="0.3s">Should you have any query in relation to this Privacy policy or
how we handle your personal data, please contact us by submitting a support ticket</h6>

						
					</div>
				</div>
	</section>

	<div class="clearfix"></div>
	<!-- ##### Our Trial Area End ##### -->

	<section class="about-us-area  section-padding-100-0 gradient-section-bg clearfix " id="affiliate">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-12 col-lg-5 offset-lg-1 col-sm-10 offset-sm-1">
					<div class="who-we-contant">
						<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s"
							style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
							<span>Get Started</span></div>

						<h4 class="fadeInUp" data-wow-delay="0.3s">It’s never too late to get started</h4>

						<p class="text-white">Trade, Buy, Sell, Swap, Mine and Save All Digtal Assets</p>
							
						<div class="clearfix"></div>
						<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><href="#home"><img alt="" src="./ic_website_sign_up.svg" style="width: 120px; height: 25px;" /></a>
							
								<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-google.svg" style="width: 120px; height: 25px;" /></a>
							
							<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-apple.svg" style="width: 120px; height: 25px;" /></a>
					</div>
				</div>

				<div class="col-12 col-lg-6 mt-s no-padding-right">
					<div class="welcome-meter fadeInUp" data-wow-delay="0.7s"><img alt=""
							class="img-responsive center-block" src="img/core-img/trading1.png" style="width: 460px; height: 260px;" /></div>
				</div>
			</div>
		</div>

			<!-- ##### Contact Area End ##### -->

			<div class="footer-content-area ">
				<div class="container">
					<div class="row ">
						<div class="col-12 col-lg-4 col-md-6">
							<div class="footer-copywrite-info">
								<!-- Copywrite -->
								<div class="copywrite_text fadeInUp" data-wow-delay="0.2s">
									<div class="footer-logo"><a href="#"><img alt="logo" src="./logo2.png"
												style="width: 230px; height: 40px;" /></a></div>

									<p>Plug-in to World's N0 1 Digital Asset management system today and Start Earning Big!.
									</p>
								</div>
								<!-- Social Icon -->

								<div class="footer-social-info fadeInUp" data-wow-delay="0.4s"></div>
							</div>
						</div>

						<div class="col-12 col-lg-3 col-md-6">
							<div class="contact_info_area d-sm-flex justify-content-between">
								<!-- Content Info -->
								<div class="contact_info mt-x text-center fadeInUp" data-wow-delay="0.3s">
									<h5>PRIVACY &amp; TOS</h5>

									<p><a href="#">Advertiser Agreement</a></p>

									<p><a href="https://coinmagnetics.com/terms">Terms Of Use</a></p>

									<p><a href="https://coinmagnetics.com/Privacy-policy">Privacy Policy</a></p>

									<p><a href="https://coinmagnetics.com/compliance">Legal & Compliance</a></p>

									<p><a href="https://coinmagnetics.com/Customer-pro">Customer protection</a></p>
								</div>
							</div>
						</div>

						<div class="col-12 col-lg-2 col-md-6 ">
							<!-- Content Info -->
							<div class="contact_info_area d-sm-flex justify-content-between">
								<div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.2s">
									<h5>NAVIGATE</h5>

									<p><a href="https://coinmagnetics.com/about">About Us</a></p>

									<p><a href="#">Developers</a></p>

									<p><a href="#">Resources</a></p>

									<p><a href="https://coinmagnetics.com/faq">FAQ</a></p>

									<p><a href="https://coinmagnetics.com/contact">Contact Us</a></p>
								</div>
							</div>
						</div>

						<div class="col-12 col-lg-3 col-md-6 ">
							<div class="contact_info_area d-sm-flex justify-content-between">
								<!-- Content Info -->
								<div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.4s">
									<h5>FIND US HERE</h5>

									<p>Mailing Address:265 Akowonjo RD,</p>

									<p>Lagos, Nigeria. Los 23401</p>

									
									<p>Email: support@coinmagnetics.com</p>
									
								<div class="dream-btn-group fadeInUp" data-wow-delay="0.4s"><a
									href="https://t.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/tg.png"
										style="width: 43px; height: 40px; float: left;" /></a>&nbsp; &nbsp;&nbsp;
								&nbsp;<a href="https://www.instagram.com/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/ig.png" style="width: 42px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp; &nbsp;<a href="https://twitter.com/Coinmagnetics"><img alt=""
										src="/img/social/tw.png" style="width: 43px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp;&nbsp;<a href="https://fb.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/fb.png" style="width: 43px; height: 40px;" /></a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- ##### Footer Area End ##### -->
		<!-- ########## All JS ########## -->
		<!-- jQuery js -->
		<script src="js/jquery.min.js"></script><!-- Popper js -->
		<script src="js/popper.min.js"></script><!-- Bootstrap js -->
		<script src="js/bootstrap.min.js"></script><!-- All Plugins js -->
		<script src="js/plugins.js"></script><!-- Parallax js -->
		<script src="js/dzsparallaxer.js"></script>
		<script src="js/jquery.syotimer.min.js"></script><!-- script js -->
		<script src="js/script.js"></script>
	</div>
</body>
<script>
$('#recipeCarousel').carousel({
  interval: 4000
})

$('.carousel .carousel-item').each(function(){
    var minPerSlide = 3;
    var next = $(this).next();
    if (!next.length) {
    next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
    
    for (var i=0;i<minPerSlide;i++) {
        next=next.next();
        if (!next.length) {
        	next = $(this).siblings(':first');
      	}
        
        next.children(':first-child').clone().appendTo($(this));
      }
});

	</script>

</html>