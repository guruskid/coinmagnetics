<?php
session_start();


//check if session id is set if it is redirect to dashboard
if(isset($_SESSION['id']) and (isset($_SESSION['2fa']) and $_SESSION['2fa'] == "yes")){
	
    header("location:dashboard");
    exit();
}else{
     

    if(isset($_SESSION['2fa'])){

        if($_SESSION['2fa'] == "pending"){
            header("location:authenticate");
            exit();
        }

        if($_SESSION['2fa'] == "no"){
            header("location:dashboard");
            exit();
        } 

    }

}


include('connection.php');

?>
<!doctype html>
<html class="no-js" lang="">



<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Forgot Password | Coin Magnetics </title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- Fontawesome CSS -->
	<link rel="stylesheet" href="css/fontawesome-all.min.css">
	<!-- Flaticon CSS -->
	<link rel="stylesheet" href="font/flaticon.css">
	<!-- Google Web Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;display=swap" rel="stylesheet">
	<!-- Custom CSS -->
	<link rel="stylesheet" href="style.css">
	<link type="text/css" rel="stylesheet" href="js/sweetalert.css">
</head>

<body>
	<!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
	<div id="wrapper" class="wrapper">
		<div class="fxt-template-animation fxt-template-layout5">
			<div class="fxt-bg-img fxt-none-767" data-bg-image="img/figure/bg5-5.png">
				<div class="fxt-intro">
					<div class="sub-title">Welcome To</div>
					<h1>Coin Magnetics</h1>
					<p>Let’s Grow Wealth Together With Coin Magnetics.</p>
				</div>
			</div>
			<div class="fxt-bg-color">
				<div class="fxt-header">
					<a href="forgot" class="fxt-logo"><img src="img/logo-7.png" alt="Logo"></a>
					<div class="fxt-page-switcher">
						<a href="login" class="switcher-text switcher-text1">LogIn</a>
						<a href="register" class="switcher-text switcher-text2">Register</a>
					</div>
				</div>

				<?php 
                if(!isset($_GET['token'])){
			?>
			
				<div class="fxt-form">
					<form method="POST">
						<div class="form-group fxt-transformY-50 fxt-transition-delay-2">
							<input type="email" class="form-control" name="email" placeholder="Your Email" required="required">
							<i class="flaticon-envelope"></i>
						</div>
						<div class="form-group fxt-transformY-50 fxt-transition-delay-4">
							<button type="submit" name="forgot"  class="fxt-btn-fill">Send Me Email</button>
						</div>
					</form>
				</div>

				<?php  

}

if(isset($_GET['token'])){

?>

<div class="fxt-form">
					<form method="POST">
						<div class="form-group fxt-transformY-50 fxt-transition-delay-2">
							<input type="password" class="form-control" id="password" name="password" placeholder="New Password" required="required">
							<i class="flaticon-envelope"></i>
						</div>
						<div class="form-group fxt-transformY-50 fxt-transition-delay-2">
							<input type="password" class="form-control" id="confirm" name="confirm" placeholder="Confirm Password" required="required">
							<i class="flaticon-envelope"></i>
						</div>
						<div class="form-group fxt-transformY-50 fxt-transition-delay-4">
							<button type="submit"  name="reset" class="fxt-btn-fill">Reset</button>
						</div>
					</form>
				</div>


				<?php } ?>


				<div class="fxt-footer">
					<ul class="fxt-socials">
						<li class="fxt-facebook fxt-transformY-50 fxt-transition-delay-5">Contact us : support@Coinmagnetics.com</li>
						
						
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- jquery-->
	<script src="js/jquery-3.5.0.min.js"></script>
	<!-- Popper js -->
	<script src="js/popper.min.js"></script>
	<!-- Bootstrap js -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Imagesloaded js -->
	<script src="js/imagesloaded.pkgd.min.js"></script>
	<!-- Validator js -->
	<script src="js/validator.min.js"></script>
	<!-- Custom Js -->
	<script src="js/main.js"></script>
	<script src="js/sweetalert.js"></script>
	<script>
        //change password 
        $("#confirm, #password").keyup(function () {
            var password = $("#password").val();
            var confirm = $("#confirm").val();

            if (password != confirm || password == "") {
                $("#password").css("border-color", "#9b1d1d");
                $("#confirm").css("border-color", "#9b1d1d");
            } else {
                $("#password").css("border-color", "green");
                $("#confirm").css("border-color", "green");

            }


        });


       

    </script>
</body>

<?php  

if(isset($_POST['forgot'])){
$email = $_POST['email'];

//check if email is registered
$check= mysqli_query($mysqli,"SELECT * FROM users WHERE email='$email' ");

if(mysqli_num_rows($check) > 0 ){

    $r = mysqli_fetch_assoc($check);
    $token = "T-capital-".mt_rand(138998, 999998);

    



$phone = $r['phone'];
$smscode = "https://treasurecapital.net/forgot?email=".$email."&token=".$token;


/*
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=DhJMgGa5VGBsHxAgljZuLn7chY6mQzJqUqiziAiPMoAWCi4V3hQV8DkprAT0&from=TC&to=".$phone."&body=Reset%20Your%20Treasure%20Capital%20Password%20using %20your%20link%20".$smscode."&dnd=3",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
));

$response = curl_exec($curl);

curl_close($curl);


*/


$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.mailjet.com/v3.1/send",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>'{
    "SandboxMode": false,
    "Messages": [
        {
            "From": {
                "Email": "info@treasurecapital.net",
                "Name": "Treasure Capital"
            },
            
            
            "To": [
                {
                    "Email": "'.$email.'",
                    "Name": ""
                }
            ],
            
            "Subject": "Reset Password",
            "TextPart": "",
            "HTMLPart": "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"><html data-editor-version=\"2\" class=\"sg-campaigns\" xmlns=\"http://www.w3.org/1999/xhtml\"><head>\n      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1\">\n      <!--[if !mso]><!-->\n      <meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge\">\n      <!--<![endif]-->\n      <!--[if (gte mso 9)|(IE)]>\n      <xml>\n        <o:OfficeDocumentSettings>\n          <o:AllowPNG/>\n          <o:PixelsPerInch>96</o:PixelsPerInch>\n        </o:OfficeDocumentSettings>\n      </xml>\n      <![endif]-->\n      <!--[if (gte mso 9)|(IE)]>\n  <style type=\"text/css\">\n    body {width: 600px;margin: 0 auto;}\n    table {border-collapse: collapse;}\n    table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}\n    img {-ms-interpolation-mode: bicubic;}\n  </style>\n<![endif]-->\n      <style type=\"text/css\">\n    body, p, div {\n      font-family: inherit;\n      font-size: 14px;\n    }\n    body {\n      color: #000000;\n    }\n    body a {\n      color: #1188E6;\n      text-decoration: none;\n    }\n    p { margin: 0; padding: 0; }\n    table.wrapper {\n      width:100% !important;\n      table-layout: fixed;\n      -webkit-font-smoothing: antialiased;\n      -webkit-text-size-adjust: 100%;\n      -moz-text-size-adjust: 100%;\n      -ms-text-size-adjust: 100%;\n    }\n    img.max-width {\n      max-width: 100% !important;\n    }\n    .column.of-2 {\n      width: 50%;\n    }\n    .column.of-3 {\n      width: 33.333%;\n    }\n    .column.of-4 {\n      width: 25%;\n    }\n    @media screen and (max-width:480px) {\n      .preheader .rightColumnContent,\n      .footer .rightColumnContent {\n        text-align: left !important;\n      }\n      .preheader .rightColumnContent div,\n      .preheader .rightColumnContent span,\n      .footer .rightColumnContent div,\n      .footer .rightColumnContent span {\n        text-align: left !important;\n      }\n      .preheader .rightColumnContent,\n      .preheader .leftColumnContent {\n        font-size: 80% !important;\n        padding: 5px 0;\n      }\n      table.wrapper-mobile {\n        width: 100% !important;\n        table-layout: fixed;\n      }\n      img.max-width {\n        height: auto !important;\n        max-width: 100% !important;\n      }\n      a.bulletproof-button {\n        display: block !important;\n        width: auto !important;\n        font-size: 80%;\n        padding-left: 0 !important;\n        padding-right: 0 !important;\n      }\n      .columns {\n        width: 100% !important;\n      }\n      .column {\n        display: block !important;\n        width: 100% !important;\n        padding-left: 0 !important;\n        padding-right: 0 !important;\n        margin-left: 0 !important;\n        margin-right: 0 !important;\n      }\n    }\n  </style>\n      <!--user entered Head Start--><link href=\"https://fonts.googleapis.com/css?family=Chivo&display=swap\" rel=\"stylesheet\"><style>\nbody {font-family: \'Chivo\', sans-serif;}\n</style><!--End Head user entered-->\n    </head>\n    <body>\n      <center class=\"wrapper\" data-link-color=\"#1188E6\" data-body-style=\"font-size:14px; font-family:inherit; color:#000000; background-color:#FFFFFF;\">\n        <div class=\"webkit\">\n          <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" class=\"wrapper\" bgcolor=\"#FFFFFF\">\n            <tbody><tr>\n              <td valign=\"top\" bgcolor=\"#FFFFFF\" width=\"100%\">\n                <table width=\"100%\" role=\"content-container\" class=\"outer\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n                  <tbody><tr>\n                    <td width=\"100%\">\n                      <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n                        <tbody><tr>\n                          <td>\n                            <!--[if mso]>\n    <center>\n    <table><tr><td width=\"600\">\n  <![endif]-->\n                                    <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:100%; max-width:600px;\" align=\"center\">\n                                      <tbody><tr>\n                                        <td role=\"modules-container\" style=\"padding:0px 0px 0px 0px; color:#000000; text-align:left;\" bgcolor=\"#FFFFFF\" width=\"100%\" align=\"left\"><table class=\"module preheader preheader-hide\" role=\"module\" data-type=\"preheader\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;\">\n    <tbody><tr>\n      <td role=\"module-content\">\n        <p></p>\n      </td>\n    </tr>\n  </tbody></table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"100%\" role=\"module\" data-type=\"columns\" style=\"padding:30px 20px 30px 30px;\" bgcolor=\"#f7f9f5\">\n    <tbody>\n      <tr role=\"module-content\">\n        <td height=\"100%\" valign=\"top\">\n          <table class=\"column\" width=\"265\" style=\"width:265px; border-spacing:0; border-collapse:collapse; margin:0px 10px 0px 0px;\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\" border=\"0\" bgcolor=\"\">\n            <tbody>\n              <tr>\n                <td style=\"padding:0px;margin:0px;border-spacing:0;\"><table class=\"wrapper\" role=\"module\" data-type=\"image\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"9bb94d6b-a9d9-4d66-b867-f52241c7c7c5\">\n    <tbody>\n      <tr>\n        <td style=\"font-size:6px; line-height:10px; padding:0px 0px 0px 0px;\" valign=\"top\" align=\"left\">\n          <img class=\"max-width\" border=\"0\" style=\"display:block; color:#000000; text-decoration:none; font-family:Helvetica, arial, sans-serif; font-size:16px;\" width=\"117\" alt=\"\" data-proportionally-constrained=\"true\" data-responsive=\"false\" src=\"https://treasurecapital.net/img/logo-5.png\" height=\"26\">\n        </td>\n      </tr>\n    </tbody>\n  </table></td>\n              </tr>\n            </tbody>\n          </table>\n          <table class=\"column\" width=\"265\" style=\"width:265px; border-spacing:0; border-collapse:collapse; margin:0px 0px 0px 10px;\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\" border=\"0\" bgcolor=\"\">\n            <tbody>\n              <tr>\n                <td style=\"padding:0px;margin:0px;border-spacing:0;\"><table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"b0d9194c-17b6-4db0-8250-0a56573ea469\" data-mc-module-version=\"2019-10-22\">\n    <tbody>\n      <tr>\n        <td style=\"padding:0px 0px 0px 0px; line-height:22px; text-align:inherit;\" height=\"100%\" valign=\"top\" bgcolor=\"\" role=\"module-content\"><div><div style=\"font-family: inherit; text-align: inherit\"><span style=\"font-size: 10px\">Treasure Capital</span></div><div></div></div></td>\n      </tr>\n    </tbody>\n  </table></td>\n              </tr>\n            </tbody>\n          </table>\n        </td>\n      </tr>\n    </tbody>\n  </table><table class=\"module\" role=\"module\" data-type=\"divider\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"2d3862ba-e4da-4865-8605-53fb723d695b\">\n    <tbody>\n      <tr>\n        <td style=\"padding:0px 0px 0px 0px;\" role=\"module-content\" height=\"100%\" valign=\"top\" bgcolor=\"\">\n          <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"100%\" height=\"1px\" style=\"line-height:1px; font-size:1px;\">\n            <tbody>\n              <tr>\n                <td style=\"padding:0px 0px 1px 0px;\" bgcolor=\"#e2efd4\"></td>\n              </tr>\n            </tbody>\n          </table>\n        </td>\n      </tr>\n    </tbody>\n  </table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"100%\" role=\"module\" data-type=\"columns\" style=\"padding:30px 20px 40px 30px;\" bgcolor=\"#f7f9f5\">\n    <tbody>\n      <tr role=\"module-content\">\n        <td height=\"100%\" valign=\"top\">\n          <table class=\"column\" width=\"530\" style=\"width:530px; border-spacing:0; border-collapse:collapse; margin:0px 10px 0px 10px;\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\" border=\"0\" bgcolor=\"\">\n            <tbody>\n              <tr>\n                <td style=\"padding:0px;margin:0px;border-spacing:0;\"><table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"6cf92250-86a4-41d8-865a-ae5fb2569556\" data-mc-module-version=\"2019-10-22\">\n    <tbody>\n      <tr>\n        <td style=\"padding:18px 0px 18px 0px; line-height:22px; text-align:inherit;\" height=\"100%\" valign=\"top\" bgcolor=\"\" role=\"module-content\"><div><div style=\"font-family: inherit; text-align: center\">Change Password</div><div></div></div></td>\n      </tr>\n    </tbody>\n  </table><table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"8c54c8a5-caee-4b33-b6b0-e8aaed51c545\" data-mc-module-version=\"2019-10-22\">\n    <tbody>\n      <tr>\n        <td style=\"padding:18px 10px 18px 10px; line-height:32px; text-align:inherit;\" height=\"100%\" valign=\"top\" bgcolor=\"\" role=\"module-content\"><div><div style=\"font-family: inherit; text-align: center\"><span style=\"color: #79a6ff; font-size: 50px\">Reset Password!</span></div><div></div></div></td>\n      </tr>\n    </tbody>\n  </table><table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"6cf92250-86a4-41d8-865a-ae5fb2569556.1\" data-mc-module-version=\"2019-10-22\">\n    <tbody>\n      <tr>\n        <td style=\"padding:0px 0px 18px 0px; line-height:22px; text-align:inherit;\" height=\"100%\" valign=\"top\" bgcolor=\"\" role=\"module-content\"><div><div style=\"font-family: inherit; text-align: center\">Click link below to Reset your accout password</div>\n\n<div></div></div></td>\n      </tr>\n    </tbody>\n  </table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"module\" data-role=\"module-button\" data-type=\"button\" role=\"module\" style=\"table-layout:fixed;\" width=\"100%\" data-muid=\"4bcb53df-57db-48a5-9aa3-4060ac494a64\">\n      <tbody>\n        <tr>\n          <td align=\"center\" bgcolor=\"\" class=\"outer-td\" style=\"padding:0px 0px 0px 0px;\">\n            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"wrapper-mobile\" style=\"text-align:center;\">\n              <tbody>\n                <tr>\n                <td align=\"center\" bgcolor=\"#333333\" class=\"inner-td\" style=\"border-radius:6px; font-size:16px; text-align:center; background-color:inherit;\">\n                  <a href=\"https://treasurecapital.net/forgot?email='.$email.'&token='.$token.'\" style=\"background-color:#333333; border:1px solid #333333; border-color:#333333; border-radius:0px; border-width:1px; color:#ffffff; display:inline-block; font-size:14px; font-weight:normal; letter-spacing:0px; line-height:normal; padding:12px 30px 12px 30px; text-align:center; text-decoration:none; border-style:solid;\" target=\"_blank\">Reset Password</a>\n                </td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n        </tr>\n      </tbody>\n    </table><table class=\"module\" role=\"module\" data-type=\"spacer\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"2edb1546-1598-49eb-995c-baf7d429f31c\">\n    <tbody>\n      <tr>\n        <td style=\"padding:0px 0px 30px 0px;\" role=\"module-content\" bgcolor=\"\">\n        </td>\n      </tr>\n    </tbody>\n  </table><table class=\"wrapper\" role=\"module\" data-type=\"image\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"3923e4a3-acc3-4c88-bb58-8e5cd32f0162\">\n    <tbody>\n      <tr>\n        <td style=\"font-size:6px; line-height:10px; padding:0px 0px 0px 0px;\" valign=\"top\" align=\"center\">\n         \n        </td>\n      </tr>\n    </tbody>\n  </table><table class=\"module\" role=\"module\" data-type=\"spacer\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"2edb1546-1598-49eb-995c-baf7d429f31c.1\">\n    <tbody>\n      <tr>\n        <td style=\"padding:0px 0px 30px 0px;\" role=\"module-content\" bgcolor=\"\">\n        </td>\n      </tr>\n    </tbody>\n  </table><table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"6cf92250-86a4-41d8-865a-ae5fb2569556.1.1\" data-mc-module-version=\"2019-10-22\">\n    <tbody>\n      <tr>\n        \n      </tr>\n    </tbody>\n  </table>\n </td>\n              </tr>\n            </tbody>\n          </table>\n          \n        </td>\n      </tr>\n    </tbody>\n  </table>\n<table class=\"module\" role=\"module\" data-type=\"text\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"table-layout: fixed;\" data-muid=\"ba2cc448-1854-4e31-b50f-551e3101d5b2\" data-mc-module-version=\"2019-10-22\">\n    <tbody>\n      <tr>\n        <td style=\"padding:40px 30px 40px 30px; line-height:22px; text-align:inherit; background-color:#79a6ff;\" height=\"100%\" valign=\"top\" bgcolor=\"#79a6ff\" role=\"module-content\"><div><div style=\"font-family: inherit; text-align: center\"><span style=\"color: #ffffff\">Need help choosing a plan? Questions about accounts?</span></div>\n<div style=\"font-family: inherit; text-align: center\"><span style=\"color: #ffffff\">We \'re here to help. Our customer services reps are available 24/7.</span></div>\n<div style=\"font-family: inherit; text-align: center\"><br></div>\n<div style=\"font-family: inherit; text-align: center\"><a href=\"https://treasurecapital.net\"><span style=\"color: #ffffff\"><u><strong>Contact Us&nbsp;</strong></u></span></a></div><div></div></div></td>\n      </tr>\n    </tbody>\n  </table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"module\" data-role=\"module-button\" data-type=\"button\" role=\"module\" style=\"table-layout:fixed;\" width=\"100%\" data-muid=\"d93b6db5-0e1f-4374-99f9-62212c9cf0f1\">\n      <tbody>\n        <tr>\n          <td align=\"center\" bgcolor=\"\" class=\"outer-td\" style=\"padding:0px 0px 20px 0px;\">\n            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"wrapper-mobile\" style=\"text-align:center;\">\n              <tbody>\n                <tr>\n                <td align=\"center\" bgcolor=\"#f5f8fd\" class=\"inner-td\" style=\"border-radius:6px; font-size:16px; text-align:center; background-color:inherit;\"></td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n        </tr>\n      </tbody>\n    </table></td>\n                                      </tr>\n                                    </tbody></table>\n                                    <!--[if mso]>\n                                  </td>\n                                </tr>\n                              </table>\n                            </center>\n                            <![endif]-->\n                          </td>\n                        </tr>\n                      </tbody></table>\n                    </td>\n                  </tr>\n                </tbody></table>\n              </td>\n            </tr>\n          </tbody></table>\n        </div>\n      </center>\n    \n  \n</body></html>",
           
            "TemplateLanguage": true,
          
            "TrackOpens": "account_default",
            "TrackClicks": "account_default"
            
        }
    ]
}',
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json",
    "Authorization: Basic YjcwM2IzMjA4MGMyNDhlMTVhOGNiZGU1ZWU4ZDZmOGE6YjMxNjAwMGVlNzk3MmQ4NjliZGUwOTgxMmE2MjMxODQ="
  ),
));

$response = curl_exec($curl);

curl_close($curl);











?>
<script>

swal({
    title: 'Forgot Password Requested',
    text: "Token sent successfully",
    type: 'success'
});

</script>

<?php


}else{

    ?>
    <script>

    swal({
    title: 'Email is not Register',
    text: "This email is not registered",
    type: 'warning'
});

    </script>

    <?php

}



}




if(isset($_POST['reset'])){

           $password = $_POST['password']; 
           $confirm = $_POST['confirm']; 
           $email = $_GET['email'];
           
           if($password == $confirm){
                $hashpassword = password_hash($password, PASSWORD_DEFAULT);

                $update = mysqli_query($mysqli,"UPDATE `users` SET `password`='$hashpassword' WHERE email='$email' ");

                if($update){

                    ?>
                <script>
                  
                    swal({
    title: 'Password reset successful',
    text: "Password has been reset successfully",
    type: 'success'
});

                </script>

        <?php

                }


           }else{

            ?>
    <script>
     
        swal({
    title: 'Password do not match',
    text: "Password you entered do not password",
    type: 'success'
});
    </script>

    <?php


           }

          

}


?>



</html>