<?php
session_start();

include('connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['id'])){
	
	header("location:index");
}else{

$get_user = mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$_SESSION['id']."' ");
$rows = mysqli_fetch_assoc($get_user);
    if(isset($_SESSION['2fa'])){

        if( ($_SESSION['2fa'] =="no" or $_SESSION['2fa'] =="pending") and $rows['2fa']==1){
            header("location:index");
        }


    }


}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
    <title>TWO FACTOR AUTHENTICATOR - Treasure Capital</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="assets/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="assets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/feather-icon.css">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="assets/css/datatables.css">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.css">
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link id="color" rel="stylesheet" href="assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
  </head>
  <body class="dark-sidebar dark-only" >
    <!-- tap on top starts-->
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>
    <!-- tap on tap ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
      <!-- Page Header Start-->
     <?php include('header.php'); ?>
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper sidebar-icon">
        <!-- Page Sidebar Start-->
      <?php include('sidebar.php'); ?>
        <!-- Page Sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col-6">
                  <h3>TWO FACTOR AUTHENTICATOR</h3>
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="dashboard"><i data-feather="home"></i></a></li>
                    <li class="breadcrumb-item">2fa</li>
                  </ol>
                </div>
                <div class="col-6">
                  <!-- Bookmark Start-->
                 
                  <!-- Bookmark Ends-->
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            <div class="row">
              <!-- Zero Configuration  Starts-->
              <div class="col-sm-12">
                <div class="card">
                  <div class="card-header">
                    <h5>Enable or Disable</h5>
                  </div>
                  <div class="card-body">

                  <?php

require_once("google_authenticator/index.php"); 

$g = new \Google\Authenticator\GoogleAuthenticator();

$secret = $rows['2fa_key'];


$two_fa_link = $g->getURL($rows['email'], 'treasurecapital.net', $secret);

   
?>
<center>
<img class="responsive-img card-border z-depth-2 mt-2"
                                            src="<?php echo $two_fa_link; ?>" alt="">
                                            </center>

                  <center><p>Before enabling two factor authentication ensure you download and install GOOGLE AUTHENTICATOR App. Then Use it to scan the QR-Code Above</p></center>


                  <br/>
                  <form method="POST">
                  <br/>
                  <div class="row">
                                    <div class="col s12">

                                        <div class="form-group">
                                        <label for="key" class="center-align">AUTHENTICATOR KEY</label>
                                            <input id="key" class="form-control" value="<?php echo $rows['2fa_key']; ?>" type="text"
                                                name="key" disabled>
                                           
                                        </div>
                                    </div>
                                </div>
                  <br/>
                  <label class="d-block" for="chk-ani">
                          <input class="checkbox_animated" name="2fa" id="chk-ani" type="checkbox"  <?php if($rows['2fa'] == 1){ echo 'checked ';  }  ?> >  Enable Two Factor Authentication
                        </label>

<br/>
                        <div class="row">
                                        <div class="input-field col s12">
                                            <button type="submit"
                                                class="btn btn-primary "
                                                style="color:white !important"  name="enable-two" >Update</button>
                                        </div>
                                    </div>

</form>

                  </div>
                </div>
              </div>
              <!-- Zero Configuration  Ends-->
          
            </div>
          </div>
          <!-- Container-fluid Ends-->
        </div>
        <!-- footer start-->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0">Copyright <?php echo date('Y'); ?> © Treasure Capital All rights reserved.</p>
              </div>
              <div class="col-md-6">
                <p class="pull-right mb-0"> </p>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- latest jquery-->
    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap js-->
    <script src="assets/js/bootstrap/popper.min.js"></script>
    <script src="assets/js/bootstrap/bootstrap.js"></script>
    <!-- feather icon js-->
    <script src="assets/js/icons/feather-icon/feather.min.js"></script>
    <script src="assets/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
    <script src="assets/js/sidebar-menu.js"></script>
    <script src="assets/js/config.js"></script>
    <!-- Plugins JS start-->
    <script src="assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/js/datatable/datatables/datatable.custom.js"></script>
    <script src="assets/js/sweet-alert/sweetalert.min.js"></script>
    <script src="assets/js/sweet-alert/sweetalert.min.js"></script>
    <script src="assets/js/tooltip-init.js"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="assets/js/script.js"></script>

    <!-- login js-->
    <!-- Plugin used-->
  </body>


  <?php
//code to update two factor authenication
if(isset($_POST['enable-two'])){

    if(isset($_POST['2fa'])){
        $fa = 1;
    }else{
        $fa = 0;
    }

//run update

$up = mysqli_query($mysqli,"UPDATE users SET 2fa='$fa' WHERE email='".$rows['email']."' ");

if($up){


?>
<script>
swal( 'Two Factor Authentication Updated',
     "Your Login Method has been updated.",
   'success'
)

setTimeout(() => {
    location = 'logout';
}, 2000);
</script>

<?php


}





}
//end of 2factor update

?>

</html>