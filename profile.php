<?php
session_start();

include('connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['id'])){
	
	header("location:login");
}else{

$get_user = mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$_SESSION['id']."' ");
$rows = mysqli_fetch_assoc($get_user);
    if(isset($_SESSION['2fa'])){

        if( ($_SESSION['2fa'] =="no" or $_SESSION['2fa'] =="pending") and $rows['2fa']==1){
            header("location:login");
        }


    }


}


?>
<!DOCTYPE html>
<html lang="en">


<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
    <title>Account Info - Coin Magnetics</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap"
        rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="assets/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="assets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/feather-icon.css">
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.css">
    <link id="color" rel="stylesheet" href="assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5dde42de43be710e1d1f5485/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
</head>

<body class="dark-sidebar dark-only">
    <!-- tap on top starts-->
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>
    <!-- tap on tap ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
        <!-- Page Header Start-->
        <?php include('header.php'); ?>
        <!-- Page Header Ends                              -->
        <!-- Page Body Start-->
        <div class="page-body-wrapper sidebar-icon">
            <!-- Page Sidebar Start-->
            <?php include('sidebar.php'); ?>
            <!-- Page Sidebar Ends-->
            <div class="page-body">
                <div class="container-fluid">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-6">
                                <h3>Update Account</h3>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="dashboard"><i data-feather="home"></i></a></li>
                                    <li class="breadcrumb-item">profile</li>
                                </ol>
                            </div>
                            <div class="col-6">
                                <!-- Bookmark Start-->

                                <!-- Bookmark Ends-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Container-fluid starts-->
                <div class="container-fluid">
                    <div class="edit-profile">

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title mb-0">Account info</h4>
                                        <div class="card-options"><a class="card-options-collapse" href="#"
                                                data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a
                                                class="card-options-remove" href="#" data-toggle="card-remove"><i
                                                    class="fe fe-x"></i></a></div>
                                    </div>
                                    <div class="card-body">
                                        <form method="POST">
                                            <div class="row mb-2">
                                                <div class="col-auto"><img class="img-70 rounded-circle" alt=""
                                                        src="assets/images/dashboard/profile.jpg"></div>
                                                <div class="col">
                                                    <h3 class="mb-1">
                                                        <?php echo $rows['firstname']." ".$rows['lastname']; ?></h3>
                                                    <p class="mb-4"><?php echo $rows['lastname']; ?></p>
                                                </div>
                                            </div>
                                            <?php { ?>
                                            <div class="form-group mb-3">
                                                <label class="form-label">Bitcoin Wallet</label>
                                                <input class="form-control" type="text"
                                                    value="<?php echo $rows['bitcoin']; ?>" required name="bitcoin"
                                                    placeholder=" Enter wallet address">
                                            </div>
                                            <div class="form-group mb-3">
                                                <label class="form-label">Tron Wallet</label>
                                                <input class="form-control" type="text"
                                                    value="<?php echo $rows['tron']; ?>" name="tron"
                                                    placeholder=" Enter wallet address">
                                            </div>
                                            <?php } ?>


                                            <div class="form-footer">
                                                <button name="address" type="submit"
                                                    class="btn btn-primary btn-block">Update</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


                            <!--<div class="col-lg-8">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title mb-0">Bank Profile</h4>

                                    </div>
                                    <div class="card-body">

                                        <?php

                      $curl = curl_init();
                      
                      curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://api.paystack.co/bank",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_HTTPHEADER => array(
                       
                        ),
                      ));
                      
                      $response = curl_exec($curl);
                      
                      curl_close($curl);
                      
                      $data = json_decode($response,true);
                      
                      
                      if($rows['account_num'] =='' and $rows['bankname']==''){
                                                          ?>

                                        <form method="POST">

                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-md-2 col-form-label">Bank
                                                    Name</label>
                                                <div class="col-md-10">
                                                    <select class="form-control" id="bankname" name="bankname"
                                                        placeholder="Select Bank " required>

                                                        <?php
                      
                                                                          for($i=0; $i<count($data['data']); $i++){
                                                                          ?>
                                                        <option
                                                            <?php if($data['data'][$i]['code'] == $rows['bankname']){echo "selected";} ?>
                                                            value="<?php echo $data['data'][$i]['code']; ?>">
                                                            <?php echo $data['data'][$i]['name']; ?></option>


                                                        <?php } ?>


                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="example-email-input" class="col-md-2 col-form-label">Account
                                                    Number</label>
                                                <div class="col-md-10">
                                                    <input type="text" id="accountnum" class="form-control"
                                                        name="accountnum" placeholder="Enter Account Number"
                                                        minlength="10" maxlength="10"
                                                        value="<?php echo $rows['account_num']; ?>" required>
                                                </div>
                                            </div>

                                            <br /> <br />
                                            <div class="form-group row">


                                                <div class="col-md-12">
                                                    <center>
                                                        <div id="load" style="display:none"
                                                            class="spinner-border text-primary m-1" role="status">
                                                            <span class="sr-only">Loading...</span>
                                                        </div>
                                                        <h4 class="text-success" id="accountname"
                                                            <?php if($rows['account_num']==""){  ?>style="display:none"
                                                            <?php } ?>><?php echo $rows['account_name'];?></h4>
                                                        <h4 class=" text-danger" id="error" style="display:none">Error
                                                        </h4>
                                                    </center>
                                                </div>
                                                <input type="hidden" name="accountname" id="realname" />
                                            </div>



                                            <button class="btn btn-primary" name="bank" id="bank" disabled
                                                type="submit">Update</button>-->

                                        </form>

                                        <?php  } ?>

                                    </div>
                                </div>
                            </div>




                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="profile-tab">
                                            <div class="custom-tab-1">
                                                <ul class="nav nav-tabs">

                                                    <li class="nav-item"><a href="#profile-settings" data-toggle="tab"
                                                            class="nav-link">Profile</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">

                                                    <div id="profile-settings" class="tab-pane active">
                                                        <div class="pt-3">
                                                            <div class="settings-form">
                                                                <h4 class="text-primary">Contact </h4>
                                                                <form method="POST">
                                                                    <div class="row">
                                                                        <div class="col">
                                                                            <div class="form-group">
                                                                                <label for="fname">Address</label>
                                                                                <textarea id="address" name="address"
                                                                                    rows="4"
                                                                                    class="form-control"><?php echo $rows['address']; ?></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col">
                                                                            <div class="form-group">
                                                                                <label for="lname">Country</label>
                                                                                <select id="countrys" name="country"
                                                                                    required class="form-control"
                                                                                    placeholder="Country">
                                                                                    <option value="" selected="">Select
                                                                                        Country </option>
                                                                                    <option value="AF">Afghanistan (AF)
                                                                                    </option>
                                                                                    <option value="AL">Albania (AL)
                                                                                    </option>
                                                                                    <option value="DZ">Algeria (DZ)
                                                                                    </option>
                                                                                    <option value="AS">American Samoa
                                                                                        (AS)</option>
                                                                                    <option value="AD">Andorra (AD)
                                                                                    </option>
                                                                                    <option value="AO">Angola (AO)
                                                                                    </option>
                                                                                    <option value="AI">Anguilla (AI)
                                                                                    </option>
                                                                                    <option value="AG">Antigua (AG)
                                                                                    </option>
                                                                                    <option value="AR">Argentina (AR)
                                                                                    </option>
                                                                                    <option value="AM">Armenia (AM)
                                                                                    </option>
                                                                                    <option value="AW">Aruba (AW)
                                                                                    </option>
                                                                                    <option value="AU">Australia (AU)
                                                                                    </option>
                                                                                    <option value="AT">Austria (AT)
                                                                                    </option>
                                                                                    <option value="AZ">Azerbaijan (AZ)
                                                                                    </option>
                                                                                    <option value="BS">The Bahamas (BS)
                                                                                    </option>
                                                                                    <option value="BH">Bahrain (BH)
                                                                                    </option>
                                                                                    <option value="BD">Bangladesh (BD)
                                                                                    </option>
                                                                                    <option value="BB">Barbados (BB)
                                                                                    </option>
                                                                                    <option value="BY">Belarus (BY)
                                                                                    </option>
                                                                                    <option value="BE">Belgium (BE)
                                                                                    </option>
                                                                                    <option value="BZ">Belize (BZ)
                                                                                    </option>
                                                                                    <option value="BJ">Benin (BJ)
                                                                                    </option>
                                                                                    <option value="BM">Bermuda (BM)
                                                                                    </option>
                                                                                    <option value="BT">Bhutan (BT)
                                                                                    </option>
                                                                                    <option value="BO">Bolivia (BO)
                                                                                    </option>
                                                                                    <option value="BA">Bosnia and
                                                                                        Herzegovina (BA)</option>
                                                                                    <option value="BW">Botswana (BW)
                                                                                    </option>
                                                                                    <option value="BR">Brazil (BR)
                                                                                    </option>
                                                                                    <option value="IO">British Indian
                                                                                        Ocean Territory (IO)</option>
                                                                                    <option value="BN">Brunei (BN)
                                                                                    </option>
                                                                                    <option value="BG">Bulgaria (BG)
                                                                                    </option>
                                                                                    <option value="BF">Burkina Faso (BF)
                                                                                    </option>
                                                                                    <option value="BI">Burundi (BI)
                                                                                    </option>
                                                                                    <option value="KH">Cambodia (KH)
                                                                                    </option>
                                                                                    <option value="CM">Cameroon (CM)
                                                                                    </option>
                                                                                    <option value="CA">Canada (CA)
                                                                                    </option>
                                                                                    <option value="CV">Cape Verde (CV)
                                                                                    </option>
                                                                                    <option value="KY">Cayman Islands
                                                                                        (KY)</option>
                                                                                    <option value="CF">Central African
                                                                                        Republic (CF)</option>
                                                                                    <option value="TD">Chad (TD)
                                                                                    </option>
                                                                                    <option value="CL">Chile (CL)
                                                                                    </option>
                                                                                    <option value="CN">China (CN)
                                                                                    </option>
                                                                                    <option value="CO">Colombia (CO)
                                                                                    </option>
                                                                                    <option value="KM">Comoros (KM)
                                                                                    </option>
                                                                                    <option value="CG">Republic of the
                                                                                        Congo (CG)</option>
                                                                                    <option value="CD">Democratic
                                                                                        Republic of Congo (CD)</option>
                                                                                    <option value="CK">Cook Islands (CK)
                                                                                    </option>
                                                                                    <option value="CR">Costa Rica (CR)
                                                                                    </option>
                                                                                    <option value="CI">CÃ´te d'Ivoire
                                                                                        (CI)</option>
                                                                                    <option value="HR">Croatia (HR)
                                                                                    </option>
                                                                                    <option value="CU">Cuba (CU)
                                                                                    </option>
                                                                                    <option value="CY">Cyprus (CY)
                                                                                    </option>
                                                                                    <option value="CZ">Czech Republic
                                                                                        (CZ)</option>
                                                                                    <option value="DK">Denmark (DK)
                                                                                    </option>
                                                                                    <option value="DJ">Djibouti (DJ)
                                                                                    </option>
                                                                                    <option value="DM">Dominica (DM)
                                                                                    </option>
                                                                                    <option value="DO">Dominican
                                                                                        Republic (DO)</option>
                                                                                    <option value="EC">Ecuador (EC)
                                                                                    </option>
                                                                                    <option value="EG">Egypt (EG)
                                                                                    </option>
                                                                                    <option value="SV">El Salvador (SV)
                                                                                    </option>
                                                                                    <option value="GQ">Equatorial Guinea
                                                                                        (GQ)</option>
                                                                                    <option value="ER">Eritrea (ER)
                                                                                    </option>
                                                                                    <option value="EE">Estonia (EE)
                                                                                    </option>
                                                                                    <option value="ET">Ethiopia (ET)
                                                                                    </option>
                                                                                    <option value="FK">Falkland Islands
                                                                                        (FK)</option>
                                                                                    <option value="FO">Faroe Islands
                                                                                        (FO)</option>
                                                                                    <option value="FJ">Fiji (FJ)
                                                                                    </option>
                                                                                    <option value="FI">Finland (FI)
                                                                                    </option>
                                                                                    <option value="FR">France (FR)
                                                                                    </option>
                                                                                    <option value="GF">French Guiana
                                                                                        (GF)</option>
                                                                                    <option value="PF">French Polynesia
                                                                                        (PF)</option>
                                                                                    <option value="GA">Gabon (GA)
                                                                                    </option>
                                                                                    <option value="GM">The Gambia (GM)
                                                                                    </option>
                                                                                    <option value="GE">Georgia (GE)
                                                                                    </option>
                                                                                    <option value="DE">Germany (DE)
                                                                                    </option>
                                                                                    <option value="GH">Ghana (GH)
                                                                                    </option>
                                                                                    <option value="GI">Gibraltar (GI)
                                                                                    </option>
                                                                                    <option value="GR">Greece (GR)
                                                                                    </option>
                                                                                    <option value="GL">Greenland (GL)
                                                                                    </option>
                                                                                    <option value="GD">Grenada (GD)
                                                                                    </option>
                                                                                    <option value="GP">Guadeloupe (GP)
                                                                                    </option>
                                                                                    <option value="GU">Guam (GU)
                                                                                    </option>
                                                                                    <option value="GT">Guatemala (GT)
                                                                                    </option>
                                                                                    <option value="GN">Guinea (GN)
                                                                                    </option>
                                                                                    <option value="GW">Guinea-Bissau
                                                                                        (GW)</option>
                                                                                    <option value="GY">Guyana (GY)
                                                                                    </option>
                                                                                    <option value="HT">Haiti (HT)
                                                                                    </option>
                                                                                    <option value="VA">Vatican City (VA)
                                                                                    </option>
                                                                                    <option value="HN">Honduras (HN)
                                                                                    </option>
                                                                                    <option value="HK">Hong Kong (HK)
                                                                                    </option>
                                                                                    <option value="HU">Hungary (HU)
                                                                                    </option>
                                                                                    <option value="IS">Iceland (IS)
                                                                                    </option>
                                                                                    <option value="IN">India (IN)
                                                                                    </option>
                                                                                    <option value="ID">Indonesia (ID)
                                                                                    </option>
                                                                                    <option value="IR">Iran (IR)
                                                                                    </option>
                                                                                    <option value="IQ">Iraq (IQ)
                                                                                    </option>
                                                                                    <option value="IE">Ireland (IE)
                                                                                    </option>
                                                                                    <option value="IL">Israel (IL)
                                                                                    </option>
                                                                                    <option value="IT">Italy (IT)
                                                                                    </option>
                                                                                    <option value="JM">Jamaica (JM)
                                                                                    </option>
                                                                                    <option value="JP">Japan (JP)
                                                                                    </option>
                                                                                    <option value="JO">Jordan (JO)
                                                                                    </option>
                                                                                    <option value="KZ">Kazakhstan (KZ)
                                                                                    </option>
                                                                                    <option value="KE">Kenya (KE)
                                                                                    </option>
                                                                                    <option value="KI">Kiribati (KI)
                                                                                    </option>
                                                                                    <option value="KR">South Korea (KR)
                                                                                    </option>
                                                                                    <option value="KW">Kuwait (KW)
                                                                                    </option>
                                                                                    <option value="KG">Kyrgyzstan (KG)
                                                                                    </option>
                                                                                    <option value="LA">Laos (LA)
                                                                                    </option>
                                                                                    <option value="LV">Latvia (LV)
                                                                                    </option>
                                                                                    <option value="LB">Lebanon (LB)
                                                                                    </option>
                                                                                    <option value="LS">Lesotho (LS)
                                                                                    </option>
                                                                                    <option value="LR">Liberia (LR)
                                                                                    </option>
                                                                                    <option value="LY">Libya (LY)
                                                                                    </option>
                                                                                    <option value="LI">Liechtenstein
                                                                                        (LI)</option>
                                                                                    <option value="LT">Lithuania (LT)
                                                                                    </option>
                                                                                    <option value="LU">Luxembourg (LU)
                                                                                    </option>
                                                                                    <option value="MO">Macau (MO)
                                                                                    </option>
                                                                                    <option value="MK">Macedonia (MK)
                                                                                    </option>
                                                                                    <option value="MG">Madagascar (MG)
                                                                                    </option>
                                                                                    <option value="MW">Malawi (MW)
                                                                                    </option>
                                                                                    <option value="MY">Malaysia (MY)
                                                                                    </option>
                                                                                    <option value="MV">Maldives (MV)
                                                                                    </option>
                                                                                    <option value="ML">Mali (ML)
                                                                                    </option>
                                                                                    <option value="MT">Malta (MT)
                                                                                    </option>
                                                                                    <option value="MH">Marshall Islands
                                                                                        (MH)</option>
                                                                                    <option value="MQ">Martinique (MQ)
                                                                                    </option>
                                                                                    <option value="MR">Mauritania (MR)
                                                                                    </option>
                                                                                    <option value="MU">Mauritius (MU)
                                                                                    </option>
                                                                                    <option value="YT">Mayotte (YT)
                                                                                    </option>
                                                                                    <option value="MX">Mexico (MX)
                                                                                    </option>
                                                                                    <option value="FM">Federated States
                                                                                        of Micronesia (FM)</option>
                                                                                    <option value="MD">Moldova (MD)
                                                                                    </option>
                                                                                    <option value="MC">Monaco (MC)
                                                                                    </option>
                                                                                    <option value="MN">Mongolia (MN)
                                                                                    </option>
                                                                                    <option value="ME">Montenegro (ME)
                                                                                    </option>
                                                                                    <option value="MS">Montserrat (MS)
                                                                                    </option>
                                                                                    <option value="MA">Morocco (MA)
                                                                                    </option>
                                                                                    <option value="MZ">Mozambique (MZ)
                                                                                    </option>
                                                                                    <option value="MM">Burma Myanmar
                                                                                        (MM)</option>
                                                                                    <option value="NA">Namibia (NA)
                                                                                    </option>
                                                                                    <option value="NR">Nauru (NR)
                                                                                    </option>
                                                                                    <option value="NP">Nepal (NP)
                                                                                    </option>
                                                                                    <option value="NL">Netherlands (NL)
                                                                                    </option>
                                                                                    <option value="AN">Netherlands
                                                                                        Antilles (AN)</option>
                                                                                    <option value="NC">New Caledonia
                                                                                        (NC)</option>
                                                                                    <option value="NZ">New Zealand (NZ)
                                                                                    </option>
                                                                                    <option value="NI">Nicaragua (NI)
                                                                                    </option>
                                                                                    <option value="NE">Niger (NE)
                                                                                    </option>
                                                                                    <option <?php if($rows['country']=="NG"){ echo 'selected';} ?> value="NG">Nigeria (NG)</option>
                                                                                    <option value="NU">Niue (NU)
                                                                                    </option>
                                                                                    <option value="NF">Norfolk Island
                                                                                        (NF)</option>
                                                                                    <option value="MP">Northern Mariana
                                                                                        Islands (MP)</option>
                                                                                    <option value="NO">Norway (NO)
                                                                                    </option>
                                                                                    <option value="OM">Oman (OM)
                                                                                    </option>
                                                                                    <option value="PK">Pakistan (PK)
                                                                                    </option>
                                                                                    <option value="PW">Palau (PW)
                                                                                    </option>
                                                                                    <option value="PS">Palestine (PS)
                                                                                    </option>
                                                                                    <option value="PA">Panama (PA)
                                                                                    </option>
                                                                                    <option value="PG">Papua New Guinea
                                                                                        (PG)</option>
                                                                                    <option value="PY">Paraguay (PY)
                                                                                    </option>
                                                                                    <option value="PE">Peru (PE)
                                                                                    </option>
                                                                                    <option value="PH">Philippines (PH)
                                                                                    </option>
                                                                                    <option value="PL">Poland (PL)
                                                                                    </option>
                                                                                    <option value="PT">Portugal (PT)
                                                                                    </option>
                                                                                    <option value="PR">Puerto Rico (PR)
                                                                                    </option>
                                                                                    <option value="QA">Qatar (QA)
                                                                                    </option>
                                                                                    <option value="RE">RÃ©union (RE)
                                                                                    </option>
                                                                                    <option value="RO">Romania (RO)
                                                                                    </option>
                                                                                    <option value="RU">Russia (RU)
                                                                                    </option>
                                                                                    <option value="RW">Rwanda (RW)
                                                                                    </option>
                                                                                    <option value="BL">Saint BarthÃ©lemy
                                                                                        (BL)</option>
                                                                                    <option value="SH">Saint Helena (SH)
                                                                                    </option>
                                                                                    <option value="KN">Saint Kitts and
                                                                                        Nevis (KN)</option>
                                                                                    <option value="LC">St. Lucia (LC)
                                                                                    </option>
                                                                                    <option value="MF">Saint Martin (MF)
                                                                                    </option>
                                                                                    <option value="PM">Saint Pierre and
                                                                                        Miquelon (PM)</option>
                                                                                    <option value="VC">Saint Vincent and
                                                                                        the Grenadines (VC)</option>
                                                                                    <option value="WS">Samoa (WS)
                                                                                    </option>
                                                                                    <option value="SM">San Marino (SM)
                                                                                    </option>
                                                                                    <option value="ST">SÃ£o TomÃ© and
                                                                                        PrÃncipe (ST)</option>
                                                                                    <option value="SA">Saudi Arabia (SA)
                                                                                    </option>
                                                                                    <option value="SN">Senegal (SN)
                                                                                    </option>
                                                                                    <option value="RS">Serbia (RS)
                                                                                    </option>
                                                                                    <option value="SC">Seychelles (SC)
                                                                                    </option>
                                                                                    <option value="SL">Sierra Leone (SL)
                                                                                    </option>
                                                                                    <option value="SG">Singapore (SG)
                                                                                    </option>
                                                                                    <option value="SK">Slovakia (SK)
                                                                                    </option>
                                                                                    <option value="SI">Slovenia (SI)
                                                                                    </option>
                                                                                    <option value="SB">Solomon Islands
                                                                                        (SB)</option>
                                                                                    <option value="SO">Somalia (SO)
                                                                                    </option>
                                                                                    <option value="ZA">South Africa (ZA)
                                                                                    </option>
                                                                                    <option value="ES">Spain (ES)
                                                                                    </option>
                                                                                    <option value="LK">Sri Lanka (LK)
                                                                                    </option>
                                                                                    <option value="SD">Sudan (SD)
                                                                                    </option>
                                                                                    <option value="SR">Suriname (SR)
                                                                                    </option>
                                                                                    <option value="SZ">Swaziland (SZ)
                                                                                    </option>
                                                                                    <option value="SE">Sweden (SE)
                                                                                    </option>
                                                                                    <option value="CH">Switzerland (CH)
                                                                                    </option>
                                                                                    <option value="SY">Syria (SY)
                                                                                    </option>
                                                                                    <option value="TW">Taiwan (TW)
                                                                                    </option>
                                                                                    <option value="TJ">Tajikistan (TJ)
                                                                                    </option>
                                                                                    <option value="TZ">Tanzania (TZ)
                                                                                    </option>
                                                                                    <option value="TH">Thailand (TH)
                                                                                    </option>
                                                                                    <option value="TL">Timor-Leste (TL)
                                                                                    </option>
                                                                                    <option value="TG">Togo (TG)
                                                                                    </option>
                                                                                    <option value="TK">Tokelau (TK)
                                                                                    </option>
                                                                                    <option value="TO">Tonga (TO)
                                                                                    </option>
                                                                                    <option value="TT">Trinidad and
                                                                                        Tobago (TT)</option>
                                                                                    <option value="TN">Tunisia (TN)
                                                                                    </option>
                                                                                    <option value="TR">Turkey (TR)
                                                                                    </option>
                                                                                    <option value="TM">Turkmenistan (TM)
                                                                                    </option>
                                                                                    <option value="TC">Turks and Caicos
                                                                                        Islands (TC)</option>
                                                                                    <option value="TV">Tuvalu (TV)
                                                                                    </option>
                                                                                    <option value="UG">Uganda (UG)
                                                                                    </option>
                                                                                    <option value="UA">Ukraine (UA)
                                                                                    </option>
                                                                                    <option value="AE">United Arab
                                                                                        Emirates (AE)</option>
                                                                                    <option value="GB">United Kingdom
                                                                                        (GB)</option>
                                                                                    <option value="US">United States
                                                                                        (US)</option>
                                                                                    <option value="UY">Uruguay (UY)
                                                                                    </option>
                                                                                    <option value="VI">US Virgin Islands
                                                                                        (VI)</option>
                                                                                    <option value="UZ">Uzbekistan (UZ)
                                                                                    </option>
                                                                                    <option value="VU">Vanuatu (VU)
                                                                                    </option>
                                                                                    <option value="VE">Venezuela (VE)
                                                                                    </option>
                                                                                    <option value="VN">Vietnam (VN)
                                                                                    </option>
                                                                                    <option value="WF">Wallis and Futuna
                                                                                        (WF)</option>
                                                                                    <option value="YE">Yemen (YE)
                                                                                    </option>
                                                                                    <option value="ZM">Zambia (ZM)
                                                                                    </option>
                                                                                    <option value="ZW">Zimbabwe (ZW)
                                                                                    </option>
                                                                                </select>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col">
                                                                            <div class="form-group">
                                                                                <label for="fname">State</label>
                                                                                <input name="state" class="form-control"
                                                                                    id="state" placeholder="Enter State"
                                                                                    value="<?php echo $rows['state']; ?>"
                                                                                    type="text" class="validate">
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="form-group col-md-12">
                                                                        <button type="submit" class="btn btn-info"
                                                                            value="" n
                                                                            name="update-contact">Update</button>
                                                                    </div>


                                                            </div>


                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>





                    </div>
                </div>
            </div>
            <!-- Container-fluid Ends-->
        </div>
        <!-- footer start-->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 footer-copyright">
                        <p class="mb-0">Copyright <?php echo date('Y'); ?> © Coin Magnetics All rights reserved.
                        </p>
                    </div>
                    <div class="col-md-6">
                        <p class="pull-right mb-0"> </p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    </div>
    <!-- latest jquery-->
    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap js-->
    <script src="assets/js/bootstrap/popper.min.js"></script>
    <script src="assets/js/bootstrap/bootstrap.js"></script>
    <!-- feather icon js-->
    <script src="assets/js/icons/feather-icon/feather.min.js"></script>
    <script src="assets/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
    <script src="assets/js/sidebar-menu.js"></script>
    <script src="assets/js/config.js"></script>
    <!-- Plugins JS start-->
    <script src="assets/js/tooltip-init.js"></script>
    <script src="assets/js/sweet-alert/sweetalert.min.js"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="assets/js/script.js"></script>

    <!-- login js-->
    <!-- Plugin used-->
</body>

<?php
if(isset($_POST['bank'])){

$bankname = mysqli_real_escape_string($mysqli, $_POST['bankname']);
$accountname = mysqli_real_escape_string($mysqli, $_POST['accountname']);
$accountnum = mysqli_real_escape_string($mysqli, $_POST['accountnum']);


//check if account already exist

$check = mysqli_query($mysqli,"SELECT * FROM users WHERE  account_num='$accountnum' ");

if(mysqli_num_rows($check) < 1 ){



$qry = mysqli_query($mysqli,"UPDATE users SET bankname='$bankname', account_num='$accountnum', account_name='$accountname' WHERE id='".$rows['id']."' ");

if($qry){
?>
<script>
swal("Bank Details Updated",
    "Bank account details has been updated",
    "success")

setTimeout(() => {
    location = "profile";
}, 3000);
</script>

<?php
}


}else{

    ?>
<script>
swal(: "Account already exist",
    "Bank Account is already in use, please change account details",
    "warning"
)
</script>

<?php

}



}





if(isset($_POST['address'])){

  $bitcoin = mysqli_real_escape_string($mysqli, $_POST['bitcoin']);
$tron = mysqli_real_escape_string($mysqli, $_POST['tron']);



$qry = mysqli_query($mysqli,"UPDATE users SET bitcoin='$bitcoin', tron='$tron'WHERE id='".$rows['id']."' ");

if($qry){
  ?>
<script>
swal("Cyrpto Details Updated",
    " Account details has been updated",
    "success")

setTimeout(() => {
    location = "profile";
}, 3000);
</script>

<?php
  }

  

}






//start of contact update
if(isset($_POST['update-contact'])){

$address = mysqli_real_escape_string($mysqli,$_POST['address']);
$country = mysqli_real_escape_string($mysqli,$_POST['country']);
$state = mysqli_real_escape_string($mysqli,$_POST['state']);
//$zip = mysqli_real_escape_string($mysqli,$_POST['zip']);



//run update

$update = mysqli_query($mysqli,"UPDATE users SET address='$address', country='$country', state='$state'WHERE email='".$rows['email']."' ");

if($update){


?>
<script>
swal("Contact Details Updated",
    "Your contact details has been updated",
    "success")

setTimeout(() => {
    location = 'profile';
}, 3000);
</script>

</script>

<?php


}



}
//end of contact update


?>



<script>
function getaccount() {

    var code = $('#bankname').val();
    var number = $('#accountnum').val();

    var settings = {
        "url": "checkaccount?account_number=" + number + "&bank_code=" + code,
        "method": "GET",
        "timeout": 0

    };

    $.ajax(settings).done(function(response) {
        console.log(response);

        $('#load').hide();

        if (response.status == true) {
            $('#realname').val(response.data.account_name)
            $('#accountname').show();
            $('#accountname').text(response.data.account_name);
            $('#bank').removeAttr("disabled");
        } else {
            $('#error').show();
            $('#bank').attr("disabled", "disabled");
            $('#error').text(response.message);
        }




    });

}

$('#bankname').change(function() {

    if ($('#accountnum').val() != '') {
        $('#load').show();
        $('#error').hide();
        $('#accountname').hide();
        getaccount();
    }


});

$('#accountnum').keyup(function() {

    if ($('#accountnum').val().length == 10) {
        $('#load').show();
        $('#error').hide();
        $('#accountname').hide();
        getaccount();
    }


})
</script>





</html>