<?php
session_start();

include('connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['id'])){
	
	header("location:login");
}else{

$get_user = mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$_SESSION['id']."' ");
$rows = mysqli_fetch_assoc($get_user);
    if(isset($_SESSION['2fa'])){

        if( ($_SESSION['2fa'] =="no" or $_SESSION['2fa'] =="pending") and $rows['2fa']==1){
            header("location:login");
        }


    }


}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
    <title>Video Testimony - Coin Magnetics</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap"
        rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="assets/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="assets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="assets/css/feather-icon.css">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="assets/css/datatables.css">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link id="color" rel="stylesheet" href="assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
</head>

<body class="dark-sidebar dark-only">
    <!-- tap on top starts-->
    <div class="tap-top"><i data-feather="chevrons-up"></i></div>
    <!-- tap on tap ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
        <!-- Page Header Start-->
        <?php include('header.php'); ?>
        <!-- Page Header Ends                              -->
        <!-- Page Body Start-->
        <div class="page-body-wrapper sidebar-icon">
            <!-- Page Sidebar Start-->
            <?php include('sidebar.php'); ?>
            <!-- Page Sidebar Ends-->
            <div class="page-body">
                <div class="container-fluid">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-6">
                                <h3>Testimony</h3>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="dashboard"><i data-feather="home"></i></a></li>
                                    <li class="breadcrumb-item">Testimony</li>
                                </ol>
                            </div>
                            <div class="col-6">
                                <!-- Bookmark Start-->

                                <!-- Bookmark Ends-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Container-fluid starts-->
                <div class="container-fluid">
                    <div class="row">
                        <!-- Zero Configuration  Starts-->
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Video Testimony</h5>
                                </div>
                                <div class="card-body">

                                    <div class="alert alert-danger" role="alert">
                                        <b>Share your video testimony and earn 2.5% of your Trading Bot amount.</b>
                                        <br /><br />
                                        How it works.
                                        <br />
                                        <ol>
                                            <li> Make a 30-45 seconds shot video with your face showing. In this short
                                                video, explaining how Coin Magnetics has impacted your life
                                                financially and recomend the system to your friend and family.</li>

                                            <li> Upload the short video on Facebook or Instagram tagging
                                                <b>FB: @Coinmagnetics IG: @Coinmagnetics</b> </li>

                                            <li>Copy and paste the video link here and click on share.</li>
                                        </ol>
                                        <br />
                                        That is it, we will verify the video and allocate your bonus to your account.
                                    </div>


                                </div>


                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">

                                            <h4 class="card-title">Add Video testimony (Optional)</h4>
                                            <p class="card-title-desc"></p>

                                            <form method="POST">


                                                <div class="form-group row">
                                                    <label for="example-email-input"
                                                        class="col-md-2 col-form-label">Attach Minig Pool</label>
                                                    <div class="col-md-10">
                                                        <select class="form-control" name="investment" required>
                                                            <option>Select Bot</option>
                                                            <?php $get_users = mysqli_query($mysqli,"SELECT * FROM investment WHERE userid='".$rows['id']."' and status=1 and testimony=0");
                                                 while($row= mysqli_fetch_assoc($get_users)){  
                                                     if( $row['status']==1){
                                                         $status="Active";
                                                     }  ?>
                                                            <option value="<?php echo $row['id']; ?>">Bot
                                                                <?php echo $row['id']." || $".$row['amount']." || ".$status; ?>
                                                            </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-search-input"
                                                        class="col-md-2 col-form-label">Video Link</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control" name="link"
                                                            placeholder="Enter link" required>
                                                    </div>
                                                </div>




                                                <button class="btn btn-primary" name="video"
                                                    type="submit">Share</button>

                                            </form>
                                        </div>
                                    </div>
                                </div>




                            </div>











                        </div>
                    </div>
                    <!-- Zero Configuration  Ends-->

                </div>




                <?php if($rows['cycle']!=0){  ?>
                <div class="container-fluid">
                    <div class="row">
                        <!-- Zero Configuration  Starts-->
                        <div class="col-sm-12">
                            <div class="card">
                            


                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">

                                            <h4 class="card-title">Add Text testimony (Required)</h4>
                                            <p class="card-title-desc"></p>

                                            <form method="POST" enctype="multipart/form-data">


                                                <div class="form-group row">
                                                    <label for="example-email-input"
                                                        class="col-md-2 col-form-label">Title  </label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" name="title" placeholder="Enter Title" minlength="8" required />
                                                           
                                                            
                                                      
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-search-input"
                                                        class="col-md-2 col-form-label">Details </label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control" name="info"
                                                            placeholder="Enter Message" minlength="15" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-search-input"
                                                        class="col-md-2 col-form-label">Upload POP image </label>
                                                    <div class="col-md-10">
                                                        <input type="file" class="form-control" name="pop"
                                                             required>
                                                    </div>
                                                </div>






                                                <button class="btn btn-primary" name="test"
                                                    type="submit">Share</button>

                                            </form>
                                        </div>
                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>
                    <!-- Zero Configuration  Ends-->

                </div>

                <?php } ?>
               
            
            <!-- Container-fluid Ends-->
        </div>
        <!-- footer start-->
        <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 footer-copyright">
                            <p class="mb-0">Copyright <?php echo date('Y'); ?> © Coin Magnetics All rights reserved.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p class="pull-right mb-0"> </p>
                        </div>
                    </div>
                </div>
            </footer>
    </div>
    </div>
    <!-- latest jquery-->
    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap js-->
    <script src="assets/js/bootstrap/popper.min.js"></script>
    <script src="assets/js/bootstrap/bootstrap.js"></script>
    <!-- feather icon js-->
    <script src="assets/js/icons/feather-icon/feather.min.js"></script>
    <script src="assets/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
    <script src="assets/js/sidebar-menu.js"></script>
    <script src="assets/js/config.js"></script>
    <!-- Plugins JS start-->
    <script src="assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/js/datatable/datatables/datatable.custom.js"></script>
    <script src="assets/js/tooltip-init.js"></script>
    <script src="assets/js/sweet-alert/sweetalert.min.js"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="assets/js/script.js"></script>

    <!-- login js-->
    <!-- Plugin used-->
</body>




<?php
if(isset($_POST['video'])){

$investmentid = mysqli_real_escape_string($mysqli, $_POST['investment']);
$video = mysqli_real_escape_string($mysqli, $_POST['link']);



        
//set message
$message =$rows['username']." :: ".$title. " - ".$info;

$add = mysqli_query($mysqli,"INSERT INTO video(`userid`, `investmentid`, `videolink`) VALUES('".$rows['id']."', '$investmentid', '$video' )");


$update = mysqli_query($mysqli,"UPDATE users SET cycle=0 WHERE id='".$rows['id']."' ");

//set message
$message ="Video testimony by ".$rows['username']." :: ".$video;



$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.telegram.org/bot1304742524:AAGfJk8BGOtPs_gavGHIRSWzy0l6sscwBCA/sendMessage?chat_id=-1001277327906&text=".urlencode ($message),
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
));

$response = curl_exec($curl);

curl_close($curl);



?>
<script>
swal('Video Shared',
    'Awaiting approval', 'success')
</script>

<?php

}
?>



<?php
if(isset($_POST['test'])){

$title = mysqli_real_escape_string($mysqli, $_POST['title']);
$info = mysqli_real_escape_string($mysqli, $_POST['info']);

$target_locate = "img/";


$extension = pathinfo($_FILES["pop"]["name"], PATHINFO_EXTENSION);
	
	$code = mt_rand(138998, 999998);
	$name = "tc-".str_replace(' ', '', $code);
	//the image with full path
	$file = $target_locate.$name.".png";
	$imageFileType = pathinfo($file,PATHINFO_EXTENSION);
	$check = getimagesize($_FILES["pop"]["tmp_name"]);

	$image = mysqli_real_escape_string($mysqli,"img/".$name.".png");

   $move=  move_uploaded_file($_FILES["pop"]["tmp_name"],$file);

   $fulllink = "https://Coinmagnetics.com/".$file;




  $name = $rows['fristname']." ".$rows['lastname'];

//set message
$message =$name." - ".$title. " - ".$info." :: ".$fulllink;


$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://api.telegram.org/bot1751403329:AAEuSOVGwjKqhFmi657n-jQSc_5c9ifa5bk/sendMessage?chat_id=-1001255282352&text='.$message,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
));

$response = curl_exec($curl);

curl_close($curl);
//echo $response;




$insert = mysqli_query($mysqli,"INSERT INTO  testimony(userid, username, title, info) VALUES('".$rows['id']."','$name', '$title', '$info')");


$update = mysqli_query($mysqli,"UPDATE users SET cycle=0 WHERE id='".$rows['id']."' ");



?>
<script>
swal('Testimony Shared',
    'your testimony has been shared ', 'success');
</script>

<?php

}
?>



</html>