<?php  
include('account/connection.php');

?>
<!DOCTYPE html>
<html lang="zxx">

<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- Title -->
	<title>Coin Magnetics - Customer protection</title>
	<!-- Favicon -->
	<link href="img/core-img/favicon.png" rel="icon" /><!-- Core Stylesheet -->
	<link href="css/style.css" rel="stylesheet" /><!-- Responsive Stylesheet -->
	<link href="css/responsive.css" rel="stylesheet" />
	<style>
@media (max-width: 768px) {
    .carousel-inner .carousel-item > div {
        display: none;
    }
    .carousel-inner .carousel-item > div:first-child {
        display: block;
    }
}

.carousel-inner .carousel-item.active,
.carousel-inner .carousel-item-next,
.carousel-inner .carousel-item-prev {
    display: flex;
}

/* display 3 */
@media (min-width: 768px) {
    
    .carousel-inner .carousel-item-right.active,
    .carousel-inner .carousel-item-next {
      transform: translateX(33.333%);
    }
    
    .carousel-inner .carousel-item-left.active, 
    .carousel-inner .carousel-item-prev {
      transform: translateX(-33.333%);
    }
}

.carousel-inner .carousel-item-right,
.carousel-inner .carousel-item-left{ 
  transform: translateX(0);
}


</style>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5dde42de43be710e1d1f5485/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</head>

<body class="light-version">
	<!-- Preloader -->
	<div id="preloader">
		<div class="preload-content">
			<div id="dream-load"></div>
		</div>
	</div>
	<!-- ##### Header Area Start ##### -->

	<header class="header-area fadeInDown" data-wow-delay="0.2s">
		<div class="classy-nav-container light breakpoint-off">
			<div class="container">
				<!-- Classy Menu -->
				<nav class="classy-navbar light justify-content-between" id="dreamNav"><a class="nav-brand light"
						href="https://coinmagnetics.com/"><img alt="" src="./logo2.png" style="width: 230px; height: 40px;" /></a>
					<!-- Navbar Toggler -->

					<div class="classy-navbar-toggler demo"></div>
					<!-- Menu -->

					<div class="classy-menu">
						<!-- close btn -->
						<div class="classycloseIcon">
							<div class="cross-wrap"></div>
						</div>
						<!-- Nav Start -->

						<div class="classynav">
							<ul id="nav">
								
								<li><a href="#services1">How It Works</a></li>
								
								<li><a href="#about">About</a></li>
								<li><a href="#services">Features</a></li>
								
								
								<li><a href="#roadmap">Roadmap</a></li>
								<li><a href="#contact">Contact</a></li>
								<li>
								<style type="text/css">

a.gflag {vertical-align:middle;font-size:16px;padding:1px 0;background-repeat:no-repeat;background-image:url(//gtranslate.net/flags/16.png);}
a.gflag img {border:0;}
a.gflag:hover {background-image:url(//gtranslate.net/flags/16a.png);}
#goog-gt-tt {display:none !important;}
.goog-te-banner-frame {display:none !important;}
.goog-te-menu-value:hover {text-decoration:none !important;}
body {top:0 !important;}
#google_translate_element2 {display:none!important;}

</style>


<br /><select onchange="doGTranslate(this);"  ><option value="">Select Language</option><option value="en|en">English</option><option value="en|hy">Armenian</option><option value="en|az">Azerbaijani</option><option value="en|eu">Basque</option><option value="en|be">Belarusian</option><option value="en|ca">Catalan</option><option value="en|zh-CN">Chinese (Simplified)</option><option value="en|nl">Dutch</option><option value="en|fr">French</option><option value="en|de">German</option><option value="en|pt">Portuguese</option><option value="en|ru">Russian</option><option value="en|es">Spanish</option></select><div style="display:none" id="google_translate_element2"></div>
<script type="text/javascript">
function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'en',autoDisplay: false}, 'google_translate_element2');}
</script><script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>


<script type="text/javascript">
/* <![CDATA[ */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,f,f);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}',43,43,'||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'),0,{}))
/* ]]> */
</script>


								</li>
							</ul>
							<!-- Button --><a class="btn login-btn ml-50"
								href="https://coinmagnetics.com/">Log in</a>
						</div>
						<!-- Nav End -->
					</div>
				</nav>
			</div>
		</div>
	</header>
	<!-- ##### Header Area End ##### -->
	<section class="faq-timeline-area  section-padding-100-0 clearfix " id="about">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-12 col-lg-5 offset-lg-1 col-sm-10 offset-sm-1">
					<div class="who-we-contant">

				<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Compliance information<strong></h2>


<h4 class="fadeInUp" data-wow-delay="0.3s">Know the Coin Magnetics compliance information.</h4>

<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Last
updated: August 2021</span></div>



				<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>This is Coin Magnetics’s approach to anti-money laundering (AML) and know-your-customer (KYC) processes</strong></h2>

<h6 class="fadeInUp" data-wow-delay="0.3s">Money laundering is the process whereby the financial 
proceeds of a crime are disguised to give the impression of legitimate income. Often criminals target 
financial service providers through which they attempt to launder criminal proceeds without raising suspicion. 
In many cases, laundered funds are used to fund further crime or to finance terrorism. Sometimes both.</h6>

<ul type=disc>
 <h6 class="fadeInUp" data-wow-delay="0.3s">As a means to combat money laundering and to counter 
 terrorist financing (CTF), most countries have implemented AML and CTF legislation which imposes 
 obligations on financial service providers. Although it is not always clear in some of our countries, 
 where we have a presence, whether these obligations fall on cryptocurrency providers, these laws, 
 together with guidance from regulators, applicable task forces and industry best practice, 
 form the cornerstone of Coin Magnetics’s approach to AML and CTF. As such, Coin Magnetics has 
 implemented systems and controls that meet the standards applicable to regulated sectors such as banking. 
 This decision reflects our desire to prevent money laundering and terrorist financing</p>
 
 <h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Key components of Coin Magnetics's AML and CTF framework include the following:</strong></h6>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">•	The appointment of a Money Laundering Reporting Officer (MLRO). 
 This is an individual with a sufficient level of seniority and independence who is tasked with the responsibility 
 of overseeing compliance with the relevant legislation, regulations, rules and industry guidance;<o:p></o:p></span></li>
     
<h6 class="fadeInUp" data-wow-delay="0.3s">•	The appointment of an independent risk committee which 
reports to our board of directors regularly on all risk and compliance matters;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">•	Establishing and maintaining a risk-based approach to the 
 assessment and management of money laundering and terrorist financing risks;</h6>
</ul>


<h6 class="fadeInUp" data-wow-delay="0.3s">•	Establishing and maintaining a risk-based approach to 
Customer Due Diligence (CDD), including customer identification, verification and KYC procedures. 
To ensure we meet these standards, our customers are required to provide certain personal details 
and documents when opening a Coin Magnetics Account. The nature and extent of what is required is 
guided by the customer’s deposit and withdrawal limits and, in some cases, the customer’s country of residence. 
In certain circumstances, Coin Magnetics may perform enhanced due diligence procedures for customers presenting a 
higher risk, such as those transacting large volumes and Politically Exposed Persons (PEPs);<o:p></o:p></span></p>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	Establishing and maintaining risk-based systems and procedures 
for the monitoring of ongoing customer activity;<o:p></o:p></span></p>
</ul>


<ul type=disc>
 <h6 class="fadeInUp" data-wow-delay="0.3s">•	Establishing procedures for reporting suspicious activity internally 
 and to the relevant law enforcement authorities as appropriate;<o:p></o:p></span></li>
 
 <h6 class="fadeInUp" data-wow-delay="0.3s">•	Maintaining appropriate KYC records for the minimum prescribed periods;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">•	Providing training on the framework and raising awareness among all relevant employees;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">•	Implementing a “travel-rule” framework, where this is required, 
 to facilitate the exchange of customer information between virtual asset service providers when sending and 
 receiving cryptocurrency. The nature and extent of what is required is dependent on the sender 
 and/or receiver’s country of residence;<o:p></o:p></span></li>
     
 <h6 class="fadeInUp" data-wow-delay="0.3s">•	Designing systems and controls to allow Coin Magnetics to 
 comply with all required sanction screening processes imposed by, for example, the United Nations, European Union, 
 UK Treasury and US Office of Foreign Assets Control (OFAC) and to take measures to prevent transacting with 
 individuals, companies and countries appearing on these sanctions lists.<o:p></o:p></span></li>
     
 
						
					</div>
				</div>
	</section>

	<div class="clearfix"></div>
	<!-- ##### Our Trial Area End ##### -->

	<section class="about-us-area  section-padding-100-0 gradient-section-bg clearfix " id="affiliate">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-12 col-lg-5 offset-lg-1 col-sm-10 offset-sm-1">
					<div class="who-we-contant">
						<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s"
							style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
							<span>Get Started</span></div>

						<h4 class="fadeInUp" data-wow-delay="0.3s">It’s never too late to get started</h4>

						<p class="text-white">Trade, Buy, Sell, Swap, Mine and Save All Digtal Assets</p>
							
						<div class="clearfix"></div>
						<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><href="#home"><img alt="" src="./ic_website_sign_up.svg" style="width: 160px; height: 35px;" /></a>
							
								<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-google.svg" style="width: 160px; height: 35px;" /></a>
							
							<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-apple.svg" style="width: 160px; height: 35px;" /></a>
					</div>
				</div>

				<div class="col-12 col-lg-6 mt-s no-padding-right">
					<div class="welcome-meter fadeInUp" data-wow-delay="0.7s"><img alt=""
							class="img-responsive center-block" src="img/core-img/trading1.png" style="width: 460px; height: 260px;" /></div>
				</div>
			</div>
		</div>

			<!-- ##### Contact Area End ##### -->

			<div class="footer-content-area ">
				<div class="container">
					<div class="row ">
						<div class="col-12 col-lg-4 col-md-6">
							<div class="footer-copywrite-info">
								<!-- Copywrite -->
								<div class="copywrite_text fadeInUp" data-wow-delay="0.2s">
									<div class="footer-logo"><a href="https://coinmagnetics.com"><img alt="logo" src="./logo2.png"
												style="width: 230px; height: 40px;" /></a></div>

									<p>Plug-in to World's N0 1 Digital Asset management system today and Start Earning Big!.
									</p>
								</div>
								<!-- Social Icon -->

								<div class="footer-social-info fadeInUp" data-wow-delay="0.4s"></div>
							</div>
						</div>

						<div class="col-12 col-lg-3 col-md-6">
							<div class="contact_info_area d-sm-flex justify-content-between">
								<!-- Content Info -->
								<div class="contact_info mt-x text-center fadeInUp" data-wow-delay="0.3s">
									<h5>PRIVACY &amp; TOS</h5>

									<p><a href="https://coinmagnetics.com">Advertiser Agreement</a></p>

									<p><a href="https://coinmagnetics.com/terms">Terms Of Use</a></p>

									<p><a href="https://coinmagnetics.com/Privacy-policy">Privacy Policy</a></p>

									<p><a href="https://coinmagnetics.com/compliance">Legal & Compliance</a></p>

									<p><a href="https://coinmagnetics.com/Customer-pro">Customer Protection</a></p>
								</div>
							</div>
						</div>

						<div class="col-12 col-lg-2 col-md-6 ">
							<!-- Content Info -->
							<div class="contact_info_area d-sm-flex justify-content-between">
								<div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.2s">
									<h5>NAVIGATE</h5>

									<p><a href="#">Advertisers</a></p>

									<p><a href="#">Developers</a></p>

									<p><a href="#">Resources</a></p>

									<p><a href="#">Company</a></p>

									<p><a href="#">Connect</a></p>
								</div>
							</div>
						</div>

						<div class="col-12 col-lg-3 col-md-6 ">
							<div class="contact_info_area d-sm-flex justify-content-between">
								<!-- Content Info -->
								<div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.4s">
									<h5>CONTACT US</h5>

									<p>Mailing Address:265 Akowonjo RD,</p>

									<p>Lagos, Nigeria. Los 23401</p>

									
									<p>Email: support@coinmagnetics.com</p>
									
								<div class="dream-btn-group fadeInUp" data-wow-delay="0.4s"><a
									href="https://t.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/tg.png"
										style="width: 43px; height: 40px; float: left;" /></a>&nbsp; &nbsp;&nbsp;
								&nbsp;<a href="https://www.instagram.com/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/ig.png" style="width: 42px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp; &nbsp;<a href="https://twitter.com/Coinmagnetics"><img alt=""
										src="/img/social/tw.png" style="width: 43px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp;&nbsp;<a href="https://fb.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/fb.png" style="width: 43px; height: 40px;" /></a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- ##### Footer Area End ##### -->
		<!-- ########## All JS ########## -->
		<!-- jQuery js -->
		<script src="js/jquery.min.js"></script><!-- Popper js -->
		<script src="js/popper.min.js"></script><!-- Bootstrap js -->
		<script src="js/bootstrap.min.js"></script><!-- All Plugins js -->
		<script src="js/plugins.js"></script><!-- Parallax js -->
		<script src="js/dzsparallaxer.js"></script>
		<script src="js/jquery.syotimer.min.js"></script><!-- script js -->
		<script src="js/script.js"></script>
	</div>
</body>
<script>
$('#recipeCarousel').carousel({
  interval: 4000
})

$('.carousel .carousel-item').each(function(){
    var minPerSlide = 3;
    var next = $(this).next();
    if (!next.length) {
    next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
    
    for (var i=0;i<minPerSlide;i++) {
        next=next.next();
        if (!next.length) {
        	next = $(this).siblings(':first');
      	}
        
        next.children(':first-child').clone().appendTo($(this));
      }
});

	</script>

</html>