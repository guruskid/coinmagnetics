<?php
session_start();

include('connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['id'])){
	
	header("location:login");
}else{

$get_user = mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$_SESSION['id']."' ");
$rows = mysqli_fetch_assoc($get_user);
    if(isset($_SESSION['2fa'])){

        if( ($_SESSION['2fa'] =="no" or $_SESSION['2fa'] =="pending") and $rows['2fa']==1){
            header("location:login");
        }


    }


}


?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="author" content="treasure">
  <link rel="icon" href="img/core-img/favicon.png" type="image/x-icon">
  <link rel="shortcut icon" href="img/core-img/favicon.png" type="image/x-icon">
  <title>Dashboard - Coin Magnetics</title>
  <!-- Google font-->
  <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap"
    rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap"
    rel="stylesheet">
  <!-- Font Awesome-->
  <link rel="stylesheet" type="text/css" href="assets/css/fontawesome.css">
  <!-- ico-font-->
  <link rel="stylesheet" type="text/css" href="assets/css/icofont.css">
  <!-- Themify icon-->
  <link rel="stylesheet" type="text/css" href="assets/css/themify.css">
  <!-- Flag icon-->
  <link rel="stylesheet" type="text/css" href="assets/css/flag-icon.css">

  <link rel="stylesheet" type="text/css" href="assets/css/tour.css">
  <!-- Feather icon-->
  <link rel="stylesheet" type="text/css" href="assets/css/feather-icon.css">
  <!-- Plugins css start-->
  <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
  <link rel="stylesheet" type="text/css" href="assets/css/chartist.css">
  <link rel="stylesheet" type="text/css" href="assets/css/date-picker.css">
  <!-- Plugins css Ends-->
  <!-- Bootstrap css-->
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <!-- App css-->
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">
  <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.css">
  <link id="color" rel="stylesheet" href="assets/css/color-1.css" media="screen">
  <!-- Responsive css-->
  <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
  <style>
@media (max-width: 768px) {
    .carousel-inner .carousel-item > div {
        display: none;
    }
    .carousel-inner .carousel-item > div:first-child {
        display: block;
    }
}

.carousel-inner .carousel-item.active,
.carousel-inner .carousel-item-next,
.carousel-inner .carousel-item-prev {
    display: flex;
}

/* display 3 */
@media (min-width: 768px) {
    
    .carousel-inner .carousel-item-right.active,
    .carousel-inner .carousel-item-next {
      transform: translateX(33.333%);
    }
    
    .carousel-inner .carousel-item-left.active, 
    .carousel-inner .carousel-item-prev {
      transform: translateX(-33.333%);
    }
}

.carousel-inner .carousel-item-right,
.carousel-inner .carousel-item-left{ 
  transform: translateX(0);
}


</style>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5dde42de43be710e1d1f5485/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</head>

<body class="dark-sidebar dark-only" onload="startTime()">
  <!-- tap on top starts-->
  <div class="tap-top"><i data-feather="chevrons-up"></i></div>
  <!-- tap on tap ends-->
  <!-- page-wrapper Start-->
  <div class="page-wrapper compact-wrapper" id="pageWrapper">
    <!-- Page Header Start-->
    <?php include('header.php'); ?>
    <!-- Page Header Ends   -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper sidebar-icon">
      <!-- Page Sidebar Start-->
      <?php include('sidebar.php'); ?>
      <!-- Page Sidebar Ends-->


      <div class="page-body">
        <div class="container-fluid">
          <div class="page-header">
            <div class="row">
              <div class="col-6">
                <h3>
                  Default</h3>
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="dashboard"><i data-feather="home"></i></a></li>
                  <li class="breadcrumb-item">Dashboard</li>
                </ol>
              </div>
              <div class="col-6">
                <!-- Bookmark Start-->
                
                <!-- Bookmark Ends-->
              </div>
            </div>
          </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
        <div class="row">
                    <div class="col-12">
                       <!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>

  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
  {
  "symbols": [
    {
      "proName": "FOREXCOM:SPXUSD",
      "title": "S&P 500"
    },
    {
      "proName": "FOREXCOM:NSXUSD",
      "title": "Nasdaq 100"
    },
    {
      "proName": "FX_IDC:EURUSD",
      "title": "EUR/USD"
    },
    {
      "proName": "BITSTAMP:BTCUSD",
      "title": "BTC/USD"
    },
    {
      "proName": "BITSTAMP:ETHUSD",
      "title": "ETH/USD"
    }
  ],
  "colorTheme": "light",
  "isTransparent": false,
  "displayMode": "adaptive",
  "locale": "en"
}
  </script>
</div>
<!-- TradingView Widget END -->
                        </div>

                    </div>

                    <br/><br/>

          <div class="row second-chart-list third-news-update">
            <div class="col-xl-4 col-lg-12 xl-50 morning-sec box-col-12">
              <div class="card o-hidden profile-greeting">
                <div class="card-body">
                  <div class="media">
                    <div class="badge-groups w-100">
                      <div class="badge f-12"><i class="mr-1" data-feather="clock"></i><span id="txt"></span></div>
                      <div class="badge f-12"><i class="fa fa-spin fa-cog f-14"></i></div>
                    </div>
                  </div>
                  <div class="greeting-user text-center">
                    <div class="profile-vector" ><img class="img-fluid" src="assets/images/dashboard/welcome.png" alt="">
                    </div>
                    <h4 class="f-w-600"><span id="greeting">Good Morning</span> <span class="right-circle"><i
                          class="fa fa-check-circle f-14 middle"></i></span></h4>
                    <p><span> Generate 10% Revenue in just 30 days.</span></p>
                    <div class="whatsnew-btn"><a href="hash" class="btn btn-primary">Rent Trading Bot</a><a href="https://t.me/treasurecapital" class="btn btn-primary">Join Telegram Group</a></div>
                    <div class="left-icon"><i class="fa fa-bell"> </i></div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-6 col-lg-12">
              
                <div class="card o-hidden">
                  <div class="card-body">
                    <div class="ecommerce-widgets media">
                      <div class="media-body">
                        <p class="f-w-500 font-roboto">Wallet Balance <span class="badge pill-badge-primary ml-3">Current</span></p>
                        <h4 class="f-w-500 mb-0 f-26">$<span class="counters"><?php echo $rows['wallet']; ?></span></h4>
                      </div>
                      <div class="ecommerce-box light-bg-primary"><i class="fa fa-heart" aria-hidden="true"></i></div>
                    </div>
                  </div>
                </div>

<?php 
$getmine =mysqli_query($mysqli,"SELECT * FROM investment WHERE userid='".$rows['id']."' ");

$totalmine=0;
$totalrevenune =0;
while($r=mysqli_fetch_assoc($getmine)){
  $totalmine += $r['amount'];
  $totalrevenune +=$r['added_roi'];
}
?>
                
                  <div class="card o-hidden">
                    <div class="card-body">
                      <div class="media">
                        <div class="media-body">
                          <p class="f-w-500 font-roboto">Total Mining Pool <span class="badge pill-badge-primary ml-3">Update</span></p>
                          <div class="progress-box">
                            <h4 class="f-w-500 mb-0 f-26">$<span class="counter"><?php echo $totalmine; ?></span></h4>
                            <div class="progress sm-progress-bar progress-animate app-right d-flex justify-content-end">
                              <div class="progress-gradient-primary" role="progressbar" style="width: 55%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"><span class="font-primary">100%</span><span class="animate-circle"></span></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>



                <div class="card o-hidden">
                    <div class="card-body <?php if($rows['wesscore']<=5){ echo 'bg-danger'; }else{ echo 'bg-success';} ?>">
                      <div class="media">
                        <div class="media-body">
                          <p class="f-w-500 font-roboto">Withdrawal Eligibility Score  <span class="badge pill-badge-primary ml-3">WES</span></p>
                          <div class="progress-box">
                            <h4 class="f-w-500 mb-0 f-26"><span class="counter"><?php echo $rows['wesscore']; ?></span></h4>
                            <div class="progress sm-progress-bar progress-animate app-right d-flex justify-content-end">
                              <div class="progress-gradient-primary" role="progressbar" style="width: 55%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"><span class="font-primary">day</span><span class="animate-circle"></span></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>



            



              
            </div>
             
         

            <div class="col-xl-9 xl-100 chart_data_left box-col-12">
              <div class="card">
                <div class="card-body p-0">
                  <div class="row m-0 chart-main">
                    <div class="col-xl-3 col-md-6 col-sm-6 p-0 box-col-6">
                      <div class="media align-items-center">
                        <div class="hospital-small-chart">
                          <div class="small-bar">
                            <div class="small-chart flot-chart-container"></div>
                          </div>
                        </div>
                        <?php $getwith = mysqli_query($mysqli,"SELECT * FROM withdrawal WHERE userid='".$rows['id']."' "); ?>
                        <div class="media-body">
                          <div class="right-chart-content">
                            <h4><?php echo mysqli_num_rows($getwith); ?></h4><span>Withdrawals </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-sm-6 p-0 box-col-6">
                      <div class="media align-items-center">
                        <div class="hospital-small-chart">
                          <div class="small-bar">
                            <div class="small-chart1 flot-chart-container"></div>
                          </div>
                        </div>
                        <?php $gett = mysqli_query($mysqli,"SELECT * FROM activity WHERE userid='".$rows['id']."' "); ?>
                        <div class="media-body">
                          <div class="right-chart-content">
                            <h4><?php echo mysqli_num_rows($gett); ?></h4><span>Transactions</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-sm-6 p-0 box-col-6">
                      <div class="media align-items-center">
                        <div class="hospital-small-chart">
                          <div class="small-bar">
                            <div class="small-chart2 flot-chart-container"></div>
                          </div>
                        </div>
                        <div class="media-body">
                          <div class="right-chart-content">
                            <h4><?php echo mysqli_num_rows($getmine); ?></h4><span>Total Pools</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-sm-6 p-0 box-col-6">
                      <div class="media border-none align-items-center">
                        <div class="hospital-small-chart">
                          <div class="small-bar">
                            <div class="small-chart3 flot-chart-container"></div>
                          </div>
                        </div>
                        <div class="media-body">
                          <div class="right-chart-content">
                            <h4><?php echo $rows['level_command']; ?></h4><span>Commander Level</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

 

        
           
            <div class="col-xl-12">
              <div class="card o-hidden dash-chart">
                <div class="card-header card-no-border">
                  <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                      <li><i class="fa fa-spin fa-cog"></i></li>
                      <li><i class="view-html fa fa-code"></i></li>
                      <li><i class="icofont icofont-maximize full-card"></i></li>
                      <li><i class="icofont icofont-minus minimize-card"></i></li>
                      <li><i class="icofont icofont-refresh reload-card"></i></li>
                      <li><i class="icofont icofont-error close-card"></i></li>
                    </ul>
                  </div>
                  <div class="media">
                    <div class="media-body">
                      <p><span class="f-w-500 font-roboto">Total Revenue</span><span class="font-primary f-w-700 ml-2"> </span></p>
                      <h4 class="f-w-500 mb-0 f-26">$<span class="counter"><?php echo $totalrevenune; ?></span></h4>
                    </div>
                  </div>
                </div>
                <div class="card-body pt-0">
                  <div class="negative-container">
                    <div id="negative-chart"></div>
                  </div>
                  <div class="code-box-copy">
                   
                    
                  </div>
                </div>
              </div>
            </div>
            </div>

            <div id="recipeCarousel" class="carousel slide "  data-ride="carousel" >
            <div class="carousel-inner row ">

       <?php  $gettest = mysqli_query($mysqli,"SELECT * FROM testimony  ORDER BY id DESC LIMIT 5");
             $i=0;
             while($test = mysqli_fetch_assoc($gettest) ){ $i++; ?>
              <div class="carousel-item <?php if($i==1){ echo" active";} ?> ">
            <div class=" col-md-4">
            <div class="card testimonial text-center">
                  <div class="card-body">
                    <div class="owl-carousel owl-theme" id="owl-carousel-testimonial">
                      <div class="item"><i class="icon-quote-left"></i>
                        <p><?php echo $test['info']; ?></p><img class="img-80" src="../assets/images/dashboard/welcome.png" alt="">
                        <h5 class="font-primary"><?php echo $test['username'] ?></h5><span><?php echo $test['title']; ?></span>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>


              <?php } ?>
              
    </div>
</div>



              <div class="row">
            <div class="col-xl-6 xl-100 box-col-12">
                <div class="card">
                  <div class="card-header">
                    <h5>GENERAL ACTIVITIES </h5>
                    <div class="card-header-right">
                      
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="user-status table-responsive">
                      <table class="table table-bordernone">
                        <thead>
                          <tr>
                            <th scope="col">Action</th>
                            <th scope="col">Username</th>
                            <th scope="col">Date</th>
                            <th scope="col">Status</th>
                            <th scope="col">Amount</th>
                          </tr>
                        </thead>
                        <tbody>

                        <?php $get = mysqli_query($mysqli,"SELECT * FROM activity ORDER BY id DESC LIMIT 10");
                              $i=0;
                              while($row= mysqli_fetch_assoc($get)){ 
                                $getu = mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$row['userid']."' ");
                                $r = mysqli_fetch_assoc($getu);
                                 ?>
                          <tr>
                            <td class="f-w-600"><?php echo $row['action']; ?></td>
                            <td class="digits"><?php echo $r['firstname']." ".$r['lastname']; ?></td>
                            <td class="digits"><?php echo $row['date']; ?></td>
                            <td class="font-primary"><?php echo $row['status']; ?></td>
                            <td>
                              <div class="span badge badge-pill <?php if($row['status']=="Confirmed" or $row['status']=="Proccessed" or $row['status']=="Completed"){ echo 'pill-badge-success';}elseif($row['status']=="Pending" or $row['status']=="Requested" ){ echo 'pill-badge-secondary';} ?> ">$<?php echo $row['amount']; ?></div>
                            </td>
                          </tr>
                          <?php } ?>



                         
                        </tbody>
                      </table>
                    </div>
                    
                  </div>
                </div>
              </div>




            
          </div>
        </div>
        <!-- Container-fluid Ends-->
      </div>



      <!-- footer start-->
      <footer class="footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6 footer-copyright">
              <p class="mb-0">Copyright <?php echo date('Y'); ?> © Coin Magnetics All rights reserved.</p>
            </div>
            <div class="col-md-6">
              <p class="pull-right mb-0"> </p>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!-- latest jquery-->
  <script src="assets/js/jquery-3.5.1.min.js"></script>
  <!-- Bootstrap js-->
  <script src="assets/js/bootstrap/popper.min.js"></script>
  <script src="assets/js/bootstrap/bootstrap.js"></script>
  <!-- feather icon js-->
  <script src="assets/js/icons/feather-icon/feather.min.js"></script>
  <script src="assets/js/icons/feather-icon/feather-icon.js"></script>
  <!-- Sidebar jquery-->
  <script src="assets/js/sidebar-menu.js"></script>
  <script src="assets/js/config.js"></script>
  <!-- Plugins JS start-->
  <script src="assets/js/chart/chartist/chartist.js"></script>
  <script src="assets/js/chart/chartist/chartist-plugin-tooltip.js"></script>
  <script src="assets/js/chart/knob/knob.min.js"></script>
  <script src="assets/js/chart/knob/knob-chart.js"></script>
  <script src="assets/js/chart/apex-chart/apex-chart.js"></script>
  <script src="assets/js/chart/apex-chart/stock-prices.js"></script>
  <script src="assets/js/notify/bootstrap-notify.min.js"></script>
  <script src="assets/js/dashboard/default.js"></script>

  <script src="assets/js/datepicker/date-picker/datepicker.js"></script>
  <script src="assets/js/datepicker/date-picker/datepicker.en.js"></script>
  <script src="assets/js/datepicker/date-picker/datepicker.custom.js"></script>
  <script src="assets/js/typeahead/handlebars.js"></script>
  <script src="assets/js/typeahead/typeahead.bundle.js"></script>
  <script src="assets/js/typeahead/typeahead.custom.js"></script>
  <script src="assets/js/typeahead-search/handlebars.js"></script>
  <script src="assets/js/dashboard/dashboard_2.js"></script>
  <script src="assets/js/typeahead-search/typeahead-custom.js"></script>
  <script src="assets/js/sweet-alert/sweetalert.min.js"></script>
  <script src="assets/js/tooltip-init.js"></script>
  <script src="assets/js/tour/intro.js"></script>
    <script src="assets/js/tour/intro-init.js"></script>
  <!-- Plugins JS Ends-->
  <!-- Theme js-->
  <script src="assets/js/script.js"></script>

  <!-- login js-->
  <!-- Plugin used-->
</body>
<?php if($rows['cycle']==1){  ?>
<script>
swal('Add testimony ',
    'Please add testimony before you can proceed', 'info')
</script>
<?php } ?>
<script>
$('#recipeCarousel').carousel({
  interval: 4000
})

$('.carousel .carousel-item').each(function(){
    var minPerSlide = 3;
    var next = $(this).next();
    if (!next.length) {
    next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
    
    for (var i=0;i<minPerSlide;i++) {
        next=next.next();
        if (!next.length) {
        	next = $(this).siblings(':first');
      	}
        
        next.children(':first-child').clone().appendTo($(this));
      }
});

</script>

<?php

if($rows['wes']==0 and $rows['wesscore']<1){

  $getlast = mysqli_query($mysqli,"SELECT * FROM  investment WHERE userid='".$rows['id']."' ORDER BY id DESC  ");
  $total = 0;
  while($inn = mysqli_fetch_assoc($getlast)){
    $total +=$inn['amount'];

  }


  if(mysqli_num_rows($getlast)>0){  $min= $total; }else{ $min= $row['min_amount']; }


  ?>
  <script>
  
  
  swal(
       'Your WITHDRAWAL ELIGIBILITY SCORE(WES) is zero.',
      "Please Rent a New bot of $<?php echo $min; ?>  or above to boost your WES .",
      'warning'
  )
  
  
  </script>
  
  <?php


  }

?>

</html>