<?php
session_start();

include('connection.php');


//check if session id is set if it is redirect to login
if(!isset($_SESSION['id'])){
	
	header("location:login");
}else{

$get_user = mysqli_query($mysqli,"SELECT * FROM users WHERE id='".$_SESSION['id']."' ");
$rows = mysqli_fetch_assoc($get_user);
    if(isset($_SESSION['2fa'])){

        if( ($_SESSION['2fa'] =="no" or $_SESSION['2fa'] =="pending") and $rows['2fa']==1){
            header("location:login");
        }


    }


}



$orderid = $_GET['orderid'];

$getorder = mysqli_query($mysqli,"SELECT * FROM pending WHERE chargeid='$orderid'");
$row = mysqli_fetch_assoc($getorder);


?>
<!DOCTYPE html>

<html lang="en">

<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
        <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
        <meta name="theme-color" content="#000000">
       
        <link href="monnify_files/css" rel="stylesheet">
        <link rel="stylesheet" href="monnify_files/animate.css">
        <title>Monnify Payment Gateway</title>

<style>
/* latin-ext */
@font-face {
  font-family: 'Oxygen';
  font-style: normal;
  font-weight: 300;
  src: local('Oxygen Light'), local('Oxygen-Light'), url(https://fonts.gstatic.com/s/oxygen/v10/2sDcZG1Wl4LcnbuCJW8zZmW5Kb8VZBHR.woff2) format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Oxygen';
  font-style: normal;
  font-weight: 300;
  src: local('Oxygen Light'), local('Oxygen-Light'), url(https://fonts.gstatic.com/s/oxygen/v10/2sDcZG1Wl4LcnbuCJW8zaGW5Kb8VZA.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* latin-ext */
@font-face {
  font-family: 'Oxygen';
  font-style: normal;
  font-weight: 400;
  src: local('Oxygen Regular'), local('Oxygen-Regular'), url(https://fonts.gstatic.com/s/oxygen/v10/2sDfZG1Wl4LcnbuKgE0mRUe0A4Uc.woff2) format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Oxygen';
  font-style: normal;
  font-weight: 400;
  src: local('Oxygen Regular'), local('Oxygen-Regular'), url(https://fonts.gstatic.com/s/oxygen/v10/2sDfZG1Wl4LcnbuKjk0mRUe0Aw.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* latin-ext */
@font-face {
  font-family: 'Oxygen';
  font-style: normal;
  font-weight: 700;
  src: local('Oxygen Bold'), local('Oxygen-Bold'), url(https://fonts.gstatic.com/s/oxygen/v10/2sDcZG1Wl4LcnbuCNWgzZmW5Kb8VZBHR.woff2) format('woff2');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Oxygen';
  font-style: normal;
  font-weight: 700;
  src: local('Oxygen Bold'), local('Oxygen-Bold'), url(https://fonts.gstatic.com/s/oxygen/v10/2sDcZG1Wl4LcnbuCNWgzaGW5Kb8VZA.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}


@charset "UTF-8";

/*!
 * animate.css -http://daneden.me/animate
 * Version - 3.7.0
 * Licensed under the MIT license - http://opensource.org/licenses/MIT
 *
 * Copyright (c) 2018 Daniel Eden
 */

@-webkit-keyframes bounce {
  from,
  20%,
  53%,
  80%,
  to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  40%,
  43% {
    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);
    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);
    -webkit-transform: translate3d(0, -30px, 0);
    transform: translate3d(0, -30px, 0);
  }

  70% {
    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);
    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);
    -webkit-transform: translate3d(0, -15px, 0);
    transform: translate3d(0, -15px, 0);
  }

  90% {
    -webkit-transform: translate3d(0, -4px, 0);
    transform: translate3d(0, -4px, 0);
  }
}

@keyframes bounce {
  from,
  20%,
  53%,
  80%,
  to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  40%,
  43% {
    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);
    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);
    -webkit-transform: translate3d(0, -30px, 0);
    transform: translate3d(0, -30px, 0);
  }

  70% {
    -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);
    animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);
    -webkit-transform: translate3d(0, -15px, 0);
    transform: translate3d(0, -15px, 0);
  }

  90% {
    -webkit-transform: translate3d(0, -4px, 0);
    transform: translate3d(0, -4px, 0);
  }
}

.bounce {
  -webkit-animation-name: bounce;
  animation-name: bounce;
  -webkit-transform-origin: center bottom;
  transform-origin: center bottom;
}

@-webkit-keyframes flash {
  from,
  50%,
  to {
    opacity: 1;
  }

  25%,
  75% {
    opacity: 0;
  }
}

@keyframes flash {
  from,
  50%,
  to {
    opacity: 1;
  }

  25%,
  75% {
    opacity: 0;
  }
}

.flash {
  -webkit-animation-name: flash;
  animation-name: flash;
}

/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */

@-webkit-keyframes pulse {
  from {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }

  50% {
    -webkit-transform: scale3d(1.05, 1.05, 1.05);
    transform: scale3d(1.05, 1.05, 1.05);
  }

  to {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}

@keyframes pulse {
  from {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }

  50% {
    -webkit-transform: scale3d(1.05, 1.05, 1.05);
    transform: scale3d(1.05, 1.05, 1.05);
  }

  to {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}

.pulse {
  -webkit-animation-name: pulse;
  animation-name: pulse;
}

@-webkit-keyframes rubberBand {
  from {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }

  30% {
    -webkit-transform: scale3d(1.25, 0.75, 1);
    transform: scale3d(1.25, 0.75, 1);
  }

  40% {
    -webkit-transform: scale3d(0.75, 1.25, 1);
    transform: scale3d(0.75, 1.25, 1);
  }

  50% {
    -webkit-transform: scale3d(1.15, 0.85, 1);
    transform: scale3d(1.15, 0.85, 1);
  }

  65% {
    -webkit-transform: scale3d(0.95, 1.05, 1);
    transform: scale3d(0.95, 1.05, 1);
  }

  75% {
    -webkit-transform: scale3d(1.05, 0.95, 1);
    transform: scale3d(1.05, 0.95, 1);
  }

  to {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}

@keyframes rubberBand {
  from {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }

  30% {
    -webkit-transform: scale3d(1.25, 0.75, 1);
    transform: scale3d(1.25, 0.75, 1);
  }

  40% {
    -webkit-transform: scale3d(0.75, 1.25, 1);
    transform: scale3d(0.75, 1.25, 1);
  }

  50% {
    -webkit-transform: scale3d(1.15, 0.85, 1);
    transform: scale3d(1.15, 0.85, 1);
  }

  65% {
    -webkit-transform: scale3d(0.95, 1.05, 1);
    transform: scale3d(0.95, 1.05, 1);
  }

  75% {
    -webkit-transform: scale3d(1.05, 0.95, 1);
    transform: scale3d(1.05, 0.95, 1);
  }

  to {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}

.rubberBand {
  -webkit-animation-name: rubberBand;
  animation-name: rubberBand;
}

@-webkit-keyframes shake {
  from,
  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  10%,
  30%,
  50%,
  70%,
  90% {
    -webkit-transform: translate3d(-10px, 0, 0);
    transform: translate3d(-10px, 0, 0);
  }

  20%,
  40%,
  60%,
  80% {
    -webkit-transform: translate3d(10px, 0, 0);
    transform: translate3d(10px, 0, 0);
  }
}

@keyframes shake {
  from,
  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  10%,
  30%,
  50%,
  70%,
  90% {
    -webkit-transform: translate3d(-10px, 0, 0);
    transform: translate3d(-10px, 0, 0);
  }

  20%,
  40%,
  60%,
  80% {
    -webkit-transform: translate3d(10px, 0, 0);
    transform: translate3d(10px, 0, 0);
  }
}

.shake {
  -webkit-animation-name: shake;
  animation-name: shake;
}

@-webkit-keyframes headShake {
  0% {
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }

  6.5% {
    -webkit-transform: translateX(-6px) rotateY(-9deg);
    transform: translateX(-6px) rotateY(-9deg);
  }

  18.5% {
    -webkit-transform: translateX(5px) rotateY(7deg);
    transform: translateX(5px) rotateY(7deg);
  }

  31.5% {
    -webkit-transform: translateX(-3px) rotateY(-5deg);
    transform: translateX(-3px) rotateY(-5deg);
  }

  43.5% {
    -webkit-transform: translateX(2px) rotateY(3deg);
    transform: translateX(2px) rotateY(3deg);
  }

  50% {
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }
}

@keyframes headShake {
  0% {
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }

  6.5% {
    -webkit-transform: translateX(-6px) rotateY(-9deg);
    transform: translateX(-6px) rotateY(-9deg);
  }

  18.5% {
    -webkit-transform: translateX(5px) rotateY(7deg);
    transform: translateX(5px) rotateY(7deg);
  }

  31.5% {
    -webkit-transform: translateX(-3px) rotateY(-5deg);
    transform: translateX(-3px) rotateY(-5deg);
  }

  43.5% {
    -webkit-transform: translateX(2px) rotateY(3deg);
    transform: translateX(2px) rotateY(3deg);
  }

  50% {
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }
}

.headShake {
  -webkit-animation-timing-function: ease-in-out;
  animation-timing-function: ease-in-out;
  -webkit-animation-name: headShake;
  animation-name: headShake;
}

@-webkit-keyframes swing {
  20% {
    -webkit-transform: rotate3d(0, 0, 1, 15deg);
    transform: rotate3d(0, 0, 1, 15deg);
  }

  40% {
    -webkit-transform: rotate3d(0, 0, 1, -10deg);
    transform: rotate3d(0, 0, 1, -10deg);
  }

  60% {
    -webkit-transform: rotate3d(0, 0, 1, 5deg);
    transform: rotate3d(0, 0, 1, 5deg);
  }

  80% {
    -webkit-transform: rotate3d(0, 0, 1, -5deg);
    transform: rotate3d(0, 0, 1, -5deg);
  }

  to {
    -webkit-transform: rotate3d(0, 0, 1, 0deg);
    transform: rotate3d(0, 0, 1, 0deg);
  }
}

@keyframes swing {
  20% {
    -webkit-transform: rotate3d(0, 0, 1, 15deg);
    transform: rotate3d(0, 0, 1, 15deg);
  }

  40% {
    -webkit-transform: rotate3d(0, 0, 1, -10deg);
    transform: rotate3d(0, 0, 1, -10deg);
  }

  60% {
    -webkit-transform: rotate3d(0, 0, 1, 5deg);
    transform: rotate3d(0, 0, 1, 5deg);
  }

  80% {
    -webkit-transform: rotate3d(0, 0, 1, -5deg);
    transform: rotate3d(0, 0, 1, -5deg);
  }

  to {
    -webkit-transform: rotate3d(0, 0, 1, 0deg);
    transform: rotate3d(0, 0, 1, 0deg);
  }
}

.swing {
  -webkit-transform-origin: top center;
  transform-origin: top center;
  -webkit-animation-name: swing;
  animation-name: swing;
}

@-webkit-keyframes tada {
  from {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }

  10%,
  20% {
    -webkit-transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);
    transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);
  }

  30%,
  50%,
  70%,
  90% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
  }

  40%,
  60%,
  80% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
  }

  to {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}

@keyframes tada {
  from {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }

  10%,
  20% {
    -webkit-transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);
    transform: scale3d(0.9, 0.9, 0.9) rotate3d(0, 0, 1, -3deg);
  }

  30%,
  50%,
  70%,
  90% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, 3deg);
  }

  40%,
  60%,
  80% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
    transform: scale3d(1.1, 1.1, 1.1) rotate3d(0, 0, 1, -3deg);
  }

  to {
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}

.tada {
  -webkit-animation-name: tada;
  animation-name: tada;
}

/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */

@-webkit-keyframes wobble {
  from {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  15% {
    -webkit-transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg);
    transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg);
  }

  30% {
    -webkit-transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg);
    transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg);
  }

  45% {
    -webkit-transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg);
    transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg);
  }

  60% {
    -webkit-transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg);
    transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg);
  }

  75% {
    -webkit-transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg);
    transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg);
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes wobble {
  from {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  15% {
    -webkit-transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg);
    transform: translate3d(-25%, 0, 0) rotate3d(0, 0, 1, -5deg);
  }

  30% {
    -webkit-transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg);
    transform: translate3d(20%, 0, 0) rotate3d(0, 0, 1, 3deg);
  }

  45% {
    -webkit-transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg);
    transform: translate3d(-15%, 0, 0) rotate3d(0, 0, 1, -3deg);
  }

  60% {
    -webkit-transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg);
    transform: translate3d(10%, 0, 0) rotate3d(0, 0, 1, 2deg);
  }

  75% {
    -webkit-transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg);
    transform: translate3d(-5%, 0, 0) rotate3d(0, 0, 1, -1deg);
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.wobble {
  -webkit-animation-name: wobble;
  animation-name: wobble;
}

@-webkit-keyframes jello {
  from,
  11.1%,
  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  22.2% {
    -webkit-transform: skewX(-12.5deg) skewY(-12.5deg);
    transform: skewX(-12.5deg) skewY(-12.5deg);
  }

  33.3% {
    -webkit-transform: skewX(6.25deg) skewY(6.25deg);
    transform: skewX(6.25deg) skewY(6.25deg);
  }

  44.4% {
    -webkit-transform: skewX(-3.125deg) skewY(-3.125deg);
    transform: skewX(-3.125deg) skewY(-3.125deg);
  }

  55.5% {
    -webkit-transform: skewX(1.5625deg) skewY(1.5625deg);
    transform: skewX(1.5625deg) skewY(1.5625deg);
  }

  66.6% {
    -webkit-transform: skewX(-0.78125deg) skewY(-0.78125deg);
    transform: skewX(-0.78125deg) skewY(-0.78125deg);
  }

  77.7% {
    -webkit-transform: skewX(0.390625deg) skewY(0.390625deg);
    transform: skewX(0.390625deg) skewY(0.390625deg);
  }

  88.8% {
    -webkit-transform: skewX(-0.1953125deg) skewY(-0.1953125deg);
    transform: skewX(-0.1953125deg) skewY(-0.1953125deg);
  }
}

@keyframes jello {
  from,
  11.1%,
  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  22.2% {
    -webkit-transform: skewX(-12.5deg) skewY(-12.5deg);
    transform: skewX(-12.5deg) skewY(-12.5deg);
  }

  33.3% {
    -webkit-transform: skewX(6.25deg) skewY(6.25deg);
    transform: skewX(6.25deg) skewY(6.25deg);
  }

  44.4% {
    -webkit-transform: skewX(-3.125deg) skewY(-3.125deg);
    transform: skewX(-3.125deg) skewY(-3.125deg);
  }

  55.5% {
    -webkit-transform: skewX(1.5625deg) skewY(1.5625deg);
    transform: skewX(1.5625deg) skewY(1.5625deg);
  }

  66.6% {
    -webkit-transform: skewX(-0.78125deg) skewY(-0.78125deg);
    transform: skewX(-0.78125deg) skewY(-0.78125deg);
  }

  77.7% {
    -webkit-transform: skewX(0.390625deg) skewY(0.390625deg);
    transform: skewX(0.390625deg) skewY(0.390625deg);
  }

  88.8% {
    -webkit-transform: skewX(-0.1953125deg) skewY(-0.1953125deg);
    transform: skewX(-0.1953125deg) skewY(-0.1953125deg);
  }
}

.jello {
  -webkit-animation-name: jello;
  animation-name: jello;
  -webkit-transform-origin: center;
  transform-origin: center;
}

@-webkit-keyframes heartBeat {
  0% {
    -webkit-transform: scale(1);
    transform: scale(1);
  }

  14% {
    -webkit-transform: scale(1.3);
    transform: scale(1.3);
  }

  28% {
    -webkit-transform: scale(1);
    transform: scale(1);
  }

  42% {
    -webkit-transform: scale(1.3);
    transform: scale(1.3);
  }

  70% {
    -webkit-transform: scale(1);
    transform: scale(1);
  }
}

@keyframes heartBeat {
  0% {
    -webkit-transform: scale(1);
    transform: scale(1);
  }

  14% {
    -webkit-transform: scale(1.3);
    transform: scale(1.3);
  }

  28% {
    -webkit-transform: scale(1);
    transform: scale(1);
  }

  42% {
    -webkit-transform: scale(1.3);
    transform: scale(1.3);
  }

  70% {
    -webkit-transform: scale(1);
    transform: scale(1);
  }
}

.heartBeat {
  -webkit-animation-name: heartBeat;
  animation-name: heartBeat;
  -webkit-animation-duration: 1.3s;
  animation-duration: 1.3s;
  -webkit-animation-timing-function: ease-in-out;
  animation-timing-function: ease-in-out;
}

@-webkit-keyframes bounceIn {
  from,
  20%,
  40%,
  60%,
  80%,
  to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }

  0% {
    opacity: 0;
    -webkit-transform: scale3d(0.3, 0.3, 0.3);
    transform: scale3d(0.3, 0.3, 0.3);
  }

  20% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1);
    transform: scale3d(1.1, 1.1, 1.1);
  }

  40% {
    -webkit-transform: scale3d(0.9, 0.9, 0.9);
    transform: scale3d(0.9, 0.9, 0.9);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(1.03, 1.03, 1.03);
    transform: scale3d(1.03, 1.03, 1.03);
  }

  80% {
    -webkit-transform: scale3d(0.97, 0.97, 0.97);
    transform: scale3d(0.97, 0.97, 0.97);
  }

  to {
    opacity: 1;
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}

@keyframes bounceIn {
  from,
  20%,
  40%,
  60%,
  80%,
  to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }

  0% {
    opacity: 0;
    -webkit-transform: scale3d(0.3, 0.3, 0.3);
    transform: scale3d(0.3, 0.3, 0.3);
  }

  20% {
    -webkit-transform: scale3d(1.1, 1.1, 1.1);
    transform: scale3d(1.1, 1.1, 1.1);
  }

  40% {
    -webkit-transform: scale3d(0.9, 0.9, 0.9);
    transform: scale3d(0.9, 0.9, 0.9);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(1.03, 1.03, 1.03);
    transform: scale3d(1.03, 1.03, 1.03);
  }

  80% {
    -webkit-transform: scale3d(0.97, 0.97, 0.97);
    transform: scale3d(0.97, 0.97, 0.97);
  }

  to {
    opacity: 1;
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
  }
}

.bounceIn {
  -webkit-animation-duration: 0.75s;
  animation-duration: 0.75s;
  -webkit-animation-name: bounceIn;
  animation-name: bounceIn;
}

@-webkit-keyframes bounceInDown {
  from,
  60%,
  75%,
  90%,
  to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }

  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, -3000px, 0);
    transform: translate3d(0, -3000px, 0);
  }

  60% {
    opacity: 1;
    -webkit-transform: translate3d(0, 25px, 0);
    transform: translate3d(0, 25px, 0);
  }

  75% {
    -webkit-transform: translate3d(0, -10px, 0);
    transform: translate3d(0, -10px, 0);
  }

  90% {
    -webkit-transform: translate3d(0, 5px, 0);
    transform: translate3d(0, 5px, 0);
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes bounceInDown {
  from,
  60%,
  75%,
  90%,
  to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }

  0% {
    opacity: 0;
    -webkit-transform: translate3d(0, -3000px, 0);
    transform: translate3d(0, -3000px, 0);
  }

  60% {
    opacity: 1;
    -webkit-transform: translate3d(0, 25px, 0);
    transform: translate3d(0, 25px, 0);
  }

  75% {
    -webkit-transform: translate3d(0, -10px, 0);
    transform: translate3d(0, -10px, 0);
  }

  90% {
    -webkit-transform: translate3d(0, 5px, 0);
    transform: translate3d(0, 5px, 0);
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.bounceInDown {
  -webkit-animation-name: bounceInDown;
  animation-name: bounceInDown;
}

@-webkit-keyframes bounceInLeft {
  from,
  60%,
  75%,
  90%,
  to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }

  0% {
    opacity: 0;
    -webkit-transform: translate3d(-3000px, 0, 0);
    transform: translate3d(-3000px, 0, 0);
  }

  60% {
    opacity: 1;
    -webkit-transform: translate3d(25px, 0, 0);
    transform: translate3d(25px, 0, 0);
  }

  75% {
    -webkit-transform: translate3d(-10px, 0, 0);
    transform: translate3d(-10px, 0, 0);
  }

  90% {
    -webkit-transform: translate3d(5px, 0, 0);
    transform: translate3d(5px, 0, 0);
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes bounceInLeft {
  from,
  60%,
  75%,
  90%,
  to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }

  0% {
    opacity: 0;
    -webkit-transform: translate3d(-3000px, 0, 0);
    transform: translate3d(-3000px, 0, 0);
  }

  60% {
    opacity: 1;
    -webkit-transform: translate3d(25px, 0, 0);
    transform: translate3d(25px, 0, 0);
  }

  75% {
    -webkit-transform: translate3d(-10px, 0, 0);
    transform: translate3d(-10px, 0, 0);
  }

  90% {
    -webkit-transform: translate3d(5px, 0, 0);
    transform: translate3d(5px, 0, 0);
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.bounceInLeft {
  -webkit-animation-name: bounceInLeft;
  animation-name: bounceInLeft;
}

@-webkit-keyframes bounceInRight {
  from,
  60%,
  75%,
  90%,
  to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }

  from {
    opacity: 0;
    -webkit-transform: translate3d(3000px, 0, 0);
    transform: translate3d(3000px, 0, 0);
  }

  60% {
    opacity: 1;
    -webkit-transform: translate3d(-25px, 0, 0);
    transform: translate3d(-25px, 0, 0);
  }

  75% {
    -webkit-transform: translate3d(10px, 0, 0);
    transform: translate3d(10px, 0, 0);
  }

  90% {
    -webkit-transform: translate3d(-5px, 0, 0);
    transform: translate3d(-5px, 0, 0);
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes bounceInRight {
  from,
  60%,
  75%,
  90%,
  to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }

  from {
    opacity: 0;
    -webkit-transform: translate3d(3000px, 0, 0);
    transform: translate3d(3000px, 0, 0);
  }

  60% {
    opacity: 1;
    -webkit-transform: translate3d(-25px, 0, 0);
    transform: translate3d(-25px, 0, 0);
  }

  75% {
    -webkit-transform: translate3d(10px, 0, 0);
    transform: translate3d(10px, 0, 0);
  }

  90% {
    -webkit-transform: translate3d(-5px, 0, 0);
    transform: translate3d(-5px, 0, 0);
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.bounceInRight {
  -webkit-animation-name: bounceInRight;
  animation-name: bounceInRight;
}

@-webkit-keyframes bounceInUp {
  from,
  60%,
  75%,
  90%,
  to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }

  from {
    opacity: 0;
    -webkit-transform: translate3d(0, 3000px, 0);
    transform: translate3d(0, 3000px, 0);
  }

  60% {
    opacity: 1;
    -webkit-transform: translate3d(0, -20px, 0);
    transform: translate3d(0, -20px, 0);
  }

  75% {
    -webkit-transform: translate3d(0, 10px, 0);
    transform: translate3d(0, 10px, 0);
  }

  90% {
    -webkit-transform: translate3d(0, -5px, 0);
    transform: translate3d(0, -5px, 0);
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes bounceInUp {
  from,
  60%,
  75%,
  90%,
  to {
    -webkit-animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }

  from {
    opacity: 0;
    -webkit-transform: translate3d(0, 3000px, 0);
    transform: translate3d(0, 3000px, 0);
  }

  60% {
    opacity: 1;
    -webkit-transform: translate3d(0, -20px, 0);
    transform: translate3d(0, -20px, 0);
  }

  75% {
    -webkit-transform: translate3d(0, 10px, 0);
    transform: translate3d(0, 10px, 0);
  }

  90% {
    -webkit-transform: translate3d(0, -5px, 0);
    transform: translate3d(0, -5px, 0);
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.bounceInUp {
  -webkit-animation-name: bounceInUp;
  animation-name: bounceInUp;
}

@-webkit-keyframes bounceOut {
  20% {
    -webkit-transform: scale3d(0.9, 0.9, 0.9);
    transform: scale3d(0.9, 0.9, 0.9);
  }

  50%,
  55% {
    opacity: 1;
    -webkit-transform: scale3d(1.1, 1.1, 1.1);
    transform: scale3d(1.1, 1.1, 1.1);
  }

  to {
    opacity: 0;
    -webkit-transform: scale3d(0.3, 0.3, 0.3);
    transform: scale3d(0.3, 0.3, 0.3);
  }
}

@keyframes bounceOut {
  20% {
    -webkit-transform: scale3d(0.9, 0.9, 0.9);
    transform: scale3d(0.9, 0.9, 0.9);
  }

  50%,
  55% {
    opacity: 1;
    -webkit-transform: scale3d(1.1, 1.1, 1.1);
    transform: scale3d(1.1, 1.1, 1.1);
  }

  to {
    opacity: 0;
    -webkit-transform: scale3d(0.3, 0.3, 0.3);
    transform: scale3d(0.3, 0.3, 0.3);
  }
}

.bounceOut {
  -webkit-animation-duration: 0.75s;
  animation-duration: 0.75s;
  -webkit-animation-name: bounceOut;
  animation-name: bounceOut;
}

@-webkit-keyframes bounceOutDown {
  20% {
    -webkit-transform: translate3d(0, 10px, 0);
    transform: translate3d(0, 10px, 0);
  }

  40%,
  45% {
    opacity: 1;
    -webkit-transform: translate3d(0, -20px, 0);
    transform: translate3d(0, -20px, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(0, 2000px, 0);
    transform: translate3d(0, 2000px, 0);
  }
}

@keyframes bounceOutDown {
  20% {
    -webkit-transform: translate3d(0, 10px, 0);
    transform: translate3d(0, 10px, 0);
  }

  40%,
  45% {
    opacity: 1;
    -webkit-transform: translate3d(0, -20px, 0);
    transform: translate3d(0, -20px, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(0, 2000px, 0);
    transform: translate3d(0, 2000px, 0);
  }
}

.bounceOutDown {
  -webkit-animation-name: bounceOutDown;
  animation-name: bounceOutDown;
}

@-webkit-keyframes bounceOutLeft {
  20% {
    opacity: 1;
    -webkit-transform: translate3d(20px, 0, 0);
    transform: translate3d(20px, 0, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(-2000px, 0, 0);
    transform: translate3d(-2000px, 0, 0);
  }
}

@keyframes bounceOutLeft {
  20% {
    opacity: 1;
    -webkit-transform: translate3d(20px, 0, 0);
    transform: translate3d(20px, 0, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(-2000px, 0, 0);
    transform: translate3d(-2000px, 0, 0);
  }
}

.bounceOutLeft {
  -webkit-animation-name: bounceOutLeft;
  animation-name: bounceOutLeft;
}

@-webkit-keyframes bounceOutRight {
  20% {
    opacity: 1;
    -webkit-transform: translate3d(-20px, 0, 0);
    transform: translate3d(-20px, 0, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(2000px, 0, 0);
    transform: translate3d(2000px, 0, 0);
  }
}

@keyframes bounceOutRight {
  20% {
    opacity: 1;
    -webkit-transform: translate3d(-20px, 0, 0);
    transform: translate3d(-20px, 0, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(2000px, 0, 0);
    transform: translate3d(2000px, 0, 0);
  }
}

.bounceOutRight {
  -webkit-animation-name: bounceOutRight;
  animation-name: bounceOutRight;
}

@-webkit-keyframes bounceOutUp {
  20% {
    -webkit-transform: translate3d(0, -10px, 0);
    transform: translate3d(0, -10px, 0);
  }

  40%,
  45% {
    opacity: 1;
    -webkit-transform: translate3d(0, 20px, 0);
    transform: translate3d(0, 20px, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(0, -2000px, 0);
    transform: translate3d(0, -2000px, 0);
  }
}

@keyframes bounceOutUp {
  20% {
    -webkit-transform: translate3d(0, -10px, 0);
    transform: translate3d(0, -10px, 0);
  }

  40%,
  45% {
    opacity: 1;
    -webkit-transform: translate3d(0, 20px, 0);
    transform: translate3d(0, 20px, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(0, -2000px, 0);
    transform: translate3d(0, -2000px, 0);
  }
}

.bounceOutUp {
  -webkit-animation-name: bounceOutUp;
  animation-name: bounceOutUp;
}

@-webkit-keyframes fadeIn {
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
}

@keyframes fadeIn {
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
}

.fadeIn {
  -webkit-animation-name: fadeIn;
  animation-name: fadeIn;
}

@-webkit-keyframes fadeInDown {
  from {
    opacity: 0;
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes fadeInDown {
  from {
    opacity: 0;
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.fadeInDown {
  -webkit-animation-name: fadeInDown;
  animation-name: fadeInDown;
}

@-webkit-keyframes fadeInDownBig {
  from {
    opacity: 0;
    -webkit-transform: translate3d(0, -2000px, 0);
    transform: translate3d(0, -2000px, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes fadeInDownBig {
  from {
    opacity: 0;
    -webkit-transform: translate3d(0, -2000px, 0);
    transform: translate3d(0, -2000px, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.fadeInDownBig {
  -webkit-animation-name: fadeInDownBig;
  animation-name: fadeInDownBig;
}

@-webkit-keyframes fadeInLeft {
  from {
    opacity: 0;
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes fadeInLeft {
  from {
    opacity: 0;
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.fadeInLeft {
  -webkit-animation-name: fadeInLeft;
  animation-name: fadeInLeft;
}

@-webkit-keyframes fadeInLeftBig {
  from {
    opacity: 0;
    -webkit-transform: translate3d(-2000px, 0, 0);
    transform: translate3d(-2000px, 0, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes fadeInLeftBig {
  from {
    opacity: 0;
    -webkit-transform: translate3d(-2000px, 0, 0);
    transform: translate3d(-2000px, 0, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.fadeInLeftBig {
  -webkit-animation-name: fadeInLeftBig;
  animation-name: fadeInLeftBig;
}

@-webkit-keyframes fadeInRight {
  from {
    opacity: 0;
    -webkit-transform: translate3d(100%, 0, 0);
    transform: translate3d(100%, 0, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes fadeInRight {
  from {
    opacity: 0;
    -webkit-transform: translate3d(100%, 0, 0);
    transform: translate3d(100%, 0, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.fadeInRight {
  -webkit-animation-name: fadeInRight;
  animation-name: fadeInRight;
}

@-webkit-keyframes fadeInRightBig {
  from {
    opacity: 0;
    -webkit-transform: translate3d(2000px, 0, 0);
    transform: translate3d(2000px, 0, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes fadeInRightBig {
  from {
    opacity: 0;
    -webkit-transform: translate3d(2000px, 0, 0);
    transform: translate3d(2000px, 0, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.fadeInRightBig {
  -webkit-animation-name: fadeInRightBig;
  animation-name: fadeInRightBig;
}

@-webkit-keyframes fadeInUp {
  from {
    opacity: 0;
    -webkit-transform: translate3d(0, 100%, 0);
    transform: translate3d(0, 100%, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes fadeInUp {
  from {
    opacity: 0;
    -webkit-transform: translate3d(0, 100%, 0);
    transform: translate3d(0, 100%, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.fadeInUp {
  -webkit-animation-name: fadeInUp;
  animation-name: fadeInUp;
}

@-webkit-keyframes fadeInUpBig {
  from {
    opacity: 0;
    -webkit-transform: translate3d(0, 2000px, 0);
    transform: translate3d(0, 2000px, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes fadeInUpBig {
  from {
    opacity: 0;
    -webkit-transform: translate3d(0, 2000px, 0);
    transform: translate3d(0, 2000px, 0);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.fadeInUpBig {
  -webkit-animation-name: fadeInUpBig;
  animation-name: fadeInUpBig;
}

@-webkit-keyframes fadeOut {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
  }
}

@keyframes fadeOut {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
  }
}

.fadeOut {
  -webkit-animation-name: fadeOut;
  animation-name: fadeOut;
}

@-webkit-keyframes fadeOutDown {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(0, 100%, 0);
    transform: translate3d(0, 100%, 0);
  }
}

@keyframes fadeOutDown {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(0, 100%, 0);
    transform: translate3d(0, 100%, 0);
  }
}

.fadeOutDown {
  -webkit-animation-name: fadeOutDown;
  animation-name: fadeOutDown;
}

@-webkit-keyframes fadeOutDownBig {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(0, 2000px, 0);
    transform: translate3d(0, 2000px, 0);
  }
}

@keyframes fadeOutDownBig {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(0, 2000px, 0);
    transform: translate3d(0, 2000px, 0);
  }
}

.fadeOutDownBig {
  -webkit-animation-name: fadeOutDownBig;
  animation-name: fadeOutDownBig;
}

@-webkit-keyframes fadeOutLeft {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }
}

@keyframes fadeOutLeft {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }
}

.fadeOutLeft {
  -webkit-animation-name: fadeOutLeft;
  animation-name: fadeOutLeft;
}

@-webkit-keyframes fadeOutLeftBig {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(-2000px, 0, 0);
    transform: translate3d(-2000px, 0, 0);
  }
}

@keyframes fadeOutLeftBig {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(-2000px, 0, 0);
    transform: translate3d(-2000px, 0, 0);
  }
}

.fadeOutLeftBig {
  -webkit-animation-name: fadeOutLeftBig;
  animation-name: fadeOutLeftBig;
}

@-webkit-keyframes fadeOutRight {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(100%, 0, 0);
    transform: translate3d(100%, 0, 0);
  }
}

@keyframes fadeOutRight {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(100%, 0, 0);
    transform: translate3d(100%, 0, 0);
  }
}

.fadeOutRight {
  -webkit-animation-name: fadeOutRight;
  animation-name: fadeOutRight;
}

@-webkit-keyframes fadeOutRightBig {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(2000px, 0, 0);
    transform: translate3d(2000px, 0, 0);
  }
}

@keyframes fadeOutRightBig {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(2000px, 0, 0);
    transform: translate3d(2000px, 0, 0);
  }
}

.fadeOutRightBig {
  -webkit-animation-name: fadeOutRightBig;
  animation-name: fadeOutRightBig;
}

@-webkit-keyframes fadeOutUp {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
  }
}

@keyframes fadeOutUp {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
  }
}

.fadeOutUp {
  -webkit-animation-name: fadeOutUp;
  animation-name: fadeOutUp;
}

@-webkit-keyframes fadeOutUpBig {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(0, -2000px, 0);
    transform: translate3d(0, -2000px, 0);
  }
}

@keyframes fadeOutUpBig {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(0, -2000px, 0);
    transform: translate3d(0, -2000px, 0);
  }
}

.fadeOutUpBig {
  -webkit-animation-name: fadeOutUpBig;
  animation-name: fadeOutUpBig;
}

@-webkit-keyframes flip {
  from {
    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0)
      rotate3d(0, 1, 0, -360deg);
    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0) rotate3d(0, 1, 0, -360deg);
    -webkit-animation-timing-function: ease-out;
    animation-timing-function: ease-out;
  }

  40% {
    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)
      rotate3d(0, 1, 0, -190deg);
    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)
      rotate3d(0, 1, 0, -190deg);
    -webkit-animation-timing-function: ease-out;
    animation-timing-function: ease-out;
  }

  50% {
    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)
      rotate3d(0, 1, 0, -170deg);
    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)
      rotate3d(0, 1, 0, -170deg);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }

  80% {
    -webkit-transform: perspective(400px) scale3d(0.95, 0.95, 0.95) translate3d(0, 0, 0)
      rotate3d(0, 1, 0, 0deg);
    transform: perspective(400px) scale3d(0.95, 0.95, 0.95) translate3d(0, 0, 0)
      rotate3d(0, 1, 0, 0deg);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }

  to {
    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0)
      rotate3d(0, 1, 0, 0deg);
    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0) rotate3d(0, 1, 0, 0deg);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }
}

@keyframes flip {
  from {
    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0)
      rotate3d(0, 1, 0, -360deg);
    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0) rotate3d(0, 1, 0, -360deg);
    -webkit-animation-timing-function: ease-out;
    animation-timing-function: ease-out;
  }

  40% {
    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)
      rotate3d(0, 1, 0, -190deg);
    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)
      rotate3d(0, 1, 0, -190deg);
    -webkit-animation-timing-function: ease-out;
    animation-timing-function: ease-out;
  }

  50% {
    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)
      rotate3d(0, 1, 0, -170deg);
    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 150px)
      rotate3d(0, 1, 0, -170deg);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }

  80% {
    -webkit-transform: perspective(400px) scale3d(0.95, 0.95, 0.95) translate3d(0, 0, 0)
      rotate3d(0, 1, 0, 0deg);
    transform: perspective(400px) scale3d(0.95, 0.95, 0.95) translate3d(0, 0, 0)
      rotate3d(0, 1, 0, 0deg);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }

  to {
    -webkit-transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0)
      rotate3d(0, 1, 0, 0deg);
    transform: perspective(400px) scale3d(1, 1, 1) translate3d(0, 0, 0) rotate3d(0, 1, 0, 0deg);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }
}

.animated.flip {
  -webkit-backface-visibility: visible;
  backface-visibility: visible;
  -webkit-animation-name: flip;
  animation-name: flip;
}

@-webkit-keyframes flipInX {
  from {
    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
    opacity: 0;
  }

  40% {
    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }

  60% {
    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 10deg);
    transform: perspective(400px) rotate3d(1, 0, 0, 10deg);
    opacity: 1;
  }

  80% {
    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -5deg);
    transform: perspective(400px) rotate3d(1, 0, 0, -5deg);
  }

  to {
    -webkit-transform: perspective(400px);
    transform: perspective(400px);
  }
}

@keyframes flipInX {
  from {
    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
    opacity: 0;
  }

  40% {
    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }

  60% {
    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 10deg);
    transform: perspective(400px) rotate3d(1, 0, 0, 10deg);
    opacity: 1;
  }

  80% {
    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -5deg);
    transform: perspective(400px) rotate3d(1, 0, 0, -5deg);
  }

  to {
    -webkit-transform: perspective(400px);
    transform: perspective(400px);
  }
}

.flipInX {
  -webkit-backface-visibility: visible !important;
  backface-visibility: visible !important;
  -webkit-animation-name: flipInX;
  animation-name: flipInX;
}

@-webkit-keyframes flipInY {
  from {
    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);
    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
    opacity: 0;
  }

  40% {
    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -20deg);
    transform: perspective(400px) rotate3d(0, 1, 0, -20deg);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }

  60% {
    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 10deg);
    transform: perspective(400px) rotate3d(0, 1, 0, 10deg);
    opacity: 1;
  }

  80% {
    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -5deg);
    transform: perspective(400px) rotate3d(0, 1, 0, -5deg);
  }

  to {
    -webkit-transform: perspective(400px);
    transform: perspective(400px);
  }
}

@keyframes flipInY {
  from {
    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);
    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
    opacity: 0;
  }

  40% {
    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -20deg);
    transform: perspective(400px) rotate3d(0, 1, 0, -20deg);
    -webkit-animation-timing-function: ease-in;
    animation-timing-function: ease-in;
  }

  60% {
    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 10deg);
    transform: perspective(400px) rotate3d(0, 1, 0, 10deg);
    opacity: 1;
  }

  80% {
    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -5deg);
    transform: perspective(400px) rotate3d(0, 1, 0, -5deg);
  }

  to {
    -webkit-transform: perspective(400px);
    transform: perspective(400px);
  }
}

.flipInY {
  -webkit-backface-visibility: visible !important;
  backface-visibility: visible !important;
  -webkit-animation-name: flipInY;
  animation-name: flipInY;
}

@-webkit-keyframes flipOutX {
  from {
    -webkit-transform: perspective(400px);
    transform: perspective(400px);
  }

  30% {
    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
    opacity: 1;
  }

  to {
    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
    opacity: 0;
  }
}

@keyframes flipOutX {
  from {
    -webkit-transform: perspective(400px);
    transform: perspective(400px);
  }

  30% {
    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
    transform: perspective(400px) rotate3d(1, 0, 0, -20deg);
    opacity: 1;
  }

  to {
    -webkit-transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
    transform: perspective(400px) rotate3d(1, 0, 0, 90deg);
    opacity: 0;
  }
}

.flipOutX {
  -webkit-animation-duration: 0.75s;
  animation-duration: 0.75s;
  -webkit-animation-name: flipOutX;
  animation-name: flipOutX;
  -webkit-backface-visibility: visible !important;
  backface-visibility: visible !important;
}

@-webkit-keyframes flipOutY {
  from {
    -webkit-transform: perspective(400px);
    transform: perspective(400px);
  }

  30% {
    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -15deg);
    transform: perspective(400px) rotate3d(0, 1, 0, -15deg);
    opacity: 1;
  }

  to {
    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);
    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);
    opacity: 0;
  }
}

@keyframes flipOutY {
  from {
    -webkit-transform: perspective(400px);
    transform: perspective(400px);
  }

  30% {
    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, -15deg);
    transform: perspective(400px) rotate3d(0, 1, 0, -15deg);
    opacity: 1;
  }

  to {
    -webkit-transform: perspective(400px) rotate3d(0, 1, 0, 90deg);
    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);
    opacity: 0;
  }
}

.flipOutY {
  -webkit-animation-duration: 0.75s;
  animation-duration: 0.75s;
  -webkit-backface-visibility: visible !important;
  backface-visibility: visible !important;
  -webkit-animation-name: flipOutY;
  animation-name: flipOutY;
}

@-webkit-keyframes lightSpeedIn {
  from {
    -webkit-transform: translate3d(100%, 0, 0) skewX(-30deg);
    transform: translate3d(100%, 0, 0) skewX(-30deg);
    opacity: 0;
  }

  60% {
    -webkit-transform: skewX(20deg);
    transform: skewX(20deg);
    opacity: 1;
  }

  80% {
    -webkit-transform: skewX(-5deg);
    transform: skewX(-5deg);
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes lightSpeedIn {
  from {
    -webkit-transform: translate3d(100%, 0, 0) skewX(-30deg);
    transform: translate3d(100%, 0, 0) skewX(-30deg);
    opacity: 0;
  }

  60% {
    -webkit-transform: skewX(20deg);
    transform: skewX(20deg);
    opacity: 1;
  }

  80% {
    -webkit-transform: skewX(-5deg);
    transform: skewX(-5deg);
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.lightSpeedIn {
  -webkit-animation-name: lightSpeedIn;
  animation-name: lightSpeedIn;
  -webkit-animation-timing-function: ease-out;
  animation-timing-function: ease-out;
}

@-webkit-keyframes lightSpeedOut {
  from {
    opacity: 1;
  }

  to {
    -webkit-transform: translate3d(100%, 0, 0) skewX(30deg);
    transform: translate3d(100%, 0, 0) skewX(30deg);
    opacity: 0;
  }
}

@keyframes lightSpeedOut {
  from {
    opacity: 1;
  }

  to {
    -webkit-transform: translate3d(100%, 0, 0) skewX(30deg);
    transform: translate3d(100%, 0, 0) skewX(30deg);
    opacity: 0;
  }
}

.lightSpeedOut {
  -webkit-animation-name: lightSpeedOut;
  animation-name: lightSpeedOut;
  -webkit-animation-timing-function: ease-in;
  animation-timing-function: ease-in;
}

@-webkit-keyframes rotateIn {
  from {
    -webkit-transform-origin: center;
    transform-origin: center;
    -webkit-transform: rotate3d(0, 0, 1, -200deg);
    transform: rotate3d(0, 0, 1, -200deg);
    opacity: 0;
  }

  to {
    -webkit-transform-origin: center;
    transform-origin: center;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    opacity: 1;
  }
}

@keyframes rotateIn {
  from {
    -webkit-transform-origin: center;
    transform-origin: center;
    -webkit-transform: rotate3d(0, 0, 1, -200deg);
    transform: rotate3d(0, 0, 1, -200deg);
    opacity: 0;
  }

  to {
    -webkit-transform-origin: center;
    transform-origin: center;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    opacity: 1;
  }
}

.rotateIn {
  -webkit-animation-name: rotateIn;
  animation-name: rotateIn;
}

@-webkit-keyframes rotateInDownLeft {
  from {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate3d(0, 0, 1, -45deg);
    transform: rotate3d(0, 0, 1, -45deg);
    opacity: 0;
  }

  to {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    opacity: 1;
  }
}

@keyframes rotateInDownLeft {
  from {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate3d(0, 0, 1, -45deg);
    transform: rotate3d(0, 0, 1, -45deg);
    opacity: 0;
  }

  to {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    opacity: 1;
  }
}

.rotateInDownLeft {
  -webkit-animation-name: rotateInDownLeft;
  animation-name: rotateInDownLeft;
}

@-webkit-keyframes rotateInDownRight {
  from {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate3d(0, 0, 1, 45deg);
    transform: rotate3d(0, 0, 1, 45deg);
    opacity: 0;
  }

  to {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    opacity: 1;
  }
}

@keyframes rotateInDownRight {
  from {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate3d(0, 0, 1, 45deg);
    transform: rotate3d(0, 0, 1, 45deg);
    opacity: 0;
  }

  to {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    opacity: 1;
  }
}

.rotateInDownRight {
  -webkit-animation-name: rotateInDownRight;
  animation-name: rotateInDownRight;
}

@-webkit-keyframes rotateInUpLeft {
  from {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate3d(0, 0, 1, 45deg);
    transform: rotate3d(0, 0, 1, 45deg);
    opacity: 0;
  }

  to {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    opacity: 1;
  }
}

@keyframes rotateInUpLeft {
  from {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate3d(0, 0, 1, 45deg);
    transform: rotate3d(0, 0, 1, 45deg);
    opacity: 0;
  }

  to {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    opacity: 1;
  }
}

.rotateInUpLeft {
  -webkit-animation-name: rotateInUpLeft;
  animation-name: rotateInUpLeft;
}

@-webkit-keyframes rotateInUpRight {
  from {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate3d(0, 0, 1, -90deg);
    transform: rotate3d(0, 0, 1, -90deg);
    opacity: 0;
  }

  to {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    opacity: 1;
  }
}

@keyframes rotateInUpRight {
  from {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate3d(0, 0, 1, -90deg);
    transform: rotate3d(0, 0, 1, -90deg);
    opacity: 0;
  }

  to {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    opacity: 1;
  }
}

.rotateInUpRight {
  -webkit-animation-name: rotateInUpRight;
  animation-name: rotateInUpRight;
}

@-webkit-keyframes rotateOut {
  from {
    -webkit-transform-origin: center;
    transform-origin: center;
    opacity: 1;
  }

  to {
    -webkit-transform-origin: center;
    transform-origin: center;
    -webkit-transform: rotate3d(0, 0, 1, 200deg);
    transform: rotate3d(0, 0, 1, 200deg);
    opacity: 0;
  }
}

@keyframes rotateOut {
  from {
    -webkit-transform-origin: center;
    transform-origin: center;
    opacity: 1;
  }

  to {
    -webkit-transform-origin: center;
    transform-origin: center;
    -webkit-transform: rotate3d(0, 0, 1, 200deg);
    transform: rotate3d(0, 0, 1, 200deg);
    opacity: 0;
  }
}

.rotateOut {
  -webkit-animation-name: rotateOut;
  animation-name: rotateOut;
}

@-webkit-keyframes rotateOutDownLeft {
  from {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    opacity: 1;
  }

  to {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate3d(0, 0, 1, 45deg);
    transform: rotate3d(0, 0, 1, 45deg);
    opacity: 0;
  }
}

@keyframes rotateOutDownLeft {
  from {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    opacity: 1;
  }

  to {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate3d(0, 0, 1, 45deg);
    transform: rotate3d(0, 0, 1, 45deg);
    opacity: 0;
  }
}

.rotateOutDownLeft {
  -webkit-animation-name: rotateOutDownLeft;
  animation-name: rotateOutDownLeft;
}

@-webkit-keyframes rotateOutDownRight {
  from {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    opacity: 1;
  }

  to {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate3d(0, 0, 1, -45deg);
    transform: rotate3d(0, 0, 1, -45deg);
    opacity: 0;
  }
}

@keyframes rotateOutDownRight {
  from {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    opacity: 1;
  }

  to {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate3d(0, 0, 1, -45deg);
    transform: rotate3d(0, 0, 1, -45deg);
    opacity: 0;
  }
}

.rotateOutDownRight {
  -webkit-animation-name: rotateOutDownRight;
  animation-name: rotateOutDownRight;
}

@-webkit-keyframes rotateOutUpLeft {
  from {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    opacity: 1;
  }

  to {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate3d(0, 0, 1, -45deg);
    transform: rotate3d(0, 0, 1, -45deg);
    opacity: 0;
  }
}

@keyframes rotateOutUpLeft {
  from {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    opacity: 1;
  }

  to {
    -webkit-transform-origin: left bottom;
    transform-origin: left bottom;
    -webkit-transform: rotate3d(0, 0, 1, -45deg);
    transform: rotate3d(0, 0, 1, -45deg);
    opacity: 0;
  }
}

.rotateOutUpLeft {
  -webkit-animation-name: rotateOutUpLeft;
  animation-name: rotateOutUpLeft;
}

@-webkit-keyframes rotateOutUpRight {
  from {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    opacity: 1;
  }

  to {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate3d(0, 0, 1, 90deg);
    transform: rotate3d(0, 0, 1, 90deg);
    opacity: 0;
  }
}

@keyframes rotateOutUpRight {
  from {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    opacity: 1;
  }

  to {
    -webkit-transform-origin: right bottom;
    transform-origin: right bottom;
    -webkit-transform: rotate3d(0, 0, 1, 90deg);
    transform: rotate3d(0, 0, 1, 90deg);
    opacity: 0;
  }
}

.rotateOutUpRight {
  -webkit-animation-name: rotateOutUpRight;
  animation-name: rotateOutUpRight;
}

@-webkit-keyframes hinge {
  0% {
    -webkit-transform-origin: top left;
    transform-origin: top left;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
  }

  20%,
  60% {
    -webkit-transform: rotate3d(0, 0, 1, 80deg);
    transform: rotate3d(0, 0, 1, 80deg);
    -webkit-transform-origin: top left;
    transform-origin: top left;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
  }

  40%,
  80% {
    -webkit-transform: rotate3d(0, 0, 1, 60deg);
    transform: rotate3d(0, 0, 1, 60deg);
    -webkit-transform-origin: top left;
    transform-origin: top left;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
    opacity: 1;
  }

  to {
    -webkit-transform: translate3d(0, 700px, 0);
    transform: translate3d(0, 700px, 0);
    opacity: 0;
  }
}

@keyframes hinge {
  0% {
    -webkit-transform-origin: top left;
    transform-origin: top left;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
  }

  20%,
  60% {
    -webkit-transform: rotate3d(0, 0, 1, 80deg);
    transform: rotate3d(0, 0, 1, 80deg);
    -webkit-transform-origin: top left;
    transform-origin: top left;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
  }

  40%,
  80% {
    -webkit-transform: rotate3d(0, 0, 1, 60deg);
    transform: rotate3d(0, 0, 1, 60deg);
    -webkit-transform-origin: top left;
    transform-origin: top left;
    -webkit-animation-timing-function: ease-in-out;
    animation-timing-function: ease-in-out;
    opacity: 1;
  }

  to {
    -webkit-transform: translate3d(0, 700px, 0);
    transform: translate3d(0, 700px, 0);
    opacity: 0;
  }
}

.hinge {
  -webkit-animation-duration: 2s;
  animation-duration: 2s;
  -webkit-animation-name: hinge;
  animation-name: hinge;
}

@-webkit-keyframes jackInTheBox {
  from {
    opacity: 0;
    -webkit-transform: scale(0.1) rotate(30deg);
    transform: scale(0.1) rotate(30deg);
    -webkit-transform-origin: center bottom;
    transform-origin: center bottom;
  }

  50% {
    -webkit-transform: rotate(-10deg);
    transform: rotate(-10deg);
  }

  70% {
    -webkit-transform: rotate(3deg);
    transform: rotate(3deg);
  }

  to {
    opacity: 1;
    -webkit-transform: scale(1);
    transform: scale(1);
  }
}

@keyframes jackInTheBox {
  from {
    opacity: 0;
    -webkit-transform: scale(0.1) rotate(30deg);
    transform: scale(0.1) rotate(30deg);
    -webkit-transform-origin: center bottom;
    transform-origin: center bottom;
  }

  50% {
    -webkit-transform: rotate(-10deg);
    transform: rotate(-10deg);
  }

  70% {
    -webkit-transform: rotate(3deg);
    transform: rotate(3deg);
  }

  to {
    opacity: 1;
    -webkit-transform: scale(1);
    transform: scale(1);
  }
}

.jackInTheBox {
  -webkit-animation-name: jackInTheBox;
  animation-name: jackInTheBox;
}

/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */

@-webkit-keyframes rollIn {
  from {
    opacity: 0;
    -webkit-transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);
    transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes rollIn {
  from {
    opacity: 0;
    -webkit-transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);
    transform: translate3d(-100%, 0, 0) rotate3d(0, 0, 1, -120deg);
  }

  to {
    opacity: 1;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.rollIn {
  -webkit-animation-name: rollIn;
  animation-name: rollIn;
}

/* originally authored by Nick Pettit - https://github.com/nickpettit/glide */

@-webkit-keyframes rollOut {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);
    transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);
  }
}

@keyframes rollOut {
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
    -webkit-transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);
    transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);
  }
}

.rollOut {
  -webkit-animation-name: rollOut;
  animation-name: rollOut;
}

@-webkit-keyframes zoomIn {
  from {
    opacity: 0;
    -webkit-transform: scale3d(0.3, 0.3, 0.3);
    transform: scale3d(0.3, 0.3, 0.3);
  }

  50% {
    opacity: 1;
  }
}

@keyframes zoomIn {
  from {
    opacity: 0;
    -webkit-transform: scale3d(0.3, 0.3, 0.3);
    transform: scale3d(0.3, 0.3, 0.3);
  }

  50% {
    opacity: 1;
  }
}

.zoomIn {
  -webkit-animation-name: zoomIn;
  animation-name: zoomIn;
}

@-webkit-keyframes zoomInDown {
  from {
    opacity: 0;
    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);
    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
  }
}

@keyframes zoomInDown {
  from {
    opacity: 0;
    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);
    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -1000px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
  }
}

.zoomInDown {
  -webkit-animation-name: zoomInDown;
  animation-name: zoomInDown;
}

@-webkit-keyframes zoomInLeft {
  from {
    opacity: 0;
    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);
    transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
  }
}

@keyframes zoomInLeft {
  from {
    opacity: 0;
    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);
    transform: scale3d(0.1, 0.1, 0.1) translate3d(-1000px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(10px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
  }
}

.zoomInLeft {
  -webkit-animation-name: zoomInLeft;
  animation-name: zoomInLeft;
}

@-webkit-keyframes zoomInRight {
  from {
    opacity: 0;
    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);
    transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
  }
}

@keyframes zoomInRight {
  from {
    opacity: 0;
    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);
    transform: scale3d(0.1, 0.1, 0.1) translate3d(1000px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(-10px, 0, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
  }
}

.zoomInRight {
  -webkit-animation-name: zoomInRight;
  animation-name: zoomInRight;
}

@-webkit-keyframes zoomInUp {
  from {
    opacity: 0;
    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);
    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
  }
}

@keyframes zoomInUp {
  from {
    opacity: 0;
    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);
    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 1000px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }

  60% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
  }
}

.zoomInUp {
  -webkit-animation-name: zoomInUp;
  animation-name: zoomInUp;
}

@-webkit-keyframes zoomOut {
  from {
    opacity: 1;
  }

  50% {
    opacity: 0;
    -webkit-transform: scale3d(0.3, 0.3, 0.3);
    transform: scale3d(0.3, 0.3, 0.3);
  }

  to {
    opacity: 0;
  }
}

@keyframes zoomOut {
  from {
    opacity: 1;
  }

  50% {
    opacity: 0;
    -webkit-transform: scale3d(0.3, 0.3, 0.3);
    transform: scale3d(0.3, 0.3, 0.3);
  }

  to {
    opacity: 0;
  }
}

.zoomOut {
  -webkit-animation-name: zoomOut;
  animation-name: zoomOut;
}

@-webkit-keyframes zoomOutDown {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }

  to {
    opacity: 0;
    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);
    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);
    -webkit-transform-origin: center bottom;
    transform-origin: center bottom;
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
  }
}

@keyframes zoomOutDown {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, -60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }

  to {
    opacity: 0;
    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);
    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, 2000px, 0);
    -webkit-transform-origin: center bottom;
    transform-origin: center bottom;
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
  }
}

.zoomOutDown {
  -webkit-animation-name: zoomOutDown;
  animation-name: zoomOutDown;
}

@-webkit-keyframes zoomOutLeft {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: scale(0.1) translate3d(-2000px, 0, 0);
    transform: scale(0.1) translate3d(-2000px, 0, 0);
    -webkit-transform-origin: left center;
    transform-origin: left center;
  }
}

@keyframes zoomOutLeft {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(42px, 0, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: scale(0.1) translate3d(-2000px, 0, 0);
    transform: scale(0.1) translate3d(-2000px, 0, 0);
    -webkit-transform-origin: left center;
    transform-origin: left center;
  }
}

.zoomOutLeft {
  -webkit-animation-name: zoomOutLeft;
  animation-name: zoomOutLeft;
}

@-webkit-keyframes zoomOutRight {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: scale(0.1) translate3d(2000px, 0, 0);
    transform: scale(0.1) translate3d(2000px, 0, 0);
    -webkit-transform-origin: right center;
    transform-origin: right center;
  }
}

@keyframes zoomOutRight {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(-42px, 0, 0);
  }

  to {
    opacity: 0;
    -webkit-transform: scale(0.1) translate3d(2000px, 0, 0);
    transform: scale(0.1) translate3d(2000px, 0, 0);
    -webkit-transform-origin: right center;
    transform-origin: right center;
  }
}

.zoomOutRight {
  -webkit-animation-name: zoomOutRight;
  animation-name: zoomOutRight;
}

@-webkit-keyframes zoomOutUp {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }

  to {
    opacity: 0;
    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);
    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);
    -webkit-transform-origin: center bottom;
    transform-origin: center bottom;
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
  }
}

@keyframes zoomOutUp {
  40% {
    opacity: 1;
    -webkit-transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);
    transform: scale3d(0.475, 0.475, 0.475) translate3d(0, 60px, 0);
    -webkit-animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }

  to {
    opacity: 0;
    -webkit-transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);
    transform: scale3d(0.1, 0.1, 0.1) translate3d(0, -2000px, 0);
    -webkit-transform-origin: center bottom;
    transform-origin: center bottom;
    -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
    animation-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1);
  }
}

.zoomOutUp {
  -webkit-animation-name: zoomOutUp;
  animation-name: zoomOutUp;
}

@-webkit-keyframes slideInDown {
  from {
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
    visibility: visible;
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes slideInDown {
  from {
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
    visibility: visible;
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.slideInDown {
  -webkit-animation-name: slideInDown;
  animation-name: slideInDown;
}

@-webkit-keyframes slideInLeft {
  from {
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
    visibility: visible;
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes slideInLeft {
  from {
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
    visibility: visible;
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.slideInLeft {
  -webkit-animation-name: slideInLeft;
  animation-name: slideInLeft;
}

@-webkit-keyframes slideInRight {
  from {
    -webkit-transform: translate3d(100%, 0, 0);
    transform: translate3d(100%, 0, 0);
    visibility: visible;
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes slideInRight {
  from {
    -webkit-transform: translate3d(100%, 0, 0);
    transform: translate3d(100%, 0, 0);
    visibility: visible;
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.slideInRight {
  -webkit-animation-name: slideInRight;
  animation-name: slideInRight;
}

@-webkit-keyframes slideInUp {
  from {
    -webkit-transform: translate3d(0, 100%, 0);
    transform: translate3d(0, 100%, 0);
    visibility: visible;
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes slideInUp {
  from {
    -webkit-transform: translate3d(0, 100%, 0);
    transform: translate3d(0, 100%, 0);
    visibility: visible;
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.slideInUp {
  -webkit-animation-name: slideInUp;
  animation-name: slideInUp;
}

@-webkit-keyframes slideOutDown {
  from {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  to {
    visibility: hidden;
    -webkit-transform: translate3d(0, 100%, 0);
    transform: translate3d(0, 100%, 0);
  }
}

@keyframes slideOutDown {
  from {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  to {
    visibility: hidden;
    -webkit-transform: translate3d(0, 100%, 0);
    transform: translate3d(0, 100%, 0);
  }
}

.slideOutDown {
  -webkit-animation-name: slideOutDown;
  animation-name: slideOutDown;
}

@-webkit-keyframes slideOutLeft {
  from {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  to {
    visibility: hidden;
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }
}

@keyframes slideOutLeft {
  from {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  to {
    visibility: hidden;
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
  }
}

.slideOutLeft {
  -webkit-animation-name: slideOutLeft;
  animation-name: slideOutLeft;
}

@-webkit-keyframes slideOutRight {
  from {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  to {
    visibility: hidden;
    -webkit-transform: translate3d(100%, 0, 0);
    transform: translate3d(100%, 0, 0);
  }
}

@keyframes slideOutRight {
  from {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  to {
    visibility: hidden;
    -webkit-transform: translate3d(100%, 0, 0);
    transform: translate3d(100%, 0, 0);
  }
}

.slideOutRight {
  -webkit-animation-name: slideOutRight;
  animation-name: slideOutRight;
}

@-webkit-keyframes slideOutUp {
  from {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  to {
    visibility: hidden;
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
  }
}

@keyframes slideOutUp {
  from {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  to {
    visibility: hidden;
    -webkit-transform: translate3d(0, -100%, 0);
    transform: translate3d(0, -100%, 0);
  }
}

.slideOutUp {
  -webkit-animation-name: slideOutUp;
  animation-name: slideOutUp;
}

.animated {
  -webkit-animation-duration: 1s;
  animation-duration: 1s;
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;
}

.animated.infinite {
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
}

.animated.delay-1s {
  -webkit-animation-delay: 1s;
  animation-delay: 1s;
}

.animated.delay-2s {
  -webkit-animation-delay: 2s;
  animation-delay: 2s;
}

.animated.delay-3s {
  -webkit-animation-delay: 3s;
  animation-delay: 3s;
}

.animated.delay-4s {
  -webkit-animation-delay: 4s;
  animation-delay: 4s;
}

.animated.delay-5s {
  -webkit-animation-delay: 5s;
  animation-delay: 5s;
}

.animated.fast {
  -webkit-animation-duration: 800ms;
  animation-duration: 800ms;
}

.animated.faster {
  -webkit-animation-duration: 500ms;
  animation-duration: 500ms;
}

.animated.slow {
  -webkit-animation-duration: 2s;
  animation-duration: 2s;
}

.animated.slower {
  -webkit-animation-duration: 3s;
  animation-duration: 3s;
}

@media (print), (prefers-reduced-motion) {
  .animated {
    -webkit-animation: unset !important;
    animation: unset !important;
    -webkit-transition: none !important;
    transition: none !important;
  }
}



*{box-sizing:border-box;font-family:Oxygen,Helvetica Neue,sans-serif}body{margin:0;padding:0;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;font-weight:400}#root,body{width:100%;height:100%}#root{background:rgba(53,53,53,.92);border:1px solid hsla(0,0%,59.2%,.228431);box-sizing:border-box;-webkit-backdrop-filter:blur(13px);backdrop-filter:blur(13px);position:fixed;overflow-y:scroll;top:0;left:0}.clearfix{clear:both;height:0}button,input,option,select{border:none;outline:none;background-color:#fff}option,select{-webkit-appearance:none;-moz-appearance:none}select::-ms-expand{display:none}option{padding:8px 0}.monnify-input{background:#fff;border:1px solid hsla(0,0%,59.2%,.261662);box-sizing:border-box;border-radius:3px;padding:8px 15px;font-weight:400;cursor:pointer;transition-property:all;transition-duration:.25s;transition-timing-function:cubic-bezier(.645,.045,.355,1)}@media screen and (max-width:737px){.monnify-input{padding-left:15px;padding-right:15px}}.monnify-input .monnify-input-label{font-size:12px;color:rgba(74,74,74,.2934);letter-spacing:.0126316em;line-height:16px;display:block;width:100%;text-align:left;cursor:pointer;margin-bottom:4px}.monnify-input.invalid{border-color:#f03}.monnify-input.focus{border-color:#00b8c2}.monnify-input.focus .monnify-input-label{color:#00b8c2}.monnify-input input,.monnify-input select{font-size:16px;letter-spacing:.0126316em;color:#4a4a4a;width:100%;display:block;line-height:20px;font-weight:600}.monnify-input input:-ms-input-placeholder,.monnify-input select:-ms-input-placeholder{color:rgba(74,74,74,.15)}.monnify-input input::-ms-input-placeholder,.monnify-input select::-ms-input-placeholder{color:rgba(74,74,74,.15)}.monnify-input input::-webkit-input-placeholder,.monnify-input select::-webkit-input-placeholder{color:rgba(74,74,74,.15)}.monnify-input input::placeholder,.monnify-input select::placeholder{color:rgba(74,74,74,.15)}.monnify-input input::-moz-placeholder,.monnify-input input::placeholder,.monnify-input select::-moz-placeholder,.monnify-input select::placeholder{opacity:1}.gray-button,.primary-button,.secondary-button,.tertiary-button{font-size:15px;text-align:center;letter-spacing:.000909091em;color:#fff;font-weight:600;border-radius:4px;width:100%;padding:8px 15px;line-height:34px;border:none;cursor:pointer;vertical-align:middle}.primary-button{background:linear-gradient(180deg,#005f7d 34.12%,#00b8c2 140.29%)}.secondary-button{background:linear-gradient(175.24deg,#f0aa22 -8.91%,#f05822 99.52%)}.tertiary-button{background-color:linear-gradient(#005e81,#2cdae5)}.gray-button{background:linear-gradient(175.24deg,#afafaf -8.91%,#bfbfbf 99.52%)}.overflow-ellipsis{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.italic{font-style:italic}.hide{display:none;visibility:hidden}.show{visibility:visible}.alert,.show{display:block}.alert{border:none;width:100%;padding:10px;text-align:center;border-radius:5px;font-weight:13px;line-height:16px;font-weight:600;margin:20px auto}.alert.error{background:rgba(235,87,87,.1);color:#eb5757}body{font-family:Oxygen,Helvetica Neue,sans-serif;font-size:12px}.monnify-app{width:370px;max-width:100%;margin:50px auto;overflow:hidden}@media screen and (max-width:737px){.monnify-app{padding:15px}}.monnify-app .monnify-main-content{background-color:#fff;border-radius:6px}.monnify-app .monnify-main-content .content{min-height:200px;border-radius:6px;position:relative;background-color:#fff}.monnify-footer{padding:20px 0;text-align:center;font-size:14px;line-height:18px;letter-spacing:.0126316em;color:#fff;background:transparent;font-weight:600}@media screen and (max-width:737px){.monnify-footer{font-size:13px}}.monnify-footer .monnify-logo svg{vertical-align:middle}.monnify-header{color:#fff;width:100%;font-size:13px;background:transparent;clear:both;display:block;font-weight:600;padding-bottom:4px;margin-top:20px}.monnify-header .left{float:left;clear:left;width:50%;line-height:24px}.monnify-header .left a{cursor:pointer}.monnify-header .left a,.monnify-header .left a:active,.monnify-header .left a:focus,.monnify-header .left a:hover,.monnify-header .left a:visited{color:#fff;text-decoration:none}.monnify-header .left a svg{margin-right:10px;vertical-align:middle}.monnify-header .right{float:right;clear:right;text-align:right;width:40%;line-height:24px}.monnify-user-card{font-size:13px;color:#fff;height:24px}.monnify-user-card .user-identity{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;max-width:calc(100% - 50px);width:calc(100% - 50px);display:inline-block;line-height:24px}.monnify-user-card .user-avatar{float:right;margin-left:7px;display:inline-block;width:24px;height:24px;border-radius:24px;background-size:contain;background:no-repeat 50%}.monnify-payment-data-bar{background:#00b8c2;background-image:url(/images/bg1.png);background-image:url(/images/bg1.png),linear-gradient(180deg,#005f7d 34.12%,#00b8c2 140.29%);font-size:12px;line-height:24px;text-align:center;color:#fff;padding-top:30px;border-radius:6px 6px 0 0;font-weight:500;position:relative;margin-bottom:-6px}.monnify-payment-data-bar .ribbon{font-size:9px;font-weight:600;width:85px;position:absolute;background:#fba019;text-transform:uppercase;color:#0a2240;text-align:center;padding:0 16px;box-shadow:1px 0 rgba(3,41,59,.38);left:-10px;line-height:24px;top:25px}.monnify-payment-data-bar .ribbon .ribbon-content:before{content:"";position:absolute;display:block;border-style:solid;border-color:#b06f0d transparent transparent;bottom:-10px;left:0;border-width:10px 0 0 10px}.monnify-payment-data-bar .ribbon:before{background:#fba019 none repeat scroll 0 0;content:"";position:absolute;right:-5px;transform:skew(-20deg);width:10px;height:24px}.monnify-payment-data-bar .merchant-logo-section{z-index:5;position:absolute;width:60px;height:60px;margin-top:-64px;left:50%;margin-left:-30px;border-radius:50%;border:5px solid #005f7d;background-color:#fff;padding:5px}.monnify-payment-data-bar .merchant-logo-section span{display:block;width:100%;height:100%;background:no-repeat 50%;background-size:contain}.monnify-payment-data-bar .total-payable-section{position:relative;padding-bottom:25px}.monnify-payment-data-bar .total-payable-section label{margin:0;padding:0}.monnify-payment-data-bar .total-payable-section .amount{font-size:30px;font-weight:600}.monnify-payment-data-bar .amount-fee-section{display:flex;justify-content:space-between;background:rgba(0,0,0,.4);height:54px;padding:10px;font-size:13px;align-content:center}.monnify-payment-data-bar .amount-fee-section div{display:flex;padding:2px}.monnify-payment-data-bar .amount-fee-section .amount{font-weight:600}.monnify-payment-data-bar .merchant-section{margin-top:17px}.monnify-payment-data-bar .merchant-section label{margin:0;padding:0;font-weight:400}.monnify-payment-data-bar .merchant-section .merchant-name{font-size:16px;font-weight:600;line-height:16px}.monnify-flow{overflow:hidden}@media screen and (max-width:737px){.monnify-flow{border-radius:0 0 6px 6px}}.monnify-flow .not-receipt-cont{position:relative;height:100%;width:100%;top:0;left:0}.monnify-flow .monnify-flow-container{background-color:#f7f7f7;border-radius:0 0 6px 6px}.monnify-flow .overlay-loader{position:absolute;z-index:999;width:100%;height:100%;top:0;left:0;background-color:hsla(0,0%,100%,.9);border-radius:6px;text-align:center}.monnify-flow .overlay-loader .loader-content{top:50%;position:relative;margin-top:-60px}.monnify-flow .overlay-loader .loader-content .icon{background:#fff;box-shadow:0 9px 14px rgba(0,0,0,.11);border-radius:5px;text-align:center;width:60px;height:60px;padding:13px;margin:0 auto}.monnify-flow .overlay-loader .loader-content .icon:before{content:"";width:28px;height:28px;display:inline-block;border:3px solid #fba019;border-left-color:transparent;border-radius:50%;transition-delay:.5s;transition-duration:1s;-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-animation-name:rotate;animation-name:rotate;-webkit-animation-timing-function:linear;animation-timing-function:linear}.monnify-flow .overlay-loader .loader-content .text{font-size:12px;line-height:30px;color:#4f4f4f;font-weight:600}@-webkit-keyframes rotate{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}@keyframes rotate{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}.monnify-bank-transfer-flow{background:#f5f5f5;border-radius:0 0 6px 6px;padding:20px}.monnify-bank-transfer-flow .disable-btn{opacity:.5;cursor:not-allowed}@media screen and (max-width:737px){.monnify-bank-transfer-flow{margin-top:13px}}.monnify-bank-transfer-flow .loading{text-align:center}.monnify-bank-transfer-flow .loading .monnify-inline-loader{margin:40px auto}.monnify-bank-transfer-flow .loaded{position:relative}.monnify-bank-transfer-flow .instruction{font-size:13px;line-height:20px;text-align:center;letter-spacing:.0126316em;color:#4f4f4f;font-weight:500;width:90%;margin:0 auto 20px}.monnify-bank-transfer-flow .label{font-size:12px;letter-spacing:.0126316em;color:#828282;font-weight:500}.monnify-bank-transfer-flow .info{position:absolute;background:#fff;top:87.8%;left:0;box-shadow:0 4px 12px rgba(0,0,0,.15);display:flex;align-items:center;padding:20px;width:100%;border-radius:10px;height:50px;opacity:0;z-index:1}.monnify-bank-transfer-flow .info.animate{-webkit-animation:easeIn .5s ease-in forwards,easeOut .5s ease-out 3s forwards;animation:easeIn .5s ease-in forwards,easeOut .5s ease-out 3s forwards}.monnify-bank-transfer-flow .info .info-icon{margin-top:5px;margin-right:10px;color:#00b8c2}.monnify-bank-transfer-flow .value{font-size:14px;letter-spacing:.0126316em;color:#4f4f4f;font-weight:600;margin-top:4px}.monnify-bank-transfer-flow .action{margin-top:20px;z-index:3;position:relative}.monnify-bank-transfer-flow .error-cont{text-align:center;padding:0 10px 40px}.monnify-bank-transfer-flow .error-cont .icon{margin:10px auto 20px}.monnify-bank-transfer-flow .error-cont .message{color:#828282;font-size:14px;font-weight:500;line-height:28px}@-webkit-keyframes easeIn{0%{top:87.8%;opacity:0}to{top:72.8%;opacity:1}}@keyframes easeIn{0%{top:87.8%;opacity:0}to{top:72.8%;opacity:1}}@-webkit-keyframes easeOut{0%{top:72.8%;opacity:1}to{top:87.8%;opacity:0}}@keyframes easeOut{0%{top:72.8%;opacity:1}to{top:87.8%;opacity:0}}@-webkit-keyframes popOut{0%{top:87.8%;opacity:0}25%{top:72.8%;opacity:1}80%{top:72.8%;opacity:1}}@keyframes popOut{0%{top:87.8%;opacity:0}25%{top:72.8%;opacity:1}80%{top:72.8%;opacity:1}}@media (min-width:480px){@-webkit-keyframes popOut{0%{top:87.8%;opacity:0}25%{top:72.8%;opacity:1}80%{top:72.8%;opacity:1}}@keyframes popOut{0%{top:87.8%;opacity:0}25%{top:72.8%;opacity:1}80%{top:72.8%;opacity:1}}}.monnify-inline-loader .lds-ripple{display:inline-block;position:relative;width:64px;height:64px}.monnify-inline-loader .lds-ripple div{position:absolute;border:4px solid #00b8c2;opacity:1;border-radius:50%;-webkit-animation:lds-ripple 1s cubic-bezier(0,.2,.8,1) infinite;animation:lds-ripple 1s cubic-bezier(0,.2,.8,1) infinite}.monnify-inline-loader .lds-ripple div:nth-child(2){-webkit-animation-delay:-.5s;animation-delay:-.5s}@-webkit-keyframes lds-ripple{0%{top:28px;left:28px;width:0;height:0;opacity:1}to{top:-1px;left:-1px;width:58px;height:58px;opacity:0}}@keyframes lds-ripple{0%{top:28px;left:28px;width:0;height:0;opacity:1}to{top:-1px;left:-1px;width:58px;height:58px;opacity:0}}.busy-loader{z-index:1;width:24px;height:24px;border-radius:50%;border:2px solid rgba(0,0,0,.15);border-top-color:#3498db;-webkit-animation:spin 1s linear infinite;animation:spin 1s linear infinite;display:inline-block;vertical-align:middle;margin-right:5px}.gray-button .busy-loader,.secondary-button .busy-loader{border-top-color:#00b8c2}.tertiary-button .busy-loader{border-top-color:#fba019}@-webkit-keyframes spin{0%{-webkit-transform:rotate(0deg)}to{-webkit-transform:rotate(1turn)}}@keyframes spin{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}.monnify-timer .timer{position:relative;width:1em;height:1em;font-size:18px;float:left;display:inline-block;background-color:rgba(124,164,170,.2);border-radius:50%}.monnify-timer .timer>.percent{position:absolute;top:1.05em;left:0;width:3.33em;font-size:.3em;text-align:center}.monnify-timer .timer>#slice{position:absolute;width:1em;height:1em;clip:rect(0,1em,1em,.5em)}.monnify-timer .timer>#slice.gt50{clip:rect(auto,auto,auto,auto)}.monnify-timer .timer>#slice>.pie{border:.1em solid rgba(124,164,170,.2);position:absolute;width:.8em;height:.8em;clip:rect(0,.5em,1em,0);border-radius:.5em}.monnify-timer .timer>#slice>.pie.fill{transform:rotate(180deg)!important}.monnify-timer .timer.fill>.percent{display:none}.monnify-timer .timer.fill>#slice>.pie{border:transparent;background-color:#fba019;width:1em;height:1em}.monnify-timer .timer-text{font-size:13px;line-height:16px;letter-spacing:.0126316em;color:#828282;float:left;font-weight:600;margin-left:7px}.monnify-select{display:block}.monnify-select .monnify-input-label{margin-bottom:5px}.monnify-select select{width:100%;font-size:15px;background-origin:content-box;background-size:contain;background:100% no-repeat url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAG1BMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACUUeIgAAAACXRSTlMABAkOHCEvR0wWYdaEAAAANklEQVR42q2LQQoAIAzD5tSt/3+xYIcFz8upCdSamN9YCI7AZk8EPd1euc5LlXKVchW68GF9HIfWAPZBMX0UAAAAAElFTkSuQmCC)}.bank-transfer__ussd{padding:20px 20px 0}.bank-transfer__ussd .monnify-input .monnify-input-label{font-size:11px}.bank-transfer__ussd .error-loading-banks{color:#f03;line-height:20px}.bank-transfer__ussd .error-loading-banks a{cursor:pointer}.bank-transfer__ussd .error-loading-banks a:hover{border-bottom:1px dotted #828282}.bank-transfer__ussd .title{font-weight:600;font-size:13px;line-height:19px;letter-spacing:.0126316em;color:#00b8c2;margin-bottom:10px}.bank-transfer__ussd .bank-selector-cont{margin-bottom:20px}.bank-transfer__ussd .details-cont .message{font-weight:600;font-size:12px;line-height:18px;letter-spacing:.0126316em;color:#4f4f4f}.bank-transfer__ussd .details-cont .code{color:#00b8c2;letter-spacing:.06em;font-weight:600;font-size:17px;line-height:25px;margin-bottom:5px;margin-top:4px}.bank-transfer__ussd .details-cont a{display:block;padding:20px;line-height:18px;color:#00b8c2;font-weight:600;font-size:13px;letter-spacing:.0126316em;text-align:center;cursor:pointer}.bank-transfer__ussd .details-cont a svg{vertical-align:bottom}.merchant-account-cont{background:#fff;border:1px solid hsla(0,0%,59.2%,.261662);border-radius:4px;box-sizing:border-box}.merchant-account-cont .part-trigger{color:#00b8c2;font-weight:600;font-size:13px;line-height:19px;padding:15px 20px;text-align:center;cursor:pointer}.merchant-account-cont .ussd-part{border-top:1px solid rgba(0,0,0,.05)}.merchant-account-cont .ib-part .account-details-section{padding:20px;box-sizing:border-box}.merchant-account-cont .ib-part .account-details-section .title-bar{padding-left:3px}.merchant-account-cont .ib-part .account-details-section .timer-cont{float:right}.merchant-account-cont .ib-part .account-details-section .account-number{font-size:26px;line-height:32px;letter-spacing:.06em;color:#00b8c2;font-weight:600}.merchant-account-cont .ib-part .account-bank-section{margin-top:20px}.merchant-account-cont .ib-part .account-bank-section .account-name,.merchant-account-cont .ib-part .account-bank-section .bank{margin-bottom:20px}.merchant-account-cont .ib-part .account-bank-section .account-name .label,.merchant-account-cont .ib-part .account-bank-section .bank .label{line-height:18px}@media screen and (max-width:767px){.monnify-flow-selector{border-radius:6px}}.monnify-flow-selector .selector{padding:20px 20px 0;border-radius:6px 6px 0 0}@media screen and (max-width:767px){.monnify-flow-selector .selector{padding:11px 0 0}}.monnify-flow-selector .selector .icon-section{float:left;display:inline-block;text-align:center;padding:8px;color:#00b8c2;background-color:rgba(0,184,194,.1);border-radius:38px}.monnify-flow-selector .selector .icon-section svg{height:22px;width:22px;vertical-align:middle;position:relative}.monnify-flow-selector .selector .icon-section.PayWithCard svg{top:4px}.monnify-flow-selector .selector .icon-section.PayWithQR svg,.monnify-flow-selector .selector .icon-section.PayWithUSSD svg{top:3px;left:3px}.monnify-flow-selector .selector .flow-name{font-size:15px;color:#333;font-weight:600}.monnify-flow-selector .selector label{font-size:15px;line-height:24px;color:#30424c;font-weight:600;margin-bottom:20px;display:block}@media screen and (max-width:737px){.monnify-flow-selector .selector label{font-size:12px;color:rgba(48,66,76,.5);padding:0 20px;margin-bottom:0}}.monnify-flow-selector .selector .flow-group{background:#fff;box-sizing:border-box;border-radius:5px}.monnify-flow-selector .selector .flow-group a.flow{display:block;padding:20px 5px;border-bottom:1px solid rgba(0,0,0,.1);cursor:pointer}@media screen and (max-width:737px){.monnify-flow-selector .selector .flow-group a.flow{padding:20px}}.monnify-flow-selector .selector .flow-group a.flow:hover{background-color:rgba(0,184,194,.07)}.monnify-flow-selector .selector .flow-group a.flow:last-of-type{border-bottom:none}.monnify-flow-selector .selector .flow-group a.flow .text-section{float:left;margin-left:11px;width:calc(100% - 70px)}.monnify-flow-selector .selector .flow-group a.flow .text-section .flow-name{margin-bottom:5px}.monnify-flow-selector .selector .flow-group a.flow .text-section .description{font-size:12px;letter-spacing:.0126316em;color:#bdbdbd}.monnify-flow-selector .selector .flow-group a.flow .text-section:after{content:"";width:26px;height:26px;border:1px solid hsla(0,0%,59.2%,.492357);border-radius:26px;float:right;position:relative;top:-20px;display:none}.monnify-flow-selector .selector .flow-group a.flow .nav-icon{width:20px;display:inline-block;text-align:center;float:left;padding-top:14px}.monnify-flow-selector .selector .flow-group .filler{min-height:60px;display:block}.monnify-flow-selector .selected-flow{background:#fff;box-sizing:border-box;padding:12px 22px;border-bottom:1px solid rgba(0,0,0,.07);border-radius:6px 6px 0 0}@media screen and (max-width:737px){.monnify-flow-selector .selected-flow{padding:10px 20px;box-shadow:none;border:none}}.monnify-flow-selector .selected-flow .flow-name{margin-left:11px;float:left;line-height:38px;font-size:14px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.monnify-flow-selector .selected-flow .change-link{font-size:14px;letter-spacing:.0126316em;font-weight:600;cursor:pointer;line-height:38px;color:#30424c}.monnify-flow-selector .selected-flow .change-link span{color:#00b8c2;margin-right:9px}.monnify-qr-flow{text-align:center;padding:20px}@media screen and (max-width:737px){.monnify-qr-flow{margin-top:13px}}.monnify-qr-flow .loading{padding-top:50px;padding-bottom:50px}.monnify-qr-flow .loaded .instruction{font-size:13px;line-height:19px;text-align:center;letter-spacing:.0126316em;color:#828282;width:80%;font-weight:500;margin:0 auto}.monnify-qr-flow .loaded .data{text-align:center;margin:20px auto;min-height:150px}.monnify-qr-flow .loaded .data img{width:149px;margin:0 auto;box-shadow:0 6px 11px rgba(0,0,0,.09);border-radius:5px}.monnify-qr-flow .loaded .data .partners{text-align:center;margin:25px auto}.monnify-qr-flow .loaded .data .action{margin-top:30px}.monnify-card-flow{padding:20px;background-color:#f7f7f7}@media screen and (max-width:737px){.monnify-card-flow{margin-top:13px;padding-left:10px;padding-right:10px}}.monnify-card-flow .input-section{margin-bottom:17px}.monnify-card-flow .input-section.card-details-section .left{float:left;max-width:49%;max-width:calc(50% - 2px);width:49%;width:calc(50% - 2px);margin-right:2px}.monnify-card-flow .input-section.card-details-section .right{float:left;max-width:49%;max-width:calc(50% - 2px);width:49%;width:calc(50% - 2px);margin-left:2px}.monnify-card-flow .input-section.pin-section{margin-top:26px}.monnify-card-flow .action{margin-top:30px;margin-bottom:5px}.monnify-card-pan-input{background-color:#fff}.monnify-card-pan-input input{width:90%;width:calc(100% - 35px);float:left}@media screen and (max-width:737px){.monnify-card-pan-input input{width:90%;width:calc(100% - 30px)}}.monnify-card-pan-input .card-type{float:right;height:18px;width:30px;display:inline-block;border-radius:3px;background-color:#f9f9f9;background-position:50%;background-repeat:no-repeat;background-size:contain;background-origin:border-box}.monnify-card-pan-input .card-type.visa{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAdcAAACmCAMAAAC/UzUjAAAA9lBMVEUAAAAaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3EaH3ESp62kAAAAUXRSTlMABAgMEBQYHCAkKCwwNDg8QERITFBUWFxgZGhscHR4fH6Bg4WHiYuNj5GTlZeZm52foaOlp6mrra+ztbe5u72/wcPFx8vP09fb3+Pn6+/z9/u+sm/iAAAOeklEQVR4Ae3dbVca2bIH8OpunkEEBUQFHIdrNIkxjNfJwevxchlBnumu7/9l7jor67yQk4m1/7VLV9bq3/tJJtKy965/1W5KvbFUKpVKpVKpVCqVSqVSqVQqKOw322e/fRp89+m339rNWiGgX1Uqd9i9GS/5x1ZPg16jQDr2cgWNPPlQEInohbAgEJKjsNYbrvl1m8cPjexbfQYBuVuxznp8USOdKouU6IUGC1TIRbH7ELOD2XUzQzqV88cVv6ZGzvLswbITksINSzzQSxf8ujggsVJ/xu6Sca9IqKD9zBJdctZgL1ZNgkVblmjQS4/8ujEJZU4nDJv3K4SoPrPMN3L2gT35IyDQCUssA3pJ8jRckUj5a8w60xY56yUstCBnheObGXvxV0SYCUv0dj8LFmiSQO2e9c7I1UeWyxIi27iast7/BYTYY4k4Sy8ds0BB8NeP2IcDcnTJDg4JlWvdLFjpCyG+sMQt7biW7NXpNYU/2Y8MuWmzi3PSKHUfE8Zhj1W4YYkq7Zjy64b0c8FZzH7MyE1hyy7uSSlq3W4YNg9JAnhwJ7QjTPSLXmXKvtySm3+wkzXpBfWbNYM65OyJJdq0Y1+96AVnCXtzSk4q7KhIPgT12y0j5uSqzBLrkHZ0tIte7n/Zoz1ycsuOmuRJePTIgH1y9JklLmnXN+Wit7dkj+KAXGRjdvSB/ClcrNjVJbkJ1yyQFGjXQrfoNbfs0xM56bGrMfkUtJ7YzZTctMDtYFa36J0m7NUVuQgW7GpLnu0P2UlETkbgob+hWvQ67FmLXByyuzL5VhnabSBK6G6sr1n0Oqyh364+sLtj8q82sdrwX6Knp0fFotdi39bowyz3iQwE3cTk7w/WLBBn6D9s8EVvP2Hf7tGCv9yETOyvLP7+Jlp2LuOLXn7F3p2hhVO5JCQTpa3Bxu2BwV6WNrzoBRP27wCMm/UbF71D/xu3Anw2/AQvehespwtzppYlWsB/e694XcBniCm66FUT1lOFOfumyQKgmHiueAUrFlgFRN7CnGDGOvof+R1jnsnKN6htUNsy10ef+QOggGcf5uQSBmXIyIHns9w9CyR52oWHOZkN6+i3NGeMqpORYMMSOZLJJ3CLJRrmfGQ9XZgTrBjVJSt3XpthzlmiRrvwMCcbI5/a4l/WnsKcJsOGZKXNEj0SCZbwbgENc87YxfbxQ6uap3/LFGpHZ7eTrS7MGTFsSVYKPpvTD8EtCRDmAN+Bm5t6SD9UbF1NEjTMKbNClqzMsNMcvrveRERwmIOPrCxPQ/qZ6PDTAgpzPrNCg6xcYxEsvt//TLsUYc43lknOQ0l82Z87hzmZLSv0SU+TcFVJoIeMRqrCnChmkdUeCVU+rt3CnFPWeCArOX/H9AULPOKLVAstcM/zTt19T3wGrWOADZmZssCNtxpHA9+WF8FFZFsiN9UySdXtuzJAV74i2DtoNFIV5jwr+030hsZdVLiGp/pLNlachCdQmJNRRjN6+cS46xEXJX4i2C4LxDn6kTCBBtBqypRT74KVxmTmiQWO/Gwg7hS3jByCe9EymQlXrLQN7J85VQRbw89LcJhzxRIBvd8hcaa73cY+q3v0smuaKP7jObppoff7qlsev+OmLow9RLCZWLMznWNhzhgohOCA6xb6FX7NNelpAom8h7LLJqQfyoLbn8U7H3O+8itKQQxPP+mdA7sW6CR5qcmB9tDPdUoo7XfUhGisbiLG1fR90lXGr3IRhjkh+rlym2x0BV8xV/B4sV6wVUf7A9VoxAO45VowUEcEgLuCJEt09J7H6wftdQRRDHyV6ydzFiwzy5GBQ8lzXLRvIlYGbBntHMNcNYx2pPhceVYi/+5F/8sb+MeiV9VeMzFRNd8docHHmKU2B2/fQbQNZQ0DGTKz0ZVZ99DRSHmYs9H22V+GBMHnfAfCPWH9fdOmG+W1eV+JSBvm7Oizg1mVEPhoZF0Yl52RmY7qGBhtwdFIPMyBOne/ZMmfY2EPaR4/aeiVVBFsGx2NxMMc7GL0TScgAPYlc0HfLfVNxLiV5hfuCW8MwMMcrLlo3nyzrWZZvMbl6V3HOdqaxuhVSOQxzMGHcyY18uFWvGz17JuIdQfQK01j9AXtAsMc/UDxqEJquVh8fKgDP5s3HecYyTaG8tFIMMzRd3kOS+aVnCQnb8J6JDsLuNe1jQ/4AGHOji5Dkj8KJIEPmD04PHsbsjOA45gRPsALhzn6qeb4Y4ZwDZey5y2e/Ou10ZbuEt7qCYU5O84ZtemFdkHJNnTZ7x+RmRxaG7jUjoFsVF22mRXDFi3ClJz2eVXbJmJ9m+gQvWx4GxHpwhxB3QcxKhPis9O6EyZ4zUbvGntFU0s+GqkIc0qC7ixIchmRs2jjNq0yxUt5ek2s6POo7cy+0k6dFbassagZHPYvHX9lKmQmA+1qC+re44l6SrTFOp8jcvMs/5yES8UJveu8ZAebFWhqm5f7kkticfM9clFzjb4qigxU7wqo5gUrfDTyuz0PPa7BP1kn6fi9o67r2hc4JTsHQATbVHeodnzcmhI9sdKfkcdb85K8c79ORO85L5mEQE0hzqG5iLyzKzdjpWmOhM5d9xPqJmL7eckKMNV7Rz8zB/4Ekw92WfJ2a96R+9faO8/oHgNFvKp2Fy76R0d/MQC4M6YFVGGKilxEr+ZazwuW6umYQ2+XDEX/YKVtmQTGSFZs30SsO3KM3T+UE2CxwiaTzllpVfJya94Bcn1vhuyMHAs/99BopPMfMSGhww3rzLMe3kS9CpAs8pDsnLl13ecS/UsLN15f31OY6F8nr7417yOU156Rnapb6ainv3eqKAtzxIJ+wioX+kbrCtTrc092gq1TRW8BVHbBMEeuOmOVmjbNfKYfWSruedB7cHmsDlQVQDzMsX1v/iJU1uR64KBMnux0XS64/sZAYouHOXLFB1boK+eY8uBPtkl2Kg6l2lyCjUbiYY7cwYwF3MuehQTNJOv2b+XWbU/rLh2ecZaI9GGOu+Bkxaivqopcm34o0oTUekP5b+AM+BGhYQ4gOt/ofmGxXq44QrP4LRk6ERfJakg3Nx7mADLoJ3uu2L3f4v/UMtkpibfxd/hopD7Msf1kl4rA6xC/t+yIDK2ExdpsrC8o4GGO8Se7D+8G1gG+lfhEhm6F360dfDQSD3MQmX7Mri7hUZePhA/lT8hQW5jPPOM1OTzMweT+SNjNDL4PfU9xVI9DslOQfV1U8dFIPMyBVcY+3kLV1Y0hfdI8FHoL0WZoAFy8h4c5eu0NvwDsfoKFrkPvWDPEpHcjOWdlYh8vni++4Z2V2f9hBz1wM5BXBfIDMtSSxCunwCIFhjll8uQkYbEBNs45op/ZYkmQJzlJgXqCjkbiYY5edcVSI+zL5c/2zyzsm4hVCeN/icq62wze12pUNS2tWGiOjPnq1UgP37fdi67N+0KvCfAwx/qDXSCXo+h1yVCDX7GKtsC6CIY5DfKpDr8N8IT1jJuI1cW9M2yF2nWKhzmwLyyDDBvqLUhPUxgBRiPBMGdBfuUSlthCVzjqZcnQBeutAnyDZvrNNMR+ce74TRySoQPWOyMi+zAH0IE+11zMb6JPhqKEtZKcp8dn/30e2idJw7yBe7I0Zjk8Cz8Dwhy9GvLzDZb8NtakgcVneDCNhznTd1pkrrFryfWKZKjGSlMSWANhjt4pUiB45LfSpPedl8RHI1Vhjt4dsC8tsz37JmL987kJ4dxoR9n+TYGCvO0zv5kxWeqxykcS0IQ5+zXbrcMaeNuIH1uyVGWVEglowpwrfmoQohizxBBYkj0p0/vOS2rnpFRhzoSZ5ychuYqeWeRU2Klv4JgsDe2KYXiYs7uv21wWyEn2L+S0Uee39IksdRi3CIiswpzdZ+KhGZJYZQ6l6t/4LU3JUoVxPdqBhzmCp25zUw9IIuonUJU2n7AlgwobUDSARiPxMEf2TKxvW1l6Raa3Avd9fX5be2TpjlEDklCFOXP+D9OrVpH+TtS4i1luJL81z0CHLJ0YPG9YmCN/JjZP193G3sskKSw3P4xidtKSl08M3JKlEoMmtAsPcyJsWmu1GI++e16xu3kgP2YbeCZTS8YckYgmzDlnU23VBlIvQ5YGDFmH/vZl14pnAjcPkD43j+pkqc2QCxIpakovG7bUAmICr7pkqcCIpEAiTUWptMiWHuWjkUaGZGpu+f90pYg2jthQXFL+EPSWZOqaAQckM1aFOYY67qex1cDJnF+TI0stdjcjmSBWLNUTtvNPYId27jvbbpClnO5p15efG/gzgVpmpbfm4a1m9fdqIsYnUuKMz+axHB7wgbZ7wGjk2Pv40wOZwNexLz4Px0v8mQAlTeANqHzifb54Q6Yadj0cz/owx8AJcoRPsv5vyCqSpUxi1UuX0RzPZ2ykA23QhgY9Cy0y9QRUakTq2jDHQHKKLeRNg6bAKzJ1gY9G2oU5B2xi28S+8DchuQqT92gixn+E51674qb4MwFYVaFb85hvLA4a24AsRTE+GvlrhTnjHNobXzOp5FXI1MjmzouCJsxZs3dJP8BuzQNruce6JmK9c3w08hcKc2ZV/JTXJ4Dyrdx6Nbx9wyrMabFncT8E3iQEvLHJ6q3cgGCLH+p/kTBnWNT0d00JMtY1Ees9sNQmIrIKc+zayEY1+hsfLXtClW/l1uvho5E2YQ6dbyw+VfDWvCRnFYF2yFQVa5G3C3OIwvaIfYgHFXX79ANhisomYr2NfjRSH+bsynfGrDTtZIFgAuypNXsrt/28ZINE8DBnR7Z9t2LUrF+mn9tniW1EAIO3cpvNSy4DIvswZ0fl9G7BruKHbtHXdNItofq6JmK94kCk5fuPrJBMrnE+nLPQetjbD0kg+DqQ2CNUdfCaA0pFe63fb8fLn32iT4PuYZ5Sv6RCtdH+7ffB4HH0b4PB7712vRRRKpVKpVKpVCqVSqVSqVQqlfru/wEJBRsNARGD9QAAAABJRU5ErkJggg==)}.monnify-card-pan-input .card-type.mastercard{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANsAAAC0CAMAAAAq/6n9AAAA+VBMVEUAAAAAAADrABv3nhsAAADrABv3nhsAAADrABv3nhsAAADrABv3nhsAAADrABv3nhsAAADrABvxTxv3nhsAAADrABv3nhsAAADrABv3nhsAAADrABv3nhsAAADrABv3nhsAAADrABv3nhsAAADrABv3nhsAAADrABv3nhsAAADrABvzURf3nhsAAADrABv3nhsAAADrABvsBhnuDBjvEhbwGBTxHhPzJBH0Kg/1MA72NQz3nhv4Owr4Vw74lhj4mhn5QQj5jhT5khb6Rwf6hhH6ihP7TQX7fw77gg/8dwr8ewz9UwP9bwf9cwj+WQL+ZwP+awX/XwD/YwIThlsQAAAAMHRSTlMAEBAQICAgMDAwQEBAUFBQYGBgYHBwcICAgI+Pj5+fn6+vr7+/v8/Pz9/f39/v7+956nHiAAAHJklEQVR4AezBMQEAAADCIPun9lkMYAAAAABAzs4d/zZqQ3EAf8JKVYSViiEiUFCiO4FAWQTybdet823dXbd2vd7aOP//H7Mf0LS0TReHZ79HpX7+g6+wnx8YO4iiNOtNowiGkPIs782kFDAKUboo1RNtOY8nYOskKapOP9EsZxI4naalelG7iAM4RCTLTr+omoVMweYrdci7/40nkvf6kHVBHy+ulZ1FBPvJpbbTJEAoyFplr4zhuaTS9rpc8CSzsJrCY2drbY8wXdqq45W7I1NW+njdDHyLajXMPICeKPQwjQSfgrkarJ32w7HTgxXCY9mvFcYiALHUGE0InqQKqf6u0Ugz8GKhsD78eqmxluBeUCusH67N5jeN1QhfUw3hx3tjzPYLPlzoOFrrJJqbcF04wmgE4VijjSxc4Giu4cO5Lyj4aB/uzK6HS3y4saxr6to8ttHjWOdShfbJPHWr0WaAdqrQfjLP/aHRwjG0I/fmuS2+QVkLwJkrtCuzzzeNVgBKpNB+Nvv9pdEkYLgfkS5HZTOWGjmyWhm0CuujedlnjdUJGCpzX0gcl5Oc/7HxPjiGx8b64Hw/NicPDgaJfRZJZ6Uy4Xq1MYcwveyc+mtJnDYnIU8neW0O2fB0lSt8u2UOc/A6MNIhyTQoU99DsrfhaCpLgirpplJWcCyFdm5s/KnRGF5KfzE2vtK/oqbeekn+nnKh0Iwd+k+VJcHq1vtC3nYptI/GzmfqYhIotAtj50ajCaoyyZBNUme7Mna+UWdLX1W2GcOnEoYF7i0b1r2xs32F2Ywt/mz4tzfMG9zbfHvLNn1V2c6o+5JPxs6tu77krZ8Emi94jr7i8e3hMOzlcH3C4/+IN1dod8bGA/2OQKzQfjc2/tZoCf12wAVVmQz5N4THsy28oHnL2VJ8nmSYcAzTrRf4+W/Sw3+UAo72jmSFw69u7wH4BiXDkCQYlOcUu2+C6f/yO/9NyZLrn9cLhvcbsn75nqFPZvihi+F3LobfZ/h+nNkx9buh/5XoKxDDjNs6mG3sxwMYDggwnaEazVmqwMGJRW//J3QCUKa+FvAbhkJCMCodjsg8T8JkZIfEto7OsMu8Wlesh4TPPfT//VHhMClmCQwXu59yN26bLYTMdV95637LjbOeXLndcVuO6nqP/4rl5pIgGkM4gmgs4a76AUkRDREOUVBuCeYaz1Lgt/gjRK3C+l5jdRK8mNQKJ4Nc4zQn4EumEFYRAMg1wYqNuBIJdWmcKDCXIPmVtWqIOoJ/yUYP0eXg3WTAatDGsCvpBlR+fzMNk67NAnhM5J3vZIh0rX0JiQN4TiT2RaUjS9YL4hp33av1ha9NIoDcJKtR1/TaXNTb5CfAZBIvVmq/cj4FG2dFpfdbL5MT4BVE2bysdy/FLrM4gmPIJK92r8ZuqiKXYmSXmkeAI+W4Iv3Dfhm0zI/CYDwBQQgIPQiBgiAEBA+C3//T7aSm1W7fdy87wy5/5jmZ5mnMz1o789X/Ul999dVX1Hsn+O+0i+x/Kpv0LvBefdm+bI7IA+mL4zWKWXJ0VzZoIgyTCrddRHjD19gTld4L0ZkEYpG0oQVEDllkd2CFz5xF0anHKjmM1gI4FskR38DGvUvqh3bY2hhtI+fLCGW35xNaNyUE6ZdkdbcAqtcoH3YPgKlbLtqchzZrX05rmNYW38E2Nbs92vNtzemFGVa4sy1uW5rFiWW9D1KfMjbjuVnfxFZ3tvqFuVoLqM22ndPRtFhTeaOYmra/i7wyTWR8CIqatrDrTc7YWuIUrffEXI5uo0bCLHe2l5UBsg4zc30Xm+AJuYNN5ceFjGBrKSNRQIXxcZbo/mIdkJWx7WhXijtG6mjnk90WNsPwuiDe+noL2zEv2uMCCKNqfcU4UNrF1uiXczKfd0MaI30UduGkvKrznJvWNYN9AuX3sI1RuZoZVW39bUq5XhTJ7J9sTffzIRkVJ0KdLpsQxxAnm1knJYS3sMlVlxe29Qzehmm+6OL+ztZv8iubjUx5tg/lB7YEpg+yeTsuVdFMmFofqviPbO53trSw1R/YsuXwg2zrnkyTgTbOds4b29x4dOlGVCbNfMFV7qc9WT+/J4+WmrPTazKcMS9sht9wWla2dOuS5prtDzb9PkRbkU+y6RFdvHbTjI0K3dnyAHJkB/0BR5XubP78sYKsVaplkPuDzXWDw9w/x2Yr14uUrrKuauKxJ/25zO1lKOcHOHNSEy5s9nx6FRl9B42aSOtPNuBptSS6T7DNY7EdpnCG165yzUJnbhMtbPZ8TW2cuhY82R4/yFyr/AE2wGQgZqKzxxphKLQRO3WzkYp/nI7xlgp13OUn28Mq20iGUvO/+o9zyBO56+8JWnZj3tw0ITFzJLiE4RX7JeAwi9hohLwYwb8iuibyd6tW8YAjiVLCX+3BAQ0AAADCIPuntscHLAwAAAAADvJW2hmIUANEAAAAAElFTkSuQmCC)}.monnify-card-pan-input .card-type.verve{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARoAAABiCAYAAACPtRyiAAAWD0lEQVR4Ae1dDWxUV3a+uC5CyKUoQixyEKUJa8/MmvGMMQYbe2beFKjDUoos1kXEATOe95YiSiISsREhTiiJXNZLWJamlGSzFiEsZV3CEkpZwrKIsjSlhM0iRCmlBBwXHP+MB3sYnLEZXs81g8JP/PPOfe/NG/t80qfnBPvdd+fd+825555zLksFNDufHtnmsk8JuR1z212OQLvbvjbkstcAtwLfTpD/XNPudqwF8t8pbc21Z4Xc2SPZgyAQCITPn346vT3Xlg+isRq4G0TjPIhGDK4qkj0hl+NCu8u+B+73Avxc0JSVlc4IBMLwwpe52WNBEJaG3PZ6EIIwFwiD2QHCsx+EJ9A2dco4RiAQhiYacyalw0RfCMJyQMhiEWcPPMdhYHmb3U7LLAJhKKB5qm0cWC7rwd/SxCe6xdgKgvNmizN7AiMQCKmH5lzbeJjEW2AyR4GqlQki2AXPur3FlT2REQiEVBAY+2iYtBthiRThkzi1aOeCU9vuto1h1gSBQOB+D5iwjVYTEISF0wx9WVo/YgQjEAgWAUzMTJiYBy0tIAhCn4615WZPZkkHgUBWTBkITcj6woFmB/SxgpkPAoHAg+BgAm5LVQFBWDd1zc7sUcwcEAiE9lw731E6xSfgsKLLfhZSImhnikAwGq252TaIi7mS8qKBF5tG8Nu4GIFAMMofY8vjQW5AdVjT5ehoc9k8TF8QCIS2XHvRQ3lJxChYdrOZfiAQyJL5RpEhRsGyKWaiIBDIJ2PL6n+5RMuoEPls8CAQIBR/HEymgRy/RJf9OlDrbhSBQLhhs/E4mZMkJIOOsznbkvNtLXE2BAKBl8okAdEsNjsZgUAYtMgsIOHAio1tKSMQCANF/TomkPNXMDfKaXuK9Q0CgQCxIfsfnTyd3w+oPZ9fEWLnqu9bThQ6AxXC/Yqs+ZtvKDNhP0YlJvoGgUSm7BsnZWGeGo9E1Lt376IZ+/iw5YTmq331Qn2Kx2Jq2FfYl78mwB4GgUBodTlGwxZtQ5+T8qP9YpOy67YaLp5uHaEpcKnxjpti4vmbo/1tebe2ue1jGYFAeMgBXN3fxITlk9Ck5Lz12iuWEZrImtXC/Ym8+PxAu1Cb2X0QCJTH1BuY19Hv5Mz7jnqn6YbQxOw+/YllhCb264/FLLRwGKyi3IGihrvanLZJjEAg9FoztYOZnLff2yFmBdy5o94slZIuMmHvTPCvfCXUl669Px9sbM0OGmEEEhl+auQgTyy4uXCeejceF5qg0a2bky400TdeF142dVT81WDbi0ENn0xGIAxva8bxspZJ2v3Z74QmaM//Xk660HT/3uQ+uOw1bLiCQGjJ+VYamPaajke59bevilsDz34vaSJz8y+fEbfK3vqh1gzv0Jdu+/DMgyIQ4Jt2ntaJyreo49GomH9jz66kCc3td7aLOYG7u9Xwn5VoT03ItS9hwxMEEhpHPSrQ7cCHYpO1PaS2T881X2j4ztn1/xONncGWkjjKhh8IVNDKkYE9F7sz8Jzw8ikRum8qO+VK8ed+fiW2/Z5WOD2CEQjDbEt7scikhTwfMcvg+DHzUw4O/lLome98+aXanj9VpIzECkYgDDOh2SW0RfxWrfm+DgGGZ4n7lm6/u120Xs0BNlxAIFzOykoDn0Gz0MT1F/OkQrHdmx9tMk1obr2+XjzY8LtzxITGbY+E3Nkj2TfBG8xkklIKXM0kuRZYB9zL/PI+uO4B7gBuhH+vBBawosqRzOooCTzB/EoePO88uFZAX1bA9QW48j4qwIp7/xbMZ1LVBCZJjGAASp5NZ76gEz77JUyS1wHfBu6G91APV86dwK3Al4ALmU+erNdJkzm6hPEfOSwWj/I/l8yLnfndp2JLvVO/1edZ4NgaxjGzIp1JwblMkrfDC78Cg0DVRrkL/vYocCXzyuNYsuGRM3pFQ5Jr4NmOwbVZe5+UCPzdGeAO+DkA4tt/XR9J9gE3ILiejShnusOv5AOrUSyp0jf8wVs1vnds+OXDcP8o4l00wt+/C9fZTFqehiw67lBwE0X/RMuOinKhZzArojny4mpdnuWzPHdN70D3K9eBqk7sumfxBM3Nq/JzcZGXQvuHgV1AVVfyb9v+AMKGv3fQxvQGWArIfl7R8RlmAuvhvj36vQv5MtwzwIqC6Vr9M+/ptl187apYTM0v9hgfO/PeO2JO4JZmcAI7hZ6hyf0ddX3hXPWPpADqZWsQnGrmec7YZZVPnnLP/FY6eLuGkZv5/cG3fDT8Xgx5f0V/oVHOI/u5XQeBsQEPYdrXwAvMFyzWEqh3Rre8oS2CTuGODrV9pts4oYFdojvNzUl1Ah/LL1CzvBXIl4sgX374ZP2tG6lqIvcdYb8tEZw3iAl2FPkZ7WZ6gi9f/Uoc+SwL8O+kPI0LMlZwEYyDoFazHGh3QEcwJn6mD4alWWr8K7FM6Fuv/MAwoYG4F3En8LzZ6PZrZ/jVP5SC+BeLpaQ0MV+VSzdnoqS8jFzr4+kJZg5CaNYglwPXmZ7lVv1yOdoKBf8Wss1x0P9jyHbFyDcoZizreynV5rJn6h6f8i8fidWpOXPauLozv/m1mBP4xHHcDhNwVdE88RcqJjatYOrmJM33IMbWQS8ZsG34gg4dfSM7kO/oCFJkJkGbl5I7vmBXtKg8rS//TJHuEbeVz4pZDeCovbmgVP/YmTkeHq8j8mzoouorir6r94vFik0DXMcL+h52mf/s8lENz3cZOVFW6Sg02GdYjWhrIvCqRcbXpr7ym8qNsBx6/vuimB/kZ+/oX3fmJ2+JOYEbv+AOb83tbpw525iXih8Mh2A7N7WEBmKJNEy8LcjP5SDTA77gZAGr6imNbWXA352z1PjyyY/70sCkX2VIQNwbr4lN6tYWSLR06ptA2XBNLKDwxz/S3O5H0wvVP5BkawkNkG9Dp5hFs0SD0PiR7UR02aGT5BXId3Ie0dZey40t7u/yKmMejaFZZ0iIf1G+Gu/sEEy0XK3fck5ZLnhqQxd3dGtq81reVDXTt0x8qQM7IsCN8N9r4Vp9L0pYuSjsHPYEMqwvNAj/CY/vkOQQ8nPx6yA0+5FC86bGdioEP9ceuMcp4Db4eV1ifG3qtez8gqEKcJ9HfTRvGuV47dr9vpjj9eQJ/RzUhw8JPQscL6O5TblovsAg4NG0SnG/OyGS7ATWi8alWF9o5C6WX56ucRLuQk8QEXgC6eh4Ip9cwAYLnwLpHHIr8vOM8ohoEO++fXUly0Ylgi+xvp8o88D9tRYiN796nfhWsviWO74K4H9Om4ZdMvVwh6CmrVYpuAi11SzJzTxHSlxoBC0rST4CrANuA25PLAdOwzWS+L0ziCDCMuQznRf0z/iQ7Tay/HxmvB9KvgjpCFka3nUG+ssMrHCc0CAYO3VSzCn8Dj44Tq/M8u5zv9fcZvmshVgroxJprvu5SCHaW5wEoWlifv6NKttYf3AtTWP+IE/CnKv9GYOjsXE+QqkbkrwZ+d63Db6N5RNQKR6Q2oDacYQta5QvSJKbeMyV4UsnzsjqvxZzCjeL1XwBCqdF3Hplrab2LuS51HRMUB5PWhMBBNEh2jxkotCAEIIfwiuPZgiY6CtZKfAOLiGXTT4N/dqA+ux9ch7DAtI7UHE6klKKcAYjOC1HvfNFQ9Kq73UqleK7XwXayoy+UjgXMQm58zI4lomAO0H9mp3EMSZVZZgkNBeZmYAyCEgfxhFkSoYN2V4zK64aXEY0t/IkRAIuL/sgCilYimh3x32hWWX4uUm1fydYkuGkQOmKfxVbum3/e81tZqPymOQNOn2LL0V86ywYkkIjyWOQWeQx5l06BtHeWsOTKCXZj7NmApk6pVac0di3hvsJleWGV7MrLlDjkYiYU/gv/lx7u7NLhIpx8dMr4R6a2vx0Wh52+3Yy0wPFVWDiKhGNg2Gz5YUGLzYHkJN/MWISnkK25TfUCSzJB3X8PFciloWTwaJBpCAg2PXzXWLlI+p+qr0cxLYfC7UJNYU1t7l5ph+z83Iuqb4JSTk5hIWmAp23owVgMaCytSWlmRc90yBmFxB9CTK94AtORLRfztqc2ZmmHNS24BlumeCti1AIfCUubb4hwaNUOpYsQu02odex4sCZ8JIShu30ISo0VRm43Sc5wqvcGfpNz8kTVAeL4uBYlJjxRFo9ISmNmre50WUiEITMZ7Hdn3VrTdvt6j77KaqPNu+zmgfbCL+yH16eohdRmcPe4PghKTQAgTiQhRraOI5so1iDNeNDJ4vqOr6USyjrMORGFb5ClPoUSwOAM7IH3RZEFZteqrMFiKs1YwFKcv7QFRqlzNBiWL6q8bhlk9zAC0Zp2EVbmaJj6zS6lCc+q/u/xJYzi8sGbAMcx0LLNL4dD0svzX07N83NP9hUFZr5Q1ZovMoovjxEJVkWV44yMIlyk0bLrDZFx9bV+6UiFNOOOXn1ZTEH7Yf1Azued/5MLEt705uovn2cX5CyQgOsGLJCA+DBkMgUiUWDuPcJZARyDj5/K6XYwTjgwHmHWULDg98g2hctAvzQt7BnZt/3h3rD8ZthgZrFN3nmOapv9dOLUllolCEuNB7kt/G+AWoDT0Iumz5D9OFgio6tHsZxIWcEdwg3myU20W1bxCyOzZv6tpg2vKrPNjqC7xeUpLLQrBzSQsN9IX65AXeSRNUYA4L01iCE5liKjq34g8mVO007jtZbyC0TsUp303J0r+zHy3zeLJXQ/aqzrtCQRQPg26xIUQj2c89zqG94LyJSF7lEswBjDxbAwkcII9i15wOxXaHnVz6+qxV4Tr8APQR3FRSnsjN4yZAXGp88BfnZnOgjeM2FvN8hZPj/0RQdXyF2Hy1TszMgSjhqltDwc6vv9vTg85/+/fEjaWMf/0qoIHpH+UJsf1LfRwNZtkNeaPBWQZxJwcl6lYQAljHAsPHRQLH4R6vt7TXTqvnqV4fETkoom/+1cJX6hU44iP3234T7cyR/RuoKjU9xDhOhqURaIRseq6TnV5oxx8Yg6hJbeNcJYRGGXI5SM4UGQvx1Oz4XTk0QuhfUFBbuz1lsQqUkXwIeTSo9yzNSVWgQpwZEUMF1D55Z5EcHAeLLNeCr6h1L8vh6mT2IG08+yXefGswUm+7/+ERsq9sLW92FeWo8jN/S7rlwXpe+3HDn8JeKGQibmfWR+kIjeribPzj3AX/JQeSkdzAs/MhTOH1Vk5nVAMuntWYKTedKWfgIlOgbr4vlUP1gjW79eRJ16oF8hoTGVKdwvlBGN5w5jjtvXD4lWAtmAbKw1lJmNbQ6HWNhwnSYKTY9F/FpCXduXFd7rlzG/33DtT63yjH0l3wPF2fglaeQ0Jhq1ZzFFcQKTOD+GtPP0uLwVWF3zQ4zKwJ8NTUmCg1YFC+ihUKU0Y2v6dqXNYXPYP0020loTAQ+P6karo2Ivw0zjzKaiSA/n/H7IL/Icqxn1bgdT5hq1YBFgT9FEs87Lc1q+wyXrn35xfRZ+KAmKegkoTEJcHge8pC0GFKgtugkkIeQn/1RfgyyFX016021ajZWm2/NbKnVvR9fgEN4JL5UxDl+TAgJjUmAHSBkP5K3NJbkF9DPwf/WamiZmjUK6tRcNS/Z0oVPtkQwfhOSJ2dNN6Qvc0oWicQcHOalCUhoTHEKZyESIpPrI5GCTwkegbuYWQ2Q1b3ATKsm+tYPxUVE/GA6Ye6Y4RU9AvYTXpuVhMYUq+aICZGx85iegPEhZF3xuJaseWlWW0LtMy3ZclY+Ih4GGX8jzTKsH9dh+fSEb7noAA1zU7f3DGS9IQWn9CYKllSNN1VoJAsKjV+eb/C54ZeZtDzNsOhmPE8xX7BA/7G1nJ/Z7dFsOYVyHRNCLvNKSNz+h58YLjRd79cZ3o+XCkv1Gqz8bOoamKT5mgcsd/75lEyex8S/xYD7mP+hw8d8Q1FoEOUjtB66Z86pl/0ewK806ZYewIXLE0AclVs5Ep7DCazgQafAU1+foyWf174L5bLPN82q8cxQ452dxlkzXV1qeI7X8H58nudU/9gX0HvghhPh5NuB6+C/e4tOw3UFjxrl8R3AtxPHrXw2cKi9XDnshYZDkhXDspaNcu5L8ioDRPESsD5RMvQl4EqgAuRt8S+qTcA6eI/HYOxcHcC/FYGTNVBLqM2mWTU//UfjrJl/2m2az+nFwtKLFk9220BCA/AGRsHEaTXg860xThyX8KTOC5YeXx55HNOKa+4/TQexOW6KVSMVqfFbt/S3ZmIxnuVtisjwkyU+cE3nB6N/YmGh2UlCM2BRLHxclKcq0+AE0SIzds3wDOYzDNpy7eNAbC7rPzFxBcYRhc3NEpnGNrcjMzGAn0JGc5rBEyQ0CXgD/MiUqI4i/p5Jzuxq6wqNvEhkF2qKGfWFw7M93J+in9D09CDO70YxDKd/Oh+J15iLSMIzgXIDCc1DVs023QL0eIyOGeAbBODkt6jF/BITQavL5uITynCrZvdO/ayZAx+aYclEIPaoqI9BvMSCYtPDZixLJ6FJwFM1iS95dJhg9eafW2XBMp98Y0IUba7sAqPFJjwHrJrbt8V9M93dCGsGITKubP8AkahlfPvPWg67qskkNA8Aljzi1oz5OWtcbKxn2fRRGxlp2TQbatV8sFPcmvnlPsOXS9ySGaQDLw8m3BULDQgfCc3DwYxClicPLUgWINqXZ5dbyHK+oGeawpSQ23HZUF/N7duC1sxc4ywZl6MBnOQ5GncLxvAqb9bYMZArSWges2rqBML7XRYoV1qECEJEExtLg9iNso0zcuu7a1cd3prZ/88Gioz9dLvLnimQM1QAL+O45WJpSGj4TmHMEtYM3kk8MpHp3WzBWBo8vnD+CY+z2WKMVVPCrRqcNTN/rlEis6N96rdH6pRFPJNJ8h4T/TdRmPQHoc0A81Y9QUKDryts+eJSngCP5VoBPGeilXyV7+ABPb0pHkYArJsFRvhtbm18je8caWK0tsYAkbGHwYoxJtXeu3wsL/cIrOfHcugbCi8fuZeiEPQJJ2pKsgKsQ7CGpQp88kQQxp2Dp7zW+n0KOhNpKqd19OPE+TKNB3/CVen1cZkF2IGZgMj6tjyhT4fbXLZJDAVE2UafksWzYBODYxeT+DJLPg8vl39jXIdrE/x3I1wv36uBKx/llhFwE89TgetsMF0nPrZOJhBKKjKYJBcnxslW4P5E+YlLiTHFxxYfYw0w7i7wCHfgQeC7wPXw/5b0Fnn3LMuwQomJMmBDyouMy3Ed+rGEWRMEAgF2ZEbDUqMalhyRFBSZKC/U3jbVnsGsDwKBABOW70zVwuSNpIbA2Le2OrMyGYFASD2EnNljwYezFiZyo+V8MG5HEzxXdavTPo4RCITUR5vdngZLqvm9TmOXoyuJAhOD5zgIz1B2w2ZLZ0MTBAIByoWO4c5W4G6wKlqNFxd7CNraC20tbXFmjWXDCwQCoXHSpLQ2l90JQrACllg7wdo4G3KLWDz2GNzrHHAXcBXcM49bU4xAIBAeREvOt9Jg92oSCI4PxKICxOMFuG4A1gK3JMh/3tD7b274nVy7H5I+J/PKgCypIBAI/w/YARLR78gqTAAAAABJRU5ErkJggg==)}.monnify-secret-input .monnify-secret-input-label{font-size:13px;color:rgba(74,74,74,.29);letter-spacing:.0126316em;line-height:13px;margin-bottom:10px;display:block;width:100%;text-align:center}.monnify-secret-input .pincode-input-container{margin:0 auto;width:-moz-fit-content;width:-webkit-fit-content;width:fit-content}.monnify-secret-input .pincode-input-container .pincode-input-text{padding:0!important;margin:0!important;text-align:center;border:1px solid hsla(0,0%,59.2%,.2)!important;border-right:none!important;background:#fff!important;width:66px!important;height:50px!important;font-size:24px!important;color:#4f4f4f}.monnify-secret-input .pincode-input-container .pincode-input-text:last-of-type{border-right:1px solid hsla(0,0%,59.2%,.2)!important;border-radius:0 3px 3px 0}.monnify-secret-input .pincode-input-container .pincode-input-text:first-of-type{border-radius:3px 0 0 3px}.monnify-secret-input .pincode-input-container .pincode-input-text:focus{outline:none;box-shadow:none}.monnify-cvv-input{background-color:#fff}.monnify-cvv-input input{width:calc(100% - 25px);float:left}.monnify-cvv-input .tooltip{cursor:pointer;float:right;display:inline-block;width:20px;height:20px}.cvv-tip .react-tooltip-lite{border:none;background:#00b8c2;width:150px;color:#fff;border-radius:3px;font-size:12px;font-weight:500;font-family:Oxygen,Helvetica Neue,sans-serif;padding:2px;font-style:italic}.cvv-tip .react-tooltip-lite-arrow{border-color:#00b8c2;position:relative}.cvv-tip .react-tooltip-lite-arrow:before{content:"";position:absolute;width:0;height:0;z-index:99;display:block}.cvv-tip .react-tooltip-lite-down-arrow:before{border-bottom:10px solid #00b8c2;border-left:10px solid transparent;border-right:10px solid transparent;left:-10px;bottom:-11px}.monnify-otp-verification .title{font-size:15px;line-height:24px;color:#30424c;font-weight:600;text-align:center;margin-bottom:15px}.monnify-otp-verification .instruction{font-size:13px;line-height:20px;color:#4f4f4f;max-width:90%;margin:10px auto;padding:10px 20px;background:#e4e3e3;border-radius:4px;text-align:center}.monnify-otp-verification .otp-field{width:90%;display:block;margin:auto;padding:10px 20px;font-size:19px;letter-spacing:1em}.monnify-otp-verification .otp-input-section{margin:30px auto}.monnify-otp-verification .otp-input-section .pincode-input-container .pincode-input-text{width:50px!important;height:50px!important}.test-card-selector{padding-bottom:5px;margin-bottom:30px;border-bottom:1px solid hsla(0,0%,59.2%,.101662)}.test-card-selector:after{content:" or ";background-color:#f7f7f7;color:hsla(0,0%,59.2%,.261662);padding:3px 5px;position:relative;bottom:-13px;left:50%;margin-left:-15px}.test-card-selector .input-section{margin-bottom:0}#secure3dFrame{height:100%}.monnify-loader{padding:85px 20px}.monnify-loader .spinner-cont{width:62px;height:62px;background:#fff;box-shadow:0 9px 14px rgba(0,0,0,.11);border-radius:5px;text-align:center;padding-top:15px;margin:0 auto}.monnify-loader .spinner-cont .spinner{font-size:10px;margin:0 auto;text-indent:-9999em;width:32px;height:32px;border-radius:50%;background:#fba019;background:linear-gradient(90deg,#fba019 10%,rgba(251,160,25,0) 42%);position:relative;-webkit-animation:load3 1.4s linear infinite;animation:load3 1.4s linear infinite;transform:translateZ(0)}.monnify-loader .spinner-cont .spinner:before{width:50%;height:50%;background:#fba019;border-radius:100% 0 0 0;position:absolute;top:0;left:0;content:""}.monnify-loader .spinner-cont .spinner:after{background:#fff;width:75%;height:75%;border-radius:50%;content:"";margin:auto;position:absolute;top:0;left:0;bottom:0;right:0}.monnify-loader .loader-text{font-size:14px;text-align:center;letter-spacing:.0126316em;color:#484848;font-weight:600;max-width:85%;line-height:1.7em;margin:25px auto 0}@-webkit-keyframes load3{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}@keyframes load3{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}#secure3dFrame{position:absolute;top:64px;left:0;width:100%;height:80%;background-color:#f1f1f1;border:0;border-radius:0 0 6px 6px}#secure3dFrame body{max-width:100%;overflow-x:hidden}.monnify-ussd-flow{padding:20px 20px 30px;background-color:#f5f5f5;border-radius:0 0 6px 6px}@media screen and (max-width:737px){.monnify-ussd-flow{margin-top:13px}}.monnify-ussd-flow .loading{padding:30px 0;text-align:center}.monnify-ussd-flow .loading svg{margin:20px auto}.monnify-ussd-flow .loaded .ussd-details-section{margin-top:19px;border:1px solid hsla(0,0%,59.2%,.261662);box-sizing:border-box;border-radius:3px;background-color:#fff}.monnify-ussd-flow .loaded .ussd-details-section .details{padding:12px 15px}.monnify-ussd-flow .loaded .ussd-details-section .details .title-bar .label-text{float:left;font-size:12px;line-height:18px;letter-spacing:.0126316em;color:#4f4f4f;font-weight:600}.monnify-ussd-flow .loaded .ussd-details-section .details .title-bar .timer-cont{float:right}.monnify-ussd-flow .loaded .ussd-details-section .details .ussd-code{font-size:19px;line-height:35px;color:#00b8c2;letter-spacing:.04em;font-weight:600;word-break:break-all}.monnify-ussd-flow .loaded .ussd-details-section a{display:block;width:100%;background-color:rgba(0,184,194,.1);text-align:center;font-size:13px;letter-spacing:.0126316em;padding:10px;font-weight:600;line-height:25px;color:#00b8c2;cursor:pointer}.monnify-ussd-flow .loaded .ussd-details-section a i{margin-right:5px;line-height:18px}.monnify-ussd-flow .loaded .ussd-details-section a i svg,.monnify-ussd-flow .loaded .ussd-details-section a svg{vertical-align:sub}.monnify-ussd-flow .loaded .action{margin-top:35px}.monnify-ussd-flow .error-cont,.monnify-ussd-flow .initiating-error-cont{text-align:center;padding:0 10px 40px}.monnify-ussd-flow .error-cont .icon,.monnify-ussd-flow .initiating-error-cont .icon{margin:10px auto 20px}.monnify-ussd-flow .error-cont .message,.monnify-ussd-flow .initiating-error-cont .message{color:#828282;font-size:14px;font-weight:500;line-height:28px}.monnify-ussd-flow .initiating-error-cont{margin:30px auto 10px}.monnify-ussd-flow .initiating{padding:20px 0;text-align:center}.monnify-receipt{position:relative;padding:60px 0 0}.monnify-receipt .bottom-section{padding:0 20px 30px;background-color:#f7f7f7;border-top:1px solid rgba(0,0,0,.05);border-radius:0 0 6px 6px}.monnify-receipt .bottom-section .status-icon{display:block;position:relative;width:72px;height:72px;margin:-36px auto 0;text-align:center;padding-top:12.5px;border-radius:72px;background:#fff;box-shadow:0 9px 14px rgba(0,0,0,.11)}.monnify-receipt .bottom-section .status-icon svg{height:46px}.monnify-receipt .bottom-section .response-cont{padding:20px 35px 15px}.monnify-receipt .bottom-section .response-cont .title{color:#484848;font-size:20px;text-align:center;font-weight:600;margin-bottom:12px}.monnify-receipt .bottom-section .response-cont .description{font-size:14px;line-height:24px;text-align:center;color:#7f7f7f;font-weight:400}.monnify-receipt .bottom-section .action{margin-top:40px}.monnify-receipt .bottom-section .action .redirection-counter{color:#7f7f7f;text-align:center;margin:7px auto}.monnify-receipt .bottom-section .action .redirection-counter b{font-weight:600}
/*# sourceMappingURL=main.2f7c168b.chunk.css.map */




        </style>
        <script
                type="text/javascript">window.MonnifyPaymentData = { "apiKey": "MK_TEST_VR7J3UAACH", "amount": 100, "currency": "NGN", "paymentReference": "1568577644707", "paymentDescription": "Dating transaction", "transactionReference": "MNFY|20190915200044|000090", "accessToken": null, "merchant": { "name": "Tobi Limited", "logo": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXsAAAD7CAMAAAC8J8RVAAABAlBMVEUAAAAAr7//rxAAvcX/tRAAt78At8f3rxD3rxj/txD/txgAtb8AtcUAur/6tRD6tRUAt78At8P7sxQAtr8AtsIAucL8shP8shb8thYAucT/txUAt78At8L6shP8tRUAusP/uBUAtsIAuML6tBT6thT9thcAucP/txYAt8H7sxQAuMIAucL9thX/thUAtsEAuMH7sxP7sxX7tRUAuML9tRP9tRX9tRYAt8H8tBUAuMIAucL8tRT8thT+thT+thYAt8H8tRYAuML8tBT+thUAuMEAuMMAucEAucP9tBT9tBX9tRT+tRUAuMEAuML9tRX9tRYAucEAucL9tRT9tRUAuML9tRUnYKa5AAAAVHRSTlMAEBAfHyAgICAgIDAwMDAwQEBAUFBQUFBQX19gYGBgb29wcHBwcH9/gICPj4+PkJCQkJCfn5+foKCvr6+vr6+wsL+/v8/Pz8/Pz8/P39/f3+/v7+/3m+JwAAAKF0lEQVR4AezdwWojORfF8SOCvo3bUEWcRUDQpBamSMBe1MI4mw+Bk9mUwILz/s8yEJjdYDId+0jXpd8T/H1R35LdJoaI+98XaAgqDXC/wuvxNPMf+RRfh95VXsk5TkPvYZfbvJ7479Ix/KqlstdWCrgh8rI09TVUZmmlQB/5HWnrW+VVuW3mt019qyzymr6kYKEy9oYnf0HqLVRGj6r9zvwjk2+VP7OO/GNbC5VpQKW2/Inky1daXTw+8ocGReUHfyYHVKfP/LHJ4cY2V6h8M7lv9HtHX6k38TrSSltpf/gu8lry4+0qP65XuYKA9EV9ebZQmZ/tjl4wfH2l/dGTz/pKo2vnndS/LPuV+mtbqXvEm6bS/ujJ5CxUnhxKWvM2jiYqJxTkE29kMFapN/FmVvpKU8/bwNs54Vp+6ysNb5wv47Uqs6BSbuJNeTOVeoG3Fc1U6iXeWGe00v6xJ5PVSvvHngxGK+0fezIZqVRLFOhMVAb5sVeIRiq1IiU6I5VKnho7E5VHKE3UyM5IpVCiSDBROUCnp0o0UqkzkQb+Oe8p40ytHMEd4oMyASqeOjsTlRNUAnU+TVRmqEwUciYqHyDyQaHORGWACJUGE5U7O7d7wWNsRaX/Q2NDpU8TlQkaI5WyiUo6SBwo5UxUPkAiUurBROXTXc6+M1EZIJEoFUxUDm32xSpHSNDE7CnQZt9m32avkIu9qjb7RAMf6Kza7It9oLOhVoDEB6WyhY9z+ASJI7VWFiI7SOxp4C1jlh8QiYFak4FHLaGxYf0LP1DrExqeYg/1r/sIkUytof7EHURi9Yeqp9gTRPYUc/iPJop5iGxY+xv2RK0ZKo5if1V+y+EEmUixrvK+J8iMrPpp66nmIOOp5qt+0kYIRYqNVZ+MAKGRYtlVfOzpIOSo9lbxsZ8gFanm6z32HaQ6qsVqj/0MsVjr6UpUCxDrqJYcvmFLtRlyscrHradcgFxHua7GjTOjgFjh1tmWP/YSa1Z319lQ7ogi9pTb4hKfKOdRhJsp93ypJ1FuRCEd9VaC7yoKHrSCraP8c/8T9TyKcSfKpVU9ox9QkM+Uy8Pl36XV2aGojgVM/vIvkIvMDmW9sPj0XYgsYPYobWQRcewdAPcrTJklnD3KO3CJzmvU4NBGX86hjb6ckcsylxp9G/7sUZPHzMWIDnXxMxdih+q4PZfg3KFGYebde3eokz/wvs0bFNG2/nl0KKItnsmjemFuky/nMbZtU44fZ96N+Ohgy3o38w68Dw4WPR5mmhYHD7s6u+OPg4N1IZp8uq5wF/yh3Wva9AWTr5CPbfKXtfe77x5FtMUzdyhJf/Tbp/Nt658H3LexffGjnBfW6eRx/9aZFTo4lNH+Y3GHQtrwR6ANv41+YTv/gEXpWI8TFualfau4nD3rcPZYHHdiFQYskM/tYr/olT87LFNkcR0KaVtnwmKN7XpZzsyiwt/tnYFuGkcXhS/zB2EjOaCbPyZKnVVqqMxK2MKJN3HjJC1MKrlda+oMOu//KlVaWaumyQKz7GGw93uBK509nLl3ht2R3aPzBbPz4+1NNRE0GU2yf0hHAzVSK71kkuV/4g6f28moWs3ZTtq+k2S5x9d4Oxl0pAaMTqzHN3Efk04kxifY3iSZx/dx2WDDwo+sRyluonzj821vEovlbFB+tVgFd9KhGp9ve514rIbLOlIdc+KxMpnSbh7k214t1iHrcJQvsMnO9PjtGpUv1CcoX5Ar++oUwki7XyjPUf+lB6PiLPqdHHOCQBYnEoLm4FQ8jn2lVYdwXID1TwFSxVbckWNOUY11rb+foxqLnyIPnacsJQC7lvWfe1QmM1GHTktWYuBRHdeTlTnBJnCdiMerGVMJYNUUMG+wGVwv3vFqvO6iRwl9k2NTLAbEwK+hw3wDUMXfd9ggz2P9q06LLD1wEiA9QfwjuvSf6dIvF984gC9+O8al9hTgip9j0yx6MU5XZ7QOZ+Vu5xSbZ9GJ8Ca8Y1nCS9SBcp814Iws47fYptoeAKoPB6iHj7KMn/ktJrPjKMgNueDyqe4t/4ayUjLUxal8E4va6MXW4P+PHPYFWh72/MgfRzVa7XuyFPuokzQy7aWMD6iTU3ZBdKLaRb6VEhLUi7ILWv5wFXpo5ahSEApCYxqujrkD7VcMuLYHXEwNfp/e2pdI4VA7SURNZott+xIpEtSPi2cX+TPd9iVSOBB4Fs0u8i+UJqcEZRe09CPbgKXWsaW4BgUl7+gELLWHoFBssjwGh3PydBUQ9+/BIS127TgsDHW6Coj7fZBYFBlH4kUc/xM5Kl9pmfmrYGGpoRPQ3VuwOCdGTkDo8DdzDGg4cuQAGkPoPC3pcng8Ku9yuJ1Of/svPUzBY8hdX4CcN14FrLRyDR4ZqaUtMNs/vGqXxD0RV/6wyYHf+rzlYxMF2YYGTIYSYHyS7WUIJs/YDzuTAOOTbC9Ttg0TMMklwPisD0VZtg1TMFlIGa2brf4H1oHJr+Q2BzBSRn+rLzSDihOxoPJISnlHX2gLDNgRcA0qKgX05fZMyuiCCzvk8EzKecJOnAIFFyPg8mKL7xy249L+UXTatz4RepxGe+6Hes+k0X4pffbFGo32BT8QBtootPfxaS9jovQFj+97j6lCET9Aen5/z5+tKOIHSC+Gvqfwnh1yFPEDpBdy/M5FLkFFVuVH+uWo/H1M/h7yihzcsC9H5UbAOfvsZE6/cPJYVoV/bvWY/bDXYMy9dz+hdx3sh70O/RvmvftdMDH8/4isR/ttFdP/X9bDg0dOTznmNcPjlqyJBY+MnXJzCeAoSP1ZW9ZmyD5EMuCRinDUn/UlAAWPPfYvTUUI6t+e9SUMz06AFCychNN/e7ui5cctCWXKbviUu76Ec/Ruqfw340DLs6XYY+9jqFSlfzb7vu6vj9tSEc9uOlJm5FTn4MnZ7NPtv1T/9Pr4SSXd2VIk7I3rTDZJ+6D/hYN2WzaHkiNHxLILxotlu1DZBeNF6S60zJW2MX7GfrF0LjtBQg/fvLH9HZYdvtrY/g6l9xyWXTBeLLvn0KbJIR1fuf+6cMouGC8p+1uVxpELxotx7ABQdsF46dEDYMouGC9DdgCYnPDx5R3Bsg9Nu77egk3k50L+ZtUH2UG6np29Kf+uk1hR+rKXsgs+oE0135NyLhvp7xjSNxMvG+lrCQE/kOVMG+nvSEiBU5A20t9x6ClXt9cgfr4nu07XEZSo4XGfG9l9uhbVuTTcx+2Hcj9I6UpMWQEXP+rYyZs4ft5EipkiGJ8GJV14xVyFQvxL7nwvtOIf9znpCTGQK7eiT418oVHfJcyKhfKN+nOV6uglVuX3qJUnauFfqWyGbuKo9WLGJFdLhbhQIxukN5ovE97IA8EcvrrG95iHC1Fecu7xDdzFqCMPDKOjq6/UcNcXo1oN2NPRq6v53P3N/OoiTXpGHi4dVU2S5FB7Ie77C1vbm4AG5GEfAAAAAElFTkSuQmCC" }, "incomeSplitConfig": [{ "subAccountCode": "MFY_SUB_319452883968", "splitAmount": 20, "feePercentage": 10.5, "feeBearer": true, "splitPercentage": 0 }], "enabledPaymentMethods": ["ACCOUNT_TRANSFER"], "customer": { "fullName": "Stephen Ikhane", "email": "stephen@ikhane.com", "mobileNumber": "" } };
                        window.MonnifyConfig = { "apiUrl": "https://sandbox.monnify.com", "webSocketUrl": "https://sandbox.monnify.com/websocket/v1/notification/subscribe", "environment": "test", "collectionChannel": "WEB_SDK", "redirectionCountdown": 5, "successMessage": "Your payment of {Amount} was successful. A receipt of this transaction will be sent to your email.", "secure3dVerificationDelay": "5000" };

                        var sourceOrigin = (MonnifyPaymentData && MonnifyPaymentData.source && MonnifyPaymentData.source.origin) || '*';</script>
        <link href="monnify_files/main.2f7c168b.chunk.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.css">
</head>

<body>
        <div id="root">
                <div class="monnify-app animated zoomIn">
                        <div class="monnify-header">
                                <div class="left"></div>
                                <div class="right overflow-ellipsis">
                                        <div class="monnify-user-card"><span
                                                        class="user-identity overflow-ellipsis"><?php echo $rows['firstname']." ".$rows['lastname']; ?></span><span class="user-avatar"
                                                        style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAdCAYAAABWk2cPAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAJqSURBVHgBvVdBaxNREJ6NVtoaYwrmIEllPSjYiLZeVBBMT+pJT55z8K7FH1D7A0riWdGci6AFQT01XhS8tAeTg4K7kJSKFYy1taUtvH6z2S2bbPe9t822HwyTvJnJx+zOm5kYpAEhRBqqCLkJGYWYPvMCxIbMQqqGYdjUC0BWgMyJaHgJMSkqODNISfSGkvuEtAhNiCXigSVUWcdMGEps+B8p1Dx1FkkAn3+1aLr2g5prG5Q6dpTy6RP0OH+Wcsf7ZWE2ZAxF1uomLUE9kkU++9agJ/PfA+fDIHw1fkVFXAbpxC6pm74li2ggs2tvP4Xab2VP0Ysbl0iBcRBXE+6XSZX3++ay1P5h8TetbG2TAg5Pws2yqPJu/t9QudDKppKU732aMy2QBnKD/UofLiwNFJn0ro7n7VxGar+eGaJUnxZpgUlNHU+uUL4aYbby1QukicsG316KgBlriZ7j6tRaq05m+XQShCOq69KByKRxQOsleOAu9G5xmerI8u/mlnN2JjlAWRTZnWxGO1vOlJuCKXPyWh9rGfJDSXpwfpjum6dlbgtM+hof7oV5TKLt8TuMAiaeGjsXZp7l6v1IMRIyOGbiSz3M/IZJK3tZvCrdL2asn2Gvo5pwx001EGQvUa+YrgVmSIV3KK/hTwUiYrhIqb4j3UcOj3+elqEe+j14atT+rNLX1j+qQzfW1p2z7sbOPTc3OEAnoUdQwRcx2LmSu9riU2TZOa9FexmzxMHAEmFLmjikHemgiS2huwOL9qMui97A8Xp77x5ZV0Q0zEEKst81dMmpvWHwwOfPoz6zTe3/M9zZKt6aKcMO99Z7tbLZl9kAAAAASUVORK5CYII=&quot;);"></span>
                                        </div>
                                </div>
                                <div class="clearfix"></div>
                        </div>
                        <div class="monnify-main-content">
                                <div class="monnify-payment-data-bar">
                                        <div class="ribbon"><span class="ribbon-content">Active </span></div>
                                        <div class="merchant-logo-section"><span
                                                        style="background-image: url(assets/images/favicon.png);"></span>
                                        </div>
                                        <div class="total-payable-section"><label>Total Amount to be paid</label>
                                                <div class="amount">NGN <?php echo $row['naira']; ?></div>
                                        </div>
                                        <div class="amount-fee-section">
                                                <div>
                                                        <div>Amount: </div>
                                                        <div class="amount">NGN <?php echo $row['naira']; ?></div>
                                                </div>
                                                <div>
                                                        <div>Fee: </div>
                                                        <div class="amount">NGN 0.00</div>
                                                </div>
                                        </div>
                                </div>
                                <div class="content">
                                        <div class="monnify-flow">
                                                <div class="not-receipt-cont">
                                                        <div class="monnify-bank-transfer-flow monnify-flow-container">
                                                                <div class="loaded">
                                                                        <div class="info "><span class="info-icon"><svg
                                                                                                viewBox="64 64 896 896"
                                                                                                focusable="false"
                                                                                                class=""
                                                                                                data-icon="info-circle"
                                                                                                width="1em" height="1em"
                                                                                                fill="currentColor"
                                                                                                aria-hidden="true">
                                                                                                <path
                                                                                                        d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm32 664c0 4.4-3.6 8-8 8h-48c-4.4 0-8-3.6-8-8V456c0-4.4 3.6-8 8-8h48c4.4 0 8 3.6 8 8v272zm-32-344a48.01 48.01 0 010-96 48.01 48.01 0 010 96z">
                                                                                                </path>
                                                                                        </svg></span><span></span></div>
                                                                        <div class="instruction">Use your
                                                                                Internet/Mobile Banking platform from
                                                                                your bank to pay into this account
                                                                                number.</div>
                                                                        <div class="merchant-account-cont">
                                                                                <div class="ib-part">
                                                                                        <div
                                                                                                class="account-details-section">
                                                                                                <div
                                                                                                        class="account-number-section">
                                                                                                        <div
                                                                                                                class="title-bar">
                                                                                                                <span
                                                                                                                        class="label">Account
                                                                                                                        Number</span><span
                                                                                                                        class="timer-cont">
                                                                                                                        <div
                                                                                                                                class="monnify-timer">
                                                                                                                                <div
                                                                                                                                        class="timer fill">
                                                                                                                                </div>
                                                                                                                                <span
                                                                                                                                        class="timer-text">0:00</span>
                                                                                                                                <div
                                                                                                                                        class="clearfix">
                                                                                                                                </div>
                                                                                                                        </div>
                                                                                                                </span>
                                                                                                                <div
                                                                                                                        class="clearfix">
                                                                                                                </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                                class="account-number">
                                                                                                                <?php echo $row['paccountnum']; ?>
                                                                                                        </div>
                                                                                                </div>
                                                                                                <div
                                                                                                        class="account-bank-section">
                                                                                                        <div
                                                                                                                class="bank">
                                                                                                                <label
                                                                                                                        class="label">Bank</label>
                                                                                                                <div
                                                                                                                        class="value">
                                                                                                                       <?php echo $row['pbank']; ?>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                                class="account-name">
                                                                                                                <label
                                                                                                                        class="label">Account
                                                                                                                        Name</label>
                                                                                                                <div
                                                                                                                        class="value">
                                                                                                                        <?php echo $row['paccountname']; ?>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                                class="clearfix">
                                                                                                        </div>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                                <div class="ussd-part">
                                                                                        <div class="part-trigger">Pay
                                                                                                via USSD</div>
                                                                                </div>
                                                                        </div>
                                                                        <div class="action">
                                                                                <div class="monnify-button"><button
                                                                                                type="button"
                                                                                                class="gray-button"
                                                                                                disabled=""><span
                                                                                                        class="busy-loader"></span>
                                                                                                Waiting for
                                                                                                payment...</button>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="monnify-footer">Powered by <span class="monnify-logo">Treasure Capital</span></div>
                </div>
        </div>
   
        <script src="monnify_files/2.595b3f54.chunk.js"></script>
        <script src="monnify_files/main.e8b7048a.chunk.js"></script>
        <script src="assets/js/jquery-3.5.1.min.js"></script>
        <script src="assets/js/sweet-alert/sweetalert.min.js"></script>

        <script>

setInterval(() => {

var orderid ='<?php echo $orderid; ?>';

var settings = {
                    "url":"checkers?orderid="+orderid,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                };

                $.ajax(settings).done(function (response) {
                    console.log(response);

                    if(response.treasure.status==1){

                                        swal(
                      'Payment Successful',
                      "You have successfully made your purchase",
                      'success'
                  );

                  setTimeout(() => {
                    location ='mining';
                  }, 3000);

                    }


                });



}, 5000);


        </script>
</body>

</html>