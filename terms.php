<?php  
include('account/connection.php');

?>
<!DOCTYPE html>
<html lang="zxx">

<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- Title -->
	<title>Coin Magnetics - Terms</title>
	<!-- Favicon -->
	<link href="img/core-img/favicon.png" rel="icon" /><!-- Core Stylesheet -->
	<link href="css/style.css" rel="stylesheet" /><!-- Responsive Stylesheet -->
	<link href="css/responsive.css" rel="stylesheet" />
	<style>
@media (max-width: 768px) {
    .carousel-inner .carousel-item > div {
        display: none;
    }
    .carousel-inner .carousel-item > div:first-child {
        display: block;
    }
}

.carousel-inner .carousel-item.active,
.carousel-inner .carousel-item-next,
.carousel-inner .carousel-item-prev {
    display: flex;
}

/* display 3 */
@media (min-width: 768px) {
    
    .carousel-inner .carousel-item-right.active,
    .carousel-inner .carousel-item-next {
      transform: translateX(33.333%);
    }
    
    .carousel-inner .carousel-item-left.active, 
    .carousel-inner .carousel-item-prev {
      transform: translateX(-33.333%);
    }
}

.carousel-inner .carousel-item-right,
.carousel-inner .carousel-item-left{ 
  transform: translateX(0);
}


</style>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5dde42de43be710e1d1f5485/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</head>

<body class="light-version">
	<!-- Preloader -->
	<div id="preloader">
		<div class="preload-content">
			<div id="dream-load"></div>
		</div>
	</div>
	<!-- ##### Header Area Start ##### -->

<header class="header-area fadeInDown" data-wow-delay="0.2s">
        <div class="classy-nav-container light breakpoint-off">
            <div class="container">
                <!-- Classy Menu -->
                <nav class="classy-navbar light justify-content-between" id="dreamNav">

                    <!-- Logo -->
                    <a class="nav-brand light" href="#"><img src="./logo2.png" style="width: 150px; height: 30px;" /></a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler demo">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- close btn -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul id="nav">
                             <li><a href="https://coinmagnetics.com">Home</a></li>
								
								<li><a href="https://coinmagnetics.com/about">About</a></li>
								<li><a href="#services">Features</a></li>
								<li><a href="https://coinmagnetics.com/faq">FAQ</a></li>
								<li><a href="https://coinmagnetics.com/contact">Contact</a></li>
								<li>
								<style type="text/css">

a.gflag {vertical-align:middle;font-size:16px;padding:1px 0;background-repeat:no-repeat;background-image:url(//gtranslate.net/flags/16.png);}
a.gflag img {border:0;}
a.gflag:hover {background-image:url(//gtranslate.net/flags/16a.png);}
#goog-gt-tt {display:none !important;}
.goog-te-banner-frame {display:none !important;}
.goog-te-menu-value:hover {text-decoration:none !important;}
body {top:0 !important;}
#google_translate_element2 {display:none!important;}

</style>


<br /><select onchange="doGTranslate(this);"  ><option value="">Select Language</option><option value="en|en">English</option><option value="en|hy">Armenian</option><option value="en|az">Azerbaijani</option><option value="en|eu">Basque</option><option value="en|be">Belarusian</option><option value="en|ca">Catalan</option><option value="en|zh-CN">Chinese (Simplified)</option><option value="en|nl">Dutch</option><option value="en|fr">French</option><option value="en|de">German</option><option value="en|pt">Portuguese</option><option value="en|ru">Russian</option><option value="en|es">Spanish</option></select><div style="display:none" id="google_translate_element2"></div>
<script type="text/javascript">
function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'en',autoDisplay: false}, 'google_translate_element2');}
</script><script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>


<script type="text/javascript">
/* <![CDATA[ */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,f,f);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}',43,43,'||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'),0,{}))
/* ]]> */
</script>


								</li>
							</ul>
							<!-- Button --><a class="btn login-btn ml-50"
								href="https://coinmagnetics.com">Log in</a>
						</div>
						<!-- Nav End -->
					</div>
				</nav>
			</div>
		</div>
	</header>
	<!-- ##### Header Area End ##### -->

	<!-- ##### About Us Area Start ##### -->

	<section class="faq-timeline-area  section-padding-150-0 clearfix " id="about">
		<div class="container-fluid h-100">
		    				<div class="row h-100 align-items-center">
			<div class="row align-items-center">
				<div class="col-12 col-lg-5 offset-lg-1 col-sm-10 offset-sm-1">
					<div class="who-we-contant">
					    
					    <h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Terms Of Use<strong></h2>
					    
					    <h5 class="fadeInUp" data-wow-delay="0.3s">Know the rules for using Coin Magnetics.</h4>
					    
						<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s"
							style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Last updated: August 2021</span></div>

						<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>1. Acceptance of terms.<strong></h4>

						<h6 class="fadeInUp" data-wow-delay="0.3s">Thank you for choosing Coin Magnetics. The following 
						Terms and Conditions and the Privacy policy (together, the “Terms”) apply to any person that 
						registers for and/or opens a Coin Magnetics Account through Coinmagnetics.com or any associated mobile applications, 
						website, or APIs (together, the “Coin Magnetics Site”).</h6>
						
						<h6 class="fadeInUp" data-wow-delay="0.3s">The Terms constitute a legally binding agreement between you and Cypher Mobiles Ltd., 
						which is a company incorporated under the laws of Nigeria. For further information on Cypher Mobiles Ltd., 
						and its operating subsidiaries, please see the Company Information page on the Coin Magnetics website. 
						For the purposes of these Terms, any reference to “we” “us” “our” “Coin Magnetics” and/or any similar term shall be construed as reference to Cypher Mobiles Ltd.</h6>
						
						<h6 class="fadeInUp" data-wow-delay="0.3s">By registering for and opening a Coin Magnetics Account, 
						you unconditionally accept these Terms and agree to be bound by and act in accordance with them. 
						You also accept and agree that you are solely responsible for understanding and complying with all laws, 
						rules, regulations and requirements of the jurisdiction in which you live that may be applicable to 
						your use of the Coin Magnetics Site and/or your Coin Magnetics Account, including but not limited to, 
						those related to export or import activity, taxes or foreign currency transactions. Depending 
						on your country of residence, you may not be able to use all the functions of the Coin Magnetics Site.</h6>
						
						<h6 class="fadeInUp" data-wow-delay="0.3s">Please read these Terms carefully before using the Coin Magnetics Site 
						because they affect your legal rights and obligations.</h6>
						
						<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>2.	Amendment of terms<strong></h4>

						<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics may amend the Terms from time to time. 
						You should visit the Coin Magnetics website regularly to check when the Terms were last updated (as displayed at the top of this document) 
						and to review the current Terms. We will do our best to notify you of any amendments to the Terms that we 
						consider likely to materially affect your rights and obligations. Any such notice will be posted on the Coin Magnetics Site, 
						or sent by email to the address associated with your Coin Magnetics Account (see Electronic Communications).</h6>
						
						<h6 class="fadeInUp" data-wow-delay="0.3s">The continued use of your Coin Magnetics Account, after any amendment to these Terms, constitutes your acceptance of the Terms, as modified by such amendment. If you do not accept the Terms, or any amendment to them, you must immediately stop using the Coin Magnetics Site and your Coin Magnetics Account..</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>3.	Eligibility<strong></h4>

<h4 class="fadeInUp" data-wow-delay="0.3s">By opening a Coin Magnetics Account, you expressly warrant and represent that:</h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	You are 18 years of age or over and have full capacity to accept the Terms and enter any transaction available through the Coin Magnetics Site;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	You will not open, or attempt to open, a Coin Magnetics Account under any name except your own; or use your Coin Magnetics Account to carry out transactions on behalf of a third party;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	You will follow the Coin Magnetics Business account application process if you seek to open and use a 
Coin Magnetics Account for a non-individual legal entity (e.g. a company, trust or partnership);</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	You will not have more than one Coin Magnetics Account; use or access any Coin Magnetics Account 
other than your own; or assist any other person in obtaining unauthorized access to any Coin Magnetics Account;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	You will provide security for transactions carried out through your Coin Magnetics Account 
by ensuring there is sufficient Local Currency or cryptocurrency (as the case may be) in your Coin Magnetics Account in advance of any transaction;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	You will not allow or facilitate the deposit of Local Currency into your Coin Magnetics Account unless 
that deposit is made in accordance with the Deposit Requirements (see Deposits);</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	You will not allow or facilitate the withdrawal of Local Currency from your Coin Magnetics Wallet to a 
bank account held in the name of a third party (see Withdrawals);</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	You will not use your Coin Magnetics Account for or in relation to any illegal activity, any 
Prohibited Activity, in violation of any laws, statutes, ordinances or regulations, or in breach of Coin Magnetics’s Export Controls and Sanctions requirements;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Any capitalized terms used in this Eligibility section shall have the meaning given to them in these Terms. 
Reference in these Terms to “Local Currency” means fiat or government issued currency (e.g. GBP, EUR, ZAR).</h6>
						
						<h6 class="fadeInUp" data-wow-delay="0.3s">By opening a Coin Magnetics Account you accept and agree that Coin Magnetics may, 
						without further notice and in its sole discretion, terminate, suspend or restrict the account of any customer who uses, or who we 
						reasonably suspect may be using, the Coin Magnetics Site or any Coin Magnetics Account in a manner that is inconsistent with the letter or spirit of these Terms.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>4.	Risks.<strong></h4>

						<h6 class="fadeInUp" data-wow-delay="0.3s">Before using the Coin Magnetics Site, you should ensure that you understand 
						the risks involved in buying, selling or trading cryptocurrencies. Cryptocurrency markets can be volatile and prices can 
						fluctuate significantly, which could result in sudden and significant increases or decreases in the value of your assets. 
						This market volatility and other risks involved in buying, selling or trading cryptocurrencies are explained on the risk 
						warning page of the Coin Magnetics website. There may be additional risks not identified in these Terms or in the risk warning.</h6>
						
						<h6 class="fadeInUp" data-wow-delay="0.3s">You should carefully assess whether your financial situation and risk tolerance is 
						suitable for buying, selling or trading cryptocurrency. You accept and agree that you are solely responsible for any 
						decision to buy, sell, trade or otherwise hold or deal with cryptocurrency.</h6>
						
<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>5.	Electronic communications<strong></h4>

<h4 class="fadeInUp" data-wow-delay="0.3s">You accept and agree that:</h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	any communications, agreements, notices and/or any other documents (together “Communications”) 
relating to your Coin Magnetics Account or your use of Coin Magnetics’s services will be provided to you electronically by posting them on 
the Coin Magnetics Site, emailing them to the email address you have provided to us, or through any other form of electronic communication. 
You consent to receiving all Communications electronically;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	you will at all times have available to you the necessary hardware and software to receive, 
access and retain Communications sent to you electronically, including a device with an internet connection and a valid and accessible email address;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	you assume full responsibility for providing Coin Magnetics with a valid and 
accessible email address to which any Communications may be sent, and for ensuring that email address and any other contact 
information is kept up to date. Any Communication sent to the email address you have provided to us will be deemed to have 
been received by you. You can amend your contact information by signing-in to your Coin Magnetics Account and accessing the Settings page.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">You may at any time withdraw your consent to receiving Communications electronically by 
contacting support@Coinmagnetics.com. You acknowledge that failure to give, or withdrawing, consent to receiving 
Communications electronically puts the security of your Coin Magnetics Account at risk (see Account Security) 
and that Coin Magnetics reserves the right to close your Coin Magnetics Account in the event you fail to give, 
or withdraw, your consent to the receipt of Communications electronically.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>6.	Identity verification.<strong></h4>

						<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics implements and maintains the highest standards of Know Your Customer (“KYC”) processes and controls as part of our commitment to combating fraud and assisting in the prevention of money laundering and terrorist financing. While our industry is largely unregulated, Coin Magnetics voluntarily adheres to local and international compliance standards in relation to customer due diligence, as explained on the Compliance page of the Coin Magnetics website.</h6>
						
						<h6 class="fadeInUp" data-wow-delay="0.3s">To ensure we meet these standards, our customers are required to provide certain personal details and documents when opening a Coin Magnetics Account (“Identity Verification”). The nature and extent of the Identity Verification required will depend upon which of our supported countries you are resident in, and the deposit and withdrawal limits that you wish to apply to your Coin Magnetics Account. In certain circumstances, Coin Magnetics may also perform enhanced due diligence (“EDD”) procedures in relation to your Coin Magnetics Account. You accept and agree that you will remain subject to such procedures at all times.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics reserves the right to, at any time:</h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	restrict or suspend your Coin Magnetics Account when we, in our sole discretion, consider it necessary to carry out further Identity Verification and/or EDD; or</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	terminate your Coin Magnetics Account if you provide, or we suspect you have provided, false information or refuse to provide information we require for Identity Verification and/or EDD.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">You accept and agree that there may be delays in accessing your Coin Magnetics Account, 
or in carrying out transactions through your Coin Magnetics Account, while we undertake any Identity Verification and/or EDD procedures.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Retention of information.<strong></h6> 

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics is required to retain certain information and documentation obtained as part of the Identity 
Verification and EDD procedures. These requirements apply even when you have terminated your relationship with Coin Magnetics. 
We reserve the right to keep such information and documentation for the required period and you accept and agree that 
information and documentation you provide to Coin Magnetics may be retained by us, including following the closure of your Coin Magnetics Account.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>7.	The Coin Magnetics wallet<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">The Coin Magnetics Wallet allows you to send, receive and store cryptocurrency (together, “Coin Magnetics Wallet Transactions”).</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Supported Cryptocurrencies.<strong></h6> 

<h6 class="fadeInUp" data-wow-delay="0.3s">The Coin Magnetics Wallet is only available in relation to the cryptocurrencies that Coin Magnetics, in its sole discretion, decides to support (“Supported Cryptocurrency” or “Supported Cryptocurrencies”). Information about the Supported Cryptocurrencies can be found on the Supported Cryptocurrency page on the Coin Magnetics website. The Supported Cryptocurrencies may change from time to time. Under no circumstances should you attempt to carry out a Coin Magnetics Wallet Transaction in relation to a cryptocurrency other than a Supported Cryptocurrency. In particular, but without limiting the generality of the foregoing, you accept and agree that you will have no access, entitlement or claim:t.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	to any cryptocurrency sent to a receive address associated with your Coin Magnetics Wallet where that cryptocurrency is not a Supported Cryptocurrency; or</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	to any cryptocurrency sent to a receive address associated with your Coin Magnetics Wallet, where that receive address is associated with another cryptocurrency. For example, you will have no access, entitlement or claim to any Bitcoin Cash (BCH) sent to a Bitcoin (BTC) receive address.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Local Currency in your Coin Magnetics Wallet.<strong></h6> 

<h6 class="fadeInUp" data-wow-delay="0.3s">Local Currency deposited into your Coin Magnetics Wallet (see Deposits) is solely for 
(1) the purchase of Supported Cryptocurrencies and/or (2) Withdrawal to an approved bank account (see Withdrawals). 
In certain countries we may also allow you to purchase specific goods and/or services from selected third parties 
using your Local Currency balance. You accept and agree that any such purchase will be subject to these Terms.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Proceeds from the sale of Supported Cryptocurrencies will be credited 
to your Local Currency Coin Magnetics Wallet, less any applicable fees (as explained in Fees, and on the Fees 
and Features page of the Coin Magnetics website). Coin Magnetics does not pay interest on Local Currency 
or Supported Cryptocurrency balances held in your Coin Magnetics Wallet.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Transaction instructions.<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics will process Coin Magnetics Wallet Transactions according to your instructions. 
You accept and agree that Coin Magnetics does not:</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	guarantee the identity of any user, receiver, or other party to a 
Coin Magnetics Wallet Transaction. You are solely responsible for ensuring all transaction details are correct, 
and you should carefully verify all transaction information prior to submitting transaction instructions to Coin Magnetics;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	have any control over, or any liability in relation to, the delivery, 
quality or any other aspect of any goods or services that you may buy from or sell to any third party. 
Coin Magnetics shall not be responsible for, and will take no action in relation to, ensuring that 
any buyer or seller you transact with using your Coin Magnetics Wallet completes the relevant 
transaction or has the requisite authority to do so.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Receiving cryptocurrency.<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">You may receive Supported Cryptocurrency into your 
Coin Magnetics Wallet by providing the sender with a receive address generated in your Coin Magnetics Wallet. 
Your Coin Magnetics Wallet will only be credited with Supported Cryptocurrency sent to a receive address 
generated through your Coin Magnetics Wallet and associated with that Supported Cryptocurrency. 
For example, your Coin Magnetics Wallet will be credited with ETH when it is sent to an ETH receive 
address generated through your Coin Magnetics Wallet.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Funds received in error.<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">In the event you know, suspect, or should reasonably 
know or suspect, that any Local Currency or cryptocurrency has been credited to your Coin Magnetics 
Wallet (or to any bank account) in error, you must immediately notify Coin Magnetics of the error by 
submitting a ticket through the Help Centre. You accept and agree that you have no claim or 
entitlement to any Local Currency or cryptocurrency received in error, and must immediately 
return such funds in accordance with the instructions received from Coin Magnetics.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>8.	Deposits<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">Where your identity has been verified 
(in accordance with the Identity Verification requirements), you may deposit Local 
Currency into your Coin Magnetics Wallet by depositing funds into a Coin Magnetics bank 
account (a “Deposit”). The Coin Magnetics bank account into which you make a Deposit will 
be a segregated account used exclusively for the purpose of holding and processing customer funds. 
Subject to these Terms, you retain a right to any funds you deposit into the 
Coin Magnetics bank account until such time as your funds are used or withdrawn.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Details of the Coin Magnetics bank account into 
which you must make the Deposit will be provided to you by Coin Magnetics, and any such bank 
account will be held and operated by a Coin Magnetics operating entity in your country or region 
(for more information on these entities, see the Company Information page on the Coin Magnetics website). 
You accept and agree that any information you provide to Coin Magnetics may be shared with such 
local entity, which will process Local Currency transactions in accordance with Coin Magnetics’s instructions.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">The payment methods by which you may Deposit Local Currency will 
depend upon the country you are in, and are set out on the Fees and Features page of the Coin Magnetics website 
(for the purposes of these Terms, the “Supported Deposit Methods”). Where you have made a Deposit using a 
Supported Deposit Method, and have adhered to the Deposit Requirements (explained below), 
your Deposit will be allocated to your Coin Magnetics Wallet.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Due to legal and operational constraints, 
you must strictly adhere to the following Deposit requirements (the “Deposit Requirements”):</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	you may only make a Deposit from a Supported Deposit Method held in your own name. You must not allow any third party to make a Deposit into your Coin Magnetics Wallet;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	you may only Deposit Local Currency using a Supported Deposit Method compatible with the deposit methods available in your country or region;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	except in the case of a SEPA transfer, you may only make Deposits denominated in the Local Currency associated with your 
Coin Magnetics Wallet. For example, if you have a ZAR wallet, you must only make Deposits in South African Rand;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	where you make a Deposit using a credit or debit card, you authorise 
Coin Magnetics to facilitate the debiting of the funds from your bank account in order to complete the Deposit;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">o	you must not use cash or cheques to make a Deposit. Coin Magnetics reserves 
the right to refuse any Deposit made by cash or cheque. Should Coin Magnetics decide, in its sole discretion, to allow a 
Deposit by cash or cheque on an exceptional basis, the allocation of such Deposit will be delayed to allow fraud and 
compliance checks to take place, and a Cash Deposit Penalty (see the Fees and Features page of the Coin Magnetics website) 
may be deducted from it. Any Coin Magnetics Account that receives more than one cash or cheque deposit may, 
at Coin Magnetics’s sole discretion, be suspended indefinitely.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">You agree to abide by the Deposit Requirements and agree and accept 
that failure to do so may cause delay in or failure of any Deposit reflecting in your 
Coin Magnetics Wallet, and/or restriction, suspension or termination of your Coin Magnetics Account.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Allocation of deposits. .<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">When you request to make a Deposit through your Coin Magnetics Wallet, 
you will be provided with a Reference Number or Unique Deposit Amount. To permit timely and successful allocation 
of a Deposit to your Coin Magnetics Wallet, you must use the Reference Number or Unique Deposit 
Amount when you make a deposit using a Supported Deposit Method. Coin Magnetics will not be responsible 
for any delay in or non-allocation of Deposits to your Coin Magnetics Wallet where you fail to 
specify the required Reference Number or Unique Deposit Amount.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Deposits may take time to reflect in your Coin Magnetics Wallet 
due to bank processing times, operational processes, and/or failure by you to adhere to the Deposit Requirements. 
Under no circumstances will Coin Magnetics have any liability to you in relation to any delay in or 
failure of a Deposit reflecting in your Coin Magnetics Wallet.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics will make all reasonable efforts to allocate Deposits 
that do not meet the Deposit Requirements. However, any Deposits less than the Minimum Unallocated Deposit 
Amount that cannot be allocated within a one month period will be retained by Coin Magnetics and you accept 
and agree that you shall have no further claim or entitlement to such funds. The Minimum Unallocated Deposit 
Amount shall until otherwise determined by Coin Magnetics be USD10.00 or the equivalent in your Local Currency.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>9.	Withdrawals<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">Where you have added your bank account details to your 
Coin Magnetics Account, you may withdraw funds from your Coin Magnetics Wallet to your bank account 
(a “Withdrawal”). Coin Magnetics will process Withdrawals in accordance with your instructions. 
You are solely responsible for ensuring that any Withdrawal instructions provided to Coin Magnetics, 
including the relevant bank account number, are complete and correct and you accept and agree that 
under no circumstances will Coin Magnetics be under any obligation to reverse or amend any Withdrawal.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics will make all reasonable efforts to process 
Withdrawals each business day, in accordance with the Deposit and Withdrawal Schedule on the Coin Magnetics website, 
but you accept and agree that Coin Magnetics provides no guarantee in relation to the Withdrawal processing period.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Withdrawals to the bank account of any third party are expressly prohibited.<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">You agree not to make, or attempt to make, any Withdrawal of funds from your 
Coin Magnetics Wallet to the bank account of a third party and accept that any such Withdrawal may be 
refused and/or result in the restriction, suspension or termination of your Coin Magnetics Account.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>10.	Instant buy or sell<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics Wallet holders may use the Instant Buy or Sell 
service to buy or sell a chosen amount of Supported Cryptocurrency at the quoted exchange rate.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">In using the Instant Buy or Sell service, you accept and agree that:</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	When you make an Instant Buy or Sell order, Coin Magnetics will 
match your order with open orders on the Coin Magnetics Exchange or other cryptocurrency exchanges available to us. 
Coin Magnetics will facilitate the trade, in accordance with your instructions;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	Coin Magnetics is not acting as your broker, intermediary, agent, 
adviser or in any fiduciary capacity and no information or communication provided to you by Coin Magnetics 
in relation to an Instant Buy or Sell transaction will constitute advice;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	when you place an order to buy or sell Supported Cryptocurrency, 
we will provide you with a quoted exchange rate. Coin Magnetics is unable to, and will not, 
specify the percentage of the quoted exchange rate that reflects Coin Magnetics’s facilitation of 
the relevant trade, as it will vary with market conditions. The quoted exchange rate may differ from 
rates seen elsewhere on the Coin Magnetics site, or on external sources, and you acknowledge 
that the quoted exchange rate is the rate that will apply to your order;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	By proceeding with your Instant Buy or Sell transaction, 
you confirm that you accept the quoted exchange rate, including its components;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	a volume limit per transaction will apply to any Instant 
Buy or Sell order (see the Fees and Features page of the Coin Magnetics website); and.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	Coin Magnetics does not guarantee the availability of, 
or that there will be no delays or interruptions in or to, the Instant Buy or Sell service.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Before confirming any Instant Buy or Sell transaction, 
you must ensure that the amount of Supported Cryptocurrency to be bought or sold is correct 
and that you agree to the quoted exchange rate. Coin Magnetics cannot reverse an executed order.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics reserves the right to delay or refuse any 
Instant Buy or Sell transaction if, in our sole discretion, we perceive there to be a risk of fraud 
or illegal activity, or where we have reasonable grounds on which to suspect an error may have been made. 
Coin Magnetics shall have no liability to you in connection with any delay 
in or non-completion of an Instant Buy or Sell transaction</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>11.	Coin Magnetics exchange<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">The Coin Magnetics Exchange is an order book exchange 
platform for cryptocurrencies that is intended for use by customers with experience in and/or 
knowledge of similar platforms. Any person using the Coin Magnetics Exchange does so entirely at their own risk</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">The Coin Magnetics Exchange is not offered to customers in all jurisdictions. 
This section of the Terms applies to you if you access and/or use the Coin Magnetics Exchange. 
In accessing and/or using the Coin Magnetics Exchange for any purpose, you agree and accept that:</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	all trades executed on the Coin Magnetics Exchange are 
executed automatically, based on the parameters of your order instructions, and cannot be reversed. 
In the case of technical issues affecting Coin Magnetics’s internal systems, Coin Magnetics may, 
where possible and in its sole discretion, take steps to reverse or otherwise amend a trade;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	all trading on the Coin Magnetics Exchange is subject 
to Coin Magnetics’s fee structure, as set out on the Fees and Features page of the Coin Magnetics website. 
By using the Coin Magnetics Exchange, you accept and agree to all applicable fees;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	Coin Magnetics makes no guarantee that the 
Coin Magnetics Exchange will be available without interruption; that there will be no delays,
failures, errors, omissions or loss of transmitted information; or that any order 
will be executed, accepted, recorded, or remain open.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics may, at any time and 
in our sole discretion, refuse any trade, impose limits on the trade amount permitted, 
or impose any other conditions or restrictions upon your use of the Coin Magnetics Exchange that 
we deem necessary. We may, for example, limit the number of open orders that you may establish 
or restrict trades from certain locations. Coin Magnetics reserves the right to take such action without prior notice.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Special note for our Malaysian customers:<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics has implemented a circuit breaker on the 
Coin Magnetics Exchange operated in Malaysia (the “Circuit Breaker”). You agree and accept that:</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	In the event of significant volatility, a circuit 
breaker will be triggered. Any trades you make when the circuit breaker has been triggered 
will move to post-only mode until the volatility has passed.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	In post-only mode, you cannot submit market orders or limit orders.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	In post-only mode, you may cancel existing orders and submit post-only limit orders.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>12.	Fees<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">The fees applicable to transactions undertaken on the Coin Magnetics Site can be viewed on 
the Fees and Features page of the Coin Magnetics website. Coin Magnetics reserves the right to change our 
fees at any time and will update the Fees and Features page accordingly. In certain circumstances, 
and at our sole discretion, Coin Magnetics may notify selected customers of a specific fee change, 
where we consider a change to be of particular relevance to such customers.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Send and Receive fees.<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">You may be charged a fee to send Supported Cryptocurrency 
from your Coin Magnetics Wallet and/or to receive Supported Cryptocurrency into your Coin Magnetics Wallet. 
Fees will apply where Supported Cryptocurrency is sent to or received from a Supported 
Cryptocurrency address (e.g. a Bitcoin address); no fees will apply where Supported Cryptocurrency is 
sent to or received from a mobile telephone number or email address. Please see the Fees and 
Features page of the Coin Magnetics website for further information on send and receive fees.:</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Deposit and Withdrawal fees.<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">All Withdrawals from your Coin Magnetics Wallet will 
incur a fee. Such fees differ by country and are set out on the Fees and Features page of the 
Coin Magnetics website. Some Deposits made using a Supported Deposit Method may incur a fee, 
as set out on the Fees and Features page. Such fees will be displayed in your 
transaction history upon completion of the transaction.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Instant Buy and Sell fees.<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">The quoted exchange rate in relation to any 
buy or sell order placed using the Instant Buy and Sell service will include an amount 
to reflect Coin Magnetics’s facilitation of the relevant trade, the percentage of 
which will vary with market conditions (see Instant Buy or Sell).</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Trade fees.<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics applies a Maker / Taker fee 
structure for customers using the Coin Magnetics Exchange, which is explained in the 
Coin Magnetics Help Centre. Please consult the Fees and Features page of the 
Coin Magnetics website for further information on applicable Maker and Taker fees. 
Coin Magnetics will, at the time of any transaction on the Coin Magnetics Exchange, 
notify you of any fees that will apply to the transaction. By proceeding with any transaction, 
you accept and agree to the applicable fees. Such fees will also be displayed in your transaction 
history upon completion of the transaction.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>13.	Account security<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics takes security very seriously 
and the measures we have in place to protect your Coin Magnetics Account are explained 
on the Security page of the Coin Magnetics website. However, you are solely responsible for:</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	maintaining adequate security and control over 
your Coin Magnetics Account sign-in details, including but not limited to any passwords, 
personal identification numbers (PINs), API keys, or any other codes associated with your Coin Magnetics Account;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	enabling any additional security features available to you, 
including the activation of two factor authentication on your Coin Magnetics account (see the Coin Magnetics Help Centre);</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	keeping your contact details up to date 
so that you can receive any notices or alerts we may send to you in relation 
to security (see Electronic Communications);</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	maintaining security and control over 
the email mailbox, phone number and two-factor authentication applications or 
devices associated with your Coin Magnetics Account.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Failure to take the above measures, 
and any other security measures available to you, may result in unauthorized access to your 
Coin Magnetics Account and the loss or theft of any cryptocurrency and/or Local Currency 
balances held in your Coin Magnetics Wallet or any linked bank account(s) and/or saved credit or 
debit card(s). Coin Magnetics shall have no liability to you for or in connection with any unauthorized access to your Coin Magnetics Account, 
where such unauthorized access was due to no fault of Coin Magnetics, and/or any failure by you to act upon any notice or alert that we send to you.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">The security of your Coin Magnetics Account may be compromised, or 
interruption caused to it, by phishing, spoofing or other attack, computer viruses, spyware, 
scare ware, Trojan horses, worms or other malware that may affect your computer or other equipment. 
Coin Magnetics strongly recommends that you regularly use reputable virus screening and prevention 
software and remain alert to the fact that SMS, email services and search engines are vulnerable to spoofing and phishing attacks.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Care should be taken in reviewing messages purporting to originate from 
Coin Magnetics and, should you have any uncertainty regarding the authenticity of any communication, you should 
log in to your Coin Magnetics Account through the Coin Magnetics website (specifically, https://www.Coinmagnetics.com, 
and not any other domain name or website purporting to be, or to be related to, Coin Magnetics) to review any transactions or required actions.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">To the maximum extent permitted by applicable law, you accept and agree that you have full 
responsibility for all activity that occurs in or through your Coin Magnetics 
Account and accept all risks of any unauthorized or authorized access to your Coin Magnetics Account.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Private keys.<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics securely stores all Supported Cryptocurrency 
private keys (“Private Keys”) associated with any Coin Magnetics Account. You accept and agree that 
Coin Magnetics shall retain full ownership and control of the Private Keys associated with your 
Coin Magnetics Account and that you shall have no control of, access to, or the ability to use, 
such Private Keys. For example, but without limiting the generality of the foregoing, Coin Magnetics will not:</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	accept or adhere to any instruction to sign any data with a Private Key;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	give access to any funds associated with your private keys, other 
than those of the Supported Cryptocurrency associated with your cryptocurrency wallet;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	allow the creation of any receive addresses associated with a 
Private Key other than receive addresses created through your Coin Magnetics Wallet. 
Coin Magnetics will not credit to your Coin Magnetics Wallet any cryptocurrency associated with a 
Private Key other than where such funds have been received via a receive address generated through your Coin Magnetics Wallet.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>14.	Service availability<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">While we will do everything we can to provide continuous operations, 
Coin Magnetics does not provide any warranty in relation to the availability of the Coin Magnetics Site or your 
Coin Magnetics Account. Without limiting the generality of the foregoing, we do not guarantee continuous 
access to the Coin Magnetics Site or your Coin Magnetics Account and make no representation that the 
Coin Magnetics Site, Coin Magnetics API, your Coin Magnetics Account and/or any products or services 
offered therein will be available without interruption; or that there will be no delays, 
failures, errors, omissions or loss of transmitted information.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>15.	Transactions on Cryptocurrency networks<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">When you use your Coin Magnetics Account to send or receive 
cryptocurrency, the transaction must be confirmed and recorded in the public ledger associated with 
the relevant cryptocurrency network (e.g. the Bitcoin network or the Ethereum network). 
That cryptocurrency network is solely responsible for verifying and confirming any such transactions. 
Coin Magnetics cannot confirm, cancel or reverse transactions on a cryptocurrency network, 
other than confirming to you that the network has completed the transaction. You accept and agree that:</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	Once submitted to a cryptocurrency network, a transaction 
will be unconfirmed for a period of time pending sufficient confirmation of the transaction by the network. 
A transaction is not complete while it is in a pending state. Funds associated with transactions 
that are in a pending state will be designated accordingly, and will not be included in your 
Coin Magnetics Wallet balance or be available to you to conduct transactions;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	When you send cryptocurrency from your Coin Magnetics Account, 
you are authorizing us to submit your transaction request to the relevant cryptocurrency network. 
Once a transaction request has been submitted to the relevant cryptocurrency network, 
the network will automatically complete or reject the request and neither you or 
Coin Magnetics will be able to cancel or otherwise modify your transaction.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	Cryptocurrency networks are operated by decentralised 
networks of independent third parties. They are not owned, controlled or operated by Coin Magnetics 
so we cannot ensure that any transaction details you submit will be confirmed by the relevant 
cryptocurrency network. You agree that any transaction details you submit may not be completed, 
or may be substantially delayed, by the cryptocurrency network used to process the transaction.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Network protocols and operating rules.<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">The underlying software protocols that govern the operation 
of the Supported Cryptocurrencies are open source. Accordingly, anyone can use, copy, modify, 
and distribute them and Coin Magnetics has no ownership of or control over these protocols. 
By using the Coin Magnetics Site, you accept and agree that</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	Coin Magnetics is not responsible for the operation 
of any cryptocurrency network’s underlying software protocols and makes no guarantee as to their availability, security, or functionality;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	the underlying software protocols are subject to sudden changes 
in operating rules (known as “forks”), and that such forks may materially affect the value, function, 
and/or name of any cryptocurrency you store in your Coin Magnetics Account. Should a fork occur, 
Coin Magnetics may, with or without notice to you, temporarily suspend our operations and, in our sole discretion, 
decide whether or not to support either branch of the forked protocol entirely; and</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	in the event Coin Magnetics decides not to support a branch of a forked protocol, 
you will not be given access to the assets on that fork. Those assets will be securely held by Coin Magnetics and we will not buy or sell them</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>16.	Account inactivity<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">Local Currency deposited into your Coin Magnetics Wallet may only be used for the purchase 
of Supported Cryptocurrencies and/or Withdrawal to an approved bank account (see The Coin Magnetics Wallet). 
You must not use your Coin Magnetics Wallet for the purpose of storing Local Currency. Where any Local Currency 
in your Coin Magnetics Wallet is not used for a period exceeding three months, Coin Magnetics may charge and obtain 
from you an Inactivity Fee of USD2.00 per month (or the equivalent in your Local Currency), which shall be collected 
from the Local Currency held in your Coin Magnetics Wallet. Should we have reasonable grounds on which to suspect 
you are using your Coin Magnetics Wallet primarily for the purpose of storing Local Currency, 
Coin Magnetics reserves the right to restrict, suspend or terminate your Coin Magnetics Account.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	in the event Coin Magnetics decides not to support a branch of a forked protocol, 
you will not be given access to the assets on that fork. Those assets will be securely held by Coin Magnetics and we will not buy or sell them</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Unclaimed property.<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">In the event any funds are held in your Coin Magnetics Wallet(s) 
and Coin Magnetics has no record of any use of your Coin Magnetics Account for several years, and we are 
unable to contact you, the law in your jurisdiction may require us to report and deliver such funds to the 
relevant authorities as unclaimed property. To the extent permitted by applicable law, 
Coin Magnetics reserves the right to deduct a dormancy fee or other administrative charges from such unclaimed funds.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>17.	Closure of your account<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">You may close your Coin Magnetics Account by submitting a closure request 
via the Help Centre. Coin Magnetics will action such requests once (i) the sum of all Local Currency and cryptocurrency 
balances in your Coin Magnetics Wallet(s) is below the Minimum Closure Amount; and (ii) no transactions have taken 
place in your Coin Magnetics Account for a period of at least 30 days. Such requirements are designed to protect you 
from loss and Coin Magnetics will not action a closure request until they are satisfied.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">You accept and acknowledge that, once your Coin Magnetics Account is closed:</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	you will have no further access to it;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	Coin Magnetics will be under no obligation to notify you of, or 
provide to you, any cryptocurrency sent to any receive address associated with your Coin Magnetics Account; and</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	Coin Magnetics reserves the right (but has no obligation) to delete 
all of your information and account data stored on our servers, and also reserves the right to 
retain any information that is required for legal or operational reasons.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">If at the date of closure of your account any Local Currency or cryptocurrency 
remains in your Coin Magnetics Wallet(s) (i.e. funds amounting to less than the Minimum Closure Amount), 
you accept and agree that Coin Magnetics shall retain such funds and that you shall have no further claim to them. 
The Minimum Closure Amount shall be USD10.00 or the equivalent in your Local Currency</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">You accept and agree that Coin Magnetics shall not be liable to 
you or any third party in relation to the closure of your Coin Magnetics Account, the termination of access to 
your Coin Magnetics Account, or for the deletion of your information or Coin Magnetics Account data.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>18.	Restriction, suspension and termination<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics reserves the right to restrict, suspend or terminate your Coin Magnetics Account where</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	we reasonably suspect your Coin Magnetics Account to be the subject of an operational 
or other error, in which case we may be required to suspend access to your account until such time as the error is rectified;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	we reasonably suspect your Coin Magnetics Account has been or is 
being used in relation to any unlawful, fraudulent or Prohibited Activity, or in breach of these Terms;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	we reasonably suspect you or your Coin Magnetics Account is or has 
been associated with, or poses a high risk of, money laundering, financing of terrorism, fraud, or any other financial crime;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	we reasonably suspect you of taking any action that 
Coin Magnetics considers to be a circumvention of Coin Magnetics’s controls, including but not limited to opening multiple Coin Magnetics Accounts;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	we reasonably suspect your involvement in any attempt to gain unauthorized access to any Coin Magnetics Account;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	your Coin Magnetics Account is or appears to be the subject of any legal, 
regulatory or government process and/or we, in our sole discretion, consider there to be a heightened risk of 
legal or regulatory non-compliance associated with your Coin Magnetics Account;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	we are compelled to do so by a prima facie valid subpoena, 
court order, or other binding order of a government or regulatory authority; or</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	your name appears on a government or international body sanctions list.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics will make all reasonable efforts to provide you with notice of 
any decision to restrict, suspend or terminate your Coin Magnetics Account, unless we are prevented from doing so by any 
legal or regulatory process or requirement, or where doing so may compromise Coin Magnetics’s security and/or risk 
management procedures. You accept and agree that Coin Magnetics is under no obligation to disclose to you the fact 
of or reason for any decision to restrict, suspend or terminate your Coin Magnetics Account, and shall have no 
liability to you in connection with the restriction, suspension or termination of your Coin Magnetics Account.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Unclaimed property.<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	Where Coin Magnetics elects to terminate your Coin Magnetics 
Account in accordance with the above, we shall (except where legally obligated not to) return your 
available cryptocurrency and/or Local Currency balances to you pursuant to the procedure set out below:</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>>>	Cryptocurrency:<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">If you have an available balance of Supported 
Cryptocurrency in your Coin Magnetics Account above the Minimum Threshold, we will liquidate the cryptocurrency 
and deposit the value realised from the liquidation into your Local Currency wallet. The Minimum 
Threshold is USD10.00, or its equivalent in any applicable Supported Cryptocurrency calculated on the 
day on which the liquidation is performed. We will not send your Supported Cryptocurrency to an alternative 
cryptocurrency wallet address. Any risk of a negative exchange rate fluctuation shall rest with you and you 
shall have no claim against Coin Magnetics for any losses you may suffer as a result of the 
liquidation of your available balance of Supported Cryptocurrency.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>>>	Local Currency:<strong></h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">We shall return to you an amount no greater than the cumulative 
value of all Local Currency Deposits to your Coin Magnetics Account, where applicable. Payment shall only 
be made to you following submission to us of valid bank account details in your name. We shall not transfer 
Local Currency to any bank account in the name of a third party.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>19.	Use of Coin Magnetics API<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">This section 19 applies to any use of Coin Magnetics’s 
Application Programming Interface and any associated documentation or materials 
(together, the “Coin Magnetics API”, see the Coin Magnetics API page of the Coin Magnetics website). 
Any use of the Coin Magnetics API is subject to this section and, for the avoidance of doubt, the Terms in their entirety.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Any use of the Coin Magnetics API is entirely at your own risk. 
It is your responsibility to keep any API keys, passwords or other credentials relating to your use of 
the Coin Magnetics API secure, and you accept and agree that you take full responsibility 
for any and all requests made over the API, whether authorized by you or not.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics sets and enforces limits on your use of 
the Coin Magnetics API (e.g. limiting the number of requests per second). You accept and agree that 
any circumvention of, or attempt to circumvent, these limits, or any use of the Coin Magnetics API 
that may adversely affect other users of the Coin Magnetics Site, may result in Coin Magnetics, 
at its sole discretion, revoking your API access.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Where market data is provided to you through the Coin Magnetics API, 
you are not permitted to redistribute or sell such data without the prior written consent of Coin Magnetics.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>20.	Financial Advice<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">Under no circumstances does any information contained on the Coin Magnetics Site, 
or provided to you through your Coin Magnetics Account or by any employee, agent or 
affiliate of Coin Magnetics, constitute financial, investment or other professional advice.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">You are solely responsible for any decision to store, buy or sell cryptocurrency, 
and such decision should take into account your risk tolerance and financial circumstances. 
For more information on the risks involved, see Risks and the Risk Warning on the Coin Magnetics website. 
You should consult your legal or tax professional in relation to your specific situation.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>21.	Taxes<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">You are solely responsible for determining whether, and to what extent, 
any taxes apply to any transactions you carry out through your Coin Magnetics Account, and for withholding, 
collecting, reporting and remitting the correct amounts of tax to the appropriate tax authorities.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>22.	Export controls and sanctions<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">Your use of the Coin Magnetics Site is subject to international 
export controls and economic sanctions requirements. By undertaking any activity on the Coin Magnetics Site 
or through your Coin Magnetics Account, including but not limited to sending, receiving, buying, selling, 
storing or trading any cryptocurrency, you agree that you will at all times comply with those requirements. 
In particular, and without any limitation to the generality of the foregoing, you may not open, use, 
or have access to any Coin Magnetics Account if:</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	you are in or under the control of, or resident of, 
any country subject to United States embargo, UN sanctions, the HM Treasury financial sanctions regime, 
or if you are on the U.S. Treasury Department's Specially Designated Nationals List or the U.S. 
Commerce Department's Denied Persons List, Unverified List, Entity List, or HM Treasury's financial sanctions regime; or</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	you intend to supply any acquired or stored cryptocurrency, 
or transact with people or businesses operating in any country subject to United States embargo or HM Treasury's 
financial sanctions regime, or to a person on the Specially Designated Nationals List, Denied Persons List, 
Unverified List, Entity List, or HM Treasury's financial sanctions regime.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>23.	Prohibited activities<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">You must not use your Coin Magnetics Account 
to undertake any of the activities or categories of activity set out in this section (each a “Prohibited Activity”):</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	violation of any laws, statutes, ordinance or regulations;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	undertaking, facilitating or supporting criminal activity of any kind, 
including but not limited to, money laundering, terrorist financing, illegal gambling operations or malicious hacking;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	abusive activity, including but not limited to:</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">>>	imposing an unreasonable or disproportionately large load on 
Coin Magnetics’s infrastructure, or otherwise taking any action that may negatively affect the performance 
of the Coin Magnetics Site or Coin Magnetics’s reputation;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">>>	attempting to gain unauthorized access to the Coin Magnetics Site or any Coin Magnetics Account;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">>>	transmitting or uploading any material to the Coin Magnetics Site 
that contains viruses, Trojan horses, worms, or any other harmful programs; or</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">>>	transferring your Coin Magnetics Account access or rights to your 
Coin Magnetics Account to a third party, unless as required by law or with Coin Magnetics’s prior consent.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	paying in to or otherwise supporting pyramid schemes, Ponzi schemes, 
matrix programs, “get rich quick” schemes, multi-level marketing programs or high-yield investment programs;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	fraudulent activity, including but not limited to taking any actions 
that defraud Coin Magnetics or a Coin Magnetics customer, or the provision of any false, inaccurate, or misleading information to Coin Magnetics;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	transactions involving items that may help facilitate or enable 
illegal activity; promote or facilitate hate, violence or racial intolerance; are considered obscene; or may be stolen goods or the proceeds of crime;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	transactions involving TOR markets, online gambling sites or mixers;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	sale or purchase of narcotics or controlled substances;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	intellectual property infringement.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">By opening a Coin Magnetics Account, you confirm that you will not use your account to 
undertake any of the above-listed Prohibited Activities or any similar or related activity.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Activities subject to the prior written approval of Coin Magnetics.<strong></h6>
<h6 class="fadeInUp" data-wow-delay="0.3s">Unless you have obtained the prior written approval 
of Coin Magnetics, you accept and agree that you will not use your Coin Magnetics Account to conduct 
or operate any of the following business activities or categories of activity:</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	money services, including but not limited to money or cryptocurrency transmission, currency or 
cryptocurrency exchange or dealing, payment service providers, e-money or any other financial services business;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	gambling or gaming services;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	charitable or religious / spiritual organizations;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	consumer lending services, including but not limited to secured and unsecured loans, cash advances, payday lending;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	investment funds, asset management, or brokerage services.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">We reserve the right to restrict, suspend or terminate your Coin Magnetics Account if we suspect, 
in our sole discretion, that you are using, or have used, your Coin Magnetics Account in association
with any of the activities listed above, or any similar or related activity, without having obtained the prior written approval of Coin Magnetics.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>24.	Disclaimer of warranties<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">The Coin Magnetics Site, your Coin Magnetics Account and any related products or services are offered on a 
strictly “as-is” and “where-available” basis and Coin Magnetics expressly disclaims, and you waive, all 
warranties of any kind, whether express or implied. Without limiting the generality of the foregoing, the 
Coin Magnetics Site, your Coin Magnetics Account, and any related products or services are offered without 
any warranty as to merchantability or fitness for any particular purpose.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">Some jurisdictions do not allow the disclaimer of implied terms in consumer contracts, 
so some or all of the disclaimers in this section may not apply to you.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>25.	Limitation of liability<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">In no event shall Coin Magnetics, its operating entities or any other affiliates 
(including their respective directors, members, employees or agents) be liable to you for 
any direct, indirect, special, consequential, exemplary or punitive damages or any other 
damages of any kind, including but not limited to loss of profit, loss of revenue, loss of business, 
loss of opportunity, loss of data, whether in contract, tort or otherwise, arising out of or in any way 
connected with your use of, inability to use, or unavailability of the Coin Magnetics Site and/or 
your Coin Magnetics Account, including without limitation any damages caused by or resulting from any 
reliance upon any information received from Coin Magnetics, or that result from mistakes, omissions, 
interruptions, deletion of files or email, errors, defects, viruses, delays in operation or transmission or 
any failure of performance, whether or not resulting from a force majeure event, communications failure, theft, 
destruction or unauthorized access to Coin Magnetics’s records, programs or services.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">In no event will any liability of Coin Magnetics, its operating entities or any other affiliates 
(including their respective directors, members, employees or agents) arising in relation to your 
use of the Coin Magnetics Site or your Coin Magnetics Account, exceed (in aggregate) the fees 
earned by Coin Magnetics in connection with your use of your Coin Magnetics Account in the six 
month period immediately preceding the event giving rise to the claim for liability.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">The above limitations of liability shall apply to the fullest extent permitted by law in the applicable 
jurisdiction. Because some jurisdictions do not allow the exclusion of certain warranties or the limitation 
or exclusion of liability for incidental or consequential damages, some of the limitations in this section may not apply to you.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>26.	Indemnity<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">To the maximum extent permitted by law, you agree to indemnify Coin Magnetics, its operating 
entities or any other affiliates (including their respective directors, members, employees and/or agents) 
against any action, liability, cost, claim, loss, damage, proceeding or expense suffered or incurred directly or 
indirectly arising from your use of or conduct in relation to the Coin Magnetics Site and/or your 
Coin Magnetics Account, or from your violation of these Terms.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>27.	Repeat-Buy<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">You may initiate recurring transactions to or from your Coin Magnetics Wallet(s) (“Repeat-Buy”). 
If you initiate a Repeat-Buy, you acknowledge and undertake that:

<h6 class="fadeInUp" data-wow-delay="0.3s">•	despite any quoted price, your Repeat-Buy transaction will be executed at the prevailing market price 
offered on the Coin Magnetics Exchange at the time that we execute the relevant Repeat-Buy order;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	you authorize us to initiate recurring electronic payments to and from your Coin Magnetics 
Wallet in accordance with your selected Supported Cryptocurrency and any corresponding Supported Deposit Method, 
such as debiting of the funds from your bank account or other payment account. your Repeat-Buy transaction will 
occur in identical, periodic installments, based on your selection (e.g. daily, weekly, monthly), 
until either you or Coin Magnetics cancels the Repeat Buy;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	where you have set up Repeat-Buy to execute against your Coin Magnetics Wallet, the Repeat-Buy 
transaction will be executed against the positive balance of available funds held in your applicable 
Coin Magnetics Wallet (i.e. this does not include funds which are related to any open orders on the 
Coin Magnetics exchange) (“Available Funds”) . You must ensure that you have sufficient Available Funds in your 
Coin Magnetics Wallet to pay for your Repeat-Buy on the scheduled payment date;</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	if any Repeat-Buy fails 3 (three) consecutive times, then we shall 
have the right to cancel or pause your Repeat-Buy; and</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">•	we may charge you a Repeat-Buy fee which will be charged in addition to 
any other fees and costs listed on our fees and features page</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>28.	Disputes<strong></h4>

<h6 class="fadeInUp" data-wow-delay="0.3s">You and we agree to notify the other party in writing of any claim or dispute that arises in relation to the 
Coin Magnetics Site, your Coin Magnetics Account or these Terms, within 30 days of such claim or dispute arising. 
You and we further agree to attempt informal resolution of any Dispute prior to bringing a claim in any court or other body.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Governing law and jurisdiction.<strong></h6> 

<h6 class="fadeInUp" data-wow-delay="0.3s">This agreement shall be governed by and construed in accordance with Singapore law, subject to any 
local mandatory rights you may have. You and we agree to submit all disputes, claims or controversies 
(including non-contractual Disputes, claims or controversies) arising out of or in connection with these Terms, 
or the breach, termination, enforcement or interpretation thereof (together, Disputes), 
o the non-exclusive jurisdiction of the courts of Singapore.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Class or representative action waiver.<strong></h6> 

<h6 class="fadeInUp" data-wow-delay="0.3s">To the maximum extent permissible by law, you and Coin Magnetics each agree that each may bring any 
Dispute against the other only in your or its individual capacity, and you and it waive any right to 
commence or participate in any class action or other representative action or proceeding against the other. 
Further, where permissible by law, unless both you and Coin Magnetics agree otherwise, no court may 
consolidate any other person’s claim(s) with your Dispute, and may not otherwise preside over any form of representative or class proceeding.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">For the avoidance of doubt, if this Class or representative action waiver is found by any 
court of competent jurisdiction to be invalid, void or unenforceable, the remainder of this Disputes clause shall remain valid and enforceable.</h6>

<h4 class="fadeInUp" data-wow-delay="0.3s"><strong>29.	Miscellaneous<strong></h4> 

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Entire agreement.<strong></h6> 

<h6 class="fadeInUp" data-wow-delay="0.3s">These Terms constitute the entire agreement and understanding between you and Coin Magnetics with respect to their 
subject matter and supersede any and all prior discussions, agreements and understandings of any kind between you 
and Coin Magnetics (including but not limited to any prior versions of these Terms).</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Severability.<strong></h6> 

<h6 class="fadeInUp" data-wow-delay="0.3s">If any provision of these Terms, as amended from time to time, is determined to be illegal, 
invalid or unenforceable, in whole or in part, under any law, such provision or part thereof shall to 
that extent be deemed not to form part of these Terms but the legality, validity and enforceability of the 
other provisions in these Terms shall not be affected and everything else in these Terms will continue in full force and effect.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s">In the event any provision or part thereof of these Terms is determined to be illegal, invalid or unenforceable, 
that provision or part thereof shall be replaced by the parties with a legal, valid and enforceable provision 
or part thereof that has, to the greatest extent possible, a similar effect as the illegal, invalid or 
unenforceable provision, given the content and purpose of these Terms.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Assignment.<strong></h6> 

<h6 class="fadeInUp" data-wow-delay="0.3s">You may not assign or transfer any of your rights or obligations under these Terms without Coin Magnetics’s 
prior written approval. You give Coin Magnetics your approval to assign or transfer these Terms in whole or in part, 
including but not limited to: (i) a subsidiary or affiliate; (ii) an acquirer of Coin Magnetics’s equity, 
business or assets; or (iii) a successor by merger.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Change of control.<strong></h6>  

<h6 class="fadeInUp" data-wow-delay="0.3s">In the event that Coin Magnetics is acquired by or merged with a third party, we reserve the right 
to transfer or assign the information we have collected from you as part of such merger, acquisition, sale, or other change of control.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Force Majeure.<strong></h6>  

<h6 class="fadeInUp" data-wow-delay="0.3s">Coin Magnetics shall not be liable for any delays, failure in performance or interruption of service 
which result directly or indirectly from any cause or condition beyond our reasonable control, including but not 
limited to, any delay or failure due to any act of God, act of civil or military authorities, act of terrorists, 
civil disturbance, war, strike or other labour dispute, fire, interruption in telecommunications or Internet 
services or network provider services, failure of equipment and/or software, other 
catastrophe or any other occurrence which is beyond our reasonable control.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Survival.<strong></h6> 

<h6 class="fadeInUp" data-wow-delay="0.3s">All provisions of these Terms that by their nature extend beyond the expiry or termination of 
these Terms, including but not limited to, sections relating to the suspension or termination of your 
Coin Magnetics Account, use of the Coin Magnetics Site, disputes with Coin Magnetics and general provisions, 
shall survive the termination of these Terms.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>Headings.<strong></h6> 

<h6 class="fadeInUp" data-wow-delay="0.3s">Section headings in these Terms are for convenience only, and 
shall not govern the meaning or interpretation of any provision of these Terms.</h6>

<h6 class="fadeInUp" data-wow-delay="0.3s"><strong>English language controls.<strong></h6> 

<h6 class="fadeInUp" data-wow-delay="0.3s">Notwithstanding any other provision of these Terms, any translation is provided solely for your 
convenience. The meanings of terms, conditions and representations herein are subject to definitions and 
interpretation in the English language. Any translation provided may not accurately represent the information in the original English.</h6>




					</div>
				</div>
	</section>

	<div class="clearfix"></div>
	<!-- ##### Our Trial Area End ##### -->

	<section class="about-us-area  section-padding-100-0 gradient-section-bg clearfix " id="affiliate">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-12 col-lg-5 offset-lg-1 col-sm-10 offset-sm-1">
					<div class="who-we-contant">
						<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s"
							style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
							<span>Get Started</span></div>

						<h4 class="fadeInUp" data-wow-delay="0.3s">It’s never too late to get started</h4>

						<p class="text-white">Trade, Buy, Sell, Swap, Mine and Save All Digtal Assets</p>
							
						<div class="clearfix"></div>
						<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><href="#home"><img alt="" src="./ic_website_sign_up.svg" style="width: 120px; height: 25px;" /></a>
							
								<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-google.svg" style="width: 120px; height: 25px;" /></a>
							
							<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-apple.svg" style="width: 120px; height: 25px;" /></a>
					</div>
				</div>

				<div class="col-12 col-lg-6 mt-s no-padding-right">
					<div class="welcome-meter fadeInUp" data-wow-delay="0.7s"><img alt=""
							class="img-responsive center-block" src="img/core-img/trading1.png" style="width: 460px; height: 260px;" /></div>
				</div>
			</div>
		</div>

			<!-- ##### Contact Area End ##### -->

			<div class="footer-content-area ">
				<div class="container">
					<div class="row ">
						<div class="col-12 col-lg-4 col-md-6">
							<div class="footer-copywrite-info">
								<!-- Copywrite -->
								<div class="copywrite_text fadeInUp" data-wow-delay="0.2s">
									<div class="footer-logo"><a href="#"><img alt="logo" src="./logo2.png"
												style="width: 230px; height: 40px;" /></a></div>

									<p>Plug-in to World's N0 1 Digital Asset management system today and Start Earning Big!.
									</p>
								</div>
								<!-- Social Icon -->

								<div class="footer-social-info fadeInUp" data-wow-delay="0.4s"></div>
							</div>
						</div>

						<div class="col-12 col-lg-3 col-md-6">
							<div class="contact_info_area d-sm-flex justify-content-between">
								<!-- Content Info -->
								<div class="contact_info mt-x text-center fadeInUp" data-wow-delay="0.3s">
									<h5>PRIVACY &amp; TOS</h5>

									<p><a href="#">Advertiser Agreement</a></p>

									<p><a href="https://coinmagnetics.com/terms">Terms Of Use</a></p>

									<p><a href="https://coinmagnetics.com/Privacy-policy">Privacy Policy</a></p>

									<p><a href="https://coinmagnetics.com/compliance">Legal & Compliance</a></p>

									<p><a href="https://coinmagnetics.com/Customer-pro">Customer protection</a></p>
								</div>
							</div>
						</div>

						<div class="col-12 col-lg-2 col-md-6 ">
							<!-- Content Info -->
							<div class="contact_info_area d-sm-flex justify-content-between">
								<div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.2s">
									<h5>NAVIGATE</h5>

									<p><a href="https://coinmagnetics.com/about">About Us</a></p>

									<p><a href="#">Developers</a></p>

									<p><a href="#">Resources</a></p>

									<p><a href="https://coinmagnetics.com/faq">FAQ</a></p>

									<p><a href="https://coinmagnetics.com/contact">Contact Us</a></p>
								</div>
							</div>
						</div>

						<div class="col-12 col-lg-3 col-md-6 ">
							<div class="contact_info_area d-sm-flex justify-content-between">
								<!-- Content Info -->
								<div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.4s">
									<h5>FIND US HERE</h5>

									<p>Mailing Address:265 Akowonjo RD,</p>

									<p>Lagos, Nigeria. Los 23401</p>

									
									<p>Email: support@coinmagnetics.com</p>
									
									<div class="dream-btn-group fadeInUp" data-wow-delay="0.4s"><a
									href="https://t.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/tg.png"
										style="width: 43px; height: 40px; float: left;" /></a>&nbsp; &nbsp;&nbsp;
								&nbsp;<a href="https://www.instagram.com/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/ig.png" style="width: 42px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp; &nbsp;<a href="https://twitter.com/Coinmagnetics"><img alt=""
										src="/img/social/tw.png" style="width: 43px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp;&nbsp;<a href="https://fb.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/fb.png" style="width: 43px; height: 40px;" /></a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- ##### Footer Area End ##### -->
		<!-- ########## All JS ########## -->
		<!-- jQuery js -->
		<script src="js/jquery.min.js"></script><!-- Popper js -->
		<script src="js/popper.min.js"></script><!-- Bootstrap js -->
		<script src="js/bootstrap.min.js"></script><!-- All Plugins js -->
		<script src="js/plugins.js"></script><!-- Parallax js -->
		<script src="js/dzsparallaxer.js"></script>
		<script src="js/jquery.syotimer.min.js"></script><!-- script js -->
		<script src="js/script.js"></script>
	</div>
</body>
<script>
$('#recipeCarousel').carousel({
  interval: 4000
})

$('.carousel .carousel-item').each(function(){
    var minPerSlide = 3;
    var next = $(this).next();
    if (!next.length) {
    next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
    
    for (var i=0;i<minPerSlide;i++) {
        next=next.next();
        if (!next.length) {
        	next = $(this).siblings(':first');
      	}
        
        next.children(':first-child').clone().appendTo($(this));
      }
});

	</script>

</html>