<?php  
include('connection.php');

?>
<!DOCTYPE html>
<html lang="zxx">

<head>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- Title -->
	<title>Coin Magnetics - Buy, Sell, Swap</title>
	<!-- Favicon -->
	<link href="img/core-img/favicon.png" rel="icon" /><!-- Core Stylesheet -->
	<link href="css/style.css" rel="stylesheet" /><!-- Responsive Stylesheet -->
	<link href="css/responsive.css" rel="stylesheet" />
	<style>
@media (max-width: 768px) {
    .carousel-inner .carousel-item > div {
        display: none;
    }
    .carousel-inner .carousel-item > div:first-child {
        display: block;
    }
}

.carousel-inner .carousel-item.active,
.carousel-inner .carousel-item-next,
.carousel-inner .carousel-item-prev {
    display: flex;
}

/* display 3 */
@media (min-width: 768px) {
    
    .carousel-inner .carousel-item-right.active,
    .carousel-inner .carousel-item-next {
      transform: translateX(33.333%);
    }
    
    .carousel-inner .carousel-item-left.active, 
    .carousel-inner .carousel-item-prev {
      transform: translateX(-33.333%);
    }
}

.carousel-inner .carousel-item-right,
.carousel-inner .carousel-item-left{ 
  transform: translateX(0);
}


</style>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5dde42de43be710e1d1f5485/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

</head>

<body class="light-version">
	<!-- Preloader -->
	<div id="preloader">
		<div class="preload-content">
			<div id="dream-load"></div>
		</div>
	</div>
	<!-- ##### Header Area Start ##### -->

<header class="header-area fadeInDown" data-wow-delay="0.2s">
        <div class="classy-nav-container light breakpoint-off">
            <div class="container-fluid"
                <!-- Classy Menu -->
                <nav class="classy-navbar light justify-content-between" id="dreamNav">

                    <!-- Logo -->
                    <a class="nav-brand light" href="#"><img src="./logo2.png" style="width: 150px; height: 30px;" /></a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler demo">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- close btn -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul id="nav">
                             <li><a href="https://coinmagnetics.com">Home</a></li>
								
								<li><a href="#services">Instant Buy/Sell</a></li>
								<li><a href="#services">Trade</a></li>
								<li><a href="https://coinmagnetics.com/faq">FAQ</a></li>
								<li><a href="https://coinmagnetics.com/about">About</a></li>
								<li><a href="https://coinmagnetics.com/contact">Contact</a></li>
								<li>
								<style type="text/css">

a.gflag {vertical-align:middle;font-size:16px;padding:1px 0;background-repeat:no-repeat;background-image:url(//gtranslate.net/flags/16.png);}
a.gflag img {border:0;}
a.gflag:hover {background-image:url(//gtranslate.net/flags/16a.png);}
#goog-gt-tt {display:none !important;}
.goog-te-banner-frame {display:none !important;}
.goog-te-menu-value:hover {text-decoration:none !important;}
body {top:0 !important;}
#google_translate_element2 {display:none!important;}

</style>


<br /><select onchange="doGTranslate(this);"  ><option value="">Select Language</option><option value="en|en">English</option><option value="en|hy">Armenian</option><option value="en|az">Azerbaijani</option><option value="en|eu">Basque</option><option value="en|be">Belarusian</option><option value="en|ca">Catalan</option><option value="en|zh-CN">Chinese (Simplified)</option><option value="en|nl">Dutch</option><option value="en|fr">French</option><option value="en|de">German</option><option value="en|pt">Portuguese</option><option value="en|ru">Russian</option><option value="en|es">Spanish</option></select><div style="display:none" id="google_translate_element2"></div>
<script type="text/javascript">
function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'en',autoDisplay: false}, 'google_translate_element2');}
</script><script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>


<script type="text/javascript">
/* <![CDATA[ */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,f,f);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}',43,43,'||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'),0,{}))
/* ]]> */
</script>


								</li>
							</ul>
							<!-- Button --><a class="btn login-btn ml-50"
								href="https://Coinmagnetics.com/">Log in</a>
								
						</div>
						
						<!-- Nav End -->
					</div>
				</nav>
			</div>
		</div>
	</header>
	
	<!-- ##### Header Area End ##### -->
	<!-- ##### Welcome Area Start ##### -->

	<section class="welcome_area clearfix dzsparallaxer auto-init none fullwidth"
		data-options="{direction: &quot;normal&quot;}" id="home">
		<div class="divimage dzsparallaxer--target"
			style="width: 101%; height: 130%; background-image: url(img/bg-img/bg-5.png)"></div>
		<!-- Hero Content -->

		<div class="hero-content transparent">
			<!-- blip -->
			<div class="dream-blip blip1"></div>

			<div class="dream-blip blip2"></div>

			<div class="dream-blip blip3"></div>

			<div class="dream-blip blip4"></div>

			<div class="container h-100">
				<div class="row h-100 align-items-center">
					<!-- Welcome Content -->
					<div class="col-12 col-lg-6 col-md-12">
					<!--	<div class="welcome-content">
							<div class="promo-section">
								<h3 class="special-head dark">Manage your digital assets in one place</h3>
							</div> -->

							<h1 class="fadeInUp" data-wow-delay="0.2s">Buy, Sell, Swap, Save and Trade Cryptocurrency in Minutes</h1>

							<p class="text-white"><strong><span
										style="font-family:lucida sans unicode,lucida grande,sans-serif;"><span
											style="color:#99cc00;"><span style="font-size:18px;">
												Get Started with as little as 10 USD.</span></span></span></strong></p>

<ul class="list-marked">
                            <li class="text-white"><i class="fa fa-check"></i>Instant access to all Cryptocurrencies like BTC, ETH & more</li>
							<li class="text-white"><i class="fa fa-check"></i>Instant deposited and withdrawal with FAIT or Crypto</li>
							<li class="text-white"><i class="fa fa-check"></i>Earn free Crypto by inviting friends to Coin Magnetics</li>


							<div class="dream-btn-group fadeInUp" data-wow-delay="0.4s">
							    <a class="btn dream-btn mr-3"
									href="https://Coinmagnetics.com/"><img alt="" src="./ic_website_sign_up.svg" style="width: 125px; height: 30px;" /></a> <a
									class="btn dream-btn mr-3" href="https://Coinmagnetics.com/"><img alt="" src="./buy.png" style="width: 125px; height: 35px;" /></a></div>

						
			</div>
		</div>
	</section>
	<!-- ##### Welcome Area End ##### -->
	<!-- ##### Welcome Area End ##### -->
	
	<!-- ##### About Us Area Start ##### -->
	
	<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <div class="tradingview-widget-copyright"><a href="https://coinmagnetics.com" rel="noopener" target="_blank"><span class="blue-text">
  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
  {
  "symbols": [
    {
      "proName": "BITSTAMP:BTCUSD",
      "title": "BTC/USD"
    },
    {
      "proName": "BITSTAMP:ETHUSD",
      "title": "ETH/USD"
    },
    {
      "description": "Binance",
      "proName": "BINANCE:BNBBTC"
    },
    {
      "description": "Binance",
      "proName": "BINANCEUS:BNBUSD"
    },
    {
      "description": "Doge",
      "proName": "COINBASE:DOGEUSD"
    },
    {
      "description": "Bitcoin",
      "proName": "BINANCE:BTCNGN"
    }
  ],
  "showSymbolLogo": true,
  "colorTheme": "light",
  "isTransparent": true,
  "displayMode": "adaptive",
  "locale": "en"
}
  </script>
</div>

<!-- TradingView Widget END -->

	<section class="faq-timeline-area  section-padding-100-10 clearfix " id="about">
	    <section class="about-us-area section-padding-100-0 bub-right clearfix">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-12 col-lg-5 offset-lg-1 col-sm-10 offset-sm-1">
					<div class="who-we-contant">
						<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s"
							style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><span>Trade on the go</span></div>

						<h4 class="fadeInUp" data-wow-delay="0.3s">The world class cryptocurrency trading platform.</h4>

						<p class="fadeInUp" data-wow-delay="0.5s">Deposit, withdraw, and trade 24/7 on our web app or mobile apps for Android and iOS, so you can experience professional-level crypto exchange features on the move..</p>

						<div class="list-wrap align-items-center">
							<div class="row">
								<div class="col-md-6">
									<div class="side-feature-list-item"><img alt="" class="check-mark-icon"
											src="img/features/socket.svg" />
										<div class="foot-c-info">Instant Connect</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="side-feature-list-item"><img alt="" class="check-mark-icon"
											src="img/features/router.svg" />
										<div class="foot-c-info">State Of the Art Automated Technology</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="side-feature-list-item"><img alt="" class="check-mark-icon"
											src="img/features/feature-3.svg" />
										<div class="foot-c-info">Massive Revenue Generation</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="side-feature-list-item"><img alt="" class="check-mark-icon"
											src="img/features/money.svg" />
										<div class="foot-c-info">Currency Flexibility</div>
									</div>
								</div>
							</div>
						</div>

						<div class="clearfix"></div>
						<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><href="#home"><img alt="" src="./ic_website_sign_up.svg" style="width: 120px; height: 25px;" /></a>
							
								<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-google.svg" style="width: 120px; height: 25px;" /></a>
							
							<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-apple.svg" style="width: 120px; height: 25px;" /></a>
					</div>
				</div>

				<div class="col-12 col-lg-6 mt-s no-padding-right">
					<div class="welcome-meter fadeInUp" data-wow-delay="0.7s"><img alt=""
							class="img-responsive center-block" src="img/core-img/onthego.png" /></div>
				</div>
			</div>
		</div>
	</section>
	<!-- ##### About Us Area End ##### -->


	<!-- ##### Our Services Area Start ##### -->

<!--	<section class="section-padding-100 gradient-section-bg clearfix bub-right">
		<div class="container">
			<div class="section-heading text-center">
				<!-- Dream Dots -->
			<!--	<div class="dream-dots justify-content-center fadeInUp" data-wow-delay="0.2s"><img alt=""
						src="img/svg/section-icon-11.svg" /></div> -->

			<!--	<h2 class="fadeInUp" data-wow-delay="0.3s">Decentralized Liquidity Mining Ecosystem</h2> -->

			<!--	<p class="text-white">Coin Magnetics Liquidity Mining Made Easy, Take advantage of our DefI Liquidity mining algorithm powered by AMM and start earning big.
Stack your BTC, ETH, TRX, DOGE and earn Magnetics 
.</p>
			</div>

			<div class="row">
				<div class="col-12 col-md-12"><img alt="" class="img-responsive center-block"
						src="img/core-img/bg5-l.png" /></div>
			</div>
		</div>
	</section> -->
	<!-- ##### Our Services Area Start ##### -->

	<section class="faq-timeline-area section-padding-100-0 clearfix" id="services">
		<div class="container">
			<div class="section-heading text-center">
				<!-- Dream Dots -->
				<div class="dream-dots justify-content-center fadeInUp" data-wow-delay="0.2s"><span>Our Services</span>
				</div>

				<h2 class="fadeInUp" data-wow-delay="0.3s"><strong>Why Chose Coin Magnetics?</strong></h2>

				<p class="fadeInUp" data-wow-delay="0.4s">Coin Magnetics makes it easy for anyone anywhere to buy, sell, swap, save or trade cryptocurrencies securely and sefe.</p>
			</div>

			<div class="row">
				<div class="col-12 col-md-6 col-lg-3">
					<!-- Content -->
					<div class="service_single_content text-center mb-100 fadeInUp" data-wow-delay="0.2s">
						<!-- Icon -->
						<div class="service_icon"><img alt="" src="img/features/connect2.png" /></div>

						<h6>INSTANT BUY/SELL</h6>

						<p>It is very simple to get started! Register an account in just few clicks, buy or sell your first crypto on minutes.</p>
					</div>
				</div>

				<div class="col-12 col-md-6 col-lg-3">
					<!-- Content -->
					<div class="service_single_content text-center mb-100 fadeInUp" data-wow-delay="0.3s">
						<!-- Icon -->
						<div class="service_icon"><img alt="" src="img/features/witdraw.png" style="width: 220px; height: 120px;"/></div>

						<h6>INSTANT DEPOSIT/WITHDRAWAL</h6>

						<p>Withdraw or deposit from or to any
							desired local bank as FIAT or Crypto wallet anytime and get credited instantly.</p>
					</div>
				</div>

				<div class="col-12 col-md-6 col-lg-3">
					<!-- Content -->
					<div class="service_single_content text-center mb-100 fadeInUp" data-wow-delay="0.4s">
						<!-- Icon -->
						<div class="service_icon"><img alt="" src="img/features/currency2.png" /></div>

						<h6>CURRENCY FLEXIBILITY</h6>

						<p>Coin Magnetics accepts multiple payment gateways including local currency (FIAT) based on
							prevailing market value of that currency.</p>
					</div>
				</div>

				<div class="col-12 col-md-6 col-lg-3">
					<!-- Content -->
					<div class="service_single_content text-center mb-100 fadeInUp" data-wow-delay="0.5s">
						<!-- Icon -->
						<div class="service_icon"><img alt="" src="img/features/tech.png" style="width: 220px; height: 120px;"/></div>

						<h6>CUTTING EDGE TECHNOLOGY</h6>

						<p>Coin Magnetics is built on the latest 7th Generation Automated Technology with improved
							Automatic Market Maker (AMM) </p>
					</div>
				</div>

				
					</div>
				</div>
	</section>

	<div class="clearfix"></div>
	<!-- ##### Our Trial Area End ##### -->

	<section class="about-us-area  section-padding-100-10 gradient-section-bg clearfix " id="affiliate">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-12 col-lg-5 offset-lg-1 col-sm-10 offset-sm-1">
					<div class="who-we-contant">
						<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s"
							style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
							<span>Get Started</span></div>

						<h4 class="fadeInUp" data-wow-delay="0.3s">It’s never too late to get started</h4>

						<p class="text-white">Buy, Sell, Swap, Save and Trade Cryptocurrency in Minutes</p>
							
						<div class="clearfix"></div>
						<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><href="#home"><img alt="" src="./ic_website_sign_up.svg" style="width: 120px; height: 25px;" /></a>
							
								<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-google.svg" style="width: 120px; height: 25px;" /></a>
							
							<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-apple.svg" style="width: 120px; height: 25px;" /></a>
					</div>
				</div>

				<div class="col-12 col-lg-6 mt-s no-padding-right">
					<div class="welcome-meter fadeInUp" data-wow-delay="0.7s"><img alt=""
							class="img-responsive center-block" src="img/core-img/start.png" style="width: 760px; height: 360px;" /></div>
				</div>
			</div>
		</div>
	</div>
	</section>
	
<!-- ##### Testimony Area Start ##### -->

	<section class="features section-padding-100-0 bub-left">
		<div class="container">
			<div class="section-heading text-center">
				<!-- Dream Dots -->
				<div class="dream-dots justify-content-center fadeInUp" data-wow-delay="0.2s"
					style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
					<span>Testimonies</span></div>

				<h2 class="fadeInUp" data-wow-delay="0.3s"
					style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;"><strong>What Our Users
						Say</strong></h2>

				<p>At Coin Magnetics, everyone is a winner!</p>

				<div id="recipeCarousel" class="carousel slide "  data-ride="carousel" >
				<div class="carousel-inner row justify-content-center " >

				<?php  $gettest = mysqli_query($mysqli,"SELECT * FROM testimony  ORDER BY id DESC LIMIT 5");
             $i=0;
             while($test = mysqli_fetch_assoc($gettest) ){ $i++; ?>
					<!-- Single Blog Post -->
					<div class="carousel-item <?php if($i==1){ echo" active";} ?> ">
					<div class=" col-12 col-md-6 col-lg-4">
						<div class="single-blog-area fadeInUp" data-wow-delay="0.2s">

							<!-- Post Content -->
							<div class="blog-content">
								<!-- Dream Dots -->

								<a href="#" class="post-title">
									<h4><?php echo $test['title']; ?></h4>
								</a>
								<p><?php echo $test['info']; ?></p>
								<a href="#" class="btn dream-btn mt-15"><?php echo $test['username'] ?></a>
							</div>
						</div>
			 </div>
					</div>

					<?php } ?>
					<a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
				
	</div>
		</div>
	</section>
	
	<div class="clearfix"></div>
		<!-- ##### Testimony Area Ends ##### -->
		
	<!-- ##### Statuse Area Start ##### -->
<section class="about-us-area section-padding-100-0 gradient-section-bg bub-right clearfix">
    <div class="container h-100">
				<div class="row h-100 align-items-center">
			<div class="row align-items-center">
				<div class="col-lg-6 col-xs-12">
					<div class="welcome-meter fadeInUp" data-wow-delay="0.7s"><img alt="" class="center-block"
							src="img/svg/about-us-3.svg" /></div>
				</div>

				<div class="service-img-wrapper how col-lg-4 col-md-9 col-sm-12 mt-s no-padding-right">
					<div class="features-list">
						<div class="who-we-contant">
							<h5 class="fadeInUp" data-wow-delay="0.3s"
					style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;"><strong>Our customer is everything</strong></h5>

							<p class="w-text fadeInUp" data-wow-delay="0.4s">We are dedicated to providing professional service with the highest degree of transparency, honesty and integrity, and strive to add value to our task of being the best in what we do.</p>
						</div>
						<ul class="list-marked">
                            <li class="text-white"><i class="fa fa-check"></i>24/7 Customer support team</li>
							<li class="text-white"><i class="fa fa-check"></i>Competent professionals</li>
							<li class="text-white"><i class="fa fa-check"></i>Loyal to our customers</li>
							<li class="text-white"><i class="fa fa-check"></i>Communicates respectfully</li>
						</ul>
						<a class="btn dream-btn mt-30 fadeInUp" data-wow-delay="0.6s" href="#">Read More</a>
					</div>
				</div>

				<?php 
$getmine =mysqli_query($mysqli,"SELECT * FROM investment ");

$totalmine=0;
$totalrevenune =0;
while($r=mysqli_fetch_assoc($getmine)){
  $totalmine += $r['amount'];
  $totalrevenune +=$r['added_roi'];
}

$getusers =mysqli_query($mysqli,"SELECT * FROM users ");

$getwith =mysqli_query($mysqli,"SELECT * FROM withdrawal ");

?>
				<div class="col-xs-10 col-sm-12 col-md-3 col-lg-2 mt-s box-list no-padding-left no-padding-right">
					<div class="counter-boxed-warrper">
						<div class="counter-boxed bg-cello text-center">
							<div class="counter animated gradient-text"><?php echo $totalmine; ?></div>

							<div class="mt-10">
								<p class="text-bismark text-sbold">Total Order In USD</p>
							</div>
						</div>

						<div class="counter-boxed bg-cello text-center">
							<div class="counter animated gradient-text"><?php echo $totalmine*4135; ?></div>

							<div class="mt-10">
								<p class="text-bismark text-sbold">Total Completed In USD</p>
							</div>
						</div>

						<div class="counter-boxed bg-cello text-center">
							<div class="counter animated gradient-text"><?php echo mysqli_num_rows($getusers); ?></div>

							<div class="mt-10">
								<h6 class="fadeInUp" data-wow-delay="0.3s">Happy Users</h6>
							</div>
						</div>

						<div class="counter-boxed bg-cello text-center">
							<div class="counter animated gradient-text"><?php echo mysqli_num_rows($getwith); ?></div>

							<div class="mt-10">
								<h6 class="fadeInUp" data-wow-delay="0.3s">Total Pay Out</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="clearfix"></div>
	<!-- ##### Statuse Area End ##### -->
	
	
	<section class="features section-padding-100-0 bub-left">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-12 col-lg-5 offset-lg-1 col-sm-10 offset-sm-1">
					<div class="who-we-contant">
						<div class="dream-dots text-left fadeInUp" data-wow-delay="0.2s"
							style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
							<span>Affiliate Program</span></div>

						<h4 class="fadeInUp" data-wow-delay="0.3s">Earn More With Your Friends And Family</h4>

						<h6 class="fadeInUp" data-wow-delay="0.3s">Earn more when you refer a friend to Coin Magnetics.
							Introduce your friends and family and earn 0.5% of all their transactions.</h6>

						<h6 class="fadeInUp" data-wow-delay="0.3s">Refer 20 and above friends to Coin Magnetics and Become a 2 star stake holder
						with ability to earn 0.2% of all their second generation transactions.</h6>

											<div class="clearfix"></div>
						<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><href="#home"><img alt="" src="./ic_website_sign_up.svg" style="width: 120px; height: 25px;" /></a>
							
								<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-google.svg" style="width: 120px; height: 25px;" /></a>
							
							<a class="btn dream-btn mt-40 fadeInUp" data-wow-delay="0.6s"
							href="https://coinmagnetics.com"><img alt="" src="./store-apple.svg" style="width: 120px; height: 25px;" /></a>
					</div>
				</div>

				<div class="col-12 col-lg-6 mt-s no-padding-right">
					<div class="welcome-meter fadeInUp" data-wow-delay="0.7s"><img alt=""
							class="img-responsive center-block" src="img/core-img/Affiliate1.png" style="width: 590px; height: 360px;"/></div>
				</div>
			</div>
		</div>
	
			<!-- ##### Contact Area End ##### -->
		

			<div class="footer-content-area ">
				<div class="container">
					<div class="row ">
						<div class="col-12 col-lg-4 col-md-6">
							<div class="footer-copywrite-info">
								<!-- Copywrite -->
								<div class="copywrite_text fadeInUp" data-wow-delay="0.2s">
									<div class="footer-logo"><a href="#"><img alt="logo" src="./logo2.png"
												style="width: 230px; height: 40px;" /></a></div>

									<p>Plug-in to World's N0 1 Digital Asset management system today and Start Earning Big!.
									</p>
								</div>
								<!-- Social Icon -->

								<div class="footer-social-info fadeInUp" data-wow-delay="0.4s"></div>
							</div>
						</div>

						<div class="col-12 col-lg-3 col-md-6">
							<div class="contact_info_area d-sm-flex justify-content-between">
								<!-- Content Info -->
								<div class="contact_info mt-x text-center fadeInUp" data-wow-delay="0.3s">
									<h5>PRIVACY &amp; TOS</h5>

									<p><a href="https://coinmagnetics.com/terms">Terms Of Use</a></p>

									<p><a href="https://coinmagnetics.com/Privacy-policy">Privacy Policy</a></p>

									<p><a href="https://coinmagnetics.com/compliance">Legal & Compliance</a></p>

									<p><a href="https://coinmagnetics.com/Customer-pro">Customer protection</a></p>
									
									<p>Advertiser Agreement</p>
								</div>
							</div>
						</div>

						<div class="col-12 col-lg-2 col-md-6 ">
							<!-- Content Info -->
							<div class="contact_info_area d-sm-flex justify-content-between">
								<div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.2s">
									<h5>NAVIGATE</h5>

									<p><a href="https://coinmagnetics.com/about">About Us</a></p>
									
									<p><a href="https://coinmagnetics.com/contact">Contact Us</a></p>
									
									<p><a href="https://coinmagnetics.com/faq">FAQ</a></p>

									<p>Developers</p>

									<p>Resources</p>
								</div>
							</div>
						</div>

						<div class="col-12 col-lg-3 col-md-6 ">
							<div class="contact_info_area d-sm-flex justify-content-between">
								<!-- Content Info -->
								<div class="contact_info mt-s text-center fadeInUp" data-wow-delay="0.4s">
									<h5>FIND US HERE</h5>

									<p>Mailing Address:265 Akowonjo RD,</p>

									<p>Lagos, Nigeria. Los 23401</p>

									
									<p>Email: support@coinmagnetics.com</p>
									
									<div class="dream-btn-group fadeInUp" data-wow-delay="0.4s"><a
									href="https://t.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/tg.png"
										style="width: 43px; height: 40px; float: left;" /></a>&nbsp; &nbsp;&nbsp;
								&nbsp;<a href="https://www.instagram.com/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/ig.png" style="width: 42px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp; &nbsp;<a href="https://twitter.com/Coinmagnetics"><img alt=""
										src="/img/social/tw.png" style="width: 43px; height: 40px;" /></a>&nbsp; &nbsp;
								&nbsp;&nbsp;<a href="https://fb.me/Coinmagnetics" target="_blank"><img alt=""
										src="/img/social/fb.png" style="width: 43px; height: 40px;" /></a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- ##### Footer Area End ##### -->
		<!-- ########## All JS ########## -->
		<!-- jQuery js -->
		<script src="js/jquery.min.js"></script><!-- Popper js -->
		<script src="js/popper.min.js"></script><!-- Bootstrap js -->
		<script src="js/bootstrap.min.js"></script><!-- All Plugins js -->
		<script src="js/plugins.js"></script><!-- Parallax js -->
		<script src="js/dzsparallaxer.js"></script>
		<script src="js/jquery.syotimer.min.js"></script><!-- script js -->
		<script src="js/script.js"></script>
	</div>
</body>
<script>
$('#recipeCarousel').carousel({
  interval: 4000
})

$('.carousel .carousel-item').each(function(){
    var minPerSlide = 3;
    var next = $(this).next();
    if (!next.length) {
    next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
    
    for (var i=0;i<minPerSlide;i++) {
        next=next.next();
        if (!next.length) {
        	next = $(this).siblings(':first');
      	}
        
        next.children(':first-child').clone().appendTo($(this));
      }
});

	</script>

</html>